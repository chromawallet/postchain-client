# Release process

This document describes the process of releasing a new version of the `pcl` package.

Here is steps to release new version:

- go to bitbucket, open branch tab and search for `dev` branch
  ![choose pipeline](assets/pipeline.png)
- click on three dots and choose `run pipeline for a branch`
- choose `custom: bump-version` pipeline
- create pull request to master
- after PR is approved and merged, run `custom: publish` pipeline for `master` branch
  ![choose pipeline](assets/publish.png)
