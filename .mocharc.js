const testEnv = process.env.TEST_ENV;

const specs = {
  unit: ["test/unit/**/*"],
  integration: ["test/integration/**/*"],
  manual: ["test/manual/**/*"],
  devnet: ["test/integrationDevnet/**/*"],
};

const selectedSpec = specs[testEnv] || specs.integration;

module.exports = {
  spec: selectedSpec,
};
