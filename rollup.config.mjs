import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";
import inject from "@rollup/plugin-inject";
import alias from "@rollup/plugin-alias";
import resolve from "@rollup/plugin-node-resolve";
import { visualizer } from "rollup-plugin-visualizer";

export default [
  //ESM
  {
    input: "./built/index.js",
    output: {
      dir: "./built/esm",
      format: "es",
      name: "Postchain client",
      sourcemap: true,
    },
    plugins: [
      resolve({ browser: true }),
      commonjs({ transformMixedEsModules: true }),
      inject({ Buffer: ["buffer", "Buffer"] }),
      json(),
      alias({
        entries: [
          { find: "crypto", replacement: "crypto-browserify" },
          { find: "stream", replacement: "stream-browserify" },
        ],
      }),
      visualizer({
        filename: "coverage/bundle-analysis-esm.html",
        title: "ESM Bundle Analysis",
      }),
    ],
  },
  //UMD
  {
    input: "./built/index.js",
    output: {
      dir: "./built/umd",
      format: "umd",
      name: "Postchain client",
      sourcemap: true,
    },
    plugins: [
      commonjs(),
      inject({ Buffer: ["buffer", "Buffer"] }),
      json(),
      resolve({ browser: true }),
      alias({
        entries: [
          { find: "crypto", replacement: "crypto-browserify" },
          { find: "stream", replacement: "stream-browserify" },
        ],
      }),
      visualizer({
        filename: "coverage/bundle-analysis-umd.html",
        title: "UMD Bundle Analysis",
      }),
    ],
  },
  //NODE
  {
    input: "./built/index.js",
    output: {
      dir: "./built/cjs",
      format: "cjs",
      name: "Postchain client",
      sourcemap: true,
    },
    plugins: [
      commonjs(),
      visualizer({
        filename: "coverage/bundle-analysis-cjs.html",
        title: "CJS Bundle Analysis",
      }),
    ],
  },
];
