# Postchain Client dapp

This directory contains a dapp that can be run locally for testing using the project's settings file `chromia.yml`

## Usage

- To run the node locally, run `chr node start --wipe`

- For extensive testing and specifically for scenarios that require communication with the System Cluster and the corresponding system nodes such as the Directory Chain,
  the dapp is also deployed in testnet using the project's setting file `chromia-devnet.yml`

- The tests are running against testnet can be found in `test/integrationDevnet` and can also be run with `npm run test:integrationDevnet`

## Notes

The dapp will require a redployment in devnet1 if it is reset
