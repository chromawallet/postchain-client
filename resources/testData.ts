import { BlockInfo, BlockInfoResponse } from "../src/blockchainClient/types";
import { toString } from "../src/formatter";

export const testBuffer = Buffer.alloc(32, 1);
export const blockInfoData: BlockInfo = {
  rid: testBuffer,
  prevBlockRid: testBuffer,
  header: testBuffer,
  height: 1,
  transactions: [{ rid: testBuffer, hash: testBuffer }],
  witness: testBuffer,
  witnesses: [testBuffer],
  timestamp: 0,
};

const bufferAsString = toString(testBuffer);

export const blockDataResponse: BlockInfoResponse = {
  rid: bufferAsString,
  prevBlockRID: bufferAsString,
  header: bufferAsString,
  height: 1,
  transactions: [{ rid: bufferAsString, hash: bufferAsString }],
  witness: bufferAsString,
  witnesses: [bufferAsString],
  timestamp: 0,
};
