import { createClient } from "../../src/blockchainClient/blockchainClient";
import { IClient } from "../../src/blockchainClient/interface";
import { AnchoringStatus, ChainConfirmationLevel } from "../../src/blockchainClient/enums";
import { expect } from "chai";
import { AnchoringClientAndSystemBrid, TransactionReceipt } from "../../src/blockchainClient/types";
import { getAnchoringClientAndSystemChainRid } from "../../src/blockchainClient/utils";
import { devnetDappBrid } from "./constants";
import { mockSignatureProvider, unsignedTx } from "../unit/common/mocks";
import { DEVNET_POOL } from "../../src/constants";

describe("getTransactionConfirmationLevel in devnet", () => {
  let client: IClient;
  let systemClients: AnchoringClientAndSystemBrid;

  before(async () => {
    client = await createClient({
      directoryNodeUrlPool: DEVNET_POOL,
      blockchainRid: devnetDappBrid,
    });
    systemClients = await getAnchoringClientAndSystemChainRid(client);
  });

  it("should get system anchoring chain 'systemAnchored' status and system anchored transaction", async () => {
    const result = await client.signAndSendUniqueTransaction(
      unsignedTx,
      mockSignatureProvider,
      true,
      undefined,
      ChainConfirmationLevel.SystemAnchoring,
    );

    let systemAnchoringConfirmation = null;
    if (result.systemAnchoredTx !== undefined) {
      systemAnchoringConfirmation = await client.getTransactionConfirmationLevel(
        result.transactionRid,
      );
    } else {
      throw new Error("Expected systemAnchoredTx to be defined but it was undefined.");
    }

    const expectedResponse: TransactionReceipt = {
      status: AnchoringStatus.SystemAnchored,
      statusCode: 200,
      transactionRid: result.transactionRid,
      clusterAnchoredTx: systemAnchoringConfirmation.clusterAnchoredTx,
      clusterAnchoringClientBrid: systemClients.anchoringClient.config.blockchainRid,
      systemAnchoredTx: systemAnchoringConfirmation.systemAnchoredTx,
      systemAnchoringClientBrid: systemClients.systemAnchoringChainBridString,
    };

    expect(systemAnchoringConfirmation).to.deep.equal(expectedResponse);
  });
});
