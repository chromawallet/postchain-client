import { expect } from "chai";
import { createClient } from "../../src/blockchainClient/blockchainClient";
import { AnchoringStatus, ChainConfirmationLevel } from "../../src/blockchainClient/enums";
import { IClient } from "../../src/blockchainClient/interface";
import { AnchoringClientAndSystemBrid } from "../../src/blockchainClient/types";
import { getAnchoringClientAndSystemChainRid } from "../../src/blockchainClient/utils";
import { mockBuffer, mockSignatureProvider, unsignedTx } from "../unit/common/mocks";
import { devnetDappBrid } from "./constants";
import { calculateBlockRID } from "../../src/ICCF/utils";
import { DEVNET_POOL } from "../../src/constants";

describe("getAnchoringStatusForBlockRid", () => {
  let client: IClient;
  let systemClients: AnchoringClientAndSystemBrid;
  before(async () => {
    client = await createClient({
      directoryNodeUrlPool: DEVNET_POOL,
      blockchainRid: devnetDappBrid,
    });
    systemClients = await getAnchoringClientAndSystemChainRid(client);
  });

  it("returns AnchoringStatus NotAnchored with invalid blockRid", async () => {
    await client.signAndSendUniqueTransaction(
      unsignedTx,
      mockSignatureProvider,
      false,
      undefined,
      ChainConfirmationLevel.None,
    );

    const anchoringStatus = await client.getAnchoringStatusForBlockRid(
      mockBuffer,
      systemClients.anchoringClient,
      systemClients.systemAnchoringChainBridString,
    );

    expect(anchoringStatus).to.deep.equal({ status: AnchoringStatus.NotAnchored });
  });

  it("returns AnchoringStatus SystemAnchored, clusterAnchoredTx and systemAnchoredTx", async () => {
    const transactionReceipt = await client.signAndSendUniqueTransaction(
      unsignedTx,
      mockSignatureProvider,
      true,
      undefined,
      ChainConfirmationLevel.SystemAnchoring,
    );

    const confirmationProof = await client.getConfirmationProof(transactionReceipt.transactionRid);
    const blockRid = calculateBlockRID(confirmationProof);

    const anchoringStatus = await client.getAnchoringStatusForBlockRid(
      blockRid,
      systemClients.anchoringClient,
      systemClients.systemAnchoringChainBridString,
    );

    expect(anchoringStatus).to.deep.equal({
      status: AnchoringStatus.SystemAnchored,
      clusterAnchoredTx: transactionReceipt.clusterAnchoredTx,
      systemAnchoredTx: transactionReceipt.systemAnchoredTx,
    });
  });
});
