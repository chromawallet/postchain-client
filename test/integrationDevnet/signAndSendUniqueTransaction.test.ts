import { createClient } from "../../src/blockchainClient/blockchainClient";
import { IClient } from "../../src/blockchainClient/interface";
import * as secp256k1 from "secp256k1";
import * as gtxTool from "../../src/gtx/gtx";
import { signDigest } from "../../src/encryption/encryption";
import { randomInt } from "crypto";
import {
  AnchoringStatus,
  ChainConfirmationLevel,
  ResponseStatus,
} from "../../src/blockchainClient/enums";
import { RawGtxBody } from "../../src/gtx/types";
import { AnchoringClientAndSystemBrid, TransactionReceipt } from "../../src/blockchainClient/types";
import { expect } from "chai";
import { getAnchoringClientAndSystemChainRid } from "../../src/blockchainClient/utils";
import { devnetDappBrid } from "./constants";
import { DEVNET_POOL } from "../../src/constants";

const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
const signerPubKeyA: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyA));

const sigProvA = {
  pubKey: signerPubKeyA,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    return signDigest(digest, signerPrivKeyA);
  },
};

const unsignedTx = {
  operations: [
    {
      name: "setInteger",
      args: [randomInt(99999)],
    },
  ],
  signers: [signerPubKeyA],
};

describe("signAndSendUniqueTransaction in devnet", () => {
  let client: IClient;
  let systemClients: AnchoringClientAndSystemBrid;
  before(async () => {
    client = await createClient({
      directoryNodeUrlPool: DEVNET_POOL,
      blockchainRid: devnetDappBrid,
    });

    systemClients = await getAnchoringClientAndSystemChainRid(client);
  });

  it("should get dapp confirmation status of 'waiting'", async () => {
    const result = await client.signAndSendUniqueTransaction(
      unsignedTx,
      sigProvA,
      false,
      undefined,
      ChainConfirmationLevel.Dapp,
    );

    const expectedResponse: TransactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: result.transactionRid,
    };

    expect(result).to.deep.equal(expectedResponse);
  });

  it("should get dapp confirmation status of 'confirmed'", async () => {
    const result = await client.signAndSendUniqueTransaction(
      unsignedTx,
      sigProvA,
      true,
      undefined,
      ChainConfirmationLevel.Dapp,
    );

    const expectedResponse: TransactionReceipt = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: result.transactionRid,
    };

    expect(result).to.deep.equal(expectedResponse);
  });

  it("should get cluster anchoring confirmation status of 'clusterAnchored'", async () => {
    const result = await client.signAndSendUniqueTransaction(
      unsignedTx,
      sigProvA,
      true,
      undefined,
      ChainConfirmationLevel.ClusterAnchoring,
    );

    const expectedResponse: TransactionReceipt = {
      status: AnchoringStatus.ClusterAnchored,
      statusCode: 200,
      transactionRid: result.transactionRid,
      clusterAnchoredTx: result.clusterAnchoredTx,
      clusterAnchoringClientBrid: systemClients.anchoringClient.config.blockchainRid,
    };

    expect(result).to.deep.equal(expectedResponse);
  });

  it("should get cluster anchoring confirmation status of 'systemAnchored'", async () => {
    const result = await client.signAndSendUniqueTransaction(
      unsignedTx,
      sigProvA,
      true,
      undefined,
      ChainConfirmationLevel.SystemAnchoring,
    );

    const expectedResponse: TransactionReceipt = {
      status: AnchoringStatus.SystemAnchored,
      statusCode: 200,
      transactionRid: result.transactionRid,
      clusterAnchoredTx: result.clusterAnchoredTx,
      clusterAnchoringClientBrid: systemClients.anchoringClient.config.blockchainRid,
      systemAnchoredTx: result.systemAnchoredTx,
      systemAnchoringClientBrid: systemClients.systemAnchoringChainBridString,
    };

    expect(result).to.deep.equal(expectedResponse);
  });
});
