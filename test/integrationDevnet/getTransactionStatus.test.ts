import { createClient } from "../../src/blockchainClient/blockchainClient";
import { IClient } from "../../src/blockchainClient/interface";
import * as secp256k1 from "secp256k1";
import * as gtxTool from "../../src/gtx/gtx";
import { signDigest } from "../../src/encryption/encryption";
import { randomInt } from "crypto";
import {
    AnchoringStatus,
    ChainConfirmationLevel,
    ResponseStatus,
    TransactionEvent,
} from "../../src/blockchainClient/enums";
import { RawGtxBody } from "../../src/gtx/types";
import { AnchoringClientAndSystemBrid, StatusObject, TransactionReceipt } from "../../src/blockchainClient/types";
import { expect } from "chai";
import { getAnchoringClientAndSystemChainRid } from "../../src/blockchainClient/utils";
import { devnetDappBrid } from "./constants";
import { DEVNET_POOL } from "../../src/constants";

const unsignedOperation = {
    name: "setInteger",
    args: [randomInt(99999)],
};

describe("getTransactionStatus in devnet", () => {
    let client: IClient;
    let systemClients: AnchoringClientAndSystemBrid;
    before(async () => {
        client = await createClient({
            directoryNodeUrlPool: DEVNET_POOL,
            blockchainRid: devnetDappBrid,
        });

        systemClients = await getAnchoringClientAndSystemChainRid(client);
    });

    it("gets status of a confirmed transaction", async () => {
        const transactionReceipt = await client.sendTransaction(unsignedOperation);
        const { status } = await client.getTransactionStatus(transactionReceipt.transactionRid);
        expect(status).to.equal(ResponseStatus.Confirmed);

    });

    it("gets status of a waiting transaction", async () => {
        const promiEvent = client.sendTransaction(unsignedOperation);
        promiEvent.on(TransactionEvent.DappReceived, async (transactionReceipt: TransactionReceipt) => {
            const { status } = await client.getTransactionStatus(transactionReceipt.transactionRid);
            expect(status).to.equal(ResponseStatus.Waiting);
        });   
    });

    it("gets status of a waiting transaction, takes a callback", async () => {
        const promiEvent = client.sendTransaction(unsignedOperation);
        promiEvent.on(TransactionEvent.DappReceived, async (transactionReceipt: TransactionReceipt) => {
            const { status } = await client.getTransactionStatus(transactionReceipt.transactionRid, (err: Error | null | undefined, data: StatusObject | null | undefined) => {
                const { status } = data ?? {};
                expect(status).to.equal(ResponseStatus.Waiting);
              },);
        });    
      });
});
