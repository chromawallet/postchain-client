import { describe } from "mocha";
import { expect } from "chai";
import { server } from "../../../mocks/servers";
import { IClient } from "../../../src/blockchainClient/interface";
import { UnexpectedStatusError } from "../../../src/restclient/errors";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { errorHandler } from "../../../mocks/handlers";
import { blockInfoData } from "../../../resources/testData";
import {
  CONFIGURED_POLL_COUNT,
  mockAnchoringTransaction,
  mockBuffer,
  mockConfirmationProof,
  mockStringBlockchainRid,
  mockStringFaultyBrid,
  mockThirtyTwoBytesBuffer,
  exampleOperation,
  mockSignatureProvider,
  mockUnsignedTx,
  mockStringDirectoryChainRid,
  CONFIGURED_POLL_INTERVAL,
} from "../common/mocks";
import * as utils from "../../../src/blockchainClient/utils";
import sinon from "sinon";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import { resolveClusterAnchoringTransactionConfirmationWithStubs } from "./helpers/sendTransaction";
import * as iccf from "../../../src/ICCF/IccfProofTxMaterialBuilder";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";
import { ResponseStatus } from "../../../src/blockchainClient/enums";
import * as gtxTool from "../../../src/gtx/gtx";
import * as handleRequestModule from "../../../src/restclient/httpUtil";

const mockAnchoringClient: IClient = clientConfiguredToD1;
const mockSystemAnchoringClient: IClient = clientConfiguredToD1;
const expectedTransactionInfo = {
  blockRid: mockThirtyTwoBytesBuffer,
  blockHeight: 0,
  blockHeader: mockThirtyTwoBytesBuffer,
  witness: mockThirtyTwoBytesBuffer,
  timestamp: 0,
  txRid: mockThirtyTwoBytesBuffer,
  txHash: mockThirtyTwoBytesBuffer,
  txData: mockThirtyTwoBytesBuffer,
};
let getTransactionStatusStub: sinon.SinonStub;
let getConfirmationProofStub: sinon.SinonStub;
let getBlockAnchoringTransactionStub: sinon.SinonStub;
let getSystemAnchoringTransactionConfirmationStub: sinon.SinonStub;
let anchoringTransactionSchemaStub: sinon.SinonStub;
let getDigestToSignStub: sinon.SinonStub;
let handleRequestSpy: sinon.SinonSpy;

describe("Blockchain client tests with mocks", function () {
  let client: IClient;

  beforeEach(async function () {
    server.listen();
    client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      dappStatusPolling: { interval: CONFIGURED_POLL_INTERVAL, count: CONFIGURED_POLL_COUNT },
    });
    getConfirmationProofStub = sinon.stub(client, "getConfirmationProof");
    getSystemAnchoringTransactionConfirmationStub = sinon.stub(
      client,
      "getSystemAnchoringTransactionConfirmation",
    );
    getBlockAnchoringTransactionStub = sinon.stub(iccf, "getBlockAnchoringTransaction");
    anchoringTransactionSchemaStub = sinon.stub(AnchoringTransactionSchema, "safeParse");
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
    getTransactionStatusStub = sinon.stub(client, "getTransactionStatus");
    getDigestToSignStub = sinon.stub(gtxTool, "getDigestToSign");
    handleRequestSpy = sinon.spy(handleRequestModule, "default");
  });

  afterEach(() => {
    server.resetHandlers();
    sinon.restore();
    sinon.reset();
  });

  describe("getTransactionsInfo", () => {
    it("throws error if brid is invalid", async () => {
      const clientFaultyBrid = await createClient({
        nodeUrlPool: [`http://localhost`, `http://localhost`],
        blockchainRid: mockStringFaultyBrid,
      });
      server.use(errorHandler.getTransactionInfo);

      try {
        await clientFaultyBrid.getTransactionsInfo();
        expect.fail();
      } catch (error) {
        expect(error).to.be.instanceOf(UnexpectedStatusError);
        expect(error.message).to.be.equal(
          `Unexpected status code from server. Code: 404. Message: {"error":"Can't find blockchain with blockchainRID: ${mockStringFaultyBrid}"}.`,
        );
      }
    });

    it("fetches a transaction for client blockchainRid", async () => {
      resolveClusterAnchoringTransactionConfirmationWithStubs(
        {
          transactionStatusStub: getTransactionStatusStub,
          confirmationProofStub: getConfirmationProofStub,
          blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
          anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
          systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
          digestToSignStub: getDigestToSignStub,
        },
        {
          sourceClient: client,
          anchoringClient: mockAnchoringClient,
        },
        {
          transactionRid: mockBuffer,
          confirmationProof: mockConfirmationProof,
          anchoringTransaction: mockAnchoringTransaction,
          directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
        },
      );
      await client.sendTransaction(exampleOperation);
      const transactionsInfo = await client.getTransactionsInfo();

      expect(transactionsInfo).to.deep.equal([expectedTransactionInfo]);
    });
    it("fetches the latest transaction when the before time is now and limit is 1", async () => {
      resolveClusterAnchoringTransactionConfirmationWithStubs(
        {
          transactionStatusStub: getTransactionStatusStub,
          confirmationProofStub: getConfirmationProofStub,
          blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
          anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
          systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
          digestToSignStub: getDigestToSignStub,
        },
        {
          sourceClient: client,
          anchoringClient: mockAnchoringClient,
        },
        {
          transactionRid: mockBuffer,
          confirmationProof: mockConfirmationProof,
          anchoringTransaction: mockAnchoringTransaction,
          directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
        },
      );
      await client.sendTransaction(exampleOperation);
      const transactionsInfo = await client.getTransactionsInfo(1, new Date());

      expect(transactionsInfo).to.deep.equal([expectedTransactionInfo]);
    });
  });

  describe("getBlockInfo", () => {
    it("gets a block and strings are formatted as buffers", async () => {
      const block = await client.getBlockInfo(1);
      expect(block).to.deep.equal(blockInfoData);
    });
  });
  describe("getLatestBlock", () => {
    it("get latest block", async () => {
      const info = await client.getLatestBlock();
      expect(info).to.deep.equal(blockInfoData);
    });
  });
  describe("getTransactionInfo", async () => {
    it("returns transaction info for client blockchain", async () => {
      resolveClusterAnchoringTransactionConfirmationWithStubs(
        {
          transactionStatusStub: getTransactionStatusStub,
          confirmationProofStub: getConfirmationProofStub,
          blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
          anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
          systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
          digestToSignStub: getDigestToSignStub,
        },
        {
          sourceClient: client,
          anchoringClient: mockAnchoringClient,
        },
        {
          transactionRid: mockBuffer,
          confirmationProof: mockConfirmationProof,
          anchoringTransaction: mockAnchoringTransaction,
          directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
        },
      );
      const transactionReceipt = await client.sendTransaction(exampleOperation);
      const transactionInfo = await client.getTransactionInfo(transactionReceipt.transactionRid);

      expect(transactionInfo).to.deep.equal(expectedTransactionInfo);
    });
  });
  describe("getTransactionCount", async () => {
    it("fetches total number of transactions for a dapp", async () => {
      const transactionsCount = await client.getTransactionCount();
      expect(transactionsCount).to.deep.equal(0);
    });
  });
  describe("getTransactionStatus", async () => {
    it("returns Waiting status", async () => {
      const transactionStatus = {
        status: ResponseStatus.Waiting,
      };
      getTransactionStatusStub.resolves(transactionStatus);
      const result = await client.getTransactionStatus(mockBuffer);

      expect(result).to.deep.equal({ status: ResponseStatus.Waiting });
    });

    it("getTransactionStatus returns Confirmed status", async () => {
      const transactionStatus = {
        status: ResponseStatus.Confirmed,
        statusCode: 200,
        transactionRid: mockBuffer,
      };
      getTransactionStatusStub.resolves(transactionStatus);
      const result = await client.getTransactionStatus(mockBuffer);
      expect(result).to.deep.equal(transactionStatus);
    });
  });
  describe("getConfirmationProof", async () => {
    it("after transaction was send", async () => {
      resolveClusterAnchoringTransactionConfirmationWithStubs(
        {
          transactionStatusStub: getTransactionStatusStub,
          confirmationProofStub: getConfirmationProofStub,
          blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
          anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
          systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
          digestToSignStub: getDigestToSignStub,
        },
        {
          sourceClient: client,
          anchoringClient: mockAnchoringClient,
        },
        {
          transactionRid: mockBuffer,
          confirmationProof: mockConfirmationProof,
          anchoringTransaction: mockAnchoringTransaction,
          directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
        },
      );
      const transactionReceipt = await client.sendTransaction(exampleOperation);
      const confirmationProof = await client.getConfirmationProof(
        transactionReceipt.transactionRid,
      );

      expect(confirmationProof).to.deep.equal(mockConfirmationProof);
    });
  });
  describe("getTransaction", async () => {
    it("gets transaction with a specific txRID", async () => {
      const result = await client.getTransaction(mockBuffer);
      expect(result).to.be.an.instanceof(Buffer);
      expect(result).to.deep.equal(mockThirtyTwoBytesBuffer);
    });
    it("gets signed transaction with a specific txRID", async () => {
      resolveClusterAnchoringTransactionConfirmationWithStubs(
        {
          transactionStatusStub: getTransactionStatusStub,
          confirmationProofStub: getConfirmationProofStub,
          blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
          anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
          systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
          digestToSignStub: getDigestToSignStub,
        },
        {
          sourceClient: client,
          anchoringClient: mockAnchoringClient,
        },
        {
          transactionRid: mockBuffer,
          confirmationProof: mockConfirmationProof,
          anchoringTransaction: mockAnchoringTransaction,
          directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
        },
      );
      const signedTx = await client.signTransaction(mockUnsignedTx, mockSignatureProvider);
      const transactionReceipt = await client.sendTransaction(signedTx);
      const result = await client.getTransaction(transactionReceipt.transactionRid);
      expect(result).to.be.an.instanceof(Buffer);
      expect(result).to.deep.equal(mockThirtyTwoBytesBuffer);
    });

    it("retries default number of times", async () => {
      try {
        await client.getTransaction(mockBuffer);
      } catch (error) {
        expect(handleRequestSpy.callCount).to.equal(9);
      }
    });

    it("uses default attemptInterval of 500", async () => {
      const invalidNodeUrlPoolClient = await createClient({
        nodeUrlPool: ["http://localhost1:7740", "http://localhost2:7740", "http://localhost3:7740"],
        blockchainRid: mockStringBlockchainRid,
        failOverConfig: {
          attemptsPerEndpoint: 1,
        },
        directoryChainRid: mockStringDirectoryChainRid,
      });

      try {
        await invalidNodeUrlPoolClient.getTransaction(mockBuffer);
      } catch (error) {
        expect(handleRequestSpy.callCount).to.equal(3);
      }
    });
  });
});
