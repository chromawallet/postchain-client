import { IClient } from "../../../src/blockchainClient/interface";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import sinon from "sinon";
import { Transaction } from "../../../src/blockchainClient/types";
import { expect } from "chai";
import { TxRejectedError, UnexpectedStatusError } from "../../../src/restclient/errors";
import { server } from "../../../mocks/servers";
import { errorHandler } from "../../../mocks/handlers";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import * as iccf from "../../../src/ICCF/IccfProofTxMaterialBuilder";
import {
  AnchoringStatus,
  ChainConfirmationLevel,
  ResponseStatus,
  TransactionEvent,
} from "../../../src/blockchainClient/enums";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";
import * as utils from "../../../src/blockchainClient/utils";
import { resolveClusterAnchoringTransactionConfirmationWithStubs } from "./helpers/sendTransaction";
import {
  CONFIGURED_POLL_COUNT,
  exampleOperation,
  mockAnchoringTransaction,
  mockBuffer,
  mockConfirmationProof,
  mockStringBlockchainRid,
  mockStringDirectoryChainRid,
  mockThirtyTwoBytesBuffer,
  mockSignatureProvider,
  signerPubKey,
  mockUnsignedTx,
  signerKeyPair,
  CONFIGURED_POLL_INTERVAL,
} from "../common/mocks";
import * as gtxTool from "../../../src/gtx/gtx";
import { randomBytes } from "../../../src/encryption/encryption";
import { ComplexArgumentObject } from "../../integration/testData";
import * as handleRequestModule from "../../../src/restclient/httpUtil";

const rejectReason =
  "[bc-rid=8E:DFB4, chain-id=0] Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";
const invalidFormatRejectReason =
  "Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";

let client: IClient;
const mockAnchoringClient: IClient = clientConfiguredToD1;
const mockSystemAnchoringClient: IClient = clientConfiguredToD1;
let getTransactionStatusStub: sinon.SinonStub;
let getConfirmationProofStub: sinon.SinonStub;
let getBlockAnchoringTransactionStub: sinon.SinonStub;
let getSystemAnchoringTransactionConfirmationStub: sinon.SinonStub;
let anchoringTransactionSchemaStub: sinon.SinonStub;
let sendTransactionSentPromiEventDappReceived: sinon.SinonSpy;
let sendTransactionSentPromiEventDappConfirmed: sinon.SinonSpy;
let sendTransactionSentPromiEventClusterConfirmed: sinon.SinonSpy;
let sendTransactionSentPromiEventSystemConfirmed: sinon.SinonSpy;
let getDigestToSignStub: sinon.SinonStub;
let handleRequestSpy: sinon.SinonSpy;

const mockUnsignedMultiOperationTx: Transaction = {
  operations: [exampleOperation, { name: "nop", args: [randomBytes(32)] }],
  signers: [signerPubKey],
};

describe("sendTransaction", async () => {
  before(async () => {
    server.listen();
    client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      dappStatusPolling: utils.setStatusPolling({
        interval: CONFIGURED_POLL_INTERVAL,
        count: CONFIGURED_POLL_COUNT,
      }),
      clusterAnchoringStatusPolling: utils.setStatusPolling(),
      systemAnchoringStatusPolling: utils.setStatusPolling(),
      directoryChainRid: mockStringDirectoryChainRid,
    });
  });
  beforeEach(() => {
    getConfirmationProofStub = sinon.stub(client, "getConfirmationProof");
    getSystemAnchoringTransactionConfirmationStub = sinon.stub(
      client,
      "getSystemAnchoringTransactionConfirmation",
    );
    getBlockAnchoringTransactionStub = sinon.stub(iccf, "getBlockAnchoringTransaction");
    anchoringTransactionSchemaStub = sinon.stub(AnchoringTransactionSchema, "safeParse");
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
    getTransactionStatusStub = sinon.stub(client, "getTransactionStatus");
    getDigestToSignStub = sinon.stub(gtxTool, "getDigestToSign");
    handleRequestSpy = sinon.spy(handleRequestModule, "default");
  });
  afterEach(() => {
    server.resetHandlers();
    sinon.restore();
    sinon.reset();
  });

  it("should throw error if endpoint returns 400 status", async () => {
    server.use(errorHandler.txError);
    try {
      await client.sendTransaction(exampleOperation);
      expect.fail();
    } catch (error) {
      expect(error).to.be.instanceOf(UnexpectedStatusError);
    }
  }).timeout(4000);

  it("should throw TxRejectedError with detailed error information if transaction is rejected", async () => {
    getTransactionStatusStub.throws(new TxRejectedError(rejectReason));
    server.use(errorHandler.statusRejected);
    try {
      await client.sendTransaction(exampleOperation);
      expect.fail();
    } catch (error) {
      expect(error).to.be.instanceOf(TxRejectedError);
      expect(error.message).to.equal(`Transaction was rejected, ${rejectReason}`);
      expect(error.fullReason).to.equal(`${rejectReason}`);
      expect(error.shortReason).to.equal(
        "Expected at least two operations, make sure that you included auth",
      );
      expect(error.operation).to.equal("news_feed_ch5.news_feed:make_post");
      expect(error.rellLine).to.equal("bc-rid=8E:DFB4, chain-id=0");
      expect(getTransactionStatusStub.callCount).to.equal(1);
    }
  });

  it("throws error if transaction gets rejected, rejectReason is not standard format and error will not containing full reason, short reason, operation and rell line", async () => {
    getTransactionStatusStub.throws(new TxRejectedError(invalidFormatRejectReason));
    server.use(errorHandler.txStatusInvalidRejectFormat);
    try {
      await client.sendTransaction(exampleOperation);
      expect.fail();
    } catch (error) {
      console.log(error);

      expect(error).to.be.instanceOf(TxRejectedError);
      expect(error.message).to.equal(`Transaction was rejected, ${invalidFormatRejectReason}`);
      expect(error.fullReason).to.equal(`${invalidFormatRejectReason}`);
      expect(error.shortReason).to.be.undefined;
      expect(error.operation).to.be.undefined;
      expect(error.rellLine).to.be.undefined;
      expect(getTransactionStatusStub.callCount).to.equal(1);
    }
  });

  it('should return status "Confirmed" when transaction is confirmed', async () => {
    const transactionReceipt = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    };
    getDigestToSignStub.returns(mockBuffer);
    getTransactionStatusStub.resolves(transactionReceipt);
    const status = await client.sendTransaction(exampleOperation);

    expect(status).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.callCount).to.equal(1);
  });

  it('should return status "Waiting" when transaction is waiting and reaches configured poll count', async () => {
    const transactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockBuffer,
    };
    getDigestToSignStub.returns(mockBuffer);
    getTransactionStatusStub.resolves(transactionReceipt);
    server.use(errorHandler.statusWaiting);

    const status = await client.sendTransaction(exampleOperation);

    expect(status).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.callCount).to.equal(CONFIGURED_POLL_COUNT);
  }).timeout(4000);

  it('should return status "Unknown" when transaction status is unknown and reaches configured poll count', async () => {
    const transactionReceipt = {
      status: ResponseStatus.Unknown,
      statusCode: 200,
      transactionRid: mockBuffer,
    };
    getDigestToSignStub.returns(mockBuffer);
    getTransactionStatusStub.resolves(transactionReceipt);
    server.use(errorHandler.statusUnknown);
    const status = await client.sendTransaction(exampleOperation);
    expect(status).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.callCount).to.equal(CONFIGURED_POLL_COUNT);
  }).timeout(20000);

  it("returns status confirmed after getting status waiting and retries until status is confirmed", async () => {
    const transactionReceipt = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    };

    getTransactionStatusStub.onCall(0).resolves({
      status: ResponseStatus.Unknown,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
    getTransactionStatusStub.onCall(1).resolves(transactionReceipt);
    getDigestToSignStub.returns(mockBuffer);
    const status = await client.sendTransaction(exampleOperation);

    expect(status).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.callCount).to.equal(2);
  });

  it("should get tx-status when `confirmationLevel` is set to Dapp", async () => {
    const transactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockBuffer,
    };
    getTransactionStatusStub.resolves(transactionReceipt);
    await client.sendTransaction(exampleOperation, true, undefined, ChainConfirmationLevel.Dapp);
    expect(getTransactionStatusStub.callCount).to.equal(CONFIGURED_POLL_COUNT);
  }).timeout(10000);

  it("should NOT poll for a tx-status when given the flag: `doStatusPolling` set to false", async () => {
    const transactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockBuffer,
    };
    getDigestToSignStub.returns(mockBuffer);
    getTransactionStatusStub.resolves(transactionReceipt);
    const result = await client.sendTransaction(exampleOperation, false);
    expect(result).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.callCount).to.equal(0);
  });

  it("should throw TxRejectedError with failedAnchoring anchoring status", async () => {
    getDigestToSignStub.returns(mockBuffer);
    getTransactionStatusStub.resolves({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
    anchoringTransactionSchemaStub.callsFake(() => ({
      success: false,
      error: { issues: [{ message: "Invalid transaction", path: ["txRid"] }] },
    }));

    try {
      await client.sendTransaction(
        exampleOperation,
        true,
        undefined,
        ChainConfirmationLevel.ClusterAnchoring,
      );
      expect.fail(AnchoringStatus.FailedAnchoring);
    } catch (error) {
      expect(error).to.be.instanceOf(TxRejectedError);
      expect(error.message).to.equal(
        `Transaction was rejected, ${AnchoringStatus.FailedAnchoring}`,
      );
    }
  });

  it("sends an unsigned transaction", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.sendTransaction(exampleOperation);

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signs and sends a transaction", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const signedTx = await client.signTransaction(mockUnsignedTx, mockSignatureProvider);

    const transactionReceipt = await client.sendTransaction(
      signedTx,
      true,
      undefined,
      ChainConfirmationLevel.Dapp,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("sends a transaction with multiple operations", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const signedMultiOperationTx = await client.signTransaction(
      mockUnsignedMultiOperationTx,
      mockSignatureProvider,
    );

    const transactionReceipt = await client.sendTransaction(signedMultiOperationTx);

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signAndSendUniqueTransaction sends a transaction", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      mockUnsignedTx,
      mockSignatureProvider,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signAndSendUniqueTransaction sends an operation", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      exampleOperation,
      mockSignatureProvider,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signAndSendUniqueTransaction signs if no pubKey was provided", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      exampleOperation,
      signerKeyPair,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signAndSendUniqueTransaction adds a nop", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      exampleOperation,
      signerKeyPair,
    );

    const transactionReceiptCopy = await client.signAndSendUniqueTransaction(
      exampleOperation,
      signerKeyPair,
    );

    const expectedResponse = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    };

    expect(transactionReceipt).to.deep.equal(expectedResponse);
    expect(transactionReceiptCopy).to.deep.equal(expectedResponse);
  });

  it("signAndSendUniqueTransaction signs and sends a transaction with multiple operations", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      mockUnsignedMultiOperationTx,
      mockSignatureProvider,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("signAndSendUniqueTransaction signs and sends an operation with large complex arguments", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const transactionReceipt = await client.signAndSendUniqueTransaction(
      {
        name: "set_globals",
        args: [
          ComplexArgumentObject.client_data,
          ComplexArgumentObject.server_data,
          ComplexArgumentObject.args,
        ],
      },
      mockSignatureProvider,
    );

    expect(transactionReceipt).to.deep.equal({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
  });

  it("should confirm the transaction is added in a block in the cluster anchoring chain with sendTransation", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const expectedResponse = {
      status: AnchoringStatus.ClusterAnchored,
      statusCode: 200,
      transactionRid: mockBuffer,
      clusterAnchoredTx: mockAnchoringTransaction,
      clusterAnchoringClientBrid: mockAnchoringClient.config.blockchainRid,
    };

    const result = await client.sendTransaction(
      exampleOperation,
      true,
      undefined,
      ChainConfirmationLevel.ClusterAnchoring,
    );

    expect(result).to.deep.equal(expectedResponse);
    expect(getBlockAnchoringTransactionStub.calledOnce).to.be.true;
    expect(getConfirmationProofStub.calledOnce).to.be.true;
    expect(anchoringTransactionSchemaStub.calledOnceWith(mockAnchoringTransaction)).to.be.true;
  });

  it("should sendTransaction with one sent promiEvent when confirmation level is set to None", async () => {
    sendTransactionSentPromiEventDappReceived = sinon.spy();

    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const promiEvent = client.sendTransaction(
      exampleOperation,
      true,
      undefined,
      ChainConfirmationLevel.None,
    );

    promiEvent.on(TransactionEvent.DappReceived, sendTransactionSentPromiEventDappReceived);

    const transactionResult = await promiEvent;

    expect(sendTransactionSentPromiEventDappReceived.calledOnce).to.be.true;
    const sentEventArgs = sendTransactionSentPromiEventDappReceived.getCall(0).args[0];

    const transactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockBuffer,
    };

    expect(sentEventArgs).to.deep.equal(transactionReceipt);
    expect(transactionResult).to.deep.equal(transactionReceipt);
  });

  it("should sendTransaction with two sent promiEvents when confirmation level is set to Dapp", async () => {
    sendTransactionSentPromiEventDappReceived = sinon.spy();
    sendTransactionSentPromiEventDappConfirmed = sinon.spy();

    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const promiEvent = client.sendTransaction(
      exampleOperation,
      true,
      undefined,
      ChainConfirmationLevel.Dapp,
    );

    promiEvent.on(TransactionEvent.DappReceived, sendTransactionSentPromiEventDappReceived);
    promiEvent.on(TransactionEvent.DappConfirmed, sendTransactionSentPromiEventDappConfirmed);

    const transactionResult = await promiEvent;

    expect(sendTransactionSentPromiEventDappReceived.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventDappConfirmed.calledOnce).to.be.true;
    const sentEventArgs1 = sendTransactionSentPromiEventDappReceived.getCall(0).args[0];
    const sentEventArgs2 = sendTransactionSentPromiEventDappConfirmed.getCall(0).args[0];

    const transactionReceipt = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    };

    expect(sentEventArgs1).to.deep.equal(transactionReceipt);
    expect(sentEventArgs2).to.deep.equal(transactionReceipt);
    expect(transactionResult).to.deep.equal(transactionReceipt);
  });

  it("should sendTransaction with three sent promiEvents when confirmation level is set to ClusterAnchoringChain", async () => {
    sendTransactionSentPromiEventDappReceived = sinon.spy();
    sendTransactionSentPromiEventDappConfirmed = sinon.spy();
    sendTransactionSentPromiEventClusterConfirmed = sinon.spy();

    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const promiEvent = client.sendTransaction(
      exampleOperation,
      true,
      undefined,
      ChainConfirmationLevel.ClusterAnchoring,
    );

    promiEvent.on(TransactionEvent.DappReceived, sendTransactionSentPromiEventDappReceived);
    promiEvent.on(TransactionEvent.DappConfirmed, sendTransactionSentPromiEventDappConfirmed);
    promiEvent.on(TransactionEvent.ClusterAnchoringConfirmation, sendTransactionSentPromiEventClusterConfirmed);

    const transactionResult = await promiEvent;

    expect(sendTransactionSentPromiEventDappReceived.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventDappConfirmed.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventClusterConfirmed.calledOnce).to.be.true;
    const sentEventArgs1 = sendTransactionSentPromiEventDappReceived.getCall(0).args[0];
    const sentEventArgs2 = sendTransactionSentPromiEventDappConfirmed.getCall(0).args[0];
    const sentEventArgs3 = sendTransactionSentPromiEventClusterConfirmed.getCall(0).args[0];

    const transactionReceipt = {
      status: AnchoringStatus.ClusterAnchored,
      statusCode: 200,
      transactionRid: mockBuffer,
      clusterAnchoredTx: mockAnchoringTransaction,
      clusterAnchoringClientBrid: mockAnchoringClient.config.blockchainRid,
    };

    expect(sentEventArgs1).to.deep.equal(transactionReceipt);
    expect(sentEventArgs2).to.deep.equal(transactionReceipt);
    expect(sentEventArgs3).to.deep.equal(transactionReceipt);
    expect(transactionResult).to.deep.equal(transactionReceipt);
  });

  it("should sendTransaction with four sent promiEvents when confirmation level is set to SystemAnchoring", async () => {
    sendTransactionSentPromiEventDappReceived = sinon.spy();
    sendTransactionSentPromiEventDappConfirmed = sinon.spy();
    sendTransactionSentPromiEventClusterConfirmed = sinon.spy();
    sendTransactionSentPromiEventSystemConfirmed = sinon.spy();

    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    const promiEvent = client.sendTransaction(
      exampleOperation,
      true,
      undefined,
      ChainConfirmationLevel.SystemAnchoring,
    );

    promiEvent.on(TransactionEvent.DappReceived, sendTransactionSentPromiEventDappReceived);
    promiEvent.on(TransactionEvent.DappConfirmed, sendTransactionSentPromiEventDappConfirmed);
    promiEvent.on(TransactionEvent.ClusterAnchoringConfirmation, sendTransactionSentPromiEventClusterConfirmed);
    promiEvent.on(TransactionEvent.SystemAnchoringConfirmation, sendTransactionSentPromiEventSystemConfirmed);

    const transactionResult = await promiEvent;

    expect(sendTransactionSentPromiEventDappReceived.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventDappConfirmed.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventClusterConfirmed.calledOnce).to.be.true;
    expect(sendTransactionSentPromiEventSystemConfirmed.calledOnce).to.be.true;
    const sentEventArgs1 = sendTransactionSentPromiEventDappReceived.getCall(0).args[0];
    const sentEventArgs2 = sendTransactionSentPromiEventDappConfirmed.getCall(0).args[0];
    const sentEventArgs3 = sendTransactionSentPromiEventClusterConfirmed.getCall(0).args[0];
    const sentEventArgs4 = sendTransactionSentPromiEventClusterConfirmed.getCall(0).args[0];

    const transactionReceipt = {
      status: AnchoringStatus.SystemAnchored,
      statusCode: 200,
      transactionRid: mockBuffer,
      clusterAnchoredTx: mockAnchoringTransaction,
      clusterAnchoringClientBrid: mockAnchoringClient.config.blockchainRid,
      systemAnchoredTx: mockAnchoringTransaction,
      systemAnchoringClientBrid: mockSystemAnchoringClient.config.blockchainRid,
    };

    expect(sentEventArgs1).to.deep.equal(transactionReceipt);
    expect(sentEventArgs2).to.deep.equal(transactionReceipt);
    expect(sentEventArgs3).to.deep.equal(transactionReceipt);
    expect(sentEventArgs4).to.deep.equal(transactionReceipt);
    expect(transactionResult).to.deep.equal(transactionReceipt);
  });

  it("should fail to confirm the transaction in the anchoring chain", async () => {
    getTransactionStatusStub.resolves({
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
    getConfirmationProofStub
      .withArgs(mockThirtyTwoBytesBuffer)
      .rejects(new Error("Cluster anchoring chain confirmation failed: fetch failed"));

    try {
      await client.sendTransaction(exampleOperation);

      throw new Error("Cluster anchoring chain confirmation failed: fetch failed");
    } catch (error) {
      expect(error.message).to.equal("Cluster anchoring chain confirmation failed: fetch failed");
    }
  });

  it("retries default number of times", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    try {
      await client.sendTransaction(exampleOperation);
    } catch (error) {
      expect(handleRequestSpy.callCount).to.equal(9);
    }
  });

  it("retries default number of times", async () => {
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    try {
      await client.signAndSendUniqueTransaction(exampleOperation, signerKeyPair);
    } catch (error) {
      expect(handleRequestSpy.callCount).to.equal(9);
    }
  });
});
