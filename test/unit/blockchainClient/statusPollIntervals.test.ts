import { expect } from "chai";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import {
  CONFIGURED_POLL_COUNT,
  exampleOperation,
  mockBuffer,
  mockStringBlockchainRid,
} from "../common/mocks";
import { server } from "../../../mocks/servers";
import { errorHandler } from "../../../mocks/handlers";
import { sleep } from "../../../src/restclient/restclientutil";
import sinon from "sinon";
import * as utils from "../../../src/blockchainClient/utils";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import { IClient } from "../../../src/blockchainClient/interface";
import { ResponseStatus } from "../../../src/blockchainClient/enums";

let client: IClient;
let getTransactionStatusSpy: sinon.SinonSpy;
const mockAnchoringClient = clientConfiguredToD1;
const mockSystemAnchoringClient = clientConfiguredToD1;

describe("measures polling interval", async () => {
  before(async () => {
    server.listen();
    client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
    });
  });

  beforeEach(() => {
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
  });

  afterEach(() => {
    server.resetHandlers();
    sinon.restore();
    sinon.reset();
  });

  it('of sendTransaction for "Unknown" status', async () => {
    const client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      dappStatusPolling: utils.setStatusPolling({
        interval: 1000,
        count: CONFIGURED_POLL_COUNT,
      }),
      clusterAnchoringStatusPolling: utils.setStatusPolling(),
      systemAnchoringStatusPolling: utils.setStatusPolling(),
    });
    getTransactionStatusSpy = sinon.spy(client, "getTransactionStatus");
    server.use(errorHandler.statusUnknown);
    const status = client.sendTransaction(exampleOperation);
    await sleep(100);
    expect(getTransactionStatusSpy.callCount).to.equal(1);
    await sleep(1000);
    expect(getTransactionStatusSpy.callCount).to.equal(2);
    await sleep(1000);
    expect(getTransactionStatusSpy.callCount).to.equal(3);

    expect(await status).to.deep.equal({
      status: ResponseStatus.Unknown,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
    expect(getTransactionStatusSpy.callCount).to.equal(CONFIGURED_POLL_COUNT);
  });

  it('sendTransaction receives "Waiting" status when it reaches default(20) poll count', async () => {
    getTransactionStatusSpy = sinon.spy(client, "getTransactionStatus");
    server.use(errorHandler.statusWaiting);
    const status = await client.sendTransaction(exampleOperation);
    expect(status).to.deep.equal({
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockBuffer,
    });
    expect(getTransactionStatusSpy.callCount).to.equal(20);
  });
});
