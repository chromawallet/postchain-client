import { describe } from "mocha";
import { expect } from "chai";
import {
  formatBlockInfoResponse,
  getAnchoringClientAndSystemChainRid,
  nodeDiscovery,
} from "../../../src/blockchainClient/utils";
import * as restClientUtils from "../../../src/restclient/restclientutil";
import sinon from "sinon";
import { encodeValue } from "../../../src/gtx/serialization";
import {
  DirectoryNodeUrlPoolException,
  MissingBlockchainIdentifierError,
} from "../../../src/blockchainClient/errors";
import { BlockInfoResponse, Endpoint } from "../../../src/blockchainClient/types";
import { server } from "../../../mocks/servers";
import { domain } from "../../../mocks/handlers";
import { HttpResponse, http } from "msw";
import { createNodeManager } from "../../../src/restclient/nodeManager";
import {
  contentTypes,
  mockHexStringOfThirtyTwoBytesBuffer,
  mockStringBlockchainRid,
} from "../common/mocks";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import * as utils from "../../../src/blockchainClient/utils";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import { SystemChainException } from "../../../src/ICCF/error";
import { LOCAL_POOL } from "../../../src/constants";

describe("Blockchain client util tests", function () {
  let directoryNodeUrlPool: Endpoint[];
  let mockGetBrid = sinon.stub();

  beforeEach(async function () {
    server.listen();
    directoryNodeUrlPool = [{ url: domain, whenAvailable: 0 }];

    mockGetBrid = sinon.stub(restClientUtils, "getBlockchainRidFromIid");
  });

  afterEach(() => {
    server.resetHandlers();
    sinon.resetHistory();
    sinon.restore();
    mockGetBrid.reset();
  });

  const mockQueryGtvResponse = ({
    status,
    responseBody,
  }: {
    status: number;
    responseBody: BodyInit | null;
  }) => {
    server.use(
      http.post(`${domain}/query_gtv/${mockHexStringOfThirtyTwoBytesBuffer}`, () => {
        return new HttpResponse(responseBody, {
          status,
          headers: {
            "Content-Type": contentTypes.octetStream,
          },
        });
      }),
    );
  };

  describe("getNodeUrlsForBlockchainFromDirectoryChain", () => {
    it("returns list of node urls", async () => {
      mockGetBrid.resolves(mockHexStringOfThirtyTwoBytesBuffer);
      const serverReturnNodes: string[] = ["url1", "url2"];

      expect(directoryNodeUrlPool).to.be.a("array");

      mockQueryGtvResponse({
        responseBody: encodeValue(serverReturnNodes),
        status: 200,
      });

      const res = await nodeDiscovery({
        nodeManager: createNodeManager({
          nodeUrls: directoryNodeUrlPool.map(node => node.url),
        }),
        directoryEndpointPool: directoryNodeUrlPool,
        blockchainRid: mockHexStringOfThirtyTwoBytesBuffer,
      });
      expect(res).to.deep.equal(serverReturnNodes);
    });

    it("throws error if no blockchain RID or IID was provided", async () => {
      const promise = nodeDiscovery({
        nodeManager: createNodeManager({
          nodeUrls: directoryNodeUrlPool.map(node => node.url),
        }),
        directoryEndpointPool: directoryNodeUrlPool,
      });
      await expect(promise).to.be.rejectedWith(MissingBlockchainIdentifierError);
    });

    it("should throw error if directory node url pool missing", async () => {
      const serverReturnNodes: string[] = ["url1", "url2"];
      const directoryNodeUrlPool: Endpoint[] = [];

      mockQueryGtvResponse({
        responseBody: encodeValue(serverReturnNodes),
        status: 200,
      });

      const promise = nodeDiscovery({
        nodeManager: createNodeManager({
          nodeUrls: directoryNodeUrlPool.map(node => node.url),
        }),
        directoryEndpointPool: directoryNodeUrlPool,
        blockchainRid: mockHexStringOfThirtyTwoBytesBuffer,
      });

      await expect(promise).to.be.rejectedWith(DirectoryNodeUrlPoolException);
    });

    it("returns empty list if no nodes were found", async () => {
      mockGetBrid.resolves(mockHexStringOfThirtyTwoBytesBuffer);
      const serverReturnNodes: string[] = [];

      mockQueryGtvResponse({
        responseBody: encodeValue(serverReturnNodes),
        status: 200,
      });

      const res = await nodeDiscovery({
        nodeManager: createNodeManager({
          nodeUrls: directoryNodeUrlPool.map(node => node.url),
        }),
        directoryEndpointPool: directoryNodeUrlPool,
        blockchainRid: mockHexStringOfThirtyTwoBytesBuffer,
      });
      expect(res).to.deep.equal([]);
    });
  });

  it("returns empty list if no nodes were found using blockchainIID", async () => {
    mockGetBrid.resolves(mockHexStringOfThirtyTwoBytesBuffer);
    const serverReturnNodes: string[] = [];

    mockQueryGtvResponse({
      responseBody: encodeValue(serverReturnNodes),
      status: 200,
    });

    const res = await nodeDiscovery({
      nodeManager: createNodeManager({
        nodeUrls: directoryNodeUrlPool.map(node => node.url),
      }),
      directoryEndpointPool: directoryNodeUrlPool,
      blockchainIid: 0,
    });

    expect(res).to.deep.equal([]);
  });

  describe("formatBlockInfoResponse", () => {
    const baseBlockInfo: BlockInfoResponse = {
      rid: "string",
      prevBlockRID: "string",
      header: "string",
      height: 323,
      transactions: [],
      witness: "string",
      witnesses: ["string"],
      timestamp: 11,
    };

    const blockInfoWithTransactions = {
      ...baseBlockInfo,
      transactions: [
        {
          rid: "string",
          hash: "string",
        },
      ],
    };

    const blockInfoWithTransactionData = {
      ...baseBlockInfo,
      transactions: [
        {
          rid: "string",
          hash: "string",
          data: "string",
        },
      ],
    };

    it("format block info", () => {
      const result = formatBlockInfoResponse(blockInfoWithTransactionData);
      expect(result).be.not.be.null;
    });
    it("format block info without transaction data", () => {
      const result = formatBlockInfoResponse(blockInfoWithTransactions);
      expect(result).be.not.be.null;
    });
    it("format block info without transactions", () => {
      const result = formatBlockInfoResponse(baseBlockInfo);
      expect(result).be.not.be.null;
    });
  });

  it("getAnchoringClientAndSystemChainRid returns anchoringClient and systemAnchoringChainBridString", async () => {
    const client = await createClient({
      nodeUrlPool: LOCAL_POOL,
      blockchainRid: mockStringBlockchainRid,
    });

    const mockAnchoringClient = clientConfiguredToD1;
    const mockSystemAnchoringClient = clientConfiguredToD1;

    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });

    const { anchoringClient, systemAnchoringChainBridString } =
      await getAnchoringClientAndSystemChainRid(client);

    expect(mockAnchoringClient).to.equal(anchoringClient);
    expect(mockSystemAnchoringClient.config.blockchainRid).to.equal(systemAnchoringChainBridString);
  });

  describe("getSystemAnchoringChain", () => {
    it("throw an SystemChainException when invalid client is provided, with specific error message", async () => {
      const dappClient = await createClient({
        nodeUrlPool: [`http://localhost`, `http://localhost`],
        blockchainRid: mockStringBlockchainRid,
      });

      sinon.stub(dappClient, "query").resolves(null);

      try {
        await utils.getSystemAnchoringChain(dappClient);
        expect.fail();
      } catch (error) {
        expect(error).to.be.instanceOf(SystemChainException);
        expect(error.message).to.contain(
          "Query to system chain failed with error: Directory chain client must be provided.",
        );
      }
    });
  });
});
