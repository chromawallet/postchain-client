import { ResponseStatus } from "../../../../src/blockchainClient/enums";
import { IClient } from "../../../../src/blockchainClient/interface";
import { ConfirmationProof } from "../../../../src/blockchainClient/types";
import { AnchoringTransaction } from "../../../../src/ICCF/types";

export async function resolveClusterAnchoringTransactionConfirmationWithStubs(
  stubs: {
    transactionStatusStub: sinon.SinonStub;
    confirmationProofStub: sinon.SinonStub;
    blockAnchoringTransactionStub: sinon.SinonStub;
    anchoringTransactionSchemaStub: sinon.SinonStub;
    systemAnchoringTransactionConfirmationStub: sinon.SinonStub;
    digestToSignStub: sinon.SinonStub;
  },
  clients: {
    sourceClient: IClient;
    anchoringClient: IClient;
  },
  mocks: {
    transactionRid: Buffer;
    confirmationProof: ConfirmationProof;
    anchoringTransaction: AnchoringTransaction;
    directoryNodeUrlPool: string[];
  },
) {
  const {
    transactionStatusStub,
    confirmationProofStub,
    blockAnchoringTransactionStub,
    anchoringTransactionSchemaStub,
    systemAnchoringTransactionConfirmationStub,
    digestToSignStub,
  } = stubs;

  const { sourceClient, anchoringClient } = clients;
  const { transactionRid, confirmationProof, anchoringTransaction } = mocks;

  digestToSignStub.returns(transactionRid);
  transactionStatusStub.resolves({
    status: ResponseStatus.Confirmed,
    statusCode: 200,
    transactionRid: transactionRid,
  });
  confirmationProofStub.withArgs(transactionRid).resolves(confirmationProof);

  blockAnchoringTransactionStub
    .withArgs(sourceClient, anchoringClient, transactionRid, confirmationProof)
    .resolves(anchoringTransaction);

  anchoringTransactionSchemaStub.callsFake(input => {
    if (input === anchoringTransaction) {
      return { success: true, data: input };
    } else {
      return { success: false };
    }
  });

  systemAnchoringTransactionConfirmationStub.resolves(anchoringTransaction);
}
