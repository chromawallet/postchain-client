import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { IClient } from "../../../src/blockchainClient/interface";
import { mockAnchoringTransaction, mockBuffer, mockStringBlockchainRid } from "../common/mocks";
import sinon from "sinon";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import * as utils from "../../../src/blockchainClient/utils";
import * as iccfUtils from "../../../src/ICCF/utils";
import { server } from "../../../mocks/servers";
import { expect } from "chai";
import { AnchoringStatus } from "../../../src/blockchainClient/enums";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";

const mockAnchoringClient: IClient = clientConfiguredToD1;
const mockSystemAnchoringClient: IClient = clientConfiguredToD1;
let getAwaitGetAnchoringTransactionForBlockRidStub: sinon.SinonStub;
let getAnchoringTransactionSchemaValidationStub: sinon.SinonStub;
let getSystemAnchoringTransactionStub: sinon.SinonStub;
describe("getAnchoringStatusForBlockRid", async () => {
  let client: IClient;

  before(async () => {
    server.listen();
    client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
    });
  });

  beforeEach(async () => {
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
    getAwaitGetAnchoringTransactionForBlockRidStub = sinon.stub(
      iccfUtils,
      "awaitGetAnchoringTransactionForBlockRid",
    );
    getAnchoringTransactionSchemaValidationStub = sinon.stub(
      AnchoringTransactionSchema,
      "safeParse",
    );
    getSystemAnchoringTransactionStub = sinon.stub(utils, "getSystemAnchoringTransaction");
  });

  afterEach(() => {
    server.resetHandlers();
    sinon.restore();
    sinon.reset();
  });

  it("returns AnchoringStatus NotAnchored", async () => {
    getAwaitGetAnchoringTransactionForBlockRidStub.resolves(null);
    const result = await client.getAnchoringStatusForBlockRid(
      mockBuffer,
      mockAnchoringClient,
      mockStringBlockchainRid,
    );

    expect(result).to.deep.equal({ status: AnchoringStatus.NotAnchored });
  });

  it("returns AnchoringStatus ClusterAnchored and clusterAnchoredTx", async () => {
    getAwaitGetAnchoringTransactionForBlockRidStub.resolves(mockAnchoringTransaction);
    getAnchoringTransactionSchemaValidationStub.callsFake(input => {
      if (input === mockAnchoringTransaction) {
        return { success: true, data: input };
      } else {
        return { success: false };
      }
    });
    const result = await client.getAnchoringStatusForBlockRid(
      mockBuffer,
      mockAnchoringClient,
      mockStringBlockchainRid,
    );

    expect(result).to.deep.equal({
      status: AnchoringStatus.ClusterAnchored,
      clusterAnchoredTx: mockAnchoringTransaction,
    });
  });

  it("returns AnchoringStatus SystemAnchored, clusterAnchoredTx and systemAnchoredTx", async () => {
    getAwaitGetAnchoringTransactionForBlockRidStub.resolves(mockAnchoringTransaction);
    getAnchoringTransactionSchemaValidationStub.callsFake(input => {
      if (input === mockAnchoringTransaction) {
        return { success: true, data: input };
      } else {
        return { success: false };
      }
    });
    getSystemAnchoringTransactionStub.resolves(mockAnchoringTransaction);
    const result = await client.getAnchoringStatusForBlockRid(
      mockBuffer,
      mockAnchoringClient,
      mockStringBlockchainRid,
    );

    expect(result).to.deep.equal({
      status: AnchoringStatus.SystemAnchored,
      clusterAnchoredTx: mockAnchoringTransaction,
      systemAnchoredTx: mockAnchoringTransaction,
    });
  });
});
