import { HttpResponse, http } from "msw";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { expect } from "chai";
import { toString } from "../../../src/formatter";
import { server } from "../../../mocks/servers";
import { mockBufferBlockchainRid, mockStringBlockchainRid } from "../common/mocks";

const tx =
  "A581B83081B5A56B3069A12204200398A1E7B0F87709AFD14CD1821EAAAFB41931A8097FFA2D83E5FE10DA7E4DD5A51A3018A5163014A20E0C0C6D795F6F7065726174696F6EA5023000A5273025A1230421032BF0FCF83A287FB5EC71E4DAEA5DE892804A5BBBED6130DBCDDB871015C34EA0A5463044A14204404B425779BF8BD5ED503837337BBE102B7013F791F3BCFB812726EA93E076A3267656EC773AA10617E1ABDB25E97AB1CF2992CDB3E059D668C90BDA6BB91FE836";

describe("retry feature in blockchain client", async () => {
  before(async () => {
    server.listen();
  });

  afterEach(() => {
    () => server.resetHandlers();
  });

  after(() => server.resetHandlers());

  it("move to next node if current node returns unknown error status code", async () => {
    const attemptsPerEndpoint = 3;

    server.use(
      http.get(`http://localhost1/tx/*`, function () {
        return new HttpResponse("Unpredicted error", { status: 555 });
      }),
      http.get(`http://localhost/tx/*`, function () {
        return HttpResponse.json({
          tx,
        });
      }),
    );

    const client = await createClient({
      nodeUrlPool: [`http://localhost1`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      failOverConfig: {
        attemptsPerEndpoint,
      },
    });

    const res = await client.getTransaction(mockBufferBlockchainRid);

    expect(toString(res)).to.be.eq(tx);
  });

  it("returns error when all nodes are failed with request", async () => {
    const attemptsPerEndpoint = 3;

    server.use(
      http.get(`http://localhost1/tx/*`, function () {
        return new HttpResponse("Unpredicted error", { status: 550 });
      }),
      http.get(`http://localhost/tx/*`, function () {
        return new HttpResponse("Unpredicted error", { status: 550 });
      }),
    );

    const client = await createClient({
      nodeUrlPool: [`http://localhost1`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      failOverConfig: {
        attemptsPerEndpoint,
      },
    });

    try {
      await client.getTransaction(mockBufferBlockchainRid);
    } catch (error) {
      expect(error.message).to.be.eq(
        "Unexpected status code from server. Code: 550. Message: Unpredicted error.",
      );
    }
  });
});
