import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { IClient } from "../../../src/blockchainClient/interface";
import sinon from "sinon";
import * as utils from "../../../src/blockchainClient/utils";
import * as iccf from "../../../src/ICCF/IccfProofTxMaterialBuilder";
import { NetworkSettings } from "../../../src/blockchainClient/types";
import { expect } from "chai";
import { InvalidTxRidException } from "../../../src/restclient/errors";
import {
  mockAnchoringTransaction,
  mockBuffer,
  mockStringBlockchainRid,
  mockStringDirectoryChainRid,
} from "../common/mocks";

let client: IClient;
const CONFIGURED_POLL_COUNT = 5;
let anchoringClientStub: sinon.SinonStub;
let getSystemClientStub: sinon.SinonStub;
let getSystemAnchoringTransactionStub: sinon.SinonStub;

describe("getSystemAnchoringTransactionConfirmation", async () => {
  const networkSettings: NetworkSettings = {
    nodeUrlPool: [`http://localhost`, `http://localhost`],
    blockchainRid: mockStringBlockchainRid,
    dappStatusPolling: utils.setStatusPolling({
      interval: 1000,
      count: CONFIGURED_POLL_COUNT,
    }),
    clusterAnchoringStatusPolling: utils.setStatusPolling(),
    systemAnchoringStatusPolling: utils.setStatusPolling(),
    directoryChainRid: mockStringDirectoryChainRid,
  };
  before(async () => {
    client = await createClient(networkSettings);
  });

  beforeEach(() => {
    getSystemClientStub = sinon.stub(utils, "getSystemClient");
    anchoringClientStub = sinon.stub(iccf, "getAnchoringClient");
    getSystemAnchoringTransactionStub = sinon.stub(utils, "getSystemAnchoringTransaction");
  });

  afterEach(() => {
    sinon.restore();
    sinon.reset();
  });

  it("should return an InvalidTxRidException error with invalid anchoredTxRid", async () => {
    const invalidTxRid = Buffer.from("invalidRid");

    await expect(
      client.getSystemAnchoringTransactionConfirmation(
        invalidTxRid,
        client,
        mockStringBlockchainRid,
      ),
    ).to.be.rejectedWith(InvalidTxRidException, `expected length 32 of txRID, but got 10`);
  });

  it("should return a system anchoring transaction confirmation", async () => {
    getSystemClientStub.resolves(client);
    anchoringClientStub.resolves(client);
    getSystemAnchoringTransactionStub.resolves(mockAnchoringTransaction);

    const result = await client.getSystemAnchoringTransactionConfirmation(
      mockBuffer,
      client,
      mockStringBlockchainRid,
    );

    expect(result).to.deep.equal(mockAnchoringTransaction);
  });
});
