import { IClient } from "../../../src/blockchainClient/interface";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { AnchoringStatus, ResponseStatus } from "../../../src/blockchainClient/enums";
import { expect } from "chai";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";
import sinon from "sinon";
import { StatusObject } from "../../../src/blockchainClient/types";
import {
  CONFIGURED_POLL_COUNT,
  CONFIGURED_POLL_INTERVAL,
  mockAnchoringTransaction,
  mockStringBlockchainRid,
  mockStringDirectoryChainRid,
  mockThirtyTwoBytesBuffer,
} from "../common/mocks";
import * as utils from "../../../src/blockchainClient/utils";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";

const mockAnchoringClient: IClient = clientConfiguredToD1;
const mockSystemAnchoringClient: IClient = clientConfiguredToD1;
describe("getTransactionConfirmationLevel", function () {
  let client: IClient;
  let getTransactionStatusStub: sinon.SinonStub;
  let getClusterAnchoringTransactionConfirmationStub: sinon.SinonStub;
  let getSystemAnchoringTransactionConfirmationStub: sinon.SinonStub;
  let anchoringTransactionSchemaStub: sinon.SinonStub;

  beforeEach(async function () {
    client = await createClient({
      nodeUrlPool: [`http://localhost`, `http://localhost`],
      blockchainRid: mockStringBlockchainRid,
      dappStatusPolling: utils.setStatusPolling({
        interval: CONFIGURED_POLL_INTERVAL,
        count: CONFIGURED_POLL_COUNT,
      }),
      clusterAnchoringStatusPolling: utils.setStatusPolling(),
      systemAnchoringStatusPolling: utils.setStatusPolling(),
      directoryChainRid: mockStringDirectoryChainRid,
    });

    getTransactionStatusStub = sinon.stub(client, "getTransactionStatus");
    getClusterAnchoringTransactionConfirmationStub = sinon.stub(
      client,
      "getClusterAnchoringTransactionConfirmation",
    );
    getSystemAnchoringTransactionConfirmationStub = sinon.stub(
      client,
      "getSystemAnchoringTransactionConfirmation",
    );
    anchoringTransactionSchemaStub = sinon.stub(AnchoringTransactionSchema, "safeParse");
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
  });

  afterEach(() => {
    sinon.restore();
    sinon.reset();
  });

  it("returns an error with status FailedAnchoring when cluster anchoring chain confirmation failed", async () => {
    const mockStatusObject: StatusObject = {
      status: ResponseStatus.Confirmed,
    };
    getTransactionStatusStub.resolves(mockStatusObject);
    getClusterAnchoringTransactionConfirmationStub.resolves(AnchoringStatus.FailedAnchoring);

    const transactionReceipt = {
      status: AnchoringStatus.FailedAnchoring,
      statusCode: 400,
      transactionRid: mockThirtyTwoBytesBuffer,
    };

    const confirmationLevel = await client.getTransactionConfirmationLevel(
      mockThirtyTwoBytesBuffer,
    );

    expect(confirmationLevel).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.calledOnce).to.be.true;
    expect(getClusterAnchoringTransactionConfirmationStub.calledOnce).to.be.true;
  });

  it("should return Waiting status for Source chain confirmation", async () => {
    const mockStatusObject: StatusObject = {
      status: ResponseStatus.Waiting,
    };
    getTransactionStatusStub.resolves(mockStatusObject);

    const transactionReceipt = {
      status: ResponseStatus.Waiting,
      statusCode: 200,
      transactionRid: mockThirtyTwoBytesBuffer,
    };

    const confirmationLevel = await client.getTransactionConfirmationLevel(
      mockThirtyTwoBytesBuffer,
    );

    expect(confirmationLevel).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.calledOnce).to.be.true;
  });

  it("should return Confirmed status for Source chain confirmation", async () => {
    const mockStatusObject: StatusObject = {
      status: ResponseStatus.Confirmed,
    };
    getTransactionStatusStub.resolves(mockStatusObject);
    getClusterAnchoringTransactionConfirmationStub.resolves(AnchoringStatus.NotAnchored);

    const transactionReceipt = {
      status: ResponseStatus.Confirmed,
      statusCode: 200,
      transactionRid: mockThirtyTwoBytesBuffer,
    };

    const confirmationLevel = await client.getTransactionConfirmationLevel(
      mockThirtyTwoBytesBuffer,
    );

    expect(confirmationLevel).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.calledOnce).to.be.true;
    expect(getClusterAnchoringTransactionConfirmationStub.calledOnce).to.be.true;
  });

  it("should return Anchoring Transaction for Cluster Anchoring Chain confirmation", async () => {
    const mockStatusObject: StatusObject = {
      status: ResponseStatus.Confirmed,
    };
    getTransactionStatusStub.resolves(mockStatusObject);
    anchoringTransactionSchemaStub.callsFake(input => {
      if (input === mockAnchoringTransaction) {
        return { success: true, data: input };
      } else {
        return { success: false };
      }
    });

    getClusterAnchoringTransactionConfirmationStub.resolves(mockAnchoringTransaction);

    const transactionReceipt = {
      status: AnchoringStatus.ClusterAnchored,
      statusCode: 200,
      transactionRid: mockThirtyTwoBytesBuffer,
      clusterAnchoredTx: mockAnchoringTransaction,
      clusterAnchoringClientBrid: mockAnchoringClient.config.blockchainRid,
    };

    const confirmationLevel = await client.getTransactionConfirmationLevel(
      mockThirtyTwoBytesBuffer,
    );

    expect(confirmationLevel).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.calledOnce).to.be.true;
    expect(anchoringTransactionSchemaStub.calledOnce).to.be.true;
    expect(getClusterAnchoringTransactionConfirmationStub.calledOnce).to.be.true;
  });

  it("should return Anchoring Transaction for System Anchoring Chain confirmation", async () => {
    const mockStatusObject: StatusObject = {
      status: ResponseStatus.Confirmed,
    };
    getTransactionStatusStub.resolves(mockStatusObject);
    anchoringTransactionSchemaStub.callsFake(input => {
      if (input === mockAnchoringTransaction) {
        return { success: true, data: input };
      } else {
        return { success: false };
      }
    });

    getClusterAnchoringTransactionConfirmationStub.resolves(mockAnchoringTransaction);
    getSystemAnchoringTransactionConfirmationStub.resolves(mockAnchoringTransaction);

    const transactionReceipt = {
      status: AnchoringStatus.SystemAnchored,
      statusCode: 200,
      transactionRid: mockThirtyTwoBytesBuffer,
      clusterAnchoredTx: mockAnchoringTransaction,
      clusterAnchoringClientBrid: mockAnchoringClient.config.blockchainRid,
      systemAnchoredTx: mockAnchoringTransaction,
      systemAnchoringClientBrid: mockSystemAnchoringClient.config.blockchainRid,
    };

    const confirmationLevel = await client.getTransactionConfirmationLevel(
      mockThirtyTwoBytesBuffer,
    );

    expect(confirmationLevel).to.deep.equal(transactionReceipt);
    expect(getTransactionStatusStub.calledOnce).to.be.true;
    expect(anchoringTransactionSchemaStub.calledOnce).to.be.true;
    expect(getClusterAnchoringTransactionConfirmationStub.calledOnce).to.be.true;
    expect(getSystemAnchoringTransactionConfirmationStub.calledOnce).to.be.true;
  });
});
