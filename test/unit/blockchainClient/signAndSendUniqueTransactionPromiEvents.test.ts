import { server } from "../../../mocks/servers";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { IClient } from "../../../src/blockchainClient/interface";
import sinon from "sinon";
import {
  CONFIGURED_POLL_COUNT,
  CONFIGURED_POLL_INTERVAL,
  exampleOperation,
  mockAnchoringTransaction,
  mockBuffer,
  mockConfirmationProof,
  mockSignatureProvider,
  mockStringBlockchainRid,
  mockStringDirectoryChainRid,
} from "../common/mocks";
import * as utils from "../../../src/blockchainClient/utils";
import { ChainConfirmationLevel, TransactionEvent } from "../../../src/blockchainClient/enums";
import { SignedTransaction } from "../../../src/blockchainClient/types";
import { resolveClusterAnchoringTransactionConfirmationWithStubs } from "./helpers/sendTransaction";
import { clientConfiguredToD1 } from "../ICCF/iccfProofMaterialBuilder.test";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";
import * as iccf from "../../../src/ICCF/IccfProofTxMaterialBuilder";
import * as gtxTool from "../../../src/gtx/gtx";
import { GTX } from "../../../src/gtx/types";
import { expect } from "chai";
import { LOCAL_POOL } from "../../../src/constants";

let client: IClient;
const mockAnchoringClient: IClient = clientConfiguredToD1;
const mockSystemAnchoringClient: IClient = clientConfiguredToD1;
let signTransactionStub: sinon.SinonStub;
let getTransactionStatusStub: sinon.SinonStub;
let getConfirmationProofStub: sinon.SinonStub;
let getBlockAnchoringTransactionStub: sinon.SinonStub;
let anchoringTransactionSchemaStub: sinon.SinonStub;
let getSystemAnchoringTransactionConfirmationStub: sinon.SinonStub;
let getDigestToSignStub: sinon.SinonStub;
let getGTXFromBufferOrTransactionOrOperationStub: sinon.SinonStub;
let sendTransactionSentPromiEventSpy: sinon.SinonSpy;

const mockGtx: GTX = {
  blockchainRid: mockBuffer,
  operations: [{ opName: "string", args: [] }],
  signers: [],
  signatures: [],
};

describe("signAndSendUniqueTransaction emits events", async () => {
  before(async () => {
    server.listen();
    client = await createClient({
      nodeUrlPool: LOCAL_POOL,  
      blockchainRid: mockStringBlockchainRid,
      dappStatusPolling: utils.setStatusPolling({
        interval: CONFIGURED_POLL_INTERVAL,
        count: CONFIGURED_POLL_COUNT,
      }),
      clusterAnchoringStatusPolling: utils.setStatusPolling(),
      systemAnchoringStatusPolling: utils.setStatusPolling(),
      directoryChainRid: mockStringDirectoryChainRid,
    });
  });

  beforeEach(() => {
    signTransactionStub = sinon.stub(client, "signTransaction");
    getTransactionStatusStub = sinon.stub(client, "getTransactionStatus");
    getConfirmationProofStub = sinon.stub(client, "getConfirmationProof");
    getBlockAnchoringTransactionStub = sinon.stub(iccf, "getBlockAnchoringTransaction");
    anchoringTransactionSchemaStub = sinon.stub(AnchoringTransactionSchema, "safeParse");
    getSystemAnchoringTransactionConfirmationStub = sinon.stub(
      client,
      "getSystemAnchoringTransactionConfirmation",
    );
    getDigestToSignStub = sinon.stub(gtxTool, "getDigestToSign");
    sinon.stub(utils, "getAnchoringClientAndSystemChainRid").resolves({
      anchoringClient: mockAnchoringClient,
      systemAnchoringChainBridString: mockSystemAnchoringClient.config.blockchainRid,
    });
    getGTXFromBufferOrTransactionOrOperationStub = sinon.stub(
      utils,
      "getGTXFromBufferOrTransactionOrOperation",
    );
  });

  afterEach(() => {
    server.resetHandlers();
    sinon.restore();
    sinon.reset();
  });

  it("tests promi events signed, dapp received, dapp confirmed, cluster anchor confirmed, system anchor confirmed", async () => {
    signTransactionStub.resolves(mockBuffer as SignedTransaction);
    resolveClusterAnchoringTransactionConfirmationWithStubs(
      {
        transactionStatusStub: getTransactionStatusStub,
        confirmationProofStub: getConfirmationProofStub,
        blockAnchoringTransactionStub: getBlockAnchoringTransactionStub,
        anchoringTransactionSchemaStub: anchoringTransactionSchemaStub,
        systemAnchoringTransactionConfirmationStub: getSystemAnchoringTransactionConfirmationStub,
        digestToSignStub: getDigestToSignStub,
      },
      {
        sourceClient: client,
        anchoringClient: mockAnchoringClient,
      },
      {
        transactionRid: mockBuffer,
        confirmationProof: mockConfirmationProof,
        anchoringTransaction: mockAnchoringTransaction,
        directoryNodeUrlPool: utils.getUrlsFromEndpoints(client.config.endpointPool),
      },
    );

    getGTXFromBufferOrTransactionOrOperationStub.returns(mockGtx);

    try {
      const promiEvent = client.signAndSendUniqueTransaction(
        exampleOperation,
        mockSignatureProvider,
        true,
        undefined,
        ChainConfirmationLevel.SystemAnchoring
      );
      sendTransactionSentPromiEventSpy = sinon.spy(promiEvent, "emit");
      await promiEvent;
      expect(sendTransactionSentPromiEventSpy.callCount).to.equal(5);
      expect(sendTransactionSentPromiEventSpy.calledWithMatch(TransactionEvent.Signed)).to.be.true;
      expect(sendTransactionSentPromiEventSpy.calledWithMatch(TransactionEvent.DappReceived)).to.be.true;
      expect(sendTransactionSentPromiEventSpy.calledWithMatch(TransactionEvent.DappConfirmed)).to.be.true;
      expect(sendTransactionSentPromiEventSpy.calledWithMatch(TransactionEvent.ClusterAnchoringConfirmation)).to.be.true;
      expect(sendTransactionSentPromiEventSpy.calledWithMatch(TransactionEvent.SystemAnchoringConfirmation)).to.be.true;
    } catch (error) {
      console.log("ERROR in tests promi events", error);
    }
  });

});
