import { expect } from "chai";
import sinon from "sinon";
import * as handleRequestModule from "../../src/restclient/httpUtil";
import { FailoverStrategy } from "../../src/blockchainClient/enums";
import { retryRequest } from "../../src/restclient/failoverStrategies";
import { Method } from "../../src/restclient/enums";
import { createNodeManager } from "../../src/restclient/nodeManager";
import { mockNodeUrls } from "./common/mocks";

let mockHandleRequest = sinon.stub();

describe("Sticky node feature", () => {
  beforeEach(() => {
    mockHandleRequest = sinon.stub(handleRequestModule, "default");
  });
  afterEach(() => {
    sinon.resetHistory();
    sinon.restore();
    mockHandleRequest.reset();
  });

  describe("sticky node logic", async () => {
    const endpointUrlPool = mockNodeUrls;

    const retryRequestConfig = {
      endpointPool: endpointUrlPool.map(url => ({ url, whenAvailable: 0 })),
      failoverStrategy: FailoverStrategy.TryNextOnError,
      statusPollCount: 1,
      statusPollInterval: 1,
      attemptsPerEndpoint: 2,
      attemptInterval: 100,
      unreachableDuration: 5000,
    };
    const successfullResponse = { error: null, statusCode: 200, rspBody: "" };
    const failedResponse = { error: null, statusCode: 500, rspBody: "" };

    it("should NOT retain a node when sticky node is disabled.", async () => {
      mockHandleRequest.onCall(0).resolves(successfullResponse);
      mockHandleRequest.onCall(1).resolves(successfullResponse);
      mockHandleRequest.onCall(2).resolves(successfullResponse);

      const nodeManager = createNodeManager({
        nodeUrls: endpointUrlPool,
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      for (let i = 0; i < 3; i++) {
        await retryRequest({
          method: Method.GET,
          path: "/path",
          config: {
            ...retryRequestConfig,
            nodeManager,
          },
          validateStatusCode: statusCode => ![500, 503].includes(statusCode),
        });
      }
      const requestNodes = [
        mockHandleRequest.getCall(0).args[2],
        mockHandleRequest.getCall(1).args[2],
        mockHandleRequest.getCall(2).args[2],
      ];

      // Expect all nodes to not be the same.
      expect(requestNodes.every(node => node === requestNodes[0])).to.equal(false);
    });

    it("should be possible to retain the same node in between requests.", async () => {
      mockHandleRequest.onCall(0).resolves(successfullResponse);
      mockHandleRequest.onCall(1).resolves(successfullResponse);
      mockHandleRequest.onCall(2).resolves(successfullResponse);

      const nodeManager = createNodeManager({
        nodeUrls: endpointUrlPool,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      for (let i = 0; i < 3; i++) {
        await retryRequest({
          method: Method.GET,
          path: "/path",
          config: {
            ...retryRequestConfig,
            nodeManager,
          },
          validateStatusCode: statusCode => ![500, 503].includes(statusCode),
        });
      }

      const selectedStickyNode = mockHandleRequest.getCall(0).args[2];

      expect(mockHandleRequest.getCall(1).args[2]).to.equal(selectedStickyNode);
      expect(mockHandleRequest.getCall(2).args[2]).to.equal(selectedStickyNode);
      expect(mockHandleRequest.callCount).to.equal(3);
    });

    it("should set new sticky node, when orginal one fails.", async () => {
      mockHandleRequest.onCall(0).resolves(failedResponse);
      mockHandleRequest.onCall(1).resolves(failedResponse);
      mockHandleRequest.onCall(2).resolves(failedResponse);
      mockHandleRequest.onCall(3).resolves(successfullResponse);
      mockHandleRequest.onCall(4).resolves(failedResponse);
      mockHandleRequest.onCall(5).resolves(failedResponse);
      mockHandleRequest.onCall(6).resolves(successfullResponse);

      const nodeManager = createNodeManager({
        nodeUrls: endpointUrlPool,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      for (let i = 0; i < 2; i++) {
        await retryRequest({
          method: Method.GET,
          path: "/path",
          config: {
            ...retryRequestConfig,
            nodeManager,
          },
          validateStatusCode: statusCode => ![500, 503].includes(statusCode),
        });
      }

      const nodeOfRequest: string[] = [];
      for (let i = 0; i < 7; i++) {
        // Pushes the node that was used in the request of "i".
        nodeOfRequest.push(mockHandleRequest.getCall(i).args[2]);
      }

      expect(nodeOfRequest[0]).to.equal(nodeOfRequest[1]); // both requests should have used the same node and the requests should have failed
      expect(nodeOfRequest[2]).to.not.equal(nodeOfRequest[0]); // Should have picked a new node, and request should have failed.
      expect(nodeOfRequest[3]).to.equal(nodeOfRequest[2]); // Should have tried again on previous node, succeded and been set as new sticky node.
      expect(nodeOfRequest[4]).to.equal(nodeOfRequest[3]); // Should have used the sticky node, but request should have failed.
      expect(nodeOfRequest[5]).to.equal(nodeOfRequest[4]); // Should have used the sticky node, but request should have failed.
      expect(nodeOfRequest[6]).to.not.equal(nodeOfRequest[5]); // Should have tried using a new node and succeded.
      expect(mockHandleRequest.callCount).to.equal(7);
    });
  });
});
