import * as secp256k1 from "secp256k1";
import { Buffer } from "buffer";
import { sign, signDigest } from "../../src/encryption/encryption";
import { RawGtxBody } from "../../src/gtx/types";
import { getDigestToSignFromRawGtxBody } from "../../src/gtx/gtx";

const sigProv = (num: number) => {
  const s = sig(num);
  return {
    pubKey: s.pubKey,
    sign: async (rawGtxBody: RawGtxBody) => {
      const digest = getDigestToSignFromRawGtxBody(rawGtxBody);
      return signDigest(digest, s.privKey);
    },
    signature: s.signature,
  };
};

const sig = (num: number) => {
  const privKey = Buffer.from(`1234567890123456789012345678901${num}`);
  const pubKey = Buffer.from(secp256k1.publicKeyCreate(privKey));
  const signature = Buffer.from(sign(Buffer.from("hello", "utf8"), privKey));
  return { pubKey: pubKey, signature: signature, privKey: privKey };
};

const signatures = (count: number) => {
  const result: Buffer[] = [];
  for (let i = 0; i < count; i++) {
    result.push(sig(i).signature);
  }
  return result;
};

const signers = (count: number) => {
  const result: Buffer[] = [];
  for (let i = 0; i < count; i++) {
    result.push(sig(i).pubKey);
  }
  return result;
};

const privKey = (index: number) => {
  return sig(index).privKey;
};
export { signers, sig, sigProv, privKey, signatures };
