import sinon from "sinon";
import { Method } from "../../src/restclient/enums";
import { requestWithFailoverStrategy } from "../../src/restclient/restclientutil";
import { FailoverStrategy } from "../../src/blockchainClient/enums";
import * as handleRequestModule from "../../src/restclient/httpUtil";
import { createNodeManager } from "../../src/restclient/nodeManager";
import * as failoverStrategiesModule from "../../src/restclient/failoverStrategies";
import { expect } from "chai";
import { mockNodeUrls, mockStringDirectoryChainRid } from "./common/mocks";
import { setStatusPolling } from "../../src/blockchainClient/utils";

describe("requestWithFailoverStrategy", () => {
  let mockHandleRequest: sinon.SinonStub<any, any>;
  let sandbox: sinon.SinonSandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
    mockHandleRequest = sinon.stub(handleRequestModule, "default");
  });
  afterEach(() => {
    sinon.resetHistory();
    sinon.restore();
    sandbox.restore();
  });

  describe("failover strategy selection", () => {
    const config = {
      endpointPool: [
        { url: "url1", whenAvailable: 0 },
        { url: "url2", whenAvailable: 0 },
      ],
      blockchainRid: "blockchainRid",
      dappStatusPolling: setStatusPolling({ interval: 100, count: 1 }),
      clusterAnchoringStatusPolling: setStatusPolling(),
      systemAnchoringStatusPolling: setStatusPolling(),
      failoverStrategy: FailoverStrategy.QueryMajority,
      attemptsPerEndpoint: 3,
      attemptInterval: 100,
      unreachableDuration: 100,
      nodeManager: createNodeManager({
        nodeUrls: ["url1", "url2"],
      }),
      directoryChainRid: mockStringDirectoryChainRid,
    };

    it("should override 'QueryMajority' failover strategy if 'forceSingleEndpoint' is set to true.", async () => {
      const singleEndpointSpy = sandbox.spy(failoverStrategiesModule, "singleEndpoint");
      mockHandleRequest.resolves({
        error: null,
        statusCode: 500,
        rspBody: "",
      });

      expect(singleEndpointSpy.calledOnce).to.equal(false);
      await requestWithFailoverStrategy(Method.GET, "/path", config, undefined, true);

      expect(singleEndpointSpy.calledOnce).to.equal(true);
    });
  });

  describe("test endpoint pool", async () => {
    it("it tries all nodes when we don't have nodes to assure bft majority", async () => {
      mockHandleRequest.resolves({
        error: null,
        statusCode: 500,
        rspBody: "",
      });

      const currentDate = new Date();
      const config = {
        nodeManager: createNodeManager({
          nodeUrls: mockNodeUrls,
        }),
        endpointPool: [
          { url: mockNodeUrls[0], whenAvailable: 0 },
          { url: mockNodeUrls[1], whenAvailable: 0 },
          { url: mockNodeUrls[2], whenAvailable: 0 },
          { url: mockNodeUrls[3], whenAvailable: currentDate.getTime() + 10000 },
          { url: mockNodeUrls[4], whenAvailable: currentDate.getTime() + 10000 },
        ],
        blockchainRid: "blockchainRid",
        dappStatusPolling: setStatusPolling({ interval: 100, count: 10 }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
        failoverStrategy: FailoverStrategy.AbortOnError,
        attemptsPerEndpoint: 1,
        attemptInterval: 100,
        unreachableDuration: 100,
        directoryChainRid: mockStringDirectoryChainRid,
      };
      await requestWithFailoverStrategy(Method.GET, "/path", config);
      sinon.assert.callCount(mockHandleRequest, 5);
    });
    it("does not call an unreachable node", async () => {
      mockHandleRequest.resolves({
        error: null,
        statusCode: 500,
        rspBody: "",
      });

      const currentDate = new Date();
      const nodeManager = createNodeManager({
        nodeUrls: ["url1", "url2", "url3"],
      });

      sandbox.stub(nodeManager, "nodes").value([
        {
          url: "url1",
          whenAvailable: 0,
          isAvailable: true,
        },
        {
          url: "url2",
          whenAvailable: currentDate.getTime() + 1000000,
          isAvailable: false,
        },
        {
          url: "url3",
          whenAvailable: 0,
          isAvailable: true,
        },
      ]);

      const config = {
        nodeManager,
        endpointPool: [
          { url: "url1", whenAvailable: 0 },
          { url: "url2", whenAvailable: 0 },
        ],
        blockchainRid: "blockchainRid",
        dappStatusPolling: setStatusPolling({ interval: 100, count: 10 }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
        failoverStrategy: FailoverStrategy.AbortOnError,
        attemptsPerEndpoint: 1,
        attemptInterval: 100,
        unreachableDuration: 100,
        directoryChainRid: mockStringDirectoryChainRid,
      };
      await requestWithFailoverStrategy(Method.GET, "/path", config);
      sinon.assert.callCount(mockHandleRequest, 2);
    });
  });
});
