import {
  ConfirmationProof,
  KeyPair,
  Operation,
  Transaction,
} from "../../../src/blockchainClient/types";
import { toBuffer, toString } from "../../../src/formatter";
import { RawGtxBody } from "../../../src/gtx/types";
import { AnchoringTransaction } from "../../../src/ICCF/types";
import * as secp256k1 from "secp256k1";
import * as gtxTool from "../../../src/gtx/gtx";
import { signDigest } from "../../../src/encryption/encryption";
import { randomInt } from "crypto";

export const contentTypes = {
  octetStream: "application/octet-stream",
  json: "application/json",
};
export const CONFIGURED_POLL_COUNT = 5;
export const CONFIGURED_POLL_INTERVAL = 500;
export const mockThirtyTwoBytesBuffer: Buffer = Buffer.alloc(32);
export const mockHexStringOfThirtyTwoBytesBuffer: string = toString(mockThirtyTwoBytesBuffer);
export const mockStringBlockchainRid =
  "D7AE4988E9FCAC470D1B5AB9CD04A79BFE165AB616B4D038C2ED944BA2461212";
export const exampleTxHashString =
  "834ba9e86285875cd0c4f2ff2605db1ed921a1dcd47c873916d99e06f8874f5a";
export const mockBuffer = toBuffer(exampleTxHashString);
export const mockBufferBlockchainRid: Buffer = toBuffer(mockStringBlockchainRid);
export const mockStringFaultyBrid =
  "60B01AD60DD4476E2DB5141B0848B9F350D4F6F7C7F6EE5421EFC9968FF5874C";
export const mockStringDirectoryChainRid =
  "0101010101010101010101010101010101010101010101010101010101010101";
export const mockNodeUrls: string[] = ["url-1", "url-2", "url-3", "url-4", "url-5"];
export const mockAnchoringTransaction: AnchoringTransaction = {
  txRid: mockThirtyTwoBytesBuffer,
  txData: mockThirtyTwoBytesBuffer,
  txOpIndex: 1,
};
export const mockConfirmationProof: ConfirmationProof = {
  merkleProofTree: "",
  txIndex: 0,
  hash: mockThirtyTwoBytesBuffer,
  blockHeader: mockThirtyTwoBytesBuffer,
  witness: mockThirtyTwoBytesBuffer,
};
export const exampleOperation: Operation = { name: "test", args: [] };
export const signerPrivKey: Buffer = Buffer.alloc(32, "a");
export const signerPubKey: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKey));
export const signerKeyPair: KeyPair = {
  privKey: signerPrivKey,
  pubKey: signerPubKey,
};
export const mockSignatureProvider = {
  pubKey: signerPubKey,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    return signDigest(digest, signerPrivKey);
  },
};
export const mockUnsignedTx = {
  operations: [exampleOperation],
  signers: [signerPubKey],
};

export const unsignedTx: Transaction = {
  operations: [
    {
      name: "setInteger",
      args: [randomInt(99999)],
    },
  ],
  signers: [signerPubKey],
};
