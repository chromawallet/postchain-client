import { describe } from "mocha";
import { assert, expect } from "chai";
import { Buffer } from "buffer";
import { BufferId, ResponseStatus, restClient } from "../../index";
import { QueryObject } from "../../src/restclient/types";
import { RestClient } from "../../src/restclient/interfaces";
import {
  LostMessageError,
  TxRejectedError,
  UnexpectedResponseError,
  UnexpectedStatusError,
} from "../../src/restclient/errors";
import { getBlockchainRidFromIid } from "../../src/restclient/restclientutil";
import { encodeValue } from "../../src/gtx/serialization";
import type { DictPair, RawGtv } from "../../src/gtv/types";
import { server } from "../../mocks/servers";
import { HttpResponse, http } from "msw";
import { createNodeManager } from "../../src/restclient/nodeManager";
import { contentTypes, mockStringBlockchainRid, mockThirtyTwoBytesBuffer } from "./common/mocks";

type QueryInput = {
  nameOrQueryObject: string | QueryObject;
  queryArguments?: DictPair;
};
type QueryResponse = {
  answerWithCode: number;
  answerWithResponseObject: RawGtv;
};

describe("Rest client tests", function () {
  let client: RestClient;
  before(() => {
    server.listen();
  });

  beforeEach(() => {
    client = restClient.createRestClient([`http://localhost`], mockStringBlockchainRid, 5, 100, {
      attemptsPerEndpoint: 1,
      attemptInterval: 100,
    });
  });

  afterEach(() => {
    server.resetHandlers();
  });

  function clientExpectResponse(
    shouldBeError: boolean,
    expectedResponseObject: RawGtv,
    done: Mocha.Done,
  ) {
    return (error: Error | null | undefined, responseObject: RawGtv | null | undefined) => {
      if (shouldBeError) {
        assert.ok(error != null);
        assert.isOk(error?.message, expectedResponseObject as string);
      } else {
        assert.ok(error == null);
        if (expectedResponseObject === null) {
          assert.ok(responseObject === null || responseObject === undefined);
        } else {
          assert.deepEqual(
            typeof responseObject === "string" ? responseObject : JSON.stringify(responseObject),
            JSON.stringify(expectedResponseObject),
          );
        }
      }

      done();
    };
  }

  async function testQuery(queryInput: QueryInput, queryResp: QueryResponse, done: Mocha.Done) {
    server.use(
      http.post(`http://localhost/query_gtv/${mockStringBlockchainRid}`, () => {
        return new HttpResponse(encodeValue(queryResp.answerWithResponseObject), {
          status: queryResp.answerWithCode,
          headers: {
            "Content-Type": contentTypes.octetStream,
          },
        });
      }),
    );

    let error: Error | null = null;
    let responseObject: RawGtv = null;
    if (typeof queryInput.nameOrQueryObject === "string") {
      await client
        .query(queryInput.nameOrQueryObject, queryInput.queryArguments)
        .then(value => {
          responseObject = value;
        })
        .catch(e => {
          error = e;
        });
    } else {
      await client
        .query(queryInput.nameOrQueryObject)
        .then(value => {
          responseObject = value;
        })
        .catch(e => {
          error = e;
        });
    }

    if (queryResp.answerWithCode != 200) {
      assert.ok(error != null);
    } else {
      assert.ok(error == null);
    }
    if (queryResp.answerWithResponseObject === null) {
      assert.ok(responseObject === null);
    } else {
      assert.deepEqual(
        JSON.stringify(responseObject),
        JSON.stringify(queryResp.answerWithResponseObject),
      );
    }
    done();
  }

  function testPostTransaction(
    txBuffer: Buffer,
    answerWithCode: number,
    answerWithResponseObject: RawGtv,
    done: Mocha.Done,
  ) {
    server.use(
      http.post(`http://localhost/tx/${mockStringBlockchainRid}`, () => {
        return new HttpResponse(JSON.stringify(answerWithResponseObject), {
          status: answerWithCode,
          headers: {
            "Content-Type": contentTypes.json,
          },
        });
      }),
    );

    client.postTransaction(
      txBuffer,
      clientExpectResponse(answerWithCode != 200, answerWithResponseObject, done),
    );
  }

  function testGetTransaction(
    txHashBuffer: Buffer,
    answerWithCode: number,
    done: Mocha.Done,
    serverReturnTx: BufferId,
  ) {
    server.use(
      http.get(
        `http://localhost/tx/${mockStringBlockchainRid}/${txHashBuffer.toString("hex")}`,
        () => {
          return new HttpResponse(
            serverReturnTx
              ? JSON.stringify({ tx: serverReturnTx.toString("hex") })
              : JSON.stringify(null),
            {
              status: answerWithCode,
              headers: {
                "Content-Type": contentTypes.json,
              },
            },
          );
        },
      ),
    );

    client.getTransaction(
      txHashBuffer,
      clientExpectResponse(answerWithCode != 200, serverReturnTx, done),
    );
  }

  const mockTransactionStatus = ({
    txHash,
    responseBody,
    statusCode,
  }: {
    txHash?: string;
    responseBody: any;
    statusCode: number;
  }) => {
    server.use(
      http.get(`http://localhost/tx/${mockStringBlockchainRid}/${txHash}/status`, () => {
        return new HttpResponse(
          typeof responseBody === "string" ? responseBody : JSON.stringify(responseBody),
          {
            status: statusCode,
            headers: {
              "Content-Type": contentTypes.json,
            },
          },
        );
      }),
    );
  };

  function testGetStatus(
    txHashBuffer: Buffer,
    answerWithCode: number,
    serverReturnObject: Record<string, ResponseStatus>,
    done: Mocha.Done,
  ) {
    mockTransactionStatus({
      txHash: txHashBuffer.toString("hex"),
      responseBody: serverReturnObject,
      statusCode: answerWithCode,
    });

    client.status(
      txHashBuffer,
      clientExpectResponse(answerWithCode != 200, serverReturnObject, done),
    );
  }

  describe("query test", () => {
    it("Query error 400 response", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 400,
          answerWithResponseObject: null,
        },
        done,
      );
    });
    it("Query OK null response", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 200,
          answerWithResponseObject: null,
        },
        done,
      );
    });

    it("Query OK empty string response", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 200,
          answerWithResponseObject: "",
        },
        done,
      );
    });

    it("Query OK int response", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 200,
          answerWithResponseObject: 1,
        },
        done,
      );
    });
    it("Query OK empty response", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 200,
          answerWithResponseObject: {},
        },
        done,
      );
    });

    it("Query OK", done => {
      testQuery(
        {
          nameOrQueryObject: "aQueryType",
        },
        {
          answerWithCode: 200,
          answerWithResponseObject: { test: "hi" },
        },
        done,
      );
    });
  });

  describe("Post transaction tests", () => {
    it("postTransaction error 400 response", done => {
      testPostTransaction(Buffer.from("hello"), 400, { error: "test error" }, done);
    });

    it("postTransaction error 500 response", done => {
      testPostTransaction(Buffer.from("hello"), 500, { error: "test error" }, done);
    });

    it("postTransaction error OK", done => {
      testPostTransaction(Buffer.from("hello"), 200, {}, done);
    });
  });

  describe("get transaction tests", () => {
    it("getTransaction OK", done => {
      testGetTransaction(mockThirtyTwoBytesBuffer, 200, done, Buffer.from("a transaction"));
    });

    it("getTransaction 404", done => {
      testGetTransaction(
        mockThirtyTwoBytesBuffer,
        404,
        done,
        "Can't find blockchain with blockchainRid: <hex-string>",
      );
    });
  });

  describe("status tests", () => {
    it("status unknown", done => {
      testGetStatus(Buffer.alloc(32, 1), 200, { status: ResponseStatus.Unknown }, done);
    });

    it("status confirmed", done => {
      testGetStatus(Buffer.alloc(32, 1), 200, { status: ResponseStatus.Confirmed }, done);
    });

    it("status short hash", done => {
      testGetTransaction(
        Buffer.alloc(31),
        404,
        done,
        "Can't find blockchain with blockchainRid: <hex-string>",
      );
    });
  });

  describe("waitConfirmation tests", () => {
    it("test waitConfirmation confirmation", done => {
      const txHash = Buffer.alloc(32, 1);
      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: ResponseStatus.Confirmed }),
        statusCode: 200,
      });

      client.waitConfirmation(txHash).then(function (result) {
        expect(result).to.be.null;
        done();
      });
    });

    it("test waitConfirmation with uncaught error code", done => {
      const txHash = Buffer.alloc(32, 1);
      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: "" }),
        statusCode: 403,
      });

      client
        .waitConfirmation(txHash)
        .then()
        .catch(error => {
          expect(error).to.eql(new UnexpectedStatusError(403));
        })
        .finally(() => {
          done();
        });
    });

    it("test waitConfirmation lost message", done => {
      const txHash = Buffer.alloc(32, 1);

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: ResponseStatus.Unknown }),
        statusCode: 200,
      });

      client
        .waitConfirmation(txHash)
        .then()
        .catch(error => {
          expect(error).to.eql(new LostMessageError());
          done();
        });
    });

    it("test waitConfirmation rejected", done => {
      const txHash = Buffer.alloc(32, 1);

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({
          status: ResponseStatus.Rejected,
          rejectReason: "This got rejected",
        }),
        statusCode: 200,
      });

      client
        .waitConfirmation(txHash)
        .then()
        .catch(error => {
          expect(error).to.eql(new TxRejectedError("This got rejected"));
          done();
        });
    });

    it("test waitConfirmation waiting", () => {
      const txHash = Buffer.alloc(32, 1);

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: ResponseStatus.Waiting }),
        statusCode: 200,
      });

      client.waitConfirmation(txHash);
    });

    it("test waitConfirmation error response", done => {
      const txHash = Buffer.alloc(32, 1);

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: "Random response" }),
        statusCode: 200,
      });

      client
        .waitConfirmation(txHash)
        .then()
        .catch(error => {
          expect(error).to.eql(new UnexpectedResponseError());
          done();
        });
    });
  });

  describe("postAndWaitConfirmation tests", () => {
    it("test postAndWaitConfirmation success", done => {
      const txHash = Buffer.alloc(32, 1);
      const txBuffer = Buffer.from("hello");

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: ResponseStatus.Confirmed }),
        statusCode: 200,
      });

      client.postAndWaitConfirmation(txBuffer, txHash).then((result: any) => {
        expect(result).to.be.null;
        done();
      });
    });

    it("returns an error to user if an error is returned when posting transaction", done => {
      const txHash = Buffer.alloc(32, 1);
      const txBuffer = Buffer.from("hello");
      server.use(
        http.post(`http://localhost/tx/${mockStringBlockchainRid}`, () => {
          return new HttpResponse(encodeValue({ error: "some error" }), {
            status: 400,
            headers: {
              "Content-Type": contentTypes.octetStream,
            },
          });
        }),
      );
      client
        .postAndWaitConfirmation(txBuffer, txHash)
        .then()
        .catch(e => {
          expect(e.message).to.eql(
            new UnexpectedStatusError(400, `{"error":"some error"}`).message,
          );
          done();
        });
    });

    it("test postAndWaitConfirmation validated", done => {
      const txHash = Buffer.alloc(32, 1);
      const txBuffer = Buffer.from("hello");

      mockTransactionStatus({
        txHash: txHash.toString("hex"),
        responseBody: JSON.stringify({ status: ResponseStatus.Confirmed }),
        statusCode: 200,
      });

      server.use(
        http.post(`http://localhost/tx/${mockStringBlockchainRid}`, () => {
          return new HttpResponse(JSON.stringify({}), {
            status: 200,
          });
        }),
      );

      client
        .postAndWaitConfirmation(txBuffer, txHash, true)
        .then()
        .catch((error: Error) => {
          expect(error).to.eql("Automatic validation is not yet implemented");
          done();
        });
    });
  });

  describe("Get Brid test", () => {
    it("gets brid", done => {
      server.use(
        http.get(`http://localhost/brid/iid_0`, () => {
          return new HttpResponse(mockStringBlockchainRid, {
            status: 200,
            headers: {
              "Content-Type": "text/plain",
            },
          });
        }),
      );
      getBlockchainRidFromIid({
        endpointPool: [{ url: `http://localhost`, whenAvailable: 0 }],
        nodeManager: createNodeManager({
          nodeUrls: ["http://localhost"],
        }),
        chainId: 0,
      }).then((result: any) => {
        expect(result).to.equal(mockStringBlockchainRid);
        done();
      });
    });
  });
});
