import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import express from "express";
import { Express, Request, Response } from "express";
import { Server } from "http";
import bodyParser from "body-parser";

import * as pc from "../../index";
import { chromiaClientProvider } from "../../src/chromia/chromiaClientProvider";
import { ChromiaClient } from "../../src/chromia/interfaces";
import { RestClient } from "../../src/restclient/interfaces";
import { encodeValue } from "../../src/gtx/serialization";
import { RawGtv } from "../../src/gtv/types";
import {
  mockBufferBlockchainRid,
  mockStringBlockchainRid,
  mockHexStringOfThirtyTwoBytesBuffer,
  mockThirtyTwoBytesBuffer,
} from "./common/mocks";
import { LOCAL_POOL } from "../../src/constants";

const expect = chai.expect;
chai.use(chaiAsPromised);

describe("chromia client provider", function () {
  let chromiaClient: ChromiaClient;

  let app: Express;
  let client: RestClient;
  let server: Server;
  let port = 6000;

  beforeEach(function (done) {
    port++;
    app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    server = app.listen(port, () => {
      client = pc.restClient.createRestClient(
        [`http://localhost:${port}`],
        mockStringBlockchainRid,
      );
      chromiaClient = chromiaClientProvider(mockStringBlockchainRid, client);
      done();
    });
  });
  afterEach(() => {
    server.close();
  });

  function serverExpectPost(
    path: string,
    requestObject: object,
    responseCode: number,
    responseBody: Buffer,
  ) {
    app.post(path, (req: Request, res: Response) => {
      if (responseBody) {
        res.status(responseCode).send(responseBody);
      } else {
        res.status(responseCode).send();
      }
    });
  }
  it("create blockchain connection to list of nodes were the dapp is located", async () => {
    const answereWithResponseBody: RawGtv = [LOCAL_POOL, "http://localhost:7741"];
    serverExpectPost(
      `/query_gtv/${mockStringBlockchainRid}`,
      {
        type: "cm_get_blockchain_api_urls",
        blockchain_rid: mockThirtyTwoBytesBuffer,
      },
      200,
      encodeValue(answereWithResponseBody),
    );

    const blockchainClient = await chromiaClient.blockchainConnection(
      mockHexStringOfThirtyTwoBytesBuffer,
    );
    const urls = blockchainClient.getEndpointPool();
    expect(urls).to.deep.equal(answereWithResponseBody);
  });

  it("throws error when no nodes are hosting the dapp", async () => {
    const answereWithResponseBody: string[] = [];
    serverExpectPost(
      `/query_gtv/${mockStringBlockchainRid}`,
      {
        type: "cm_get_blockchain_api_urls",
        blockchain_rid: mockBufferBlockchainRid,
      },
      200,
      encodeValue(answereWithResponseBody),
    );

    await expect(
      chromiaClient.blockchainConnection(mockStringBlockchainRid),
    ).to.eventually.be.rejectedWith(
      `Cannot find nodes hosting the blockchain with BRID ${mockStringBlockchainRid}`,
    );
  });
});
