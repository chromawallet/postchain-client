import { expect } from "chai";
import { decodeTransactionToGtx } from "../../src/utils/decodeTransactionToGtx";
import * as gtx from "../../src/gtx/gtx";

describe("decodeTransactionToGtx", () => {
  it("should not fail if gtx has Bigint value", () => {
    const gtxData = {
      blockchainRid: Buffer.from("test", "hex"),
      // @ts-expect-error Bigint support
      operations: [{ opName: "operation", args: [42529585n] }],
      signers: [],
    };
    const serializedGtx = gtx.serialize(gtxData);

    const decodedGtx = decodeTransactionToGtx(Buffer.from(serializedGtx));
    expect(gtxData).to.deep.eq(decodedGtx);
  });
});
