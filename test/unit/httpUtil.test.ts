import * as handleRequestModule from "../../src/restclient/httpUtil";
import * as sinon from "sinon";
import { expect } from "chai";
import { Buffer } from "buffer";
import { requestWithFailoverStrategy } from "../../src/restclient/restclientutil";
import { Method } from "../../src/restclient/enums";
import { FailoverStrategy } from "../../src/blockchainClient/enums";
import { createNodeManager } from "../../src/restclient/nodeManager";

describe("retry functionality with attempts per endpoint set to three", () => {
  let spy: sinon.SinonSpy;
  const nodeUrls: string[] = [
    "http://localhost1:7740",
    "http://localhost2:7740",
    "http://localhost3:7740",
  ];
  beforeEach(() => {
    spy = sinon.spy(handleRequestModule, "default");
  });
  afterEach(() => {
    sinon.restore();
  });
  it("retries request three times on each of the three endpoints, in total 9 attempts", async () => {
    await requestWithFailoverStrategy(
      Method.GET,
      "query/8049DB98305FDB7C7360428FB779E7858C76BBEEF4A9D7F1C2B7AA0911AC2836/?type=hello_world",
      {
        nodeManager: createNodeManager({
          nodeUrls: nodeUrls,
        }),
        endpointPool: [
          { url: nodeUrls[0], whenAvailable: 0 },
          { url: nodeUrls[1], whenAvailable: 0 },
          { url: nodeUrls[2], whenAvailable: 0 },
        ],
        pool: { maxSockets: 1 },
        pollingInterval: 100,
        failoverStrategy: FailoverStrategy.AbortOnError,
        attemptsPerEndpoint: 3,
        attemptInterval: 100,
        unreachableDuration: 100,
      },
      Buffer.from(""),
    );
    expect(spy.callCount).to.equal(9);
  });
});
