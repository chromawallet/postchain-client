import sinon from "sinon";
import {
  requestWithFailoverStrategy,
  createTimeoutPromise,
  racePromisesWithTimeout,
  groupResponses,
  checkStateOfPromises,
} from "../../src/restclient/restclientutil";
import * as failoverStrategies from "../../src/restclient/failoverStrategies";
import { Method } from "../../src/restclient/enums";
import { FailoverStrategy } from "../../src/blockchainClient/enums";
import * as handleRequestModule from "../../src/restclient/httpUtil";
import { createNodeManager } from "../../src/restclient/nodeManager";
import { expect } from "chai";
import { mockStringDirectoryChainRid } from "./common/mocks";
import { setStatusPolling } from "../../src/blockchainClient/utils";

describe("restClientUtils", () => {
  describe("createTimeoutPromise", () => {
    it("should return a promise that rejects after the specified timeout", async () => {
      const timeout = 100;
      const promise = createTimeoutPromise(timeout);
      await expect(promise).to.be.rejectedWith("Timeout exceeded");
    });
  });

  describe("checkStateOfPromises", () => {
    it("should return an empty array if there are no promises", async () => {
      const promises: Promise<string>[] = [];

      const promisesWithStates = await checkStateOfPromises(promises);
      expect(promisesWithStates).to.be.an("array").that.has.length(0);
    });

    it("should return the correct state for a fulfilled promise.", async () => {
      const promises: Promise<string>[] = [Promise.resolve("resolved")];

      const promisesWithStates = await checkStateOfPromises(promises);
      expect(promisesWithStates).to.be.an("array").that.has.length(1);
      expect(promisesWithStates[0].status).to.equal("fulfilled");
      expect(promisesWithStates[0].value).to.equal("resolved");
    });

    it("should return the correct state for a rejected promise.", async () => {
      const promises: Promise<string>[] = [Promise.reject("I got rejected")];

      const promisesWithStates = await checkStateOfPromises(promises);
      expect(promisesWithStates).to.be.an("array").that.has.length(1);
      expect(promisesWithStates[0].status).to.equal("rejected");
      expect(promisesWithStates[0].value).to.equal("I got rejected");
    });

    it("should return the correct state for a pending promise.", async () => {
      const promises: Promise<string>[] = [
        new Promise((resolve, reject) => setTimeout(() => reject("I was pending"), 10000)),
      ];

      const promisesWithStates = await checkStateOfPromises(promises);
      expect(promisesWithStates).to.be.an("array").that.has.length(1);
      expect(promisesWithStates[0].status).to.equal("pending");
      expect(promisesWithStates[0].value).to.be.an("promise");
    });
  });

  describe("racePromisesWithTimeout", async () => {
    it("should reject if the timeout is exceeded", async () => {
      const timeout = 5000;

      const successfullPromise = new Promise<string>((resolve, reject) => {
        setTimeout(() => {
          resolve("I got resolved!");
        }, 10000);
      });

      const rejectedPromise = new Promise<string>((resolve, reject) => {
        setTimeout(() => {
          resolve("I got resolved!");
        }, 12000);
      });

      const promises = [successfullPromise, rejectedPromise];

      let utilError;
      try {
        await racePromisesWithTimeout(promises, timeout);
      } catch (error) {
        utilError = error;
      }

      expect(utilError).to.be.an("error");
      expect(utilError.message).to.equal("Timeout exceeded");
    });
    it("should return rest of promises if one resolves and the timeout is not exceeded.", async () => {
      const successfullPromise = new Promise<string>((resolve, reject) => {
        setTimeout(() => {
          resolve("I got resolved!");
        }, 5000);
      });

      const rejectedPromise = new Promise<string>((resolve, reject) => {
        setTimeout(() => {
          reject("I got rejected!");
        }, 20000);
      });

      let promises = [successfullPromise, rejectedPromise];
      promises = await racePromisesWithTimeout(promises, 10000);
      expect(promises).to.be.an("array").that.has.length(1);
    });
  });

  describe("groupResponses", () => {
    describe("maxNumberOfEqualResponses", () => {
      it("should return 0 if there are no responses", () => {
        const responses: any[] = [];
        const groups = groupResponses(responses);

        const result = groups.maxNumberOfEqualResponses();
        expect(result).to.equal(0);
      });

      it("should return the count of the most common response", () => {
        const responses = [{ result: "a" }, { result: "a" }, { result: "b" }, { result: "c" }];
        const groups = groupResponses(responses);

        const result = groups.maxNumberOfEqualResponses();
        expect(result).to.equal(2);
      });
    });
    describe("numberOfDistinctResponses", () => {
      it("should return 0 if there are no responses", () => {
        const responses: any[] = [];
        const groups = groupResponses(responses);

        const result = groups.numberOfDistinctResponses();
        expect(result).to.equal(0);
      });

      it("should return the number of distinct responses", () => {
        const responses = [{ result: "a" }, { result: "a" }, { result: "b" }, { result: "c" }];
        const groups = groupResponses(responses);

        const result = groups.numberOfDistinctResponses();
        expect(result).to.equal(3);
      });
    });
    describe("majorityResponse", () => {
      it("should return the most common response", () => {
        const responses = [{ result: "a" }, { result: "a" }, { result: "b" }, { result: "c" }];
        const groups = groupResponses(responses);

        const result = groups.majorityResponse();
        expect(result).to.equal("a");
      });
    });
  });

  describe("requestWithFailoverStrategy", () => {
    let mockAbortOnError = sinon.stub();
    let mockTryNextOnError = sinon.stub();
    let mockSingleEndpoint = sinon.stub();
    let mockHandleRequest = sinon.stub();

    beforeEach(() => {
      mockAbortOnError = sinon.stub(failoverStrategies, "abortOnError");
      mockTryNextOnError = sinon.stub(failoverStrategies, "tryNextOnError");
      mockSingleEndpoint = sinon.stub(failoverStrategies, "singleEndpoint");
      mockHandleRequest = sinon.stub(handleRequestModule, "default");
    });
    afterEach(() => {
      sinon.resetHistory();
      sinon.restore();
      mockAbortOnError.reset();
      mockAbortOnError.restore();
      mockHandleRequest.reset();
      mockHandleRequest.restore();
    });

    it("calls abortOnError", async () => {
      const config = {
        nodeManager: createNodeManager({
          nodeUrls: ["url1", "url2"],
        }),
        endpointPool: [
          { url: "url1", whenAvailable: 0 },
          { url: "url2", whenAvailable: 0 },
        ],
        blockchainRid: "blockchainRid",
        dappStatusPolling: setStatusPolling({ interval: 100, count: 1 }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
        failoverStrategy: FailoverStrategy.AbortOnError,
        attemptsPerEndpoint: 3,
        attemptInterval: 100,
        unreachableDuration: 100,
        directoryChainRid: mockStringDirectoryChainRid,
      };
      await requestWithFailoverStrategy(Method.GET, "/path", config);
      sinon.assert.calledOnce(mockAbortOnError);
    });
    it("calls tryNExtOnError", async () => {
      const config = {
        nodeManager: createNodeManager({
          nodeUrls: ["url1", "url2"],
        }),
        endpointPool: [
          { url: "url1", whenAvailable: 0 },
          { url: "url2", whenAvailable: 0 },
        ],
        blockchainRid: "blockchainRid",
        dappStatusPolling: setStatusPolling({ interval: 100, count: 1 }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
        failoverStrategy: FailoverStrategy.TryNextOnError,
        attemptsPerEndpoint: 3,
        attemptInterval: 100,
        unreachableDuration: 100,
        directoryChainRid: mockStringDirectoryChainRid,
      };
      await requestWithFailoverStrategy(Method.GET, "/path", config);
      sinon.assert.calledOnce(mockTryNextOnError);
    });
    it("calls singleEndpoint", async () => {
      const config = {
        nodeManager: createNodeManager({
          nodeUrls: ["url1", "url2"],
        }),
        endpointPool: [
          { url: "url1", whenAvailable: 0 },
          { url: "url2", whenAvailable: 0 },
        ],
        blockchainRid: "blockchainRid",
        dappStatusPolling: setStatusPolling({ interval: 100, count: 1 }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
        failoverStrategy: FailoverStrategy.SingleEndpoint,
        attemptsPerEndpoint: 3,
        attemptInterval: 100,
        unreachableDuration: 100,
        directoryChainRid: mockStringDirectoryChainRid,
      };
      await requestWithFailoverStrategy(Method.GET, "/path", config);
      sinon.assert.calledOnce(mockSingleEndpoint);
    });
  });
});
