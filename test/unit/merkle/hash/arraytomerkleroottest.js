var assert = require("chai").assert;
var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var merkleHashSummary = require("../../../../src/merkle/proof/merkleproof").merkleHashSummary;
var TreeHelper = require("../merkle/treehelper").TreeHelper;

var helper = new TreeHelper();

const empty32bytesHex = "0101010101010101010101010101010101010101010101010101010101010101";
const expected1ElementArrayMerkleRoot = "080303" + empty32bytesHex;
const expected4ElementArrayMerkleRoot = "0802040404050204060407";
const expected7ElementArrayMerkleRoot = "08020305050506030507050802030509050A040A";

const inner3arrayHash = "0B050707070F0608";
const arr7Part1Hash = "08020305050506030507";
const arr7Part2Hash = "02030509050A040A";
const expected7and3ElementArrayMerkleRoot = arr7Part1Hash + inner3arrayHash + arr7Part2Hash;

describe("array to merkle root test", () => {
  it("test array of 1 root", () => {
    var calculator = new MerkleHashCalculatorDummy();
    var arr1 = Array.of(1);
    var merkleProofRoot = merkleHashSummary(arr1, calculator);
    assert.deepEqual(
      expected1ElementArrayMerkleRoot,
      helper.convertToHex(merkleProofRoot.merkleHash),
    );
  });

  it("test array of 4 root", () => {
    var calculator = new MerkleHashCalculatorDummy();
    var arr4 = Array.of(1, 2, 3, 4);
    var merkleProofRoot = merkleHashSummary(arr4, calculator);
    assert.deepEqual(
      expected4ElementArrayMerkleRoot,
      helper.convertToHex(merkleProofRoot.merkleHash),
    );
  });

  it("test array of 7 root", () => {
    var calculator = new MerkleHashCalculatorDummy();
    var arr7 = Array.of(1, 2, 3, 4, 5, 6, 7);
    var merkleProofRoot = merkleHashSummary(arr7, calculator);
    assert.deepEqual(
      expected7ElementArrayMerkleRoot,
      helper.convertToHex(merkleProofRoot.merkleHash),
    );
  });

  it("test array of 7 with inner length 3 array root", () => {
    var calculator = new MerkleHashCalculatorDummy();
    var arr = Array.of(1, 2, 3, Array.of(1, 9, 3), 5, 6, 7);
    var merkleProofRoot = merkleHashSummary(arr, calculator);
    assert.deepEqual(
      expected7and3ElementArrayMerkleRoot,
      helper.convertToHex(merkleProofRoot.merkleHash),
    );
  });
});
