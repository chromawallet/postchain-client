var assert = require("chai").assert;

var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var merkleHashSummary = require("../../../../src/merkle/proof/merkleproof").merkleHashSummary;
var TreeHelper = require("../merkle/treehelper").TreeHelper;
var helper = new TreeHelper();

const expectedMerkleRoot1 = "09037170670303";
const expectedMerkleRoot4 = "090203056A737976050803057372690505020305786C76696905070305787B730506";
const expectedMerkleRootDictInDict = "09037170670A0305696D6B6C78050C030577697A6972050B";

describe("dict to merkle root test", () => {
  it("test dict of 1 root", () => {
    var dict = { one: 1 };
    var calculator = new MerkleHashCalculatorDummy();
    var merkleProofRoot = merkleHashSummary(dict, calculator);
    assert.deepEqual(expectedMerkleRoot1, helper.convertToHex(merkleProofRoot.merkleHash));
  });

  it("test dict of 4 root", () => {
    var dict = { one: 1, two: 2, three: 3, four: 4 };
    var calculator = new MerkleHashCalculatorDummy();
    var merkleProofRoot = merkleHashSummary(dict, calculator);
    assert.deepEqual(expectedMerkleRoot4, helper.convertToHex(merkleProofRoot.merkleHash));
  });

  it("test dict of dict root", () => {
    var dict = { one: { seven: 7, eight: 8 } };
    var calculator = new MerkleHashCalculatorDummy();
    var merkleProofRoot = merkleHashSummary(dict, calculator);
    assert.deepEqual(expectedMerkleRootDictInDict, helper.convertToHex(merkleProofRoot.merkleHash));
  });
});
