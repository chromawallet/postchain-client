var assert = require("chai").assert;

var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var merkleHashSummary = require("../../../../src/merkle/proof/merkleproof").merkleHashSummary;
var TreeHelper = require("../merkle/treehelper").TreeHelper;
var helper = new TreeHelper();

const expectedMerkleRoot_dict1_array4 = "09037170670903050505060305070508";

describe("dict to merkle root test", () => {
  it("test dict of 1 root", () => {
    var dict = { one: Array.of(1, 2, 3, 4) };
    var calculator = new MerkleHashCalculatorDummy();
    var merkleProofRoot = merkleHashSummary(dict, calculator);
    assert.deepEqual(
      expectedMerkleRoot_dict1_array4,
      helper.convertToHex(merkleProofRoot.merkleHash),
    );
  });
});
