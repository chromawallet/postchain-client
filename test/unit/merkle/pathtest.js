var assert = require("chai").assert;
var expect = require("chai").expect;
var path = require("../../../src/merkle/path");

describe("path test", () => {
  it("build path from empty array", () => {
    var proofPath = path.buildPathFromArray([]);
    assert.equal(proofPath.size(), 1);
    assert.isTrue(proofPath.getCurrentPathElement() instanceof path.PathLeafElement);
    assert.isNull(proofPath.getCurrentPathElement().previous);
  });

  it("paths are equals", () => {
    var path1 = path.buildPathFromArray([0, 7, "myKey"]);
    var path2 = path.buildPathFromArray([0, 7, "myKey"]);
    assert.deepEqual(path1, path2);
  });

  it("paths are not equals", () => {
    var path1 = path.buildPathFromArray([0, 7, "myKey"]);
    var path2 = path.buildPathFromArray([0, 8, "myKey"]);
    assert.notDeepEqual(path1, path2);
  });

  it("test path get tail", () => {
    var org = path.buildPathFromArray([0, 7]);
    assert.equal(org.size(), 3);
    assert.equal(0, org.pathElements[0].index);
    var tail = path.getTailIfFirstElementIsArrayOfThisIndex(0, org);
    assert.equal(2, tail.size());
    assert.equal(7, tail.pathElements[0].index);
    var tail2 = path.getTailIfFirstElementIsArrayOfThisIndex(1, org); // won't find anything at index = 1
    assert.isNull(tail2);
    var tail7 = path.getTailIfFirstElementIsArrayOfThisIndex(7, tail);
    assert.equal(1, tail7.size());
  });
});

describe("pathset test", () => {
  var dummyPrevElem = new path.ArrayPathElement(null, 99);
  it("pathset with 3 paths", () => {
    var path1 = path.buildPathFromArray([0, 7]);
    assert.equal(3, path1.size());

    var path2 = path.buildPathFromArray([3, "myKey", 2]);
    assert.equal(4, path2.size());
    console.log(path2.debugString());

    var path3 = path.buildPathFromArray([3, "myKey", 2, 5]);
    assert.equal(5, path3.size());
    console.log(path3.debugString());

    // ------------ Build the set --------------
    var paths = new path.PathSet([path1, path2, path3]);
    assert.equal(3, paths.paths.length);

    // ------------ Dig down and extract --------------
    var elem = paths.getPathLeafOrElseAnyCurrentPathElement();
    console.log(elem);
    assert.isNotNull(elem);

    // ---- 0. The short path ---
    var pathsWithIndex0 = paths.getTailIfFirstElementIsArrayOfThisIndexFromList(0);
    assert.equal(1, pathsWithIndex0.paths.length);
    var elemIndex0 = pathsWithIndex0.getPathLeafOrElseAnyCurrentPathElement();
    expect(new path.ArrayPathElement(null, 7).equals(elemIndex0)).to.be.true;

    // ---- 1. Dead end ---
    var pathsWithIndex1 = paths.getTailIfFirstElementIsArrayOfThisIndexFromList(1);
    assert.equal(0, pathsWithIndex1.paths.length);
    var elemIndex1 = pathsWithIndex1.getPathLeafOrElseAnyCurrentPathElement();
    assert.equal(null, elemIndex1);

    // ---- 3. The long path ---
    var pathsWithIndex3 = paths.getTailIfFirstElementIsArrayOfThisIndexFromList(3);
    assert.equal(2, pathsWithIndex3.paths.length);
    var elemIndex3 = pathsWithIndex3.getPathLeafOrElseAnyCurrentPathElement();
    expect(new path.DictPathElement(null, "myKey").equals(elemIndex3)).to.be.true;

    var pathWithIndexMyKey = pathsWithIndex3.getTailIfFirstElementIsDictOfThisKeyFromList("myKey");
    assert.equal(2, pathWithIndexMyKey.paths.length);
    var elemIndex3myKey = pathWithIndexMyKey.getPathLeafOrElseAnyCurrentPathElement();
    expect(new path.ArrayPathElement(null, 2).equals(elemIndex3myKey)).to.be.true;

    // Look for First leaf
    var myKeyPathsWithIndex2 =
      pathWithIndexMyKey.getTailIfFirstElementIsArrayOfThisIndexFromList(2);
    assert.equal(2, myKeyPathsWithIndex2.paths.length);
    var elemIndex3myKeyLeaf = myKeyPathsWithIndex2.getPathLeafOrElseAnyCurrentPathElement();
    // Even though there are two possible elements, one is a leaf and that one must be returned.
    expect(new path.PathLeafElement(dummyPrevElem).equals(elemIndex3myKeyLeaf)).to.be.true;

    // Look for Second leaf
    var myKeyPathsWithIndex2And5 =
      myKeyPathsWithIndex2.getTailIfFirstElementIsArrayOfThisIndexFromList(5);
    assert.equal(1, myKeyPathsWithIndex2And5.paths.length);
    var elemIndex3myKey2Leaf = myKeyPathsWithIndex2And5.getPathLeafOrElseAnyCurrentPathElement();
    // One leaf left, should be a trivial check
    expect(new path.PathLeafElement(dummyPrevElem).equals(elemIndex3myKey2Leaf)).to.be.true;
  });
});
