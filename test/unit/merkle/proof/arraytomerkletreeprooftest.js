var assert = require("chai").assert;
var PathSet = require("../../../../src/merkle/path").PathSet;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var generateProof = require("../../../../src/merkle/proof/merkleproof").generateProof;
var merkleTreeHash = require("../../../../src/merkle/proof/merkleproof").merkleTreeHash;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var printableFactory = new PrintableTreeFactory();
var TreeHelper = require("../merkle/treehelper").TreeHelper;
var helper = new TreeHelper();

const empty32bytesHex = "0101010101010101010101010101010101010101010101010101010101010101";
const expected1ElementArrayMerkleRoot = "080303" + empty32bytesHex;
const expected4ElementArrayMerkleRoot = "0802040404050204060407";
const expected7ElementArrayMerkleRoot = "08020305050506030507050802030509050A040A";

const inner3arrayHash = "0B050707070F0608";
const arr7Part1Hash = "08020305050506030507";
const arr7Part2Hash = "02030509050A040A";
const expected7and3ElementArrayMerkleRoot = arr7Part1Hash + inner3arrayHash + arr7Part2Hash;

describe("array to merkle proof tree test", () => {
  it("test array of 1 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(0);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1);

    var expectedTree =
      " +   \n" + "/ \\ \n" + "*1 0000000000000000000000000000000000000000000000000000000000000000";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected1ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 4 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(0);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1, 2, 3, 4);

    var expectedTree =
      "   +       \n" +
      "  / \\   \n" +
      " /   \\  \n" +
      " +   0103050306   \n" +
      "/ \\     \n" +
      "*1 0203 - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected4ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 7 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(3);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1, 2, 3, 4, 5, 6, 7);

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       0102040804090309       \n" +
      "  / \\           \n" +
      " /   \\          \n" +
      " 0103030304   +   .   .   \n" +
      "    / \\         \n" +
      "- - 0204 *4 - - - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected7ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 7 with double proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var arr1 = Array.of(3);
    var arr2 = Array.of(6);
    var path1 = buildPathFromArray(arr1);
    var path2 = buildPathFromArray(arr2);
    var pathset = new PathSet(Array.of(path1, path2));
    var arr = Array.of(1, 2, 3, 4, 5, 6, 7);

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       +       \n" +
      "  / \\     / \\   \n" +
      " /   \\   /   \\  \n" +
      " 0103030304   +   0103070308   *7   \n" +
      "    / \\         \n" +
      "- - 0204 *4 - - - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected7ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 7 with inner array of 3 path to 9 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(3, 1);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1, 2, 3, Array.of(1, 9, 3), 5, 6, 7);

    var expectedTree =
      "                               +                                                               \n" +
      "                              / \\                               \n" +
      "                             /   \\                              \n" +
      "                            /     \\                             \n" +
      "                           /       \\                            \n" +
      "                          /         \\                           \n" +
      "                         /           \\                          \n" +
      "                        /             \\                         \n" +
      "                       /               \\                        \n" +
      "                      /                 \\                       \n" +
      "                     /                   \\                      \n" +
      "                    /                     \\                     \n" +
      "                   /                       \\                    \n" +
      "                  /                         \\                   \n" +
      "                 /                           \\                  \n" +
      "                /                             \\                 \n" +
      "               /                               \\                \n" +
      "               +                               0102040804090309                               \n" +
      "              / \\                                               \n" +
      "             /   \\                                              \n" +
      "            /     \\                                             \n" +
      "           /       \\                                            \n" +
      "          /         \\                                           \n" +
      "         /           \\                                          \n" +
      "        /             \\                                         \n" +
      "       /               \\                                        \n" +
      "       0103030304               +               .               .               \n" +
      "                      / \\                                       \n" +
      "                     /   \\                                      \n" +
      "                    /     \\                                     \n" +
      "                   /       \\                                    \n" +
      "   .       .       0204       *       .       .       .       .       \n" +
      "                          / \\                                   \n" +
      "                         /   \\                                  \n" +
      " .   .   .   .   .   .   +   0204   .   .   .   .   .   .   .   .   \n" +
      "                        / \\                                     \n" +
      "- - - - - - - - - - - - 0202 *9 - - - - - - - - - - - - - - - - - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected7and3ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 7 with inner array of 3 path to 3 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(2);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1, 2, 3, Array.of(1, 9, 3), 5, 6, 7);

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       0102040804090309       \n" +
      "  / \\           \n" +
      " /   \\          \n" +
      " 0103030304   +   .   .   \n" +
      "    / \\         \n" +
      "- - *3 08020404040C0305 - - - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected7and3ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });

  it("test array of 7 with inner array of 3 path to sub array proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of(3);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var arr = Array.of(1, 2, 3, Array.of(1, 9, 3), 5, 6, 7);

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       0102040804090309       \n" +
      "  / \\           \n" +
      " /   \\          \n" +
      " 0103030304   +   .   .   \n" +
      "    / \\         \n" +
      "- - 0204 *1,9,3 - - - - ";

    var merkleProofTree = generateProof(arr, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expected7and3ElementArrayMerkleRoot, helper.convertToHex(merkleProofRoot));
  });
});
