var assert = require("chai").assert;
var PathSet = require("../../../../src/merkle/path").PathSet;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var generateProof = require("../../../../src/merkle/proof/merkleproof").generateProof;
var merkleTreeHash = require("../../../../src/merkle/proof/merkleproof").merkleTreeHash;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var printableFactory = new PrintableTreeFactory();
var TreeHelper = require("../merkle/treehelper").TreeHelper;
var helper = new TreeHelper();

const expectedMerkleRoot1 = "09037170670303";
const expectedMerkleRoot4 = "090203056A737976050803057372690505020305786C76696905070305787B730506";
const expectedMerkleRootDictInDict = "09037170670A0305696D6B6C78050C030577697A6972050B";

describe("dict to merkle proof tree test", () => {
  it("test dict 1 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("one");
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: 1 };

    var expectedTree = " +   \n" + "/ \\ \n" + "02706F66 *1 ";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRoot1, helper.convertToHex(merkleProofRoot));
  });

  it("test dict 4 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("four");
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: 1, two: 2, three: 3, four: 4 };

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       010204776B75686804060204777A720405       \n" +
      "  / \\           \n" +
      " /   \\          \n" +
      " +   01037170670303   .   .   \n" +
      "/ \\             \n" +
      "0267707673 *4 - - - - - - ";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRoot4, helper.convertToHex(merkleProofRoot));
  });

  it("test dict of dict proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("one", "seven");
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: { seven: 7, eight: 8 } };

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   02706F66       *       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   0103676B696A76030A   +   \n" +
      "            / \\ \n" +
      "- - - - - - 02746677666F *7 ";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRootDictInDict, helper.convertToHex(merkleProofRoot));
  });

  it("test dict of dict where path to sub dict proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("one");
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: { seven: 7, eight: 8 } };

    var expectedTree = " +   \n" + "/ \\ \n" + "02706F66 *[object Object] ";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRootDictInDict, helper.convertToHex(merkleProofRoot));
  });
});
