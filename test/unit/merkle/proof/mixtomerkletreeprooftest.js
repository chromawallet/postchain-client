var assert = require("chai").assert;
var PathSet = require("../../../../src/merkle/path").PathSet;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var generateProof = require("../../../../src/merkle/proof/merkleproof").generateProof;
var merkleTreeHash = require("../../../../src/merkle/proof/merkleproof").merkleTreeHash;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var printableFactory = new PrintableTreeFactory();
var TreeHelper = require("../merkle/treehelper").TreeHelper;
var helper = new TreeHelper();

const expectedMerkleRoot_dict1_array4 = "09037170670903050505060305070508";

describe("dict to merkle proof tree test", () => {
  it("test dict with array where path to leaf 4 proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("one", 3);
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: Array.of(1, 2, 3, 4) };

    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   02706F66       *       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   0103030304   +   \n" +
      "            / \\ \n" +
      "- - - - - - 0204 *4 ";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRoot_dict1_array4, helper.convertToHex(merkleProofRoot));
  });

  it("test dict with array where path to sub array proof", () => {
    var calculator = new MerkleHashCalculatorDummy();

    var paths = Array.of("one");
    var path = buildPathFromArray(paths);
    var pathset = new PathSet(Array.of(path));
    var dict = { one: Array.of(1, 2, 3, 4) };

    var expectedTree = " +   \n" + "/ \\ \n" + "02706F66 *1,2,3,4";

    var merkleProofTree = generateProof(dict, pathset, calculator);
    // Print the result tree
    var printer = new TreePrinter();
    var pbt = printableFactory.buildPrintableTreeFromProofTree(merkleProofTree);
    var resultPrintout = printer.printNode(pbt);
    assert.deepEqual(expectedTree.trim(), resultPrintout.trim());

    // Make sure the merkle root stays the same as without proof
    var merkleProofRoot = merkleTreeHash(merkleProofTree, calculator);
    assert.deepEqual(expectedMerkleRoot_dict1_array4, helper.convertToHex(merkleProofRoot));
  });
});
