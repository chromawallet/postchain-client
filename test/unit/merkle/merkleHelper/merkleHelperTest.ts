import { assert } from "chai";
import { Buffer } from "buffer";
import { hash256 } from "../../../../src/encryption/encryption";
import { calculateRoot, merklePath, validateMerklePath } from "../../../../src/merkle/merkleHelper";
import { Path } from "../../../../src/merkle/types";

function merkleRoot(stringList: string[]) {
  return calculateRoot(hashList(stringList));
}

function stringToHash(string: string): Buffer {
  return hash256(Buffer.from(string));
}

function hashList(stringList: string[]): Buffer[] {
  return stringList.map(stringToHash);
}

function checkDifferent(list1: string[], list2: string[]) {
  const root1 = merkleRoot(list1);
  const root2 = merkleRoot(list2);
  assert.ok(!root1.equals(root2));
}

function testPath(stringList: string[], stringToProve: string) {
  const path = merklePath(hashList(stringList), stringToHash(stringToProve));
  assert.ok(
    validateMerklePath(path, stringToHash(stringToProve), merkleRoot(stringList)),
    `validation failed for txs ${stringList} and tx ${stringToProve}`,
  );
}

const a = ["a"];
const aa = ["a", "a"];
const abcde = ["a", "b", "c", "d", "e"];
const abcdee = ["a", "b", "c", "d", "e", "e"];
const abcdef = ["a", "b", "c", "d", "e", "f"];
const abcdefef = ["a", "b", "c", "d", "e", "f", "e", "f"];

describe("MerkleHelper", function () {
  it("merkle root of empty list is 32 0s", () => {
    assert.ok(merkleRoot([]).equals(Buffer.alloc(32)));
  });

  it("merkle root of single element", () => {
    assert.ok(merkleRoot(a).equals(stringToHash("a")));
  });

  it("merkle root collisions", function () {
    checkDifferent(a, aa);
    checkDifferent(abcde, abcdee);
    checkDifferent(abcdef, abcdefef);
  });

  it("merkle proof throws on empty list", () => {
    assert.throws(() => {
      merklePath([], stringToHash("a"));
    }, Error);
  });

  it("merkle proof throws if tx not exist in list", () => {
    assert.throws(() => {
      merklePath(hashList(abcdee), stringToHash("f"));
    }, Error);
  });

  it("merkle proof structure", () => {
    const path = merklePath(hashList(abcde), stringToHash("e"));
    assert.equal(path.length, 3);
    assert.equal(path[0].side, 1); // right
    assert.equal(path[1].side, 1); // right
    assert.equal(path[2].side, 0); // left
    assert.ok(validateMerklePath(path, stringToHash("e"), merkleRoot(abcde)));
    assert.ok(!validateMerklePath(path, stringToHash("c"), merkleRoot(abcde)));
  });

  it("merkle proof structure fails without hashes", () => {
    assert.throws(() => merklePath([], stringToHash("e")), Error);
  });

  this.timeout(400000);
  it("merkle path to size 20", () => {
    const txs: string[] = [];
    for (let i = 1; i < 20; i++) {
      txs.push(`${i}`);
      for (let j = 1; j <= i; j++) {
        testPath(txs, `${j}`);
      }
    }
  });

  it("dummy", () => {
    testPath(["1", "2", "3"], "1");
  });

  it("merkle path negative test wrong side", () => {
    const path: Path = merklePath(hashList(abcde), stringToHash("d"));
    path[1].side = 1; // Flip side of one path component
    assert.ok(!validateMerklePath(path, stringToHash("d"), merkleRoot(abcde)));
  });

  it("merkle path negative test wrong hash", () => {
    const path: Path = merklePath(hashList(abcde), stringToHash("d"));
    path[2].hash = stringToHash("invalid"); // wrong hash
    assert.ok(!validateMerklePath(path, stringToHash("d"), merkleRoot(abcde)));
  });
});
