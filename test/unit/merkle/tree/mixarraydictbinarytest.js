var assert = require("chai").assert;

var BinaryTreeFactory = require("../../../../src/merkle/binarytreefactory").BinaryTreeFactory;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var PathSet = require("../../../../src/merkle/path").PathSet;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var factory = new BinaryTreeFactory();
var printableFactory = new PrintableTreeFactory();

function buildTreeOfDict1WithSubArray4() {
  return buildTreeOfDict1WithSubArray4WithPath(null);
}

function buildTreeOfDict1WithSubArray4WithPath(path) {
  var dict = { one: Array.of(1, 2, 3, 4) };

  var fullBinaryTree;
  if (path != null) {
    fullBinaryTree = factory.buildWithPath(dict, new PathSet(Array.of(path)));
  } else {
    fullBinaryTree = factory.build(dict);
  }

  var printer = new TreePrinter();
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var treePrintout = printer.printNode(printableBinaryTree);
  return treePrintout;
}

describe("test printable mix binary tree", () => {
  it("test int dict length 1 with inner array length 4", () => {
    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   one       +       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   +   +   \n" +
      "        / \\ / \\ \n" +
      "- - - - 1 2 3 4 ";

    var treePrintout = buildTreeOfDict1WithSubArray4();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test int dict length 1 with inner array length 4 with path", () => {
    var paths = Array.of("one", 3);
    var expectedTree =
      "       *               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   one       *       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   +   +   \n" +
      "        / \\ / \\ \n" +
      "- - - - 1 2 3 *4 ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOfDict1WithSubArray4WithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });
});
