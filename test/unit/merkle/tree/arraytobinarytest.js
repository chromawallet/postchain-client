var assert = require("chai").assert;

var BinaryTreeFactory = require("../../../../src/merkle/binarytreefactory").BinaryTreeFactory;
var Path = require("../../../../src/merkle/path").Path;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var PathSet = require("../../../../src/merkle/path").PathSet;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var factory = new BinaryTreeFactory();
var printableFactory = new PrintableTreeFactory();

function buildTreeOf1() {
  return buildTreeOf1WithPath(null);
}

/**
 *
 * @param {Path} path
 */
function buildTreeOf1WithPath(path) {
  var arr = [1];
  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(arr);
  } else {
    fullBinaryTree = factory.buildWithPath(arr, new PathSet(Array.of(path)));
  }
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var printer = new TreePrinter();
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

function buildTreeOf4() {
  return buildTreeOf4WithPath(null);
}

function buildTreeOf4WithPath(path) {
  var arr = [1, 2, 3, 4];
  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(arr);
  } else {
    fullBinaryTree = factory.buildWithPath(arr, new PathSet([path]));
  }
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var printer = new TreePrinter();
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

function buildTreeOf7() {
  return buildTreeOf7WithPath(null);
}

function buildTreeOf7WithPath(path) {
  var arr = [1, 2, 3, 4, 5, 6, 7];
  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(arr);
  } else {
    fullBinaryTree = factory.buildWithPath(arr, new PathSet([path]));
  }
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var printer = new TreePrinter();
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

function buildTreeOf9() {
  return buildTreeOf9WithPath(null);
}

function buildTreeOf9WithPath(path) {
  var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(arr);
  } else {
    fullBinaryTree = factory.buildWithPath(arr, new PathSet([path]));
  }
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var printer = new TreePrinter();
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

function buildTreeOf13() {
  var arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3];
  var fullBinaryTree = factory.build(arr);
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var printer = new TreePrinter();
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

function buildTreeOf7SubTree() {
  return buildTreeOf7SubTreeWithPath(null);
}

function buildTreeOf7SubTreeWithPath(path) {
  var arr = Array.of(1, 2, 3, Array.of(1, 9, 3), 5, 6, 7);

  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(arr);
  } else {
    fullBinaryTree = factory.buildWithPath(arr, new PathSet([path]));
  }
  var printer = new TreePrinter();
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var treePrintout = printer.printNode(printableBinaryTree);

  return treePrintout;
}

describe("test printable array binary tree", () => {
  it("number array length 1", () => {
    var expectedTree = " +   \n" + "/ \\ \n" + "1 - ";

    var treePrintout = buildTreeOf1();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 1 with path", () => {
    var paths = Array.of(0);
    var expectedTreeWithPath = " *   \n" + "/ \\ \n" + "*1 - ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOf1WithPath(path);
    assert.deepEqual(expectedTreeWithPath.trim(), treePrintout.trim());
  });

  it("number array length 4", () => {
    var expectedTree =
      "   +       \n" +
      "  / \\   \n" +
      " /   \\  \n" +
      " +   +   \n" +
      "/ \\ / \\ \n" +
      "1 2 3 4 \n";

    var treePrintout = buildTreeOf4();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 4 with path", () => {
    var paths = Array.of(3);
    var expectedTree =
      "   *       \n" +
      "  / \\   \n" +
      " /   \\  \n" +
      " +   +   \n" +
      "/ \\ / \\ \n" +
      "1 2 3 *4 ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOf4WithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 7", () => {
    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       +       \n" +
      "  / \\     / \\   \n" +
      " /   \\   /   \\  \n" +
      " +   +   +   7   \n" +
      "/ \\ / \\ / \\     \n" +
      "1 2 3 4 5 6 - - ";

    var treePrintout = buildTreeOf7();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 7 with path", () => {
    var paths = Array.of(6);
    var expectedTree =
      "       *               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       +       \n" +
      "  / \\     / \\   \n" +
      " /   \\   /   \\  \n" +
      " +   +   +   *7   \n" +
      "/ \\ / \\ / \\     \n" +
      "1 2 3 4 5 6 - - ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOf7WithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 9", () => {
    var expectedTree =
      "               +                               \n" +
      "              / \\               \n" +
      "             /   \\              \n" +
      "            /     \\             \n" +
      "           /       \\            \n" +
      "          /         \\           \n" +
      "         /           \\          \n" +
      "        /             \\         \n" +
      "       /               \\        \n" +
      "       +               9               \n" +
      "      / \\                       \n" +
      "     /   \\                      \n" +
      "    /     \\                     \n" +
      "   /       \\                    \n" +
      "   +       +       .       .       \n" +
      "  / \\     / \\                   \n" +
      " /   \\   /   \\                  \n" +
      " +   +   +   +   .   .   .   .   \n" +
      "/ \\ / \\ / \\ / \\                 \n" +
      "1 2 3 4 5 6 7 8 - - - - - - - - ";

    var treePrintout = buildTreeOf9();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 13", () => {
    var expectedTree =
      "               +                               \n" +
      "              / \\               \n" +
      "             /   \\              \n" +
      "            /     \\             \n" +
      "           /       \\            \n" +
      "          /         \\           \n" +
      "         /           \\          \n" +
      "        /             \\         \n" +
      "       /               \\        \n" +
      "       +               +               \n" +
      "      / \\             / \\       \n" +
      "     /   \\           /   \\      \n" +
      "    /     \\         /     \\     \n" +
      "   /       \\       /       \\    \n" +
      "   +       +       +       3       \n" +
      "  / \\     / \\     / \\           \n" +
      " /   \\   /   \\   /   \\          \n" +
      " +   +   +   +   +   +   .   .   \n" +
      "/ \\ / \\ / \\ / \\ / \\ / \\         \n" +
      "1 2 3 4 5 6 7 8 9 0 1 2 - - - - ";

    var treePrintout = buildTreeOf13();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 7 sub tree", () => {
    var expectedTree =
      "                               +                                                               \n" +
      "                              / \\                               \n" +
      "                             /   \\                              \n" +
      "                            /     \\                             \n" +
      "                           /       \\                            \n" +
      "                          /         \\                           \n" +
      "                         /           \\                          \n" +
      "                        /             \\                         \n" +
      "                       /               \\                        \n" +
      "                      /                 \\                       \n" +
      "                     /                   \\                      \n" +
      "                    /                     \\                     \n" +
      "                   /                       \\                    \n" +
      "                  /                         \\                   \n" +
      "                 /                           \\                  \n" +
      "                /                             \\                 \n" +
      "               /                               \\                \n" +
      "               +                               +                               \n" +
      "              / \\                             / \\               \n" +
      "             /   \\                           /   \\              \n" +
      "            /     \\                         /     \\             \n" +
      "           /       \\                       /       \\            \n" +
      "          /         \\                     /         \\           \n" +
      "         /           \\                   /           \\          \n" +
      "        /             \\                 /             \\         \n" +
      "       /               \\               /               \\        \n" +
      "       +               +               +               7               \n" +
      "      / \\             / \\             / \\                       \n" +
      "     /   \\           /   \\           /   \\                      \n" +
      "    /     \\         /     \\         /     \\                     \n" +
      "   /       \\       /       \\       /       \\                    \n" +
      "   1       2       3       +       5       6       .       .       \n" +
      "                          / \\                                   \n" +
      "                         /   \\                                  \n" +
      " .   .   .   .   .   .   +   3   .   .   .   .   .   .   .   .   \n" +
      "                        / \\                                     \n" +
      "- - - - - - - - - - - - 1 9 - - - - - - - - - - - - - - - - - - ";

    var treePrintout = buildTreeOf7SubTree();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("number array length 7 sub tree with path", () => {
    var paths = Array.of(3, 0);
    var expectedTree =
      "                               *                                                               \n" +
      "                              / \\                               \n" +
      "                             /   \\                              \n" +
      "                            /     \\                             \n" +
      "                           /       \\                            \n" +
      "                          /         \\                           \n" +
      "                         /           \\                          \n" +
      "                        /             \\                         \n" +
      "                       /               \\                        \n" +
      "                      /                 \\                       \n" +
      "                     /                   \\                      \n" +
      "                    /                     \\                     \n" +
      "                   /                       \\                    \n" +
      "                  /                         \\                   \n" +
      "                 /                           \\                  \n" +
      "                /                             \\                 \n" +
      "               /                               \\                \n" +
      "               +                               +                               \n" +
      "              / \\                             / \\               \n" +
      "             /   \\                           /   \\              \n" +
      "            /     \\                         /     \\             \n" +
      "           /       \\                       /       \\            \n" +
      "          /         \\                     /         \\           \n" +
      "         /           \\                   /           \\          \n" +
      "        /             \\                 /             \\         \n" +
      "       /               \\               /               \\        \n" +
      "       +               +               +               7               \n" +
      "      / \\             / \\             / \\                       \n" +
      "     /   \\           /   \\           /   \\                      \n" +
      "    /     \\         /     \\         /     \\                     \n" +
      "   /       \\       /       \\       /       \\                    \n" +
      "   1       2       3       *       5       6       .       .       \n" +
      "                          / \\                                   \n" +
      "                         /   \\                                  \n" +
      " .   .   .   .   .   .   +   3   .   .   .   .   .   .   .   .   \n" +
      "                        / \\                                     \n" +
      "- - - - - - - - - - - - *1 9 - - - - - - - - - - - - - - - - - - ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOf7SubTreeWithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });
});
