var assert = require("chai").assert;

var BinaryTreeFactory = require("../../../../src/merkle/binarytreefactory").BinaryTreeFactory;
var Path = require("../../../../src/merkle/path").Path;
var buildPathFromArray = require("../../../../src/merkle/path").buildPathFromArray;
var PathSet = require("../../../../src/merkle/path").PathSet;
var TreePrinter = require("../merkle/treeprinter").TreePrinter;
var PrintableTreeFactory = require("../merkle/treeprinter").PrintableTreeFactory;
var factory = new BinaryTreeFactory();
var printableFactory = new PrintableTreeFactory();

function buildThreeOf1_fromDict() {
  return buildThreeOf1_fromDictWithPath(null);
}

/**
 *
 * @param {Path} path
 */
function buildThreeOf1_fromDictWithPath(path) {
  var dict = { one: 1 };

  var fullBinaryTree;
  if (path == null) {
    fullBinaryTree = factory.build(dict);
  } else {
    fullBinaryTree = factory.buildWithPath(dict, new PathSet([path]));
  }

  var printer = new TreePrinter();
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var treePrintout = printer.printNode(printableBinaryTree);
  return treePrintout;
}

function buildThreeOf4_fromDict() {
  return buildThreeOf4_fromDictWithPath(null);
}

function buildThreeOf4_fromDictWithPath(path) {
  var dict = { one: 1, two: 2, three: 3, four: 4 };
  var fullBinaryTree;
  if (path != null) {
    fullBinaryTree = factory.buildWithPath(dict, new PathSet(Array.of(path)));
  } else {
    fullBinaryTree = factory.build(dict);
  }

  var printer = new TreePrinter();
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var treePrintout = printer.printNode(printableBinaryTree);
  return treePrintout;
}

function buildTreeOf1WithSubTree() {
  return buildTreeOf1WithSubTreeWithPath(null);
}

function buildTreeOf1WithSubTreeWithPath(path) {
  var dict = { one: { seven: 7, eight: 8 } };

  var fullBinaryTree;
  if (path != null) {
    fullBinaryTree = factory.buildWithPath(dict, new PathSet(Array.of(path)));
  } else {
    fullBinaryTree = factory.build(dict);
  }

  var printer = new TreePrinter();
  var printableBinaryTree = printableFactory.buildPrintableTreeFromClfbTree(fullBinaryTree);
  var treePrintout = printer.printNode(printableBinaryTree);
  return treePrintout;
}

describe("test printable dict binary tree", () => {
  it("test number dictionary length 1", () => {
    var expectedTree = " +   \n" + "/ \\ \n" + "one 1";

    var treePrintout = buildThreeOf1_fromDict();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test number dictionary length 1 with path", () => {
    var paths = Array.of("one");
    var expectedTree = " *   \n" + "/ \\ \n" + "one *1 ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildThreeOf1_fromDictWithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test number dictionary length 4", () => {
    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       +       \n" +
      "  / \\     / \\   \n" +
      " /   \\   /   \\  \n" +
      " +   +   +   +   \n" +
      "/ \\ / \\ / \\ / \\ \n" +
      "four 4 one 1 three 3 two 2 ";

    var treePrintout = buildThreeOf4_fromDict();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test number dictionary length 4 with path", () => {
    var paths = Array.of("one");
    var expectedTree =
      "       *               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   +       +       \n" +
      "  / \\     / \\   \n" +
      " /   \\   /   \\  \n" +
      " +   +   +   +   \n" +
      "/ \\ / \\ / \\ / \\ \n" +
      "four 4 one *1 three 3 two 2 ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildThreeOf4_fromDictWithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test number dictionary length 1 sub tree length 2", () => {
    var expectedTree =
      "       +               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   one       +       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   +   +   \n" +
      "        / \\ / \\ \n" +
      "- - - - eight 8 seven 7 ";

    var treePrintout = buildTreeOf1WithSubTree();
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });

  it("test number dictionary length 1 sub tree length 2 with path", () => {
    var paths = Array.of("one", "seven");
    var expectedTree =
      "       *               \n" +
      "      / \\       \n" +
      "     /   \\      \n" +
      "    /     \\     \n" +
      "   /       \\    \n" +
      "   one       *       \n" +
      "          / \\   \n" +
      "         /   \\  \n" +
      " .   .   +   +   \n" +
      "        / \\ / \\ \n" +
      "- - - - eight 8 seven *7 ";

    var path = buildPathFromArray(paths);
    var treePrintout = buildTreeOf1WithSubTreeWithPath(path);
    assert.deepEqual(expectedTree.trim(), treePrintout.trim());
  });
});
