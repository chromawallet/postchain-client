const { Buffer } = require("buffer");
var MerkleHashCalculator = require("../../../src/merkle/merklehashcalculator").MerkleHashCalculator;
/**
 * We create a dummy serializer so that we can use for tests
 * It truncates an Int to one byte
 */
function dummySerializerFun(value) {
  if (typeof value === "number" && Number.isInteger(value)) {
    if (value > 127 && value > -1) {
      throw new Error("Test integers should be positive and should not be bigger than 127: $i");
    } else {
      var buf = Buffer.alloc(1);
      buf.writeInt8(value);
      return buf;
    }
  } else if (typeof value === "string") {
    var buf = Buffer.from(value, "utf-8"); // TODO: Do we need to think about charset?
    console.log("Dummy serializer:  string: " + value + " becomes bytes: "); // TODO
    return buf;
  } else if (Buffer.isBuffer(value)) {
    return value;
  } else {
    throw new Error("We don't use any other than Integers, Strings and ByteArray for these tests");
  }
}

/**
 * A simple dummy hashing function that's easy to test
 * It only adds 1 to all bytes in the byte array.
 *
 * @param {Buffer} buffer
 */
function dummyAddOneHashFun(buffer) {
  var retArr = Buffer.alloc(buffer.length);
  var pos = 0;
  for (var i = 0; i < buffer.length; i++) {
    retArr[pos] = buffer[i] + 1;
    pos++;
  }
  return retArr;
}

/**
 * The "dummy" version is a real calculator, but it uses simplified versions of
 * serializations and hashing
 */
function MerkleHashCalculatorDummy() {}
MerkleHashCalculatorDummy.prototype = Object.create(MerkleHashCalculator.prototype);
MerkleHashCalculatorDummy.prototype.constructor = MerkleHashCalculatorDummy;

MerkleHashCalculatorDummy.prototype.calculateLeafHash = function (value) {
  return MerkleHashCalculator.prototype.calculateHashOfValueInternal.call(
    this,
    value,
    dummySerializerFun,
    dummyAddOneHashFun,
  );
};

MerkleHashCalculatorDummy.prototype.calculateNodeHash = function (prefix, hashLeft, hashRight) {
  return MerkleHashCalculator.prototype.calculateNodeHashInternal.call(
    this,
    prefix,
    hashLeft,
    hashRight,
    dummyAddOneHashFun,
  );
};

MerkleHashCalculatorDummy.prototype.isContainerProofValueLeaf = function (value) {
  return MerkleHashCalculator.prototype.isContainerProofValueLeaf.call(this, value);
};

module.exports = { MerkleHashCalculatorDummy };
