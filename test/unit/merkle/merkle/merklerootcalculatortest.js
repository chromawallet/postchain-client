var assert = require("chai").assert;
var merkleHash = require("../../../../src/merkle/proof/merkleproof").merkleHash;
var TreeHelper = require("./treehelper").TreeHelper;
var MerkleHashCalculatorDummy = require("../merklehashcalculatordummy").MerkleHashCalculatorDummy;
var calculator = new MerkleHashCalculatorDummy();
var helper = new TreeHelper();

var empty32bytesAsHex = "0101010101010101010101010101010101010101010101010101010101010101";
var expectedMerkleRootOf1 = "080303" + empty32bytesAsHex;
var expectedMerkleRootOf4 = "0802040404050204060407";

describe("merkle root calculator test", () => {
  it("test string array length 1 merkle root", () => {
    var arr = Array.of(helper.convertToByteArray("01"));
    var merkleRoot = merkleHash(arr, calculator);
    assert.deepEqual(expectedMerkleRootOf1, helper.convertToHex(merkleRoot));
  });

  it("test string array length 4 merkle root", () => {
    var arr = Array.of(
      helper.convertToByteArray("01"),
      helper.convertToByteArray("02"),
      helper.convertToByteArray("03"),
      helper.convertToByteArray("04"),
    );
    var merkleRoot = merkleHash(arr, calculator);
    assert.deepEqual(expectedMerkleRootOf4, helper.convertToHex(merkleRoot));
  });
});
