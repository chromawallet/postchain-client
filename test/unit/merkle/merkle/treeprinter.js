var StringBuilder = require("./stringbuilder").StringBuilder;
var BinaryTreeElement = require("../../../../src/merkle/binarytree").BinaryTreeElement;
var BinaryTree = require("../../../../src/merkle/binarytree").BinaryTree;
var EmptyLeaf = require("../../../../src/merkle/binarytree").EmptyLeaf;
var Leaf = require("../../../../src/merkle/binarytree").Leaf;
var Node = require("../../../../src/merkle/binarytree").Node;
var ProofNode = require("../../../../src/merkle/proof/merkleprooftree").ProofNode;
var ProofValueLeaf = require("../../../../src/merkle/proof/merkleprooftree").ProofValueLeaf;
var ProofHashedLeaf = require("../../../../src/merkle/proof/merkleprooftree").ProofHashedLeaf;
var ProofNodeArrayHead = require("../../../../src/merkle/proof/merkleprooftree").ProofNodeArrayHead;
var ProofNodeDictHead = require("../../../../src/merkle/proof/merkleprooftree").ProofNodeDictHead;
var TreeHelper = require("./treehelper").TreeHelper;
var helper = new TreeHelper();
const { Buffer } = require("buffer");
/**
 *
 */
function PTreeElement() {}

/**
 *
 * @param {PTreeElement} left
 * @param {PTreeElement} right
 * @param {boolean} pathLeaf
 * @param {boolean} isPath
 */
function PNode(left, right, pathLeaf, isPath) {
  this.left = left;
  this.right = right;
  this.pathLeaf = pathLeaf;
  this.isPath = isPath;
}

PNode.prototype = Object.create(PTreeElement.prototype);
PNode.prototype.constructor = PNode;

/**
 *
 * @param {string} content
 * @param {PTreeElement} left
 * @param {PTreeElement} right
 * @param {boolean} pathLeaf
 * @param {boolean} isPath
 */
function PContentNode(content, left, right, pathLeaf, isPath) {
  this.content = content;
  this.left = left;
  this.right = right;
  this.pathLeaf = pathLeaf;
  this.isPath = isPath;
}
PContentNode.prototype = Object.create(PTreeElement.prototype);
PContentNode.prototype.constructor = PContentNode;

/**
 *
 * @param {string} content
 * @param {boolean} pathLeaf
 */
function PLeaf(content, pathLeaf) {
  this.content = content;
  this.pathLeaf = pathLeaf;
}
PLeaf.prototype = Object.create(PTreeElement.prototype);
PLeaf.prototype.constructor = PLeaf;

/**
 *
 */
function PEmptyElement() {}
PEmptyElement.prototype = Object.create(PTreeElement.prototype);
PEmptyElement.prototype.constructor = PEmptyElement;

/**
 *
 */
function PEmptyLeaf() {}
PEmptyLeaf.prototype = Object.create(PEmptyElement.prototype);
PEmptyLeaf.prototype.constructor = PEmptyLeaf;

/**
 *
 * @param {PEmptyElement} left
 * @param {PEmptyElement} right
 */
function PEmptyNode(left, right) {
  this.left = left;
  this.right = right;
}
PEmptyNode.prototype = Object.create(PEmptyNode.prototype);
PEmptyNode.prototype.constructor = PEmptyNode;

/**
 *
 * @param {PTreeElement} root
 */
function PrintableBinaryTree(root) {
  this.root = root;
}

var sb = new StringBuilder();
function TreePrinter() {}

/**
 * @param {PrintableBinaryTree} treePrintable
 */
TreePrinter.prototype.printNode = function (treePrintable) {
  sb = new StringBuilder();
  var root = treePrintable.root;
  var maxLevel = this.maxLevel(root);
  var nodes = new Array(root);
  this.printNodeInternal(nodes, 1, maxLevel, 0);
  return sb.toString();
};

/**
 * @param {Array} nodes
 * @param {number} level
 * @param {number} maxLevel
 * @param {number} compensateFirstSpaces
 */
TreePrinter.prototype.printNodeInternal = function (nodes, level, maxLevel, compensateFirstSpaces) {
  if (nodes.length == 0) {
    return;
  }

  var floor = maxLevel - level;
  var numberTwo = 2.0;
  var endgeLines = Math.pow(numberTwo, Math.max(floor - 1, 0));
  var firstSpaces = Math.pow(numberTwo, floor) - 1 + compensateFirstSpaces;
  var betweenSpaces = Math.pow(numberTwo, floor + 1) - 1;

  this.printWhitespaces(firstSpaces);

  var compensateForEmptNodes = compensateFirstSpaces;
  var leafCount = 0;

  var newNodes = [];
  for (var n = 0; n < nodes.length; n++) {
    var node = nodes[n];
    if (node instanceof PNode) {
      if (node.pathLeaf) {
        sb.append("#"); // This is a proof leaf
      } else if (node.isPath) {
        sb.append("*"); // This is a node part of a path
      } else {
        sb.append("+"); // Non path node
      }
      newNodes.push(node.left);
      newNodes.push(node.right);
      compensateForEmptNodes += leafCount * (betweenSpaces + 1);
    } else if (node instanceof PContentNode) {
      if (node.pathLeaf) {
        sb.append("*" + node.content);
      } else {
        sb.append(node.content);
      }
      newNodes.push(node.left);
      newNodes.push(node.right);
      compensateForEmptNodes += leafCount * (betweenSpaces + 1);
    } else if (node instanceof PEmptyNode) {
      sb.append(".");
      newNodes.push(node.left);
      newNodes.push(node.right);
      compensateForEmptNodes += leafCount * (betweenSpaces + 1);
    } else if (node instanceof PLeaf) {
      leafCount++;
      if (node.pathLeaf) {
        sb.append("*" + node.content);
      } else {
        sb.append(node.content);
      }
    } else if (node instanceof PEmptyLeaf) {
      leafCount++;
      sb.append("-");
    }
    this.printWhitespaces(betweenSpaces);
  }
  sb.append("\n");

  for (var i = 1; i <= endgeLines; i++) {
    for (var j = 0; j < nodes.length; j++) {
      this.printWhitespaces(firstSpaces - i);

      var tmpNode = nodes[j];
      if (tmpNode instanceof PNode) {
        sb.append("/");
        this.printWhitespaces(i + i - 1);
        sb.append("\\");
        this.printWhitespaces(endgeLines + endgeLines - i);
      } else if (tmpNode instanceof PContentNode) {
        this.printWhitespaces(i + i + 1);
        this.printWhitespaces(endgeLines + endgeLines - i);
      } else if (tmpNode instanceof PEmptyNode) {
        this.printWhitespaces(i + i + 1);
        this.printWhitespaces(endgeLines + endgeLines - i);
      } else if (tmpNode instanceof PLeaf) {
        this.printWhitespaces(i + 1);
        this.printWhitespaces(endgeLines + endgeLines);
      } else if (tmpNode instanceof PEmptyLeaf) {
        this.printWhitespaces(i + 1);
        this.printWhitespaces(endgeLines + endgeLines);
      }
    }
    sb.append("\n");
  }
  this.printNodeInternal(newNodes, level + 1, maxLevel, 0);
};

TreePrinter.prototype.printWhitespaces = function (count) {
  for (var i = 0; i < count; i++) {
    sb.append(" ");
  }
};

/**
 * @param {PTreeElement} node
 */
TreePrinter.prototype.maxLevel = function (node) {
  if (node instanceof PLeaf) {
    return 1;
  } else if (node instanceof PNode) {
    return Math.max(this.maxLevel(node.left), this.maxLevel(node.right)) + 1;
  }
  return 0;
};

function PrintableTreeFactory() {}

/**
 * @param {BinaryTree} tree
 */
PrintableTreeFactory.prototype.buildPrintableTreeFromClfbTree = function (tree) {
  var maxLevel = tree.maxLevel();
  var newNode = this.genericTreeInternal(1, maxLevel, tree.root, toString);
  return new PrintableBinaryTree(newNode);
};

/**
 * @param {BinaryTree} tree
 */
PrintableTreeFactory.prototype.buildPrintableTreeFromProofTree = function (tree) {
  var maxLevel = tree.maxLevel();
  var newRoot = this.fromProofTreeInternal(1, maxLevel, tree.root);
  return new PrintableBinaryTree(newRoot);
};

/**
 * @param {BinaryTreeElement} inElement
 */
PrintableTreeFactory.prototype.genericTreeInternal = function (
  currentLevel,
  maxLevel,
  inElement,
  toString,
) {
  if (inElement instanceof EmptyLeaf) {
    return new PEmptyLeaf();
  } else if (inElement instanceof Leaf) {
    if (currentLevel < maxLevel) {
      // Create node instead of leaf
      var content = toString(inElement.content);
      var emptyLeft = this.createEmptyInternal(currentLevel + 1, maxLevel);
      var emptyRight = this.createEmptyInternal(currentLevel + 1, maxLevel);
      return new PContentNode(
        content,
        emptyLeft,
        emptyRight,
        inElement.isPathLeaf(),
        inElement.isPath(),
      );
    } else {
      // Normal leaf
      var content = toString(inElement.content);
      return new PLeaf(content, inElement.isPathLeaf());
    }
  } else if (inElement instanceof Node) {
    var left = this.genericTreeInternal(currentLevel + 1, maxLevel, inElement.left, toString);
    var right = this.genericTreeInternal(currentLevel + 1, maxLevel, inElement.right, toString);
    return new PNode(left, right, inElement.isPathLeaf(), inElement.isPath());
  } else {
    throw new Error("Not handling");
  }
};

/**
 * @param {BinaryTreeElement} inElement
 */
PrintableTreeFactory.prototype.fromProofTreeInternal = function (
  currentLevel,
  maxLevel,
  inElement,
) {
  if (inElement instanceof ProofValueLeaf) {
    if (currentLevel < maxLevel) {
      // Create node instead of leaf
      var content = toString(inElement.content);
      var emptyLeft = this.createEmptyInternal(currentLevel + 1, maxLevel);
      var emptyRight = this.createEmptyInternal(currentLevel + 1, maxLevel);
      return new PContentNode(content, emptyLeft, emptyRight, true, true);
    } else {
      // Normal leaf
      var content = toString(inElement.content);
      return new PLeaf(content, true);
    }
  } else if (inElement instanceof ProofHashedLeaf) {
    if (currentLevel < maxLevel) {
      // Create node instead of leaf
      var content = helper.convertToHex(inElement.merkleHash);
      var emptyLeft = this.createEmptyInternal(currentLevel + 1, maxLevel);
      var emptyRight = this.createEmptyInternal(currentLevel + 1, maxLevel);
      return new PContentNode(content, emptyLeft, emptyRight, false, false);
    } else {
      // Normal leaf
      var content = helper.convertToHex(inElement.merkleHash);
      return new PLeaf(content, false);
    }
  } else if (inElement instanceof ProofNode) {
    var left = this.fromProofTreeInternal(currentLevel + 1, maxLevel, inElement.left);
    var right = this.fromProofTreeInternal(currentLevel + 1, maxLevel, inElement.right);
    var isPath = false;
    if (inElement instanceof ProofNodeArrayHead) {
      isPath = inElement.pathElem != null;
    } else if (inElement instanceof ProofNodeDictHead) {
      isPath = inElement.pathElem != null;
    }
    return new PNode(left, right, false, isPath);
  } else {
    throw new Error("Should have handled this element type?");
  }
};

PrintableTreeFactory.prototype.createEmptyInternal = function (currentLevel, maxLevel) {
  if (currentLevel < maxLevel) {
    var left = this.createEmptyInternal(currentLevel + 1, maxLevel);
    var right = this.createEmptyInternal(currentLevel + 1, maxLevel);
    return new PEmptyNode(left, right);
  } else {
    return new PEmptyLeaf();
  }
};

/**
 *
 * @param {*} value
 */
function toString(value) {
  if (value == null) {
    return "N/A";
  }
  if (typeof value === "number" && Number.isInteger(value)) {
    return value.toString();
  }
  if (typeof value === "string") {
    return value;
  }
  if (Buffer.isBuffer(value)) {
    return value.toString("hex");
  } else {
    return value.toString();
  }
}

module.exports = { TreePrinter, PrintableTreeFactory };
