const { toBuffer } = require("../../../../src/formatter");

function TreeHelper() {}

/**
 * @param {Buffer} bytes
 */
TreeHelper.prototype.convertToHex = function (bytes) {
  return bytes.toString("hex").toUpperCase();
};

/**
 * @param {string} hexString
 */
TreeHelper.prototype.convertToByteArray = function (hexString) {
  return toBuffer(hexString, "hex");
};

/**
 * @param {string} s
 */
TreeHelper.prototype.stripWhite = function (s) {
  return s.replace(/ /g, "");
};

module.exports = { TreeHelper };
