import { expect } from "chai";
import { dumpGtx } from "../../src/utils/dump";
import { GTX } from "../../src/gtx/types";
import { mockStringBlockchainRid } from "./common/mocks";
const mockBuffer = Buffer.from(mockStringBlockchainRid);
const gtx: GTX = {
  blockchainRid: mockBuffer,
  operations: [
    {
      opName: "func1",
      args: [
        {
          opName: "quests.player_complete_goal",
          args: [
            "9ffa667c4f5a04c8ba4f9d0a1b5da992",
            3,
            "AE08B5F39CE7E207FF1D93B80DDD3317612FAEFEDFA35BC0FCC2AE2DA4D44231",
            "8e3f8319fb3bae4defc930dc3e8562dffd3b3b306907bb235d608676107515e7",
          ],
        },
        {
          opName: "nop",
          args: ["1971338950"],
        },
      ],
    },
  ],
  signers: [mockBuffer],
  signatures: [mockBuffer],
};
const result = `blockchainRid:
44374145343938384539464341433437304431423541423943443034413739424645313635414236313642344430333843324544393434424132343631323132

operations:
 - func1({"opName":"quests.player_complete_goal","args":["9ffa667c4f5a04c8ba4f9d0a1b5da992",3,"AE08B5F39CE7E207FF1D93B80DDD3317612FAEFEDFA35BC0FCC2AE2DA4D44231","8e3f8319fb3bae4defc930dc3e8562dffd3b3b306907bb235d608676107515e7"]}, {"opName":"nop","args":["1971338950"]})

signers:
 - 44374145343938384539464341433437304431423541423943443034413739424645313635414236313642344430333843324544393434424132343631323132

signatures:
 - 44374145343938384539464341433437304431423541423943443034413739424645313635414236313642344430333843324544393434424132343631323132
`;
describe("dump Gtv and Gtx", () => {
  it("dump Gtx in readable format", () => {
    expect(dumpGtx(gtx)).to.eq(result);
  });
});
