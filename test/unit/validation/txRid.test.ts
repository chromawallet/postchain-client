import { describe } from "mocha";
import { expect } from "chai";
import { isTxRidValid } from "../../../src/blockchainClient/validation/txRid";
import { mockThirtyTwoBytesBuffer } from "../common/mocks";

describe("txRid validation", () => {
  it("should pass validation", () => {
    expect(isTxRidValid(mockThirtyTwoBytesBuffer).success).to.eq(true);
  });

  it("should fail validation", () => {
    expect(isTxRidValid(Buffer.from("811")).success).to.eq(false);
    expect(isTxRidValid(12321).success).to.eq(false);
    expect(isTxRidValid("sdfsd").success).to.eq(false);
    expect(isTxRidValid(null).success).to.eq(false);
    expect(isTxRidValid(undefined).success).to.eq(false);
  });
});
