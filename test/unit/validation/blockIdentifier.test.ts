import { describe } from "mocha";
import { expect } from "chai";
import { isBlockIdentifierValid } from "../../../src/blockchainClient/validation/blockIdentifier";

const validIdentifier = new Array(64).fill("a").join("");

describe("blockIdentifier validation", () => {
  it("should pass validation", () => {
    expect(isBlockIdentifierValid(validIdentifier).success).to.eq(true);
    expect(isBlockIdentifierValid(123).success).to.eq(true);
  });

  it("should fail validation", () => {
    expect(isBlockIdentifierValid(null).success).to.eq(false);
    expect(isBlockIdentifierValid("string with length not equal to 64 symbol").success).to.eq(
      false,
    );
    expect(isBlockIdentifierValid(undefined).success).to.eq(false);
    expect(isBlockIdentifierValid({}).success).to.eq(false);
    expect(isBlockIdentifierValid([]).success).to.eq(false);
  });
});
