import { expect } from "chai";
import {
  isBlockInfoResponse,
  isBlockInfoResponseArray,
  isString,
  isTransaction as isTransactionObject,
  isTransactionInfoInBlockResponse,
  isTransactionInfoInBlockResponseArray,
  isTransactionsCount,
  isTransactionConfirmationProof,
  isConfirmationProof,
  isTransactionInfoResponse,
  isTransactionInfoResponseArray,
  isRawGtv,
  isDictPair,
} from "../../../src/blockchainClient/validation/requests";
import {
  validBlockInfoResponse,
  validBlockInfoResponseArray,
  validConfirmationProof,
  validString,
  validTransactionConfirmationProof,
  validTransactionInfoResponse,
  validTransactionInfoInBlockResponse,
  validTransactionInfoInBlockResponseArray,
  validTransactionObject,
  validTransactionsCount,
  validTransactionInfoResponseArray,
  validObject,
} from "./validationMocks";
import { mockBuffer } from "../common/mocks";

describe("Requests validation", () => {
  describe("isString", () => {
    it("returns string", () => {
      const validResult = isString(validString);
      expect(validResult).to.deep.equal(validString);
    });
    it("returns null", () => {
      const invalidResult = isString(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isDictPair", () => {
    it("returns DictPair", () => {
      const validResult = isDictPair(validObject);
      expect(validResult).to.deep.equal(validObject);
    });
    it("returns null", () => {
      const invalidResult = isDictPair(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isRawGtv", () => {
    it("returns RawGtv for string input", () => {
      const validResult = isRawGtv(validString);
      expect(validResult).to.deep.equal(validString);
    });
    it("returns RawGtv for boolean input", () => {
      const validResult = isRawGtv(true);
      expect(validResult).to.deep.equal(true);
    });
    it("returns RawGtv for number input", () => {
      const validResult = isRawGtv(123);
      expect(validResult).to.deep.equal(123);
    });
    it("returns RawGtv for bigint input", () => {
      const validResult = isRawGtv(BigInt(123));
      expect(validResult).to.deep.equal(BigInt(123));
    });
    it("returns RawGtv for Buffer input", () => {
      const validResult = isRawGtv(mockBuffer);
      expect(validResult).to.deep.equal(mockBuffer);
    });
    it("returns RawGtv for Array input", () => {
      const validResult = isRawGtv([123]);
      expect(validResult).to.deep.equal([123]);
    });
    it("returns RawGtv for Object input", () => {
      const validResult = isRawGtv(validObject);
      expect(validResult).to.deep.equal(validObject);
    });
    it("returns RawGtv for null input", () => {
      const validResult = isRawGtv(null);
      expect(validResult).to.deep.equal(null);
    });
    it("returns null", () => {
      const invalidResult = isRawGtv(undefined);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionInfoInBlockResponse", () => {
    it("returns TransactionInfoInBlockResponse", () => {
      const validResult = isTransactionInfoInBlockResponse(validTransactionInfoInBlockResponse);
      expect(validResult).to.deep.equal(validTransactionInfoInBlockResponse);
    });
    it("returns null", () => {
      const invalidResult = isTransactionInfoInBlockResponse(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionInfoInBlockResponseArray", () => {
    it("returns TransactionInfoInBlockResponseArray", () => {
      const validResult = isTransactionInfoInBlockResponseArray(
        validTransactionInfoInBlockResponseArray,
      );
      expect(validResult).to.deep.equal(validTransactionInfoInBlockResponseArray);
    });
    it("returns null", () => {
      const invalidResult = isTransactionInfoInBlockResponseArray(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isBlockInfoResponse", () => {
    it("returns BlockInfoResponse", () => {
      const validResult = isBlockInfoResponse(validBlockInfoResponse);
      expect(validResult).to.deep.equal(validBlockInfoResponse);
    });
    it("returns null", () => {
      const invalidResult = isBlockInfoResponse(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isBlockInfoResponseArray", () => {
    it("returns BlockInfoResponseArray", () => {
      const validResult = isBlockInfoResponseArray(validBlockInfoResponseArray);
      expect(validResult).to.deep.equal(validBlockInfoResponseArray);
    });
    it("returns null", () => {
      const invalidResult = isBlockInfoResponseArray(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionObject", () => {
    it("returns TransactionObject", () => {
      const validResult = isTransactionObject(validTransactionObject);
      expect(validResult).to.deep.equal(validTransactionObject);
    });
    it("returns null", () => {
      const invalidResult = isTransactionObject(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionsCount", () => {
    it("returns TransactionsCount", () => {
      const validResult = isTransactionsCount(validTransactionsCount);
      expect(validResult).to.deep.equal(validTransactionsCount);
    });
    it("returns null", () => {
      const invalidResult = isTransactionsCount(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionConfirmationProof", () => {
    it("returns TransactionConfirmationProof", () => {
      const validResult = isTransactionConfirmationProof(validTransactionConfirmationProof);
      expect(validResult).to.deep.equal(validTransactionConfirmationProof);
    });
    it("returns null", () => {
      const invalidResult = isTransactionConfirmationProof(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isConfirmationProof", () => {
    it("returns ConfirmationProof", () => {
      const validResult = isConfirmationProof(validConfirmationProof);
      expect(validResult).to.deep.equal(validConfirmationProof);
    });
    it("returns null", () => {
      const invalidResult = isConfirmationProof(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionInfoResponse", () => {
    it("returns TransactionInfoResponse", () => {
      const validResult = isTransactionInfoResponse(validTransactionInfoResponse);
      expect(validResult).to.deep.equal(validTransactionInfoResponse);
    });
    it("returns null", () => {
      const invalidResult = isTransactionInfoResponse(123);
      expect(invalidResult).to.be.null;
    });
  });
  describe("isTransactionInfoResponseArray", () => {
    it("returns TransactionInfoResponseArray", () => {
      const validResult = isTransactionInfoResponseArray(validTransactionInfoResponseArray);
      expect(validResult).to.deep.equal(validTransactionInfoResponseArray);
    });
    it("returns null", () => {
      const invalidResult = isTransactionInfoResponseArray(123);
      expect(invalidResult).to.be.null;
    });
  });
});
