import { expect } from "chai";
import { AnchoringTransactionSchema } from "../../../src/blockchainClient/validation/anchoringTransaction";
import { mockAnchoringTransaction, mockThirtyTwoBytesBuffer } from "../common/mocks";

describe("AnchoringTransactionSchema Validation", () => {
  it("should pass validation for a valid transaction", () => {
    const result = AnchoringTransactionSchema.safeParse(mockAnchoringTransaction);

    expect(result.success).to.eq(true);
  });

  it("should fail validation with invalid txRid length", () => {
    const invalidAnchoringTransaction = {
      txRid: Buffer.from("123"),
      txData: Buffer.from("some data"),
      txOpIndex: 1,
    };

    const result = AnchoringTransactionSchema.safeParse(invalidAnchoringTransaction);

    if (!result.success) {
      expect(result.error.issues[0].message).to.eq("Rid must be 32 bytes long");
    } else {
      throw new Error("Expected validation to fail, but it succeeded");
    }
  });

  it("should fail validation with empty txRid", () => {
    const invalidAnchoringTransaction = {
      txRid: Buffer.from(""),
      txData: Buffer.from("some data"),
      txOpIndex: 1,
    };

    const result = AnchoringTransactionSchema.safeParse(invalidAnchoringTransaction);

    if (!result.success) {
      expect(result.error.issues[0].message).to.eq("Rid must be 32 bytes long");
    } else {
      throw new Error("Expected validation to fail, but it succeeded");
    }
  });

  it("should fail validation with invalid txData", () => {
    const invalidAnchoringTransaction = {
      txRid: mockThirtyTwoBytesBuffer,
      txData: Buffer.from(""),
      txOpIndex: 1,
    };

    const result = AnchoringTransactionSchema.safeParse(invalidAnchoringTransaction);

    if (!result.success) {
      expect(result.error.issues[0].message).to.eq("txData must be a non-empty Buffer");
    } else {
      throw new Error("Expected validation to fail, but it succeeded");
    }
  });

  it("should fail validation with invalid txOpIndex", () => {
    const invalidAnchoringTransaction = {
      txRid: mockThirtyTwoBytesBuffer,
      txData: Buffer.from("some data"),
      txOpIndex: -1,
    };

    const result = AnchoringTransactionSchema.safeParse(invalidAnchoringTransaction);

    if (!result.success) {
      expect(result.error.issues[0].message).to.eq("txOpIndex must be a non-negative integer");
    } else {
      throw new Error("Expected validation to fail, but it succeeded");
    }
  });

  it("should fail validation when anchoring transaction properties are missing", () => {
    const incompleteAnchoringTransaction = {
      txData: Buffer.from("some data"),
      txOpIndex: 1,
    };

    const result = AnchoringTransactionSchema.safeParse(incompleteAnchoringTransaction);

    if (!result.success) {
      expect(result.error.issues[0].path).to.deep.eq(["txRid"]);
    } else {
      throw new Error("Expected validation to fail, but it succeeded");
    }
  });
});
