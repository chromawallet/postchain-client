import { describe } from "mocha";
import { expect } from "chai";
import { isNetworkSettingValid } from "../../../src/blockchainClient/validation/networkSettings";

describe("Network settings validation", () => {
  it("should pass validation", () => {
    expect(
      isNetworkSettingValid({
        nodeUrlPool: ["http://localhost:8080"],
        blockchainIid: 1,
      }).success,
    ).to.eq(true);

    expect(
      isNetworkSettingValid({
        nodeUrlPool: ["http://localhost:8080"],
        blockchainIid: 1,
        statusPollingInterval: 1000,
        statusPollingCount: 10,
        failOverConfig: {
          strategy: "abortOnError",
        },
      }).success,
    ).to.eq(true);

    expect(
      isNetworkSettingValid({
        nodeUrlPool: ["http://localhost:8080"],
        blockchainRid: "123",
      }).success,
    ).to.eq(true);

    expect(
      isNetworkSettingValid({
        directoryNodeUrlPool: ["http://localhost:8080"],
        blockchainIid: 1,
      }).success,
    ).to.eq(true);

    expect(
      isNetworkSettingValid({
        directoryNodeUrlPool: ["http://localhost:8080"],
        blockchainRid: "123",
      }).success,
    ).to.eq(true);
  });

  it("should fail validation", () => {
    expect(
      isNetworkSettingValid({
        nodeUrlPool: ["http://localhost:8080"],
        directoryNodeUrlPool: ["http://localhost:8080"],
      }).success,
    ).to.eq(false);

    expect(
      isNetworkSettingValid({
        blockchainIid: 1,
        blockchainRid: "123",
      }).success,
    ).to.eq(false);

    expect(
      isNetworkSettingValid({
        directoryNodeUrlPool: ["http://localhost:8080"],
        blockchainRid: "123",
        failOverConfig: {
          strategy: "unknownStrategy",
        },
      }).success,
    ).to.eq(false);

    expect(isNetworkSettingValid({}).success).to.eq(false);
    expect(isNetworkSettingValid(null).success).to.eq(false);
    expect(isNetworkSettingValid(undefined).success).to.eq(false);
  });
});
