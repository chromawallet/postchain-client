import {
  BlockInfoResponse,
  ConfirmationProof,
  TransactionConfirmationProof,
  TransactionInfoInBlockResponse,
  TransactionInfoResponse,
  TransactionsCount,
} from "../../../src/blockchainClient/types";
import { RawGtv } from "../../../src/gtv/types";
import { TransactionObject } from "../../../src/restclient/types";
import { mockBuffer } from "../common/mocks";

export const validString = "string";
export const validObject = { key: "value" };
export const validTransactionInfoInBlockResponse: TransactionInfoInBlockResponse = {
  rid: validString,
  hash: validString,
  data: validString,
};
export const validTransactionInfoInBlockResponseArray: TransactionInfoInBlockResponse[] = [
  validTransactionInfoInBlockResponse,
];
export const validBlockInfoResponse: BlockInfoResponse = {
  rid: validString,
  prevBlockRID: validString,
  header: validString,
  height: 0,
  transactions: validTransactionInfoInBlockResponseArray,
  witness: validString,
  witnesses: [validString],
  timestamp: 0,
};
export const validBlockInfoResponseArray: BlockInfoResponse[] = [validBlockInfoResponse];
export const validTransactionObject: TransactionObject = { tx: validString };
export const validTransactionsCount: TransactionsCount = { transactionsCount: 0 };
export const validTransactionConfirmationProof: TransactionConfirmationProof = {
  proof: validString,
};
export const validConfirmationProof: ConfirmationProof = {
  hash: mockBuffer,
  blockHeader: mockBuffer,
  witness: mockBuffer,
  merkleProofTree: validString as RawGtv,
  txIndex: 0,
};
export const validTransactionInfoResponse: TransactionInfoResponse = {
  blockRID: validString,
  blockHeight: 0,
  blockHeader: validString,
  witness: validString,
  timestamp: 0,
  txRID: validString,
  txHash: validString,
  txData: validString,
};
export const validTransactionInfoResponseArray: TransactionInfoResponse[] = [
  validTransactionInfoResponse,
];
