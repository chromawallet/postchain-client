import { describe } from "mocha";
import { expect } from "chai";
import { isSignMethodValid } from "../../../src/blockchainClient/validation/signMethod";

describe("signMethod validation", () => {
  it("should pass validation", () => {
    expect(
      isSignMethodValid({
        privKey: Buffer.from("priv-key", "hex"),
        pubKey: Buffer.from("pub-key", "hex"),
      }).success,
    ).to.eq(true);

    expect(
      isSignMethodValid({
        pubKey: Buffer.from("pub-key", "hex"),
        sign: (buffer: Buffer) => {
          return Promise.resolve(buffer);
        },
      }).success,
    ).to.eq(true);
  });

  it("should fail validation", () => {
    expect(isSignMethodValid(null).success).to.eq(false);
    expect(isSignMethodValid(undefined).success).to.eq(false);
    expect(
      isSignMethodValid({
        privKey: Buffer.from("priv-key", "hex"),
      }).success,
    ).to.eq(false);
    expect(
      isSignMethodValid({
        pubKey: Buffer.from("pub-key", "hex"),
      }).success,
    ).to.eq(false);
    expect(
      isSignMethodValid({
        pubKey: Buffer.from("pub-key", "hex"),
        signKey: () => Promise.resolve("sdfd"),
      }).success,
    ).to.eq(false);

    expect(
      isSignMethodValid({
        signKey: () => Promise.resolve("sdfd"),
      }).success,
    ).to.eq(false);
  });
});
