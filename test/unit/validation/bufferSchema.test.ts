import { expect } from "chai";
import { BufferSchema } from "../../../src/blockchainClient/validation/bufferSchema";
import { mockThirtyTwoBytesBuffer } from "../common/mocks";

describe("BufferSchema validation", () => {
  it("should pass validation for an instance of Buffer", () => {
    const validBuffer = mockThirtyTwoBytesBuffer;
    expect(BufferSchema.safeParse(validBuffer).success).to.equal(true);
  });

  it("should pass validation for an instance of Uint8Array", () => {
    const validUint8Array = new Uint8Array([1, 2, 3, 4]);
    expect(BufferSchema.safeParse(validUint8Array).success).to.equal(true);
  });

  it("should fail validation", () => {
    const invalidBuffer = "abcd1234";
    expect(BufferSchema.safeParse(invalidBuffer).success).to.equal(false);
  });
});
