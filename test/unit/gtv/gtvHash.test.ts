import { expect } from "chai";
import { toBuffer } from "../../../src/formatter";
import { gtvHash } from "../../../src/gtv";

describe("gtvHash", function () {
  it("should return buffer when called with array containing a string and a buffer", async () => {
    const result = gtvHash(["string", toBuffer("fghjkl")]);
    expect(result).to.be.instanceof(Buffer);
  });
});
