import sinon from "sinon";
import {
  abortOnError,
  retryRequest,
  singleEndpoint,
  tryNextOnError,
  queryMajority,
} from "../../src/restclient/failoverStrategies";
import * as handleRequestModule from "../../src/restclient/httpUtil";
import { Method } from "../../src/restclient/enums";
import { ClientConfig } from "../../src/blockchainClient/types";
import { FailoverStrategy } from "../../src/blockchainClient/enums";
import { expect } from "chai";
import { createNodeManager } from "../../src/restclient/nodeManager";
import { ResponseObject } from "../../src/restclient/types";
import { errorMessages } from "../../src/restclient/failoverStrategies";
import { mockStringDirectoryChainRid } from "./common/mocks";
import { setStatusPolling } from "../../src/blockchainClient/utils";

let mockHandleRequest = sinon.stub();

describe("Failover strategies", function () {
  beforeEach(() => {
    mockHandleRequest = sinon.stub(handleRequestModule, "default");
  });
  afterEach(() => {
    sinon.resetHistory();
    sinon.restore();
    mockHandleRequest.reset();
  });

  type Input = {
    numberOfRetries: number;
    statusCode: number | null;
    error: Error | null;
    config: ClientConfig;
  };
  async function testAbortOnError({ error, statusCode, numberOfRetries, config }: Input) {
    mockHandleRequest
      .withArgs(sinon.match.any, "/path/abort-on-error-test", sinon.match.any, sinon.match.any)
      .returns({
        error,
        statusCode,
        rspBody: "",
      });

    await abortOnError({
      method: Method.GET,
      path: "/path/abort-on-error-test",
      config,
    });

    const expectedCalls = mockHandleRequest
      .getCalls()
      .filter(call =>
        call.calledWithExactly(
          sinon.match.any,
          "/path/abort-on-error-test",
          sinon.match.any,
          sinon.match.any,
        ),
      );
    expect(expectedCalls.length).to.equal(numberOfRetries);
  }
  async function testTryNextOnError({ error, statusCode, numberOfRetries, config }: Input) {
    mockHandleRequest.resolves({
      error,
      statusCode,
      rspBody: "",
    });

    await tryNextOnError({
      method: Method.GET,
      path: "/path",
      config,
    });

    sinon.assert.callCount(mockHandleRequest, numberOfRetries);
  }
  async function testSingleEndpoint({ error, statusCode, numberOfRetries, config }: Input) {
    mockHandleRequest.resolves({
      error,
      statusCode,
      rspBody: "",
    });

    await singleEndpoint({
      method: Method.GET,
      path: "/path",
      config,
    });

    sinon.assert.callCount(mockHandleRequest, numberOfRetries);
  }

  describe("AbortOnError", function () {
    const config: Omit<ClientConfig, "nodeManager"> = {
      endpointPool: [
        { url: "url1", whenAvailable: 0 },
        { url: "url2", whenAvailable: 0 },
      ],
      blockchainRid: "blockchainRid",
      dappStatusPolling: setStatusPolling({ interval: 100, count: 1 }),
      clusterAnchoringStatusPolling: setStatusPolling(),
      systemAnchoringStatusPolling: setStatusPolling(),
      failoverStrategy: FailoverStrategy.AbortOnError,
      attemptsPerEndpoint: 3,
      attemptInterval: 100,
      unreachableDuration: 100,
      directoryChainRid: mockStringDirectoryChainRid,
    };

    it("abortOnError retries when server error 500", async () => {
      await testAbortOnError({
        numberOfRetries: 6,
        statusCode: 500,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({ nodeUrls: ["url1", "url2"] }),
        },
      });
    });
    it("abortOnError retries when server error 503", async () => {
      await testAbortOnError({
        numberOfRetries: 6,
        statusCode: 503,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({ nodeUrls: ["url1", "url2"] }),
        },
      });
    });
    it("abortOnError does not retry when client error 400", async () => {
      await testAbortOnError({
        numberOfRetries: 1,
        statusCode: 400,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({ nodeUrls: ["url1", "url2"] }),
        },
      });
    });
    it("abortOnError does not retry when statuscode is 200", async () => {
      await testAbortOnError({
        numberOfRetries: 1,
        statusCode: 200,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({ nodeUrls: ["url1", "url2"] }),
        },
      });
    });
    it("abortOnError retries when error is returned", async () => {
      await testAbortOnError({
        numberOfRetries: 6,
        statusCode: null,
        error: new Error(),
        config: {
          ...config,
          nodeManager: createNodeManager({ nodeUrls: ["url1", "url2"] }),
        },
      });
    });
  });
  describe("TryNextOnError", function () {
    const config = {
      endpointPool: [
        { url: "url1", whenAvailable: 0 },
        { url: "url2", whenAvailable: 0 },
      ],
      blockchainRid: "blockchainRid",
      statusPollInterval: 100,
      statusPollCount: 1,
      failoverStrategy: FailoverStrategy.TryNextOnError,
      attemptsPerEndpoint: 3,
      attemptInterval: 100,
      unreachableDuration: 100,
      directoryChainRid: mockStringDirectoryChainRid,
    };
    it("tryNextOnError retries when client error 400", async () => {
      await testTryNextOnError({
        numberOfRetries: 6,
        statusCode: 400,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
    it("tryNextOnError retries when server error 500", async () => {
      await testTryNextOnError({
        numberOfRetries: 6,
        statusCode: 500,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
    it("tryNextOnError does not retry when statuscode is 200", async () => {
      await testTryNextOnError({
        numberOfRetries: 1,
        statusCode: 200,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
    it("tryNextOnError reties when error is returned", async () => {
      await testTryNextOnError({
        numberOfRetries: 6,
        statusCode: null,
        error: new Error(),
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
  });
  describe("SingleEndpoint", function () {
    const config = {
      endpointPool: [
        { url: "url1", whenAvailable: 0 },
        { url: "url2", whenAvailable: 0 },
      ],
      blockchainRid: "blockchainRid",
      statusPollInterval: 100,
      statusPollCount: 1,
      failoverStrategy: FailoverStrategy.TryNextOnError,
      attemptsPerEndpoint: 3,
      attemptInterval: 100,
      unreachableDuration: 100,
      directoryChainRid: mockStringDirectoryChainRid,
    };
    it("singleEndpoint does not retry when statuscode is 200", async () => {
      await testSingleEndpoint({
        numberOfRetries: 1,
        statusCode: 200,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });

    it("singleEndpoint retry when server error 400", async () => {
      await testSingleEndpoint({
        numberOfRetries: 3,
        statusCode: 400,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });

    it("singleEndpoint retry when server error 500", async () => {
      await testSingleEndpoint({
        numberOfRetries: 3,
        statusCode: 500,
        error: null,
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
    it("singleEndpoint reties when error is returned", async () => {
      await testSingleEndpoint({
        numberOfRetries: 3,
        statusCode: null,
        error: new Error(),
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: ["url1", "url2"],
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });
    });
    it("singleEndpoint throws an error when no endpoint is found", async () => {
      const nodeManager = createNodeManager({
        nodeUrls: ["url1", "url2"],
      });

      const nodeManagetGetNodeStub: sinon.SinonStub = sinon
        .stub(nodeManager, "getNode")
        .returns(null);

      await expect(
        singleEndpoint({
          method: Method.GET,
          path: "/path",
          config: {
            ...config,
            nodeManager: nodeManager,
          },
        }),
      ).to.be.rejectedWith("Cannot get endpoint. Node not found!");

      expect(nodeManagetGetNodeStub.calledOnce).to.be.true;
    });
  });
  describe("QueryMajority", function () {
    const config = {
      endpointPool: [
        { url: "url1", whenAvailable: 0 },
        { url: "url2", whenAvailable: 0 },
      ],
      blockchainRid: "blockchainRid",
      statusPollInterval: 100,
      statusPollCount: 1,
      failoverStrategy: FailoverStrategy.QueryMajority,
      attemptsPerEndpoint: 3,
      attemptInterval: 100,
      unreachableDuration: 100,
      directoryChainRid: mockStringDirectoryChainRid,
    };

    const resolveRequestsAs = (responses: ResponseObject[]) => {
      responses.forEach((response, index) => {
        mockHandleRequest.onCall(index).resolves({
          error: response.error,
          statusCode: response.statusCode,
          rspBody: response.rspBody,
        });
      });
    };

    it("Should return a succesfull response as soon as it gets an BFT-majority of successful responses", async () => {
      resolveRequestsAs([
        { error: null, statusCode: 200, rspBody: "response2" },
        { error: null, statusCode: 200, rspBody: "response1" },
        { error: null, statusCode: 200, rspBody: "response1" },
        { error: null, statusCode: 200, rspBody: "response3" },
        { error: null, statusCode: 200, rspBody: "response1" },
      ]);

      const urls = ["url1", "url2", "url3", "url4", "url5"];

      const result = await queryMajority({
        method: Method.GET,
        path: "/path",
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: urls,
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });

      expect(result.rspBody).to.equal("response1");
      expect(mockHandleRequest.callCount).to.equal(urls.length);
    });

    it("should when the failureThreshold is reached return the first failure if there are any.", async () => {
      resolveRequestsAs([
        { error: null, statusCode: 200, rspBody: "success - response 1" },
        { error: null, statusCode: 200, rspBody: "success - response 2" },
        { error: null, statusCode: 500, rspBody: "fail - response 1" },
        { error: null, statusCode: 500, rspBody: "fail - response 2" },
      ]);

      const urls = ["url1", "url2", "url3", "url4"];
      const result = await queryMajority({
        method: Method.GET,
        path: "/path",
        config: {
          ...config,
          nodeManager: createNodeManager({
            nodeUrls: urls,
          }),
          dappStatusPolling: setStatusPolling(),
          clusterAnchoringStatusPolling: setStatusPolling(),
          systemAnchoringStatusPolling: setStatusPolling(),
        },
      });

      expect(result.rspBody).to.equal("fail - response 1");
    });

    it("should when the failureThreshold is reached throw the first error, if there are no failures.", async () => {
      const clientErrorMessage = "client error";

      resolveRequestsAs([
        { error: null, statusCode: 200, rspBody: "success - response 1" },
        { error: null, statusCode: 200, rspBody: "success - response 2" },
      ]);
      mockHandleRequest.onCall(2).rejects(new Error(clientErrorMessage));
      mockHandleRequest.onCall(3).rejects(new Error(clientErrorMessage));

      const urls = ["url1", "url2", "url3", "url4"];

      let requestError;
      try {
        await queryMajority({
          method: Method.GET,
          path: "/path",
          config: {
            ...config,
            endpointPool: urls.map(url => ({ url, whenAvailable: 0 })),
            nodeManager: createNodeManager({
              nodeUrls: urls,
            }),
            dappStatusPolling: setStatusPolling(),
            clusterAnchoringStatusPolling: setStatusPolling(),
            systemAnchoringStatusPolling: setStatusPolling(),
          },
        });
      } catch (error) {
        requestError = error;
      }

      expect(requestError).to.not.be.null;
      expect(requestError.message).to.equal(clientErrorMessage);
    });

    it("should throw an error if no consensus could be reached.", async () => {
      resolveRequestsAs([
        { error: null, statusCode: 200, rspBody: "response1" },
        { error: null, statusCode: 200, rspBody: "response2" },
        { error: null, statusCode: 200, rspBody: "response3" },
        { error: null, statusCode: 200, rspBody: "response4" },
      ]);

      const urls = ["url1", "url2", "url3", "url4"];

      let requestError;
      try {
        await queryMajority({
          method: Method.GET,
          path: "/path",
          config: {
            ...config,
            endpointPool: urls.map(url => ({ url, whenAvailable: 0 })),
            nodeManager: createNodeManager({
              nodeUrls: urls,
            }),
            dappStatusPolling: setStatusPolling(),
            clusterAnchoringStatusPolling: setStatusPolling(),
            systemAnchoringStatusPolling: setStatusPolling(),
          },
        });
      } catch (error) {
        requestError = error;
      }

      expect(requestError).to.not.be.null;
      expect(requestError.message).to.equal(errorMessages.NO_CONSENSUS);
    });

    it("should throw timout error if takes longer than 15000 ms min for first requsts to resolve.", async () => {
      const urls = ["url1", "url2", "url3", "url4"];

      for (let i = 0; i < urls.length; i++) {
        mockHandleRequest.onCall(i).callsFake(
          () =>
            new Promise(resolve => {
              setTimeout(
                () =>
                  resolve({
                    error: null,
                    statusCode: 200,
                    rspBody: `response${i + 1}`,
                  }),
                16000,
              );
            }),
        );
      }

      let requestError;
      try {
        await queryMajority({
          method: Method.GET,
          path: "/path",
          config: {
            ...config,
            endpointPool: urls.map(url => ({ url, whenAvailable: 0 })),
            nodeManager: createNodeManager({
              nodeUrls: urls,
            }),
            dappStatusPolling: setStatusPolling(),
            clusterAnchoringStatusPolling: setStatusPolling(),
            systemAnchoringStatusPolling: setStatusPolling(),
          },
        });
      } catch (error) {
        requestError = error;
      }

      expect(requestError).not.to.equal("Timeout exceeded");
    });
  });
  describe("RetryRequest", function () {
    const config = {
      endpointPool: [{ url: "url1", whenAvailable: 0 }],
      blockchainRid: "brid",
      statusPollInterval: 100,
      statusPollCount: 10,
      failoverStrategy: FailoverStrategy.AbortOnError,
      attemptsPerEndpoint: 1,
      attemptInterval: 100,
      unreachableDuration: 1000,
    };
    it("update the endpointpool if a endpoint is not responding", async () => {
      mockHandleRequest.resolves({
        error: null,
        statusCode: 500,
        rspBody: "",
      });

      const nodeManager = createNodeManager({
        nodeUrls: ["url1"],
      });

      await retryRequest({
        method: Method.GET,
        path: "/path",
        config: {
          ...config,
          nodeManager,
        },
        validateStatusCode: statusCode => ![500, 503].includes(statusCode),
      });

      expect(nodeManager.nodes[0].whenAvailable).to.not.equal(0);
    });
  });
});
