import { Buffer } from "buffer";
// The lower-level client that can be used for any
// postchain client messages. It only handles binary data.
import { restClient, gtxClient } from "../../../index";

import { expect } from "chai";
import * as secp256k1 from "secp256k1";
import { gtvHash } from "../../../src/gtv";
import * as gtxTool from "../../../src/gtx/gtx";
import { RestClient } from "../../../src/restclient/interfaces";
import { GtxClient } from "../../../src/gtx/interfaces";
import { signDigest } from "../../../src/encryption/encryption";
import { RawGtxBody } from "../../../src/gtx/types";

// The higher-level client that is used for generic transactions, GTX.
// This utilizes the GTX format described below to pass function calls
// from the client to the postchain backend implementation.
const blockchainRid = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";
const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
const signerPubKeyA: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyA));
const sigProvA = {
  pubKey: signerPubKeyA,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    return signDigest(digest, signerPrivKeyA);
  },
};

const signerPrivKeyB: Buffer = Buffer.alloc(32, "b");
const signerPubKeyB: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyB));
const sigProvB = {
  pubKey: signerPubKeyB,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    signDigest(digest, signerPrivKeyB);
  },
};

function init(functionNames: string[] = []) {
  const rest: RestClient = restClient.createRestClient(
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    [process.env.TEST_NODE_URL!],
    blockchainRid,
    5,
  );
  const client: GtxClient = gtxClient.createClient(rest, blockchainRid, functionNames);
  return { client, rest };
}

describe("Test GTX against a node", function () {
  it("adds new function to client", async function () {
    const client = init(["gtx_test"]).client;
    const req = client.newTransaction([]);
    req.gtx_test(1, Math.random().toString());
    expect(req.gtx.operations[0].opName).equal("gtx_test");
  });
  it("adds signers to transaction", async function () {
    const client = init().client;
    const req = client.newTransaction([sigProvA.pubKey]);
    expect(req.gtx.signers[0]).to.deep.equal(sigProvA.pubKey);
  });

  it("signs transaction", async function () {
    const client = init().client;
    const req = client.newTransaction([sigProvA.pubKey]);
    const message: Buffer = gtvHash(gtxTool.gtxToRawGtxBody(req.gtx));
    await req.sign(sigProvA);
    expect(gtxTool.checkGTXSignatures(message, req.gtx)).to.be.true;
  });

  it("can sign a transaction if one signer is missing", async () => {
    const client = init().client;
    const req = client.newTransaction([sigProvA.pubKey, sigProvB.pubKey]);
    const message: Buffer = req.getDigestToSign();
    await req.sign(sigProvA);
    expect(gtxTool.checkExistingGTXSignatures(message, req.gtx)).to.be.true;
  });

  it("can not add operation to already signed transaction", async () => {
    const client = init().client;
    const req = client.newTransaction([sigProvA.pubKey, sigProvB.pubKey]);
    await req.sign(sigProvA);
    expect(function () {
      req.addOperation("func1", ["arg1"]);
    }).to.throw("Cannot add function calls to an already signed gtx");
  });
  it("encodes and decodes transaction", async () => {
    const client = init().client;
    const req = client.newTransaction([sigProvA.pubKey]);
    await req.sign(sigProvA);
    const encoded = req.encode();
    const decoded = client.transactionFromRawTransaction(encoded);
    expect(JSON.stringify(decoded)).to.equal(JSON.stringify(req));
  });
});
