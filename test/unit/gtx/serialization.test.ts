import _ from "lodash";
import { assert, expect } from "chai";
import { Buffer } from "buffer";
import {
  encodeValue,
  decodeValue,
  encodeValueGtx,
  decodeValueGtx,
  createTypedArg,
  parseValue,
} from "../../../src/gtx/serialization";
import type { RawGtv } from "../../../src/gtv/types";
import type { RawGtx, RawGtxBody, RawGtxOp } from "../../../src/gtx/types";
import BN from "bn.js";
import {
  mixedArray,
  nestedArray,
  nestedObject,
  simpleObject,
  stringArray,
} from "./serializationtestobjects";

function buildDefaultRawGtx(args: RawGtv[]): RawGtx {
  const operation = buildRawGtxOp("opname", args);
  return buildRawGtx(operation);
}
function buildRawGtxOp(opname: string, args: RawGtv[]): RawGtxOp {
  return [opname, args];
}
function buildRawGtx(
  rawGtxOp: RawGtxOp,
  brid: Buffer = Buffer.from(""),
  signers: Buffer[] = [],
  signatures: Buffer[] = [],
): RawGtx {
  const tx: RawGtxBody = [brid, [rawGtxOp], signers];
  return [tx, signatures];
}

function testEncodeDecode(arg: RawGtv) {
  const encoded = encodeValue(arg);
  const decoded = decodeValue(encoded);
  assert.ok(_.isEqual(arg, decoded));
}

function testEncodeDecodeGtx(arg: RawGtx) {
  const encoded = encodeValueGtx(arg);
  const decoded = decodeValueGtx(encoded);
  assert.ok(_.isEqual(arg, decoded));
}

const gtvValues = [
  { typeName: "null", args: [null] },
  { typeName: "bytearray", args: [Buffer.from("test buffer")] },
  { typeName: "string", args: ["test string"] },
  { typeName: "integer", args: [1] },
  { typeName: "dict", args: [{ name: "test", value: "test string" }] },
  {
    typeName: "array",
    args: [buildDefaultRawGtx([1]), buildDefaultRawGtx([1])],
  },
  { typeName: "bigInteger", args: [BigInt(9007199254740991)] },
  { typeName: "empty", args: [] },
];

const bigIntValue = "18446744073709551616";
const negativeBigIntValue = "-18446744073709551616";

describe("Serialization", function () {
  gtvValues.forEach(({ typeName, args }) => {
    it(`encodes and decodes a GTX object with arguments of type ${typeName}`, () => {
      const rawGtx = buildDefaultRawGtx(args);
      testEncodeDecodeGtx(rawGtx);
    });
  });
  gtvValues.forEach(({ typeName, args }) => {
    it(`encodes and decodes an input of type ${typeName}`, () => {
      testEncodeDecode(args);
    });
  });
  it(`should throw error when decoding GTV value as GTX`, () => {
    expect(function () {
      decodeValueGtx(encodeValue("test string"));
    }).to.throw(
      "Unexpected type of value: test string, expected decoded value to be of type RawGtx",
    );
  });
  it("should throw an error when decoding unsupported value", () => {
    try {
      testEncodeDecode(null);
    } catch (error) {
      expect(error).to.be.instanceOf(Error);
      expect(error.message).to.deep.equal("Invalid GTV data. Choice not matched at: (shallow)");
    }
  });
});

describe("Create typed args tests", () => {
  it("null", () => {
    const result = createTypedArg(null);
    expect(result).to.deep.equal({ type: "null", value: null });
  });

  it("Buffer", () => {
    const result = createTypedArg(Buffer.from("hello"));
    expect(result).to.deep.equal({
      type: "byteArray",
      value: Buffer.from("hello"),
    });
  });

  it("Boolean true", () => {
    const result = createTypedArg(true);
    expect(result).to.deep.equal({ type: "integer", value: 1 });
  });

  it("Boolean false", () => {
    const result = createTypedArg(false);
    expect(result).to.deep.equal({ type: "integer", value: 0 });
  });

  it("Empty String", () => {
    const result = createTypedArg("");
    expect(result).to.deep.equal({ type: "string", value: "" });
  });

  it("Escaped String", () => {
    const result = createTypedArg('"Name"');
    expect(result).to.deep.equal({ type: "string", value: '"Name"' });
  });

  it("Positive Integer", () => {
    const result = createTypedArg(1);
    expect(result).to.deep.equal({ type: "integer", value: new BN(1) });
  });

  it("Max Integer", () => {
    const result = createTypedArg(Number.MAX_SAFE_INTEGER);
    expect(result).to.deep.equal({
      type: "integer",
      value: new BN(Number.MAX_SAFE_INTEGER),
    });
  });

  it("Min Integer", () => {
    const result = createTypedArg(Number.MIN_SAFE_INTEGER);
    expect(result).to.deep.equal({
      type: "integer",
      value: new BN(Number.MIN_SAFE_INTEGER),
    });
  });

  it("Negative Integer", () => {
    const result = createTypedArg(-1);
    expect(result).to.deep.equal({ type: "integer", value: new BN(-1) });
  });

  it("Float", () => {
    expect(() => {
      createTypedArg(1.1);
    }).to.throw("Failed to encode 1.1: Error: User error: Only integers are supported");
  });

  it("Binary", () => {
    const result = createTypedArg(0b11);
    expect(result).to.deep.equal({ type: "integer", value: new BN(3) });
  });

  it("Hex", () => {
    const result = createTypedArg(0xa);
    expect(result).to.deep.equal({ type: "integer", value: new BN(10) });
  });

  it("Octal", () => {
    const result = createTypedArg(0o4);
    expect(result).to.deep.equal({ type: "integer", value: new BN(4) });
  });

  it("test value of bigInt is larger than a typescript number", () => {
    expect(() => {
      createTypedArg(Number(bigIntValue));
    }).to.throw(`Failed to encode 18446744073709552000: Error: Assertion failed`);
  });

  it("BigInt", () => {
    const result = createTypedArg(BigInt(bigIntValue));
    expect(result).to.deep.equal({
      type: "bigInteger",
      value: new BN(bigIntValue),
    });
  });

  it("Negative BigInt", () => {
    const result = createTypedArg(BigInt(negativeBigIntValue));
    expect(result).to.deep.equal({
      type: "bigInteger",
      value: new BN(negativeBigIntValue),
    });
  });

  it("Empty array", () => {
    const result = createTypedArg([]);
    expect(result).to.deep.equal({ type: "array", value: [] });
  });

  it("String array", () => {
    const result = createTypedArg(["foo", "bar"]);
    expect(result).to.deep.equal(stringArray);
  });

  it("Mixed type array", () => {
    const result = createTypedArg(["foo", 1]);
    expect(result).to.deep.equal(mixedArray);
  });

  it("Nested array", () => {
    const result = createTypedArg([["foo"], ["bar"]]);
    expect(result).to.deep.equal(nestedArray);
  });

  it("Object", () => {
    const result = createTypedArg(["queryname", { arg: "foo" }]);
    expect(result).to.deep.equal(simpleObject);
  });
  it("Nested Object", () => {
    const result = createTypedArg(["queryname", { arg: { arg1: "foo" } }]);
    expect(result).to.deep.equal(nestedObject);
  });
});

describe("ParseValue", () => {
  it("null", () => {
    const result = parseValue({ type: "null", value: null });
    expect(result).to.deep.equal(null);
  });

  it("Buffer", () => {
    const result = parseValue({
      type: "byteArray",
      value: Buffer.from("hello"),
    });
    expect(result).to.deep.equal(Buffer.from("hello"));
  });

  it("Empty String", () => {
    const result = parseValue({ type: "string", value: "" });
    expect(result).to.deep.equal("");
  });

  it("Escaped String", () => {
    const result = parseValue({ type: "string", value: '"Name"' });
    expect(result).to.deep.equal('"Name"');
  });

  it("Positive Integer", () => {
    const result = parseValue({ type: "integer", value: new BN(1) });
    expect(result).to.deep.equal(1);
  });

  it("Max Integer", () => {
    const result = parseValue({
      type: "integer",
      value: new BN(Number.MAX_SAFE_INTEGER),
    });
    expect(result).to.deep.equal(Number.MAX_SAFE_INTEGER);
  });

  it("Min Integer", () => {
    const result = parseValue({
      type: "integer",
      value: new BN(Number.MIN_SAFE_INTEGER),
    });
    expect(result).to.deep.equal(Number.MIN_SAFE_INTEGER);
  });

  it("Negative Integer", () => {
    const result = parseValue({ type: "integer", value: new BN(-1) });
    expect(result).to.deep.equal(-1);
  });

  it("BigInt", () => {
    const result = parseValue({
      type: "bigInteger",
      value: new BN(bigIntValue),
    });
    expect(result).to.deep.equal(BigInt(bigIntValue));
  });

  it("Negative BigInt", () => {
    const result = parseValue({
      type: "bigInteger",
      value: new BN(negativeBigIntValue),
    });
    expect(result).to.deep.equal(BigInt(negativeBigIntValue));
  });

  it("Empty array", () => {
    const result = parseValue({ type: "array", value: [] });
    expect(result).to.deep.equal([]);
  });

  it("String array", () => {
    const result = parseValue(stringArray);
    expect(result).to.deep.equal(["foo", "bar"]);
  });

  it("Mixed type array", () => {
    const result = parseValue(mixedArray);
    expect(result).to.deep.equal(["foo", 1]);
  });

  it("Nested array", () => {
    const result = parseValue(nestedArray);
    expect(result).to.deep.equal([["foo"], ["bar"]]);
  });

  it("Object", () => {
    const result = parseValue(simpleObject);
    expect(result).to.deep.equal(["queryname", { arg: "foo" }]);
  });

  it("Nested Object", () => {
    const result = parseValue(nestedObject);
    expect(result).to.deep.equal(["queryname", { arg: { arg1: "foo" } }]);
  });

  it("Missing type", () => {
    expect(() => {
      parseValue({ type: "missing", value: [] });
    }).to.throw('Cannot parse typedArg {"type":"missing","value":[]}. Unknown type missing');
  });
});
