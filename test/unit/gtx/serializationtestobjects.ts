import BN from "bn.js";
import { ASNValue } from "../../../src/gtx/types";

export const nestedArray: ASNValue = {
  type: "array",
  value: [
    { type: "array", value: [{ type: "string", value: "foo" }] },
    { type: "array", value: [{ type: "string", value: "bar" }] },
  ],
};

export const mixedArray: ASNValue = {
  type: "array",
  value: [
    { type: "string", value: "foo" },
    { type: "integer", value: new BN(1) },
  ],
};

export const stringArray: ASNValue = {
  type: "array",
  value: [
    { type: "string", value: "foo" },
    { type: "string", value: "bar" },
  ],
};

export const simpleObject: ASNValue = {
  type: "array",
  value: [
    { type: "string", value: "queryname" },
    {
      type: "dict",
      value: [{ name: "arg", value: { type: "string", value: "foo" } }],
    },
  ],
};

export const nestedObject: ASNValue = {
  type: "array",
  value: [
    { type: "string", value: "queryname" },
    {
      type: "dict",
      value: [
        {
          name: "arg",
          value: {
            type: "dict",
            value: [{ name: "arg1", value: { type: "string", value: "foo" } }],
          },
        },
      ],
    },
  ],
};
