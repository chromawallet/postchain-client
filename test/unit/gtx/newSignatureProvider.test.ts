import { assert } from "chai";
import { checkDigestSignature, makeKeyPair } from "../../../src/encryption/encryption";
import {
  getDigestToSignFromRawGtxBody,
  gtxToRawGtxBody,
  newSignatureProvider,
} from "../../../src/gtx/gtx";
import { SignatureProvider } from "../../../src/gtx/interfaces";
import { getGTXFromBufferOrTransactionOrOperation } from "../../../src/blockchainClient/utils";
import { Transaction } from "../../../src/blockchainClient/types";

const mockTransaction: Transaction = {
  operations: [],
  signers: [],
};

async function isSigProvWorking(sigProvider: SignatureProvider): Promise<boolean> {
  const gtx = getGTXFromBufferOrTransactionOrOperation(
    mockTransaction,
    "1212121212121212121212121212121212121212121212121212121212121212",
  );
  const rawGtxBody = gtxToRawGtxBody(gtx);
  const digest = getDigestToSignFromRawGtxBody(rawGtxBody);
  const sig = await sigProvider.sign(rawGtxBody);
  return checkDigestSignature(digest, sigProvider.pubKey, sig);
}

describe("new signature provider", () => {
  it("provides a working signatureProvider with correct pubKey from privkey", async () => {
    const keyPair = makeKeyPair();
    const sigProvider = newSignatureProvider({ privKey: keyPair.privKey });
    assert(sigProvider.pubKey.equals(keyPair.pubKey));
    assert(await isSigProvWorking(sigProvider));
  });
  it("provides a working signatureProvider with correct pubKey from keypair", async () => {
    const keyPair = makeKeyPair();
    const sigProvider = newSignatureProvider(keyPair);
    assert(sigProvider.pubKey.equals(keyPair.pubKey));
    assert(await isSigProvWorking(sigProvider));
  });
  it("provides a signatureProvider with a working Keypair from empty call", async () => {
    const sigProvider = newSignatureProvider();
    assert(await isSigProvWorking(sigProvider));
  });
});
