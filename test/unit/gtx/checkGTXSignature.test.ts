import { Buffer } from "buffer";
import { RellOperation, GTX } from "../../../src/gtx/types";

import { assert, expect } from "chai";
import {
  checkExistingGTXSignatures,
  checkGTXSignatures,
  getDigestToSign,
  sign,
} from "../../../src/gtx/gtx";
import { signers, sigProv } from "../signatures";

const blockchainRid = Buffer.from("DEADBEEF94");

describe("check gtx signature", () => {
  function test(txHash: Buffer, gtx: GTX) {
    const checkResult: boolean = checkGTXSignatures(txHash, gtx);
    assert.deepEqual(checkResult, true);
  }

  async function tx(
    calls: RellOperation[],
    signers: Buffer[],
    signaturesCount: number,
  ): Promise<GTX> {
    const tx: GTX = { blockchainRid, operations: calls, signers };
    for (let i = 0; i < signaturesCount; i++) {
      const signatureProvider = sigProv(i);
      await sign(tx, signatureProvider);
    }

    if (!tx.signatures) tx.signatures = [];
    return tx;
  }

  it("checkGTXSignatures without gtx signatures returns false", async () => {
    const aTx: GTX = await tx([], [], 0);
    const txHash: Buffer = getDigestToSign(aTx);
    const checkResult: boolean = checkGTXSignatures(txHash, aTx);
    assert.deepEqual(checkResult, false);
  });

  it("single call with one of two signatures works", async () => {
    const aTx: GTX = await tx([{ opName: "func1åäö", args: [] }], signers(2), 1);
    const txHash: Buffer = getDigestToSign(aTx);
    expect(checkExistingGTXSignatures(txHash, aTx)).to.be.true;
  });

  it("single call with enough signatures works", async () => {
    const aTx: GTX = await tx([{ opName: "func1åääö", args: [] }], signers(1), 1);
    const txHash: Buffer = getDigestToSign(aTx);
    test(txHash, aTx);
  });

  it("single call with enough multiple signatures work", async () => {
    const aTx: GTX = await tx([{ opName: "func1åääö", args: [] }], signers(2), 2);
    const txHash: Buffer = getDigestToSign(aTx);
    test(txHash, aTx);
  });

  it("multiple calls with enough multiple signatures work", async () => {
    const aTx: GTX = await tx(
      [
        { opName: "func1åääö", args: [] },
        { opName: "guess_the_function", args: ["John Doe", 2222] },
      ],
      signers(3),
      3,
    );
    const txHash: Buffer = getDigestToSign(aTx);
    test(txHash, aTx);
  });
});
