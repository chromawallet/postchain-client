import { assert, expect } from "chai";
import {
  checkSignature,
  createPublicKey,
  makeKeyPair,
  makeTuid,
  randomBytes,
  sign,
  verifyKeyPair,
} from "../../../src/encryption/encryption";
import { toBuffer } from "../../../src/formatter";

describe("Encryption test", function () {
  describe("Random bytes test", function () {
    it("randomBytes returns random bytes, different every time", () => {
      const bytes1 = randomBytes(40);
      assert.equal(40, bytes1.length);
      const bytes2 = randomBytes(40);
      assert.equal(40, bytes2.length);
      assert.ok(!bytes1.equals(bytes2));
    });

    it("randomBytes returns empty buffer of size 0", () => {
      expect(randomBytes(0).length).to.eql(0);
    });

    it("randomBytes throws on negative size", () => {
      assert.throws(() => {
        randomBytes(-1);
      }, Error);
    });

    it("randomBytes on non-integer round up to closer lower integer", () => {
      assert.equal(3, randomBytes(3.14).length);
    });
  });

  describe("Public key tests", function () {
    it("createPublicKey creates a valid secp256k1 pubKey", () => {
      const privKey = toBuffer("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      const content = toBuffer("hello");
      const pubKey = createPublicKey(privKey);
      const signature = sign(content, privKey);
      assert.ok(checkSignature(content, pubKey, signature));
    });

    it("createPublicKey throws on invalid privKey", () => {
      assert.throws(() => {
        createPublicKey(randomBytes(0));
      });
      assert.throws(() => {
        createPublicKey(randomBytes(31));
      });
      assert.throws(() => {
        createPublicKey(randomBytes(33));
      });
    });
  });

  describe("Tuid tests", function () {
    it("makes a tuid", () => {
      const tuid = makeTuid();
      assert.equal(toBuffer(tuid).length, 16);
      assert.equal(tuid.length, 32);
    });
  });

  describe("Key pair tests", function () {
    it("generates key pairs", () => {
      const user = makeKeyPair();
      assert.ok(user.privKey);
      assert.ok(user.pubKey);
    });

    it("generates key pairs from private key", () => {
      const privKey = toBuffer("0101010101010101010101010101010101010101010101010101010101010101");
      const user = makeKeyPair(privKey);
      const userVerify = verifyKeyPair(privKey);
      expect(user.pubKey).to.deep.equal(userVerify.pubKey);
      expect(user.privKey).to.deep.equal(privKey);
    });

    it("verifies the authenticity of a private key", () => {
      const user = {
        pubKey: toBuffer("031b84c5567b126440995d3ed5aaba0565d71e1834604819ff9c17f5e9d5dd078f"),
        privKey: toBuffer("0101010101010101010101010101010101010101010101010101010101010101"),
      };
      const verifiedUser = verifyKeyPair(
        toBuffer("0101010101010101010101010101010101010101010101010101010101010101"),
      );
      expect(JSON.stringify(user)).to.be.equal(JSON.stringify(verifiedUser));
    });
  });
});
