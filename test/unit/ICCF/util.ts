import { BlockHeader } from "../../../src/ICCF/types";
import { encodeValue } from "../../../src/gtx/serialization";
import { RawGtv } from "../../../src/gtv/types";
import { RawGtx } from "../../../src/gtx/types";
import { mockThirtyTwoBytesBuffer } from "../common/mocks";

export function createHeaderArray() {
  const blockHeader: BlockHeader = {
    gtvBlockchainRid: mockThirtyTwoBytesBuffer,
    gtvPreviousBlockRid: mockThirtyTwoBytesBuffer,
    gtvMerkleRootHash: mockThirtyTwoBytesBuffer,
    gtvTimestamp: 1,
    gtvHeight: 0,
    gtvDependencies: null,
    gtvExtra: { config_hash: mockThirtyTwoBytesBuffer },
  };
  return Object.values(blockHeader);
}
export function createEncodedBlockHeaderArray() {
  return encodeValue(createHeaderArray());
}

export function createRawGtx(opname: string, args: RawGtv, signers: Buffer[] = []): RawGtx {
  return [[mockThirtyTwoBytesBuffer, [[opname, [args]]], signers], []];
}
