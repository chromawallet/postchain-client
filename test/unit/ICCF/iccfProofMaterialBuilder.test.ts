import sinon from "sinon";
import { expect } from "chai";

import * as iccf from "../../../src/ICCF/IccfProofTxMaterialBuilder";
import * as bc from "../../../src/blockchainClient/blockchainClient";
import * as utils from "../../../src/ICCF/utils";
import { ResponseObjectClusterInfo } from "../../../src/ICCF/types";
import { encodeValue, encodeValueGtx } from "../../../src/gtx/serialization";
import { createEncodedBlockHeaderArray, createRawGtx } from "./util";
import { gtvHash } from "../../../src/gtv";
import { rawGtxToGtx, toBuffer } from "../../../src/formatter";
import { getDigestToSign } from "../../../src/gtx/gtx";
import { BlockAnchoringException } from "../../../src/ICCF/error";
import { IClient } from "../../../src/blockchainClient/interface";
import { FailoverStrategy } from "../../../src/blockchainClient/enums";
import { createNodeManager } from "../../../src/restclient/nodeManager";
import {
  mockAnchoringTransaction,
  mockStringDirectoryChainRid,
  mockThirtyTwoBytesBuffer,
} from "../common/mocks";
import { setStatusPolling } from "../../../src/blockchainClient/utils";

const txToProveHashMismatch = Buffer.from("sample-tx-hash");

const sourceBlockchainRid = "FCD29927AA06E75547036C8860633AF2D8DF8FF6C78F947A2D7C816CB13DC209";
const targetBlockchainRid = "FCD29927AA06E75547036C8860633AF2D8DF8FF6C78F947A2D7C816CB13DC208";
const iccfTxSigners = [Buffer.from("iccf-pubKey1"), Buffer.from("iccf-pubKey2")];

const responseObjectAnchoringProof = {
  hash: Buffer.from("anchoring-hash"),
  blockHeader: Buffer.from("anchoring-header"),
  witness: Buffer.from("witness"),
  merkleProofTree: ["merkle"],
  txIndex: 0,
};

const anchoringChainInfo: ResponseObjectClusterInfo = {
  anchoring_chain: Buffer.from("sample-anchoring-chain"),
  name: "anchoring chain",
  peers: [{ pubkey: Buffer.from("a"), api_url: "urls" }],
};
const mockGetConfirmationProof = sinon.stub();
const mockQuery = sinon.stub();
const mockSignTransaction = sinon.stub();
const mockSendTransaction = sinon.stub();
const mockSignAndSendTransaction = sinon.stub();
const mockGetTransaction = sinon.stub();
const mockGetTransactionStatus = sinon.stub();
const mockSignAndSendUniqueTransaction = sinon.stub();
const mockAddNop = sinon.stub();
const mockGetTransactionRID = sinon.stub();
const mockGetTransactionsInfo = sinon.stub();
const mockGetTransactionInfo = sinon.stub();
const mockGetTransactionCount = sinon.stub();
const mockGetBlockInfo = sinon.stub();
const mockGetLatestBlock = sinon.stub();
const mockGetBlocksInfo = sinon.stub();
const mockEncodeTransaction = sinon.stub();
const mockDecodeTransactionToGTX = sinon.stub();
const mockGetClientNodeURLPool = sinon.stub();
const mockGetClusterAnchoringTransactionConfirmation = sinon.stub();
const mockGetTransactionConfirmationLevel = sinon.stub();
const mockGetSystemAnchoringTransactionConfirmation = sinon.stub();
const mockGetAnchoringStatusForBlockRid = sinon.stub();

export const clientConfiguredToD1: IClient = {
  config: {
    nodeManager: createNodeManager({
      nodeUrls: ["url1"],
    }),
    endpointPool: [{ url: "url", whenAvailable: 0 }],
    blockchainRid: "aa",
    dappStatusPolling: setStatusPolling({ interval: 500, count: 1 }),
    clusterAnchoringStatusPolling: setStatusPolling(),
    systemAnchoringStatusPolling: setStatusPolling(),
    failoverStrategy: FailoverStrategy.AbortOnError,
    attemptsPerEndpoint: 3,
    attemptInterval: 3,
    unreachableDuration: 100,
    directoryChainRid: mockStringDirectoryChainRid,
  },
  query: mockQuery,
  signTransaction: mockSignTransaction,
  sendTransaction: mockSendTransaction,
  getTransaction: mockGetTransaction,
  getTransactionStatus: mockGetTransactionStatus,
  signAndSendUniqueTransaction: mockSignAndSendUniqueTransaction,
  addNop: mockAddNop,
  getTransactionsInfo: mockGetTransactionsInfo,
  getTransactionInfo: mockGetTransactionInfo,
  getTransactionCount: mockGetTransactionCount,
  getTransactionRid: mockGetTransactionRID,
  getBlockInfo: mockGetBlockInfo,
  getLatestBlock: mockGetLatestBlock,
  getBlocks: mockGetBlocksInfo,
  getBlocksInfo: mockGetBlocksInfo,
  encodeTransaction: mockEncodeTransaction,
  decodeTransactionToGtx: mockDecodeTransactionToGTX,
  getClientNodeUrlPool: mockGetClientNodeURLPool,
  getConfirmationProof: mockGetConfirmationProof,
  getClusterAnchoringTransactionConfirmation: mockGetClusterAnchoringTransactionConfirmation,
  getTransactionConfirmationLevel: mockGetTransactionConfirmationLevel,
  getSystemAnchoringTransactionConfirmation: mockGetSystemAnchoringTransactionConfirmation,
  getAppStructure: sinon.stub(),
  getAnchoringStatusForBlockRid: mockGetAnchoringStatusForBlockRid,
};

let mockGetClusterOfBlockchain = sinon.stub();
let mockGetAnchoringTransactionForBlockRid = sinon.stub();
let mockAwaitGetAnchoringTransactionForBlockRid = sinon.stub();
let mockGetClusterInfo = sinon.stub();
describe("iccfProofTxMaterialBuilder", async () => {
  beforeEach(() => {
    mockGetAnchoringTransactionForBlockRid = sinon.stub(
      utils,
      "getAnchoringTransactionForBlockRid",
    );
    mockAwaitGetAnchoringTransactionForBlockRid = sinon.stub(
      utils,
      "awaitGetAnchoringTransactionForBlockRid",
    );

    mockGetClusterOfBlockchain = sinon.stub(utils, "getClusterOfBlockchain");

    mockGetClusterInfo = sinon.stub(utils, "getClusterInfo");

    const mockCreateClient = sinon.stub(bc, "createClient");
    mockCreateClient.resolves(clientConfiguredToD1);

    mockGetClusterInfo.resolves(anchoringChainInfo);
  });
  afterEach(() => {
    sinon.resetHistory();
    sinon.restore();
    mockGetConfirmationProof.reset();
    mockGetTransaction.reset();
    mockGetClusterInfo.reset();
    mockGetAnchoringTransactionForBlockRid.reset();
  });
  describe("createIccfProofTx", async () => {
    it("should return iccfTx for intra-cluster operation", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);
      const txToProveSigners: Buffer[] = [];

      // Later: input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-source-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);

      const result = await iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        proofHash,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      expect(result).to.have.property("iccfTx");
      expect(result).to.not.have.property("verifiedTx");

      expect(result.iccfTx.operations[0].name).to.equal("iccf_proof");
      const args = result.iccfTx?.operations?.[0]?.args || [];
      const expectedArgs = [
        toBuffer(sourceBlockchainRid),
        proofHash,
        encodeValue(confirmationProofResponse),
      ];

      for (let i = 0; i < args?.length; i++) {
        expect(args[i]).to.deep.equal(expectedArgs[i]);
      }
    });
    it("should return iccfTx for intra-cluster operation, mismatching", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);

      const txToProveSigners: Buffer[] = [];

      // Later: input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-source-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);
      mockGetTransaction.resolves(encodeValueGtx(rawGtx));

      const result = await iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        txToProveHashMismatch,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      expect(result).to.have.property("iccfTx");
      expect(result).to.not.have.property("verifiedTx");

      expect(result.iccfTx.operations[0].name).to.equal("iccf_proof");
      const args = result.iccfTx?.operations?.[0]?.args || [];
      const expectedArgs = [
        toBuffer(sourceBlockchainRid),
        proofHash,
        encodeValue(confirmationProofResponse),
      ];

      for (let i = 0; i < args?.length; i++) {
        expect(args[i]).to.deep.equal(expectedArgs[i]);
      }
    });
    it("should return iccfTx for intra-network operation", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);
      const txToProveSigners: Buffer[] = [];

      // input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-target-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);
      mockGetConfirmationProof.onCall(1).resolves(responseObjectAnchoringProof);
      mockAwaitGetAnchoringTransactionForBlockRid.resolves(mockAnchoringTransaction);

      mockGetTransaction.resolves(encodeValueGtx(rawGtx));

      const result = await iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        proofHash,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      expect(result).to.have.property("iccfTx");
      expect(result.iccfTx).to.have.property("operations");
      expect(result.iccfTx.operations[0].name).to.equal("iccf_proof");

      const args = result.iccfTx?.operations?.[0]?.args || [];
      const expectedArgs = [
        toBuffer(sourceBlockchainRid),
        proofHash,
        encodeValue(confirmationProofResponse),
        mockAnchoringTransaction.txData,
        mockAnchoringTransaction.txOpIndex,
        encodeValue(responseObjectAnchoringProof),
      ];

      for (let i = 0; i < args?.length; i++) {
        expect(args[i]).to.deep.equal(expectedArgs[i]);
      }

      expect(result).to.have.property("verifiedTx");
      expect(result.verifiedTx).to.equal(null);
    });
    it("should return iccfTx for intra-network operation, mismatch", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);
      const txToProveSigners: Buffer[] = [];

      // input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-target-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);
      mockGetConfirmationProof.onCall(1).resolves(responseObjectAnchoringProof);
      mockAwaitGetAnchoringTransactionForBlockRid.resolves(mockAnchoringTransaction);

      mockGetTransaction.resolves(encodeValueGtx(rawGtx));

      const result = await iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        txToProveHashMismatch,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      expect(result).to.have.property("iccfTx");
      expect(result.iccfTx.operations[0].name).to.equal("iccf_proof");

      const args = result.iccfTx?.operations?.[0]?.args || [];
      const expectedArgs = [
        toBuffer(sourceBlockchainRid),
        proofHash,
        encodeValue(confirmationProofResponse),
        mockAnchoringTransaction.txData,
        mockAnchoringTransaction.txOpIndex,
        encodeValue(responseObjectAnchoringProof),
      ];

      for (let i = 0; i < args?.length; i++) {
        expect(args[i]).to.deep.equal(expectedArgs[i]);
      }

      expect(result).to.have.property("verifiedTx");
      expect(result.verifiedTx).to.deep.equal(gtx);
    });

    it("should throw a BlockAnchoringException if clusterAnchoredTxResponse is invalid", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);
      const txToProveSigners: Buffer[] = [];

      // input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-target-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);
      mockGetConfirmationProof.onCall(1).resolves(responseObjectAnchoringProof);
      mockGetAnchoringTransactionForBlockRid.resolves(null);

      mockGetTransaction.resolves(encodeValueGtx(rawGtx));

      const result = iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        txToProveHashMismatch,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      await expect(result).to.be.rejectedWith(BlockAnchoringException);
    });

    it("should throw an error when block is not present in cluster anchoring chain", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      const gtx = rawGtxToGtx(rawGtx);
      const txToProveRID = getDigestToSign(gtx);
      const txToProveSigners: Buffer[] = [];

      mockGetAnchoringTransactionForBlockRid.resolves(undefined);
      // Later: input instead of on call
      mockGetClusterOfBlockchain.onCall(0).resolves("sample-source-cluster");
      mockGetClusterOfBlockchain.onCall(1).resolves("sample-target-cluster");
      mockGetConfirmationProof.onCall(0).resolves(confirmationProofResponse);
      mockGetConfirmationProof.onCall(1).resolves(responseObjectAnchoringProof);

      mockGetTransaction.resolves(encodeValueGtx(rawGtx));
      const promise = iccf.createIccfProofTx(
        clientConfiguredToD1,
        txToProveRID,
        txToProveHashMismatch,
        txToProveSigners,
        sourceBlockchainRid,
        targetBlockchainRid,
        iccfTxSigners,
        false,
      );

      await expect(promise).to.be.rejectedWith("Block is not present in cluster anchoring chain");
    });
  });

  describe("isBlockAnchored", async () => {
    it("returns true if anchoringTx exists");
    it("returns false if no anchoringTx exists");
  });

  describe("getBlockAnchoringTransaction", async () => {
    it("returns anchoringTransaction", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      mockAwaitGetAnchoringTransactionForBlockRid.resolves(mockAnchoringTransaction);
      mockGetConfirmationProof.resolves(confirmationProofResponse);
      const anchoringTransactionResult = await iccf.getBlockAnchoringTransaction(
        clientConfiguredToD1,
        clientConfiguredToD1,
        mockThirtyTwoBytesBuffer,
      );
      expect(anchoringTransactionResult).to.deep.equal(mockAnchoringTransaction);
    });
    it("should handle null response with confirmationProof", async () => {
      const rawGtx = createRawGtx("test", 0);
      const proofHash = gtvHash(rawGtx);
      const confirmationProofResponse = {
        hash: proofHash,
        blockHeader: createEncodedBlockHeaderArray(),
      };
      mockAwaitGetAnchoringTransactionForBlockRid.resolves(null);
      mockGetConfirmationProof.resolves(confirmationProofResponse);
      const promise = await iccf.getBlockAnchoringTransaction(
        clientConfiguredToD1,
        clientConfiguredToD1,
        mockThirtyTwoBytesBuffer,
      );
      expect(promise).to.deep.equal(null);
    });
    it("should throw an error if neither a txRid or TxProof is provided as input", async () => {
      const promise = iccf.getBlockAnchoringTransaction(clientConfiguredToD1, clientConfiguredToD1);
      await expect(promise).to.be.rejectedWith("Missing a txRid or TxProof");
    });
    it("should return null if confirmationProof is missing", async () => {
      mockGetConfirmationProof.resolves(undefined);
      const result = iccf.getBlockAnchoringTransaction(
        clientConfiguredToD1,
        clientConfiguredToD1,
        mockThirtyTwoBytesBuffer,
      );

      expect(result).to.be.rejectedWith("Confirmation proof not found");
    });
  });
  describe("getAnchoringClient", async () => {
    it("return a client when inserting a blockchain RID", async () => {
      mockGetClusterOfBlockchain.resolves("source-cluster");

      const result = await iccf.getAnchoringClient(
        clientConfiguredToD1,
        mockStringDirectoryChainRid,
      );
      expect(result).to.have.property("config");
    });
    it("return a client when inserting a cluster name", async () => {
      const result = await iccf.getAnchoringClient(
        clientConfiguredToD1,
        undefined,
        "source-cluster",
      );
      expect(result).to.have.property("config");
    });
    it("should throw an error if neither blockchainRID or cluster name is provided", async () => {
      const promise = iccf.getAnchoringClient(clientConfiguredToD1);
      await expect(promise).to.be.rejectedWith("Missing a dapp blockchainRid or cluster name");
    });
    it("should throw an error if no cluster could be found", async () => {
      mockGetClusterOfBlockchain.resolves(undefined);

      const promise = iccf.getAnchoringClient(clientConfiguredToD1, mockStringDirectoryChainRid);
      await expect(promise).to.be.rejectedWith("No cluster could be found");
    });
    it("should throw an error if no cluster info could be found", async () => {
      mockGetClusterOfBlockchain.resolves("sample-source-cluster");
      mockGetClusterInfo.resolves(undefined);

      const promise = iccf.getAnchoringClient(clientConfiguredToD1, mockStringDirectoryChainRid);
      await expect(promise).to.be.rejectedWith("Cluster info could not be found");
    });
    it("should set input client network settings as anchoring client network settings", async () => {
      mockGetClusterOfBlockchain.resolves("source-cluster");
      const result = await iccf.getAnchoringClient(
        clientConfiguredToD1,
        mockStringDirectoryChainRid,
      );

      expect(clientConfiguredToD1.config).to.deep.equal(result.config);
    });
  });
});
