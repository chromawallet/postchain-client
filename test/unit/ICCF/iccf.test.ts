import { Buffer } from "buffer";
import { expect } from "chai";
import { describe } from "mocha";
import {
  DifferentNumberOfSignersException,
  MissingTransactionProof,
  ProofRidException,
  SignatureException,
  SystemChainException,
} from "../../../src/ICCF/error";
import {
  calculateBlockRID,
  fetchAndVerifyTransaction,
  getAnchoringTransactionForBlockRid,
  getClusterInfo,
  getClusterOfBlockchain,
} from "../../../src/ICCF/utils";
import { createClient } from "../../../src/blockchainClient/blockchainClient";
import { ConfirmationProof } from "../../../src/blockchainClient/types";
import { IClient } from "../../../src/blockchainClient/interface";
import { toBuffer } from "../../../src/formatter";
import { gtvHash } from "../../../src/gtv";
import { encodeValue, encodeValueGtx } from "../../../src/gtx/serialization";
import { getDigestToSign, gtxToRawGtx, sign } from "../../../src/gtx/gtx";
import { GTX, RellOperation } from "../../../src/gtx/types";
import { makeKeyPair } from "../../../src/encryption/encryption";
import { KeyPair } from "../../../src/encryption/types";
import { UnexpectedStatusError } from "../../../src/restclient/errors";
import { createHeaderArray } from "./util";
import { HttpResponse, http } from "msw";
import { server } from "../../../mocks/servers";
import { formatter } from "../../..";
import { domain } from "../../../mocks/handlers";
import {
  contentTypes,
  mockBufferBlockchainRid,
  mockStringBlockchainRid,
  mockThirtyTwoBytesBuffer,
} from "../common/mocks";

function encodeGtxToBuffer(gtx: GTX): Buffer {
  return encodeValueGtx(gtxToRawGtx(gtx));
}

async function createGtxObject(keypairs: KeyPair[], op?: RellOperation[]): Promise<GTX> {
  let gtx: GTX = {
    blockchainRid: toBuffer("fcd29927aa06e75547036c8860633af2d8df8ff6c78f947a2d7c816cb13dc201"),
    operations:
      op === undefined
        ? [
            {
              opName: "setInteger",
              args: ["126d2"],
            },
          ]
        : op,
    signers: keypairs.map(key => key.pubKey),
    signatures: [],
  };

  keypairs.forEach(async key => {
    gtx = await sign(gtx, key.privKey, key.pubKey);
  });

  return gtx;
}

const privkeyA = toBuffer("0101010101010101010101010101010101010101010101010101010101010101");
const keypairA = makeKeyPair(privkeyA);

const privkeyB = toBuffer("2222222222222222222222222222222222222222222222222222222222222222");
const keypairB = makeKeyPair(privkeyB);

let client: IClient;
describe("ICCF tests", function () {
  beforeEach(async function () {
    server.listen();
    client = await createClient({
      nodeUrlPool: [domain],
      blockchainRid: mockStringBlockchainRid,
      failOverConfig: { attemptInterval: 100 },
    });
  });

  afterEach(() => {
    server.resetHandlers();
  });

  const mockTransactionRequest = (rid: Buffer, gtxResponseTx: GTX) => {
    server.use(
      http.get(`${domain}/tx/${client.config.blockchainRid}/${rid.toString("hex")}`, () => {
        return HttpResponse.json({
          tx: encodeGtxToBuffer(gtxResponseTx).toString("hex").toUpperCase(),
        });
      }),
    );
  };

  const mockTransactionErrorOnRequest = (rid: Buffer, error: string) => {
    server.use(
      http.get(`${domain}/tx/${client.config.blockchainRid}/${rid.toString("hex")}`, () => {
        return new HttpResponse(error, {
          status: 404,
        });
      }),
    );
  };

  describe("fetchAndVerifyTransaction", async () => {
    it("should fetch and verify transaction", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const rid = getDigestToSign(gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      mockTransactionRequest(rid, gtxResponseTx);

      const { verifiedTxHash } = await fetchAndVerifyTransaction(client, rid, txProofHash, [
        keypairA.pubKey,
      ]);

      expect(verifiedTxHash).to.deep.equal(txProofHash);
    });

    it("throws error if fetched tx hash does not match hash from proof", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      mockTransactionRequest(mockThirtyTwoBytesBuffer, gtxResponseTx);
      const proofHash = Buffer.alloc(32, "a");

      try {
        await fetchAndVerifyTransaction(client, mockThirtyTwoBytesBuffer, proofHash, [
          keypairA.pubKey,
        ]);
        expect.fail("Expected an error to be thrown");
      } catch (error) {
        expect(error.message).to.contain(
          "Unable to verify source transaction proof,transaction hash in proof 6161616161616161616161616161616161616161616161616161616161616161 does not match hash from fetched transaction",
        );
      }
    });

    it("throws error if fetched tx number of signatures does not match expected list of signers", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      mockTransactionRequest(mockThirtyTwoBytesBuffer, gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      try {
        await fetchAndVerifyTransaction(client, mockThirtyTwoBytesBuffer, txProofHash, []);

        expect.fail("Expected an error to be thrown");
      } catch (error) {
        expect(error.message).to.contain(
          "Transaction signatures amount 1 do not match expected amount of signers 0",
        );
      }
    });

    it("Throws error transaction rid can not be found", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const rid = getDigestToSign(gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      mockTransactionErrorOnRequest(
        rid,
        `{ error: Can't find transaction with RID: ${rid.toString("hex")} }`,
      );

      let error;
      try {
        await fetchAndVerifyTransaction(client, rid, txProofHash, [keypairA.pubKey]);
      } catch (err) {
        error = err;
      }

      expect(error).to.be.instanceOf(UnexpectedStatusError);
    });

    it("should fetch and verify transaction incorrect order of signatures still okay", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA, keypairB]);
      const rid = getDigestToSign(gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      mockTransactionRequest(rid, gtxResponseTx);

      const { verifiedTxHash, verifiedTx } = await fetchAndVerifyTransaction(
        client,
        rid,
        txProofHash,
        [keypairB.pubKey, keypairA.pubKey],
      );

      expect(verifiedTxHash).to.deep.equal(txProofHash);
      expect(verifiedTx).to.deep.equal(gtxResponseTx);
    });

    it("should throw MissingTransactionProof when fetchedTxHash is not equal to proofHash", async () => {
      const proofHash = Buffer.from("SomeDifferentHash");
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const rid = getDigestToSign(gtxResponseTx);

      mockTransactionRequest(rid, gtxResponseTx);

      let error;
      try {
        await fetchAndVerifyTransaction(client, rid, proofHash, [keypairA.pubKey]);
      } catch (err) {
        error = err;
      }

      expect(error).to.be.instanceOf(MissingTransactionProof);
    });

    it("should throw DifferentNumberOfSignersException when the number of signatures does not match", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const rid = getDigestToSign(gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      mockTransactionRequest(rid, gtxResponseTx);

      let error;
      try {
        await fetchAndVerifyTransaction(client, rid, txProofHash, [
          keypairA.pubKey,
          keypairB.pubKey,
        ]);
      } catch (err) {
        error = err;
      }

      expect(error).to.be.instanceOf(DifferentNumberOfSignersException);
    });

    it("should throw SignatureException when expected signer (signerB) did not sign the transaction", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const rid = getDigestToSign(gtxResponseTx);
      const txProofHash = gtvHash(gtxToRawGtx(gtxResponseTx));

      mockTransactionRequest(rid, gtxResponseTx);

      let error;
      try {
        await fetchAndVerifyTransaction(client, rid, txProofHash, [keypairB.pubKey]);
      } catch (err) {
        error = err;
      }

      expect(error).to.be.instanceOf(SignatureException);
    });

    it("should throw ProofRidException when the fetched transaction RID is different", async () => {
      const gtxResponseTx: GTX = await createGtxObject([keypairA]);
      const gtxFalseResponseTx: GTX = await createGtxObject(
        [keypairA],
        [
          {
            opName: "fakeOperation",
            args: ["foo"],
          },
        ],
      );

      const rid = getDigestToSign(gtxResponseTx);

      mockTransactionRequest(rid, gtxFalseResponseTx);

      let error;
      try {
        await fetchAndVerifyTransaction(client, rid, gtvHash(gtxToRawGtx(gtxFalseResponseTx)), [
          keypairA.pubKey,
        ]);
      } catch (err) {
        error = err;
      }
      expect(error).to.be.instanceOf(ProofRidException);
    });
  });

  describe("getClusterOfBlockchain", () => {
    it("returns cluster name where a blockchain runs", async () => {
      const serverReturnCluster = "system";

      server.use(
        http.post(`${domain}/query_gtv/${mockStringBlockchainRid}`, () => {
          return HttpResponse.arrayBuffer(encodeValue(serverReturnCluster), {
            status: 200,
            headers: {
              "Content-Type": contentTypes.octetStream,
            },
          });
        }),
      );

      const res = await getClusterOfBlockchain(client, mockBufferBlockchainRid);

      expect(res).to.equal(serverReturnCluster);
    });

    it("should throw error if query to get cluster fails", async () => {
      const serverReturn = formatter.toBuffer(`{"error": "Missing query type"}`);

      server.use(
        http.post(`${domain}/query_gtv/${mockStringBlockchainRid}`, () => {
          return new HttpResponse(`{ message: ${serverReturn}}`, {
            status: 400,
          });
        }),
      );

      let error;
      try {
        await getClusterOfBlockchain(client, mockBufferBlockchainRid);
      } catch (err) {
        error = err;
      }
      expect(error).to.be.instanceOf(SystemChainException);
      expect(error.message).to.contain(serverReturn);
    });
  });

  describe("getClusterInfo", () => {
    it("should throw error if query to get cluster info fails", async () => {
      const serverReturn = formatter.toBuffer(`{"error": "Missing query type"}`);

      server.use(
        http.post(`${domain}/query_gtv/${mockStringBlockchainRid}`, () => {
          return new HttpResponse(`{ message: ${serverReturn}}`, {
            status: 400,
          });
        }),
      );

      let error;
      try {
        await getClusterInfo(client, "system");
      } catch (err) {
        error = err;
      }
      expect(error).to.be.instanceOf(SystemChainException);
      expect(error.message).to.contain(serverReturn);
    });
  });

  describe("getAnchoringTransactionForBlockRid", () => {
    it("should throw error if query to get anchoring tx fails", async () => {
      const serverReturn = formatter.toBuffer(`{"error": "Missing query type"}`);

      server.use(
        http.post(`${domain}/query_gtv/${mockStringBlockchainRid}`, () => {
          return new HttpResponse(`{ message: ${serverReturn}}`, {
            status: 400,
          });
        }),
      );

      let error;
      try {
        await getAnchoringTransactionForBlockRid(
          client,
          mockBufferBlockchainRid,
          mockThirtyTwoBytesBuffer,
        );
      } catch (err) {
        error = err;
      }
      expect(error).to.be.instanceOf(SystemChainException);
      expect(error.message).to.contain(serverReturn);
    });
  });

  describe("calculateBlockRID", () => {
    it("calculates block rid from a decoded proof ", () => {
      const arrayHeader = createHeaderArray();
      const decodedTxProof = {
        blockHeader: encodeValue(arrayHeader),
      } as unknown as ConfirmationProof;
      const sourceBlockRid = calculateBlockRID(decodedTxProof);

      expect(sourceBlockRid).to.deep.equal(gtvHash(arrayHeader));
    });

    it("throws an error when blockHeader is undefined", () => {
      const decodedTxProof = {
        blockHeader: undefined,
      } as unknown as ConfirmationProof;
      expect(() => {
        calculateBlockRID(decodedTxProof);
      }).to.throw("Failed to get blockHeader from confirmation proof");
    });

    it("throws an error when block header can not be decoded", () => {
      const decodedTxProof = {
        blockHeader: mockThirtyTwoBytesBuffer,
      } as ConfirmationProof;

      expect(() => {
        calculateBlockRID(decodedTxProof);
      }).to.throw("Choice not matched at: (shallow)");
    });
  });
});
