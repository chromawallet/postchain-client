import {
  checkGtvType,
  ensureBuffer,
  ensureString,
  matchRellErrorString,
  removeDuplicateSigners,
  toBuffer,
  toQueryObjectGTV,
} from "../../src/formatter";
import { expect } from "chai";
import { Buffer } from "buffer";
import { QueryObject, QueryObjectGTV } from "../../src/restclient/types";
import { mockHexStringOfThirtyTwoBytesBuffer, mockThirtyTwoBytesBuffer } from "./common/mocks";

const gtvValues = [
  { typeName: "null", value: null },
  { typeName: "bytearray", value: Buffer.from("test buffer") },
  { typeName: "string", value: "test string" },
  { typeName: "integer", value: 1 },
  { typeName: "dict", value: { name: "test", value: "test string" } },
  {
    typeName: "array",
    value: ["test string", [1, "test string 2"]],
  },
  { typeName: "emptyArray", value: [] },
  { typeName: "bigInteger", value: BigInt(9007199254740991) },
];

describe("Formatter tests", function () {
  describe("Buffer tests", function () {
    it("converts hex to Buffer", () => {
      const pubKey = mockHexStringOfThirtyTwoBytesBuffer;
      const bufferedPubKey = toBuffer(pubKey);
      expect(bufferedPubKey.toString("hex")).to.equal(pubKey);
    });
  });
  describe("Ensure Buffer", function () {
    it("returns Buffer when input is buffer", () => {
      const pubKey = mockThirtyTwoBytesBuffer;
      const bufferPubKey = ensureBuffer(pubKey);
      expect(bufferPubKey).to.be.an.instanceof(Buffer);
    });
    it("returns Buffer when input is string", () => {
      const pubKey = mockHexStringOfThirtyTwoBytesBuffer;
      const bufferPubKey = ensureBuffer(pubKey);
      expect(bufferPubKey).to.be.an.instanceof(Buffer);
    });
  });
  describe("Ensure String", () => {
    it("returns string when input is string", () => {
      const formattedStringValue = ensureString(mockHexStringOfThirtyTwoBytesBuffer);
      expect(formattedStringValue).to.be.a("string");
    });
    it("returns string when input is Buffer", () => {
      const formattedStringValue = ensureString(mockThirtyTwoBytesBuffer);
      expect(formattedStringValue).to.be.a("string");
    });
  });
  describe("Filter signers", function () {
    const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
    const signerPrivKeyB: Buffer = Buffer.alloc(32, "b");

    it("removes duplicate entries of signers", () => {
      const signers = [signerPrivKeyA, signerPrivKeyA, signerPrivKeyB];
      const filteredSigners = removeDuplicateSigners(signers);
      expect(filteredSigners).to.deep.equal([signerPrivKeyA, signerPrivKeyB]);
    });
  });
  describe("Check GTV type", function () {
    gtvValues.forEach(({ typeName, value }) => {
      it(`expect value to be of GTV type ${typeName}`, () => {
        expect(checkGtvType(value)).to.be.true;
      });
    });
    it(`expect boolean to not be of GTV type`, () => {
      const nonGtvValue = true;
      expect(checkGtvType(nonGtvValue)).to.be.false;
    });
  });

  describe("Format QueryObject", function () {
    it("format empty query", () => {
      const object: QueryObject = { type: "aQueryName" };
      const result: QueryObjectGTV = toQueryObjectGTV(object);
      expect(result).to.eql(["aQueryName", {}]);
    });

    it("format one arg query", () => {
      const object: QueryObject = { type: "aQueryName", argName: 1 };
      const result: QueryObjectGTV = toQueryObjectGTV(object);
      expect(result).to.eql(["aQueryName", { argName: 1 }]);
    });

    it("format two arg query", () => {
      const object: QueryObject = {
        type: "aQueryName",
        argName: 1,
        argName2: 2,
      };
      const result: QueryObjectGTV = toQueryObjectGTV(object);
      expect(result).to.eql(["aQueryName", { argName: 1, argName2: 2 }]);
    });
  });
  describe("Rell error matching function", function () {
    it("returns operation, rellLine and shortReason when string is a match", () => {
      const errorString =
        "[bc-rid=8E:DFB4, chain-id=0] Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";

      const result = matchRellErrorString(errorString);

      expect(result.operation).to.equal("news_feed_ch5.news_feed:make_post");
      expect(result.rellLine).to.equal("bc-rid=8E:DFB4, chain-id=0");
      expect(result.shortReason).to.equal(
        "Expected at least two operations, make sure that you included auth",
      );
    });
    it("returns operation, rellLine nad shortReason when string is a match, different string", () => {
      const errorString =
        "[bc-rindslhferuhb] Operation 'news_hciduhcidhws_feed:make_post' failed: Expec.rvblcs(dhs) sure that you included auth";

      const result = matchRellErrorString(errorString);

      expect(result.operation).to.equal("news_hciduhcidhws_feed:make_post");
      expect(result.rellLine).to.equal("bc-rindslhferuhb");
      expect(result.shortReason).to.equal("Expec.rvblcs(dhs) sure that you included auth");
    });
    it("returns undefined when string is not a match", () => {
      const errorString =
        "Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";

      const result = matchRellErrorString(errorString);

      expect(result.operation).to.be.undefined;
      expect(result.rellLine).to.be.undefined;
      expect(result.shortReason).to.be.undefined;
    });
  });
});
