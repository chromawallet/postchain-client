import { expect } from "chai";
import { spy } from "sinon";
import { debug, info, error, warning, setLogLevel, getLogLevel, logger } from "../../src/logger";
import { LogLevel } from "../../src/logger";

describe("logger", () => {
  let consoleSpy: any;

  beforeEach(() => {
    consoleSpy = spy(console, "log");
  });

  afterEach(() => {
    consoleSpy.restore();
  });

  it("should log debug message when log level is set to debug", () => {
    setLogLevel(LogLevel.Debug);
    debug("Debug message", "TestModule");
    expect(consoleSpy.calledOnce).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("TestModule")).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("Debug message")).to.be.true;
  });

  it("should NOT log debug message when log level is set to warning", () => {
    setLogLevel(LogLevel.Warning);
    debug("Debug message", "TestModule");
    expect(consoleSpy.called).to.be.false;
  });

  it("should log info message when log level is set to info", () => {
    setLogLevel(LogLevel.Info);
    info("Info message", "TestModule");
    expect(consoleSpy.calledOnce).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("TestModule")).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("Info message")).to.be.true;
  });

  it("should log error message when log level is set to error", () => {
    setLogLevel(LogLevel.Error);
    error("Error message", "TestModule");
    expect(consoleSpy.calledOnce).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("TestModule")).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("Error message")).to.be.true;
  });

  it("should log warning message when log level is set to warning", () => {
    setLogLevel(LogLevel.Warning);
    warning("Warning message", "TestModule");
    expect(consoleSpy.calledOnce).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("TestModule")).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("Warning message")).to.be.true;
  });

  it("should get the current log level", () => {
    setLogLevel(LogLevel.Debug);
    expect(getLogLevel()).to.equal(3);
  });

  it("should create a logger instance with the specified module name", () => {
    const log = logger("TestModule");
    log.debug("Debug message");
    expect(consoleSpy.calledOnce).to.be.true;
    expect(consoleSpy.firstCall.args[0].includes("TestModule")).to.be.true;
  });

  it("should only log messages for the specified log level: DEBUG", () => {
    setLogLevel(LogLevel.Debug);
    debug("Debug message");
    info("Info message", "TestModule");
    warning("Warning message", "TestModule");
    error("Error message", "TestModule");

    expect(consoleSpy.callCount).to.equal(4);
  });

  it("should only log messages for the specified log level: INFO", () => {
    setLogLevel(LogLevel.Info);
    debug("Debug message");
    info("Info message", "TestModule");
    warning("Warning message", "TestModule");
    error("Error message", "TestModule");

    expect(consoleSpy.callCount).to.equal(3);
  });

  it("should only log messages for the specified log level: WARNING", () => {
    setLogLevel(LogLevel.Warning);
    debug("Debug message");
    info("Info message", "TestModule");
    warning("Warning message", "TestModule");
    error("Error message", "TestModule");

    expect(consoleSpy.callCount).to.equal(2);
  });

  it("should only log messages for the specified log level: ERROR", () => {
    setLogLevel(LogLevel.Error);
    debug("Debug message");
    info("Info message", "TestModule");
    warning("Warning message", "TestModule");
    error("Error message", "TestModule");

    expect(consoleSpy.callCount).to.equal(1);
  });

  it("should only log messages for the specified log level: DISABLED", () => {
    setLogLevel(LogLevel.Disabled);
    debug("Debug message");
    info("Info message", "TestModule");
    warning("Warning message", "TestModule");
    error("Error message", "TestModule");

    expect(consoleSpy.callCount).to.equal(0);
  });
});
