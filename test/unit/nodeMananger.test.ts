import { expect } from "chai";
import * as nodeManagerModule from "../../src/restclient/nodeManager";

import sinon from "sinon";
import { mockNodeUrls } from "./common/mocks";

let sandbox: sinon.SinonSandbox;

describe("NodeManager", () => {
  const nodeUrls = ["http://localhost:3000", "http://localhost2:5000"];
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => {
    sandbox.restore();
  });

  describe("createNodeManager", () => {
    it('should return a NodeManager object when "createNodeManager" given correct params', () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: ["http://localhost:3000"],
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      expect(nodeManager).to.haveOwnProperty("nodes");
      expect(nodeManager).to.haveOwnProperty("stickedNode");
      expect(nodeManager).to.haveOwnProperty("getAvailableNodes");
      expect(nodeManager).to.haveOwnProperty("setStickyNode");
      expect(nodeManager).to.haveOwnProperty("getNode");
      expect(nodeManager).to.haveOwnProperty("makeNodeUnavailable");
    });

    it('should set "stickedNode" to equal null when "useStickyNode" is set to false', () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: ["http://localhost:3000"],
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      expect(nodeManager.stickedNode).to.equal(null);
    });
  });
  describe("getAvailableNodes", () => {
    it("should return an array of available nodes, without any unavailable nodes in it", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      const availableNodes = nodeManager.getAvailableNodes();

      expect(availableNodes).to.have.lengthOf(2);
      expect(availableNodes).to.have.deep.members([
        { url: nodeUrls[0], whenAvailable: 0, isAvailable: true },
        { url: nodeUrls[1], whenAvailable: 0, isAvailable: true },
      ]);
    });
    it("should return the array of nodes in a shuffled order if sticky node is not enabled", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: mockNodeUrls,
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      const availableNodesSet = [
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
      ];

      expect(availableNodesSet.every(set => set === availableNodesSet[0])).to.equal(false);
    });
    it("should return a shuffled array where the sticky node is first, if sticky node is enabled and one is set.", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: mockNodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      const availableNodesSet = [
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
      ];

      expect(availableNodesSet.every(set => set === availableNodesSet[0])).to.equal(false);

      const stickyNode = nodeManager.nodes[0];
      nodeManager.setStickyNode(stickyNode);

      const availableNodesSetWithStickyNode = [
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
        nodeManager.getAvailableNodes(),
      ];

      expect(availableNodesSetWithStickyNode.every(set => set[0] === stickyNode)).to.equal(true);
    });
  });
  describe("setStickyNode", () => {
    it("should update the given node to be the new sticky node.", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      expect(nodeManager.stickedNode).to.equal(null);
      nodeManager.setStickyNode(nodeManager.nodes[0]);
      expect(nodeManager.stickedNode).to.equal(nodeManager.nodes[0]);
    });
  });
  describe("getNode", () => {
    it("should return null when no nodes are available", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      sandbox.stub(nodeManager, "getAvailableNodes").returns([]);

      expect(nodeManager.getNode()).to.equal(null);
    });
    it("should return a random node when sticky node is disabled", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: false,
        unavailableDuration: 5000,
      });

      expect(nodeManager.getNode()).to.not.equal(null);
    });
    it("should return a random node and set it as the new sticky node if sticky node is enabled. ", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      expect(nodeManager.stickedNode).to.equal(null);
      const randomNode = nodeManager.getNode();
      expect(randomNode).to.not.equal(null);
      expect(nodeManager.stickedNode).to.equal(randomNode);
    });
    it("should return a the sticky node if sticky node is enabled and the node is available. ", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      const stickyNode = nodeManager.nodes[0];
      expect(nodeManager.stickedNode).to.equal(null);
      nodeManager.setStickyNode(stickyNode);
      expect(nodeManager.stickedNode).to.equal(stickyNode);
      expect(nodeManager.getNode()).to.equal(stickyNode);
    });
  });
  describe("makeAllNodesAvailable", () => {
    it("should set all nodes to available", async () => {
      const nodeUrls = [
        "http://localhost1:1000",
        "http://localhost2:2000",
        "http://localhost3:3000",
        "http://localhost4:4000",
        "http://localhost5:5000",
      ];
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      expect(nodeManager.getAvailableNodes().length).to.equal(5);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      nodeManager.makeNodeUnavailable(nodeManager.getNode().url);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      nodeManager.makeNodeUnavailable(nodeManager.getNode().url);
      expect(nodeManager.getAvailableNodes().length).to.equal(3);
      nodeManager.makeAllNodesAvailable();
      expect(nodeManager.getAvailableNodes().length).to.equal(5);
    });
  });
  describe("makeNodeUnavailable", () => {
    it("should set the sticky node to null if the node is the sticky node", () => {
      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: 5000,
      });

      const stickyNode = nodeManager.nodes[0];
      nodeManager.setStickyNode(stickyNode);
      expect(nodeManager.stickedNode).to.equal(stickyNode);
      nodeManager.makeNodeUnavailable(stickyNode.url);
      expect(nodeManager.stickedNode).to.equal(null);
    });

    it("should set the node with the provided node url, to unavailable for the configured duration", async () => {
      const nodeUrls = [
        "http://localhost:1000",
        "http://localhost2:2000",
        "http://localhost3:3000",
        "http://localhost4:4000",
      ];
      const unavailableDuration = 30000;

      const nodeManager = nodeManagerModule.createNodeManager({
        nodeUrls: nodeUrls,
        useStickyNode: true,
        unavailableDuration: unavailableDuration,
      });

      nodeManager.makeNodeUnavailable(nodeManager.nodes[0].url);

      const availableNodesByUrls = nodeManager.getAvailableNodes().map(node => node.url);

      expect(nodeManager.getAvailableNodes()).to.have.lengthOf(3);
      expect(availableNodesByUrls).to.not.include(nodeUrls[0]);
    });
  });
});
