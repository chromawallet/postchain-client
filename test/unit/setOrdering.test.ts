import { expect } from "chai";
import { gtvHash } from "../../src/gtv";

describe("Sets hashing", () => {
  it("comparison of set results", async () => {
    const set1: Set<string> = new Set(["a", "b"]);
    const set2: Set<string> = new Set(["b", "a"]);
    const set1Hashed = gtvHash(set1);
    const set2Hashed = gtvHash(set2);

    expect(set1Hashed.compare(set2Hashed)).to.equal(0);
    expect(set1Hashed.toString("hex").toUpperCase()).to.deep.equal(
      set2Hashed.toString("hex").toUpperCase(),
    );
  });
});
