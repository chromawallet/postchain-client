import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import * as secp256k1 from "secp256k1";
import { randomInt } from "crypto";
import * as sinon from "sinon";
import { Buffer } from "buffer";

import { createClient } from "../../src/blockchainClient/blockchainClient";
import { IClient } from "../../src/blockchainClient/interface";
import { signDigest } from "../../src/encryption/encryption";
import {
  KeyPair,
  QueryObject,
  Transaction,
  TransactionReceipt,
  StatusObject,
} from "../../src/blockchainClient/types";
import { getDigestToSign } from "../../src/gtx/gtx";
import { GTX, GtxBody, RawGtxBody } from "../../src/gtx/types";
import * as gtxTool from "../../src/gtx/gtx";
import * as handleRequestModule from "../../src/restclient/httpUtil";
import { ComplexArgumentObject } from "./testData";
import { toBuffer } from "../../src/formatter";
import { RawGtv } from "../../src/gtv/types";
import { MissingBlockchainIdentifierError } from "../../src/blockchainClient/errors";
import { ChainConfirmationLevel, ResponseStatus, TransactionEvent } from "../../src/blockchainClient/enums";
import {
  CONFIGURED_POLL_COUNT,
  exampleOperation,
  mockStringDirectoryChainRid,
  mockThirtyTwoBytesBuffer,
} from "../unit/common/mocks";
import { setStatusPolling } from "../../src/blockchainClient/utils";
import { LOCAL_POOL } from "../../src/constants";

const expect = chai.expect;
chai.use(chaiAsPromised);

const invalidEndpointPool = [
  "http://localhost1:7740",
  "http://localhost2:7740",
  "http://localhost3:7740",
];

const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
const signerPubKeyA: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyA));
const signerKeyPairA: KeyPair = {
  privKey: signerPrivKeyA,
  pubKey: signerPubKeyA,
};
const sigProvA = {
  pubKey: signerPubKeyA,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    return signDigest(digest, signerPrivKeyA);
  },
};

const signerPrivKeyB: Buffer = Buffer.alloc(32, "b");
const signerPubKeyB: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyB));
const signerKeyPairB = {
  privKey: signerPrivKeyB,
  pubKey: signerPubKeyB,
};
const unsignedNonUniqueTx = {
  operations: [
    {
      name: "setInteger",
      args: [1],
    },
  ],
  signers: [signerPubKeyA],
};

const unsignedNonUniqueTxWithTwoSigners = {
  operations: [
    {
      name: "setInteger",
      args: [1],
    },
  ],
  signers: [signerPubKeyA, signerPubKeyB],
};

const signedNonUniqueTxHex =
  "a581bb3081b8a56e306ca1220420bfb34abea1045bb97ed18a5dbb2b8dce84cb181585d55abb3" +
  "21a33133dba0595a51d301ba5193017a20c0c0a736574496e7465676572a5073005a303020101" +
  "a5273025a123042102e5a018b3a2e155316109d9cdc5eab739759c0e07e0c00bf9fccb8237fe4" +
  "d7f02a5463044a1420440eb6c468bc2693a218a71e7b50465ba08343780d0829ec0132337cf8b" +
  "293d841e09ac203ef0200ac7641a359a98ba1b37baace48fd96c21f7870e197f3246de0b";

const unsignedTx = {
  operations: [
    {
      name: "setInteger",
      args: [randomInt(99999)],
    },
  ],
  signers: [signerPubKeyA],
};

const getTxWithGTXAndTxRID = (client: IClient, signers: Buffer[] = []) => {
  const operation = {
    name: "setInteger",
    args: [randomInt(99999)],
  };

  const gtxBody: GtxBody = {
    blockchainRid: toBuffer(client.config.blockchainRid),
    operations: [
      {
        opName: operation.name,
        args: operation.args,
      },
    ],
    signers,
  };
  const gtx: GTX = {
    ...gtxBody,
    signatures: [],
  };
  const tx: Transaction = {
    operations: [operation],
    signers,
  };
  const txRID = getDigestToSign(gtx);

  return { operation, gtx, txRID, tx };
};

describe("Blockchain client", () => {
  let client: Promise<IClient>;
  let faultyClient: Promise<IClient>;
  let stableConfigClient: Promise<IClient>;
  let handleRequestSpy: sinon.SinonSpy;

  before(done => {
    faultyClient = createClient({
      nodeUrlPool: invalidEndpointPool,
      blockchainRid: process.env.BLOCKCHAIN_RID,
      failOverConfig: {
        attemptInterval: 1,
      },
      directoryChainRid: mockStringDirectoryChainRid,
    });
    stableConfigClient = createClient({
      nodeUrlPool: LOCAL_POOL,
      blockchainRid: "BFB34ABEA1045BB97ED18A5DBB2B8DCE84CB181585D55ABB321A33133DBA0595",
    });
    client = createClient({
      nodeUrlPool: LOCAL_POOL,
      blockchainIid: 0,
    }).finally(() => {
      done();
    });
    handleRequestSpy = sinon.spy(handleRequestModule, "default");
  });

  beforeEach(() => {
    handleRequestSpy.resetHistory();
  });

  after(() => {
    handleRequestSpy.restore();
  });

  // TODO: Clean up after tests? E.g. wipe database?
  describe("Init client", () => {
    it("inits client", async () => {
      const client = await createClient({
        nodeUrlPool: "https://localhost:7740",
        blockchainRid: process.env.BLOCKCHAIN_RID,
        directoryChainRid: mockStringDirectoryChainRid,
      });

      const { nodeManager, ...restClientConfig } = client.config;

      expect(restClientConfig).to.deep.equal({
        endpointPool: [{ url: "https://localhost:7740", whenAvailable: 0 }],
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failoverStrategy: "abortOnError",
        attemptsPerEndpoint: 3,
        attemptInterval: 500,
        unreachableDuration: 30000,
        directoryChainRid: mockStringDirectoryChainRid,
        dappStatusPolling: {
          interval: 500,
          count: 20,
        },
        clusterAnchoringStatusPolling: {
          interval: 500,
          count: 20,
        },
        systemAnchoringStatusPolling: {
          interval: 500,
          count: 20,
        },
      });

      expect(nodeManager).to.have.property("nodes");
      expect(nodeManager).to.have.property("getAvailableNodes");
      expect(nodeManager).to.have.property("getNode");
      expect(nodeManager).to.have.property("setStickyNode");
      expect(nodeManager).to.have.property("makeNodeUnavailable");
      expect(nodeManager.stickedNode).to.be.null;
      expect(nodeManager.nodes).to.have.deep.members([
        {
          url: "https://localhost:7740",
          whenAvailable: 0,
          isAvailable: true,
        },
      ]);
    });

    it("fails to init client if no blockchainRID or blockchainIID is provided", async () => {
      const promise = createClient({
        nodeUrlPool: "localhost:7740",
      });
      await expect(promise).to.be.rejectedWith(new MissingBlockchainIdentifierError().message);
    });
    it("fails to init client if no nodeUrlPool or directoryNodeUrlPool was provided", async () => {
      const promise = createClient({});
      await expect(promise).to.be.rejectedWith(new MissingBlockchainIdentifierError().message);
    });

    it.skip("inits client with node discovery");

    it("inits client with Blockchain IID", async () => {
      const client = await createClient({
        nodeUrlPool: [LOCAL_POOL],
        blockchainIid: 0,
        directoryChainRid: mockStringDirectoryChainRid,
      });

      const { nodeManager, ...restClientConfig } = client.config;

      expect(restClientConfig).to.deep.equal({
        endpointPool: [{ url: LOCAL_POOL, whenAvailable: 0 }],
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failoverStrategy: "abortOnError",
        attemptsPerEndpoint: 3,
        attemptInterval: 500,
        unreachableDuration: 30000,
        directoryChainRid: mockStringDirectoryChainRid,
        dappStatusPolling: {
          interval: 500,
          count: 20,
        },
        clusterAnchoringStatusPolling: {
          interval: 500,
          count: 20,
        },
        systemAnchoringStatusPolling: {
          interval: 500,
          count: 20,
        },
      });

      expect(nodeManager).to.have.property("nodes");
      expect(nodeManager).to.have.property("getAvailableNodes");
      expect(nodeManager).to.have.property("getNode");
      expect(nodeManager).to.have.property("setStickyNode");
      expect(nodeManager).to.have.property("makeNodeUnavailable");
      expect(nodeManager.stickedNode).to.deep.equal({
        url: LOCAL_POOL,
        whenAvailable: 0,
        isAvailable: true,
      });
      expect(nodeManager.nodes).to.have.deep.members([
        {
          url: LOCAL_POOL,
          whenAvailable: 0,
          isAvailable: true,
        },
      ]);
    });
  });
  describe("query", () => {
    it("returns a value", async () => {
      const result = await (
        await client
      ).query("test_boolean", {
        arg1: true,
      });
      expect(result).to.equal(1);
    });

    it("takes a query object as argument", async () => {
      const result = await (
        await client
      ).query({
        name: "test_boolean",
        args: {
          arg1: true,
        },
      });
      expect(result).to.equal(1);
    });
    it("works with large complex object as argument", async () => {
      const result = await (
        await client
      ).query({
        name: "test_complex_object",
        args: ComplexArgumentObject,
      });
      expect(result).to.equal("");
    });

    it("takes a query object without arguments as argument", async () => {
      const result = await (await client).query({ name: "test_without_args" });
      expect(result).to.equal(1);
    });

    it("returns a promise", async () => {
      const promise = (await client).query("test_boolean", {
        arg1: true,
      });
      expect(promise.then).to.be.a("function");
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("takes a callback", done => {
      client.then(_client => {
        _client.query(
          "test_boolean",
          {
            arg1: true,
          },
          (err, result) => {
            expect(result).to.equal(1);
            done();
          },
        );
      });
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("takes a callback when using query object as argument", done => {
      client.then(_client => {
        _client.query(
          {
            name: "test_boolean",
            args: {
              arg1: true,
            },
          },
          undefined,
          (err, result) => {
            expect(result).to.equal(1);
            done();
          },
        );
      });
    });

    // Attempt at testing typescript generics. Will make the test suite fail
    // if the query function doesn't take these generics
    it("takes a typed argument", async () => {
      const _client = await client;
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const result: number = await _client.query<number, { arg1: boolean }>("test_boolean", {
        arg1: true,
      });
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const resultQueryObject: number = await _client.query<number, { arg1: boolean }>({
        name: "test_boolean",
        args: {
          arg1: true,
        },
      });
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const resultNoArgs: number = await _client.query<number, undefined>("test_without_args");
      _client.query<number, { arg1: boolean }>(
        "test_boolean",
        {
          arg1: true,
        },
        (err, response) => {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const numberResponse: number = response;
        },
      );
    });
    it("takes a nullable argument", async () => {
      const nullableStructQueryObject: QueryObject<RawGtv, { arg1: null[] }> = {
        name: "test_nullable_struct",
        args: {
          arg1: [null],
        },
      };
      const result = await (await client).query(nullableStructQueryObject);
      expect(result).to.deep.equal({ int: null });
    });
    it("rejects promise on error", async () => {
      const promise = (await client).query("does_not_exist");
      await expect(promise).to.be.rejected;
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("propagates errors when using a callback", done => {
      client.then(_client => {
        _client.query("does_not_exist", {}, (err, result) => {
          expect(result).to.be.null;
          expect(err).to.be.an("error");
          done();
        });
      });
    });

    it("works without arguments", async () => {
      const result = await (await client).query("test_without_args");
      expect(result).to.equal(1);
    });

    it("returns string key map", async () => {
      const result: { sample_key: string } = await (await client).query("test_map");
      expect(result["sample_key"]).to.equal("sample_value");
    });

    it("returns byte array key map as an array with buffers", async () => {
      const result: string[] = await (await client).query("test_map_with_bytearray_key");
      expect(Array.isArray(result)).to.be.true;
      expect(Buffer.isBuffer(result[0][0])).to.be.true;
      expect(result[0][1]).to.equal("sample_value");
    });

    // TODO:  make generic test that takes query name and arguments
    // same as gtxClientIntegrationTest.ts
    it.skip("works with basic types as arguments");

    it("retries default number of times", async () => {
      try {
        await (await faultyClient).query("test_without_arguments");
      } catch (err) {
        expect(handleRequestSpy.callCount).to.equal(9);
      }
    });

    it("retries configured number of times", async () => {
      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
          attemptInterval: 1,
        },
        directoryChainRid: mockStringDirectoryChainRid,
      });

      try {
        await _client.query("test_without_arguments");
      } catch (err) {
        expect(handleRequestSpy.callCount).to.equal(3);
      }
    });

    // TODO: Mock backend and use fake timers to make this more robust
    it("uses default attemptInterval of 500", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
        },
        directoryChainRid: mockStringDirectoryChainRid,
      });

      _client.query("test_without_arguments");

      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(250);
      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
    });

    // TODO: Mock backend and use fake timers to make this more robust
    it("uses configured attemptInterval", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
          attemptInterval: 100,
        },
        directoryChainRid: mockStringDirectoryChainRid,
      });

      _client.query("test_without_arguments");

      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(50);
      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(3);
    });
    it("inits client with Blockchain IID and send query", async () => {
      const client = await createClient({
        nodeUrlPool: [LOCAL_POOL],
        blockchainIid: 0,
      });

      const result = await client.query("test_boolean", {
        arg1: true,
      });
      expect(result).to.equal(1);
    });
  });

  describe("signTransaction", () => {
    it("returns transaction with a signature when using signature provider", async () => {
      const signedTx = await (
        await stableConfigClient
      ).signTransaction(unsignedNonUniqueTx, sigProvA);
      expect(signedTx.toString("hex")).to.equal(signedNonUniqueTxHex);
    });

    it("returns transaction with a signature when using key pair", async () => {
      const signedTx = await (
        await stableConfigClient
      ).signTransaction(unsignedNonUniqueTx, {
        privKey: signerPrivKeyA,
        pubKey: signerPubKeyA,
      });
      expect(signedTx.toString("hex")).to.equal(signedNonUniqueTxHex);
    });

    it("adds signature to an already signed transaction", async () => {
      const _client = await stableConfigClient;
      const signedTxA = await _client.signTransaction(
        unsignedNonUniqueTxWithTwoSigners,
        signerKeyPairA,
      );
      const signedTx = await _client.signTransaction(signedTxA, signerKeyPairB);
      const deserialized = gtxTool.deserialize(signedTx);
      expect(deserialized.signatures).to.have.length(2);
      expect(signedTx.toString("hex")).to.equal(
        "a582012930820125a58194308191a1220420bfb34abea1045bb97ed18a5dbb2b8dce84" +
        "cb181585d55abb321a33133dba0595a51d301ba5193017a20c0c0a736574496e746567" +
        "6572a5073005a303020101a54c304aa123042102e5a018b3a2e155316109d9cdc5eab7" +
        "39759c0e07e0c00bf9fccb8237fe4d7f02a123042103cb144659ef300200912762786a" +
        "173663150592d78f4408390c47e9956d9a6c31a5818b308188a1420440c6d576625143" +
        "eb97c783efc56a4fef7fa9f76dff775e65222c3217aef96c93be43a4d3d96f8f9c301f" +
        "da9d9577c86f0dd400f803988bb7b97107c7d61e27b99fa1420440817ef63a1c3bc642" +
        "6d9c23521ece05ad1b9e6607f68d25c699e4380865e00a82044d3ad31f422bdd9e39d2" +
        "3769680fcb97bec393b3505de8743709dd3f373487",
      );
    });

    it.skip("returns error if signers does not match signMethod");

    it("takes a callback, transaction with a signature when using signature provider", done => {
      stableConfigClient.then(_client => {
        _client.signTransaction(unsignedNonUniqueTx, sigProvA, (err, signedTx) => {
          expect(signedTx?.toString("hex")).to.equal(signedNonUniqueTxHex);
          done();
        });
      });
    });

    it("takes a callback, transaction with a signature when using key pair", done => {
      stableConfigClient.then(_client => {
        _client.signTransaction(
          unsignedNonUniqueTx,
          { privKey: signerPrivKeyA, pubKey: signerPubKeyA },
          (err, signedTx) => {
            expect(signedTx?.toString("hex")).to.equal(signedNonUniqueTxHex);
            done();
          },
        );
      });
    });

    it("rejects promise if signer does not exist", async () => {
      const promise = (await stableConfigClient).signTransaction(unsignedTx, {
        privKey: signerPrivKeyB,
        pubKey: signerPubKeyB,
      });
      await expect(promise).to.be.rejectedWith(
        "No such signer, remember to add signer to transaction before" + " adding a signature",
      );
    });

    it("propagates errors when using a callback if signer does not exist", done => {
      stableConfigClient.then(_client => {
        _client.signTransaction(
          unsignedTx,
          { privKey: signerPrivKeyB, pubKey: signerPubKeyB },
          (err, result) => {
            expect(result).to.be.null;
            expect(err).to.be.be.an("error");
            done();
          },
        );
      });
    });
  });

  describe("sendTransaction", () => {
    it("returns a PromiEvent", async () => {
      const promiEvent = (await client).sendTransaction({
        name: "setInteger",
        args: [randomInt(99999)],
      });
      expect(promiEvent.then).to.be.a("function");
      expect(promiEvent.on).to.be.a("function");
      expect(promiEvent.once).to.be.a("function");
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("takes a callback", done => {
      client.then(_client => {
        _client.sendTransaction(
          {
            name: "setInteger",
            args: [randomInt(99999)],
          },
          true,
          (err, receipt) => {
            const transactionReceipt = receipt as TransactionReceipt;

            expect(transactionReceipt?.status).to.equal(ResponseStatus.Waiting);
            expect(transactionReceipt?.statusCode).to.equal(200);
            expect(transactionReceipt?.transactionRid).to.not.be.null;
            done();
          },
        );
      });
    });

    it("rejects promise on error", async () => {
      const promise = (await client).sendTransaction({
        name: "doesNotExist",
      });
      await expect(promise).to.be.rejected;
    });

    it.skip("rejects promise if transaction is rejected by network");

    it("rejects promise if signers and signatures array have different size", async () => {
      const signedTx = await (
        await client
      ).signTransaction(unsignedNonUniqueTxWithTwoSigners, sigProvA);
      const deserializedSignedTx = gtxTool.deserialize(signedTx);
      expect(deserializedSignedTx.signers.length).to.not.equal(
        deserializedSignedTx.signatures!.length,
      );
      const promise = (await client).sendTransaction(signedTx);
      await expect(promise).to.be.rejectedWith("Not matching number of signers and signatures");
    });

    it("propagates errors when using a callback", done => {
      client.then(_client => {
        _client.sendTransaction(
          {
            name: "doesNotExist",
          },
          true,
          (err, receipt) => {
            expect(receipt).to.be.null;
            expect(err).to.be.be.an("error");
            done();
          },
        );
      });
    });

    it.skip("resolves promise once the transaction is accepted");

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("emits PromiEvent 'sent' once transaction is sent", async () => {
      const client = await createClient({
        nodeUrlPool: LOCAL_POOL,
        blockchainIid: 0,
      });

      const promiEvent = client.sendTransaction(
        {
          name: "setInteger",
          args: [randomInt(99999)],
        },
        true,
        undefined,
        ChainConfirmationLevel.None,
      );

      promiEvent.on(TransactionEvent.DappReceived, (receipt: TransactionReceipt) => {

        expect(receipt.status).to.equal(ResponseStatus.Waiting);
        expect(receipt.statusCode).to.equal(200);
        expect(receipt.transactionRid).to.not.be.null;
      });
    });

    it.skip("resolves to a TransactionReceipt");

    // TODO: Test fails when running entire test suite. Make it more robust
    // by mocking the backend or writing unit tests
    it.skip("uses default attemptInterval of 500", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
        },
      });

      _client.sendTransaction({
        name: "setInteger",
        args: [randomInt(99999)],
      });

      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(250);
      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
    });

    // TODO: Test fails when running entire test suite. Make it more robust
    // by mocking the backend or writing unit tests
    it.skip("uses default pollingInterval of 500", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      (await client).sendTransaction({
        name: "setInteger",
        args: [randomInt(99999)],
      });

      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(250);
      // will have sent the transaction and polled for status once
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(4);
    });

    // TODO: Test fails when running entire test suite. Make it more robust
    // by mocking the backend or writing unit tests
    it.skip("uses configured pollingInterval of 100", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));
      const _client = await createClient({
        nodeUrlPool: LOCAL_POOL,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        dappStatusPolling: setStatusPolling({ interval: 100, count: CONFIGURED_POLL_COUNT }),
        clusterAnchoringStatusPolling: setStatusPolling(),
        systemAnchoringStatusPolling: setStatusPolling(),
      });
      _client.sendTransaction({
        name: "setInteger",
        args: [randomInt(99999)],
      });

      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(100);
      expect(handleRequestSpy.callCount).to.equal(4);
    });
  });

  describe("addNop", () => {
    it("adds nop to transaction object", async () => {
      const transaction = (await client).addNop(unsignedTx);

      const nopOperation = transaction.operations[1];

      expect(nopOperation).to.include({ name: "nop" });
      expect(nopOperation.args).to.have.lengthOf(1);
      expect(nopOperation.args?.[0]).to.be.instanceof(Buffer);
      // TODO changed nop to add randomBytes instead of randomInt because if failed in the browser
      //expect((nopOperation.args?.[0] as number) % 1).to.equal(0); // Is integer
    });

    it("adds nop with a unique argument", async () => {
      const transactionOne = (await client).addNop({
        operations: [
          {
            name: "setInteger",
            args: [1],
          },
        ],
        signers: [signerPubKeyA],
      });
      const transactionTwo = (await client).addNop({
        operations: [
          {
            name: "setInteger",
            args: [1],
          },
        ],
        signers: [signerPubKeyA],
      });

      const nopOperationOne = transactionOne.operations[1];
      const nopOperationTwo = transactionTwo.operations[1];

      expect(nopOperationOne).to.not.equal(nopOperationTwo);
    });
  });

  describe("signAndSendUniqueTransaction", () => {
    it("returns a PromiEvent", async () => {
      const promiEvent = (await client).signAndSendUniqueTransaction(
        {
          name: "setInteger",
          args: [randomInt(99999)],
        },
        sigProvA,
      );
      expect(promiEvent.then).to.be.a("function");
      expect(promiEvent.on).to.be.a("function");
      expect(promiEvent.once).to.be.a("function");
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("takes a callback", done => {
      client.then(_client => {
        _client.signAndSendUniqueTransaction(
          {
            name: "setInteger",
            args: [randomInt(99999)],
          },
          sigProvA,
          true,
          (err, receipt) => {
            const transactionReceipt = receipt as TransactionReceipt;

            expect(transactionReceipt?.status).to.equal(ResponseStatus.Waiting);
            expect(transactionReceipt?.statusCode).to.equal(200);
            expect(transactionReceipt?.transactionRid).to.not.be.null;
            done();
          },
        );
      });
    });

    it.skip("rejects promise if send failed");

    it("rejects promise if transaction has empty list of signers", async () => {
      const promise = (await client).signAndSendUniqueTransaction(
        {
          operations: [
            {
              name: "setInteger",
              args: [randomInt(99999)],
            },
          ],
          signers: [],
        },
        sigProvA,
      );
      await expect(promise).to.be.rejectedWith(
        "No such signer, remember to add signer to transaction before adding a signature",
      );
    });

    it("rejects promise if transaction has signers that do not match signMethod", async () => {
      const promise = (await client).signAndSendUniqueTransaction(
        {
          operations: [
            {
              name: "setInteger",
              args: [randomInt(99999)],
            },
          ],
          signers: [signerPubKeyB],
        },
        sigProvA,
      );
      await expect(promise).to.be.rejectedWith(
        "No such signer, remember to add signer to transaction before adding a signature",
      );
    });

    it.skip("rejects promise if transaction is rejected by network");

    it("propagates errors when using a callback", done => {
      client.then(_client => {
        _client.signAndSendUniqueTransaction(
          {
            name: "doesNotExist",
          },
          sigProvA,
          true,
          (err, receipt) => {
            expect(receipt).to.be.null;
            expect(err).to.be.be.an("error");
            done();
          },
        );
      });
    });

    // TODO: Results in a timeout error if expect statements fails. Fix this.
    it("emits PromiEvent 'sent' once transaction is sent", async () => {
      const client = await createClient({
        nodeUrlPool: LOCAL_POOL,
        blockchainIid: 0,
      });

      const promiEvent = client.signAndSendUniqueTransaction(
        {
          name: "setInteger",
          args: [randomInt(99999)],
        },
        sigProvA,
        true,
        undefined,
        ChainConfirmationLevel.None,
      );

      promiEvent.on(TransactionEvent.DappReceived, (receipt: TransactionReceipt) => {
        expect(receipt.status).to.equal(ResponseStatus.Waiting);
        expect(receipt.statusCode).to.equal(200);
        expect(receipt.transactionRid).to.not.be.null;
      });
    });

    it.skip("resolves promise once the transaction is accepted");
    it.skip("resolves to a TransactionReceipt");

    // TODO: Test fails when running entire test suite. Make it more robust
    // by mocking the backend or writing unit tests
    it.skip("uses default attemptInterval of 500", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
        },
      });

      _client.signAndSendUniqueTransaction(
        {
          name: "setInteger",
          args: [randomInt(99999)],
        },
        signerKeyPairA,
      );
      await sleep(10);
      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(440);
      expect(handleRequestSpy.callCount).to.equal(1);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(2);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
      await sleep(500);
      expect(handleRequestSpy.callCount).to.equal(3);
    }).timeout(5000);
  });

  describe("getTransaction", () => {
    it("rejects promise if a specific txRID does not exist in blockchain", async () => {
      const _client = await client;
      const promise = _client.getTransaction(Buffer.alloc(32, "a"));
      await expect(promise).to.be.rejectedWith(
        `{"error":"Can't find transaction with RID: 6161616161616161616161616161616161616161616161616161616161616161"}`,
      );
    });

    it("returns a promise", async () => {
      const _client = await client;
      const { txRID } = getTxWithGTXAndTxRID(_client);
      const promise = _client.getTransaction(txRID);
      expect(promise.then).to.be.a("function");
    });

    it("rejects promise if transaction RID is invalid", async () => {
      const _client = await client;

      const promise = _client.getTransaction(Buffer.from("invalid"));
      await expect(promise).to.be.rejectedWith("expected length 32 of txRID, but got 7");
    });

    it("propagates errors when using a callback", done => {
      client.then(_client => {
        _client.getTransaction(Buffer.from("invalid"), (err, serializedMessage) => {
          expect(err?.message).to.equal("expected length 32 of txRID, but got 7");
          expect(serializedMessage).to.be.null;
          done();
        });
      });
    });
  });

  describe("getTransactionStatus", () => {
    it("gets status of an unknown transaction", async () => {
      const _client = await client;
      const { txRID } = getTxWithGTXAndTxRID(_client);
      const { status } = await _client.getTransactionStatus(txRID);
      expect(status).to.equal(ResponseStatus.Unknown);
    });

    it.skip("gets status of a rejected transaction");

    it("returns a promise", async () => {
      const _client = await client;
      const { txRID } = getTxWithGTXAndTxRID(_client);
      const promise = _client.getTransactionStatus(txRID);
      expect(promise.then).to.be.a("function");
    });

    it("rejects promise if transaction RID is invalid", async () => {
      const promise = (await client).getTransactionStatus(Buffer.from("invalid"));
      await expect(promise).to.be.rejectedWith("expected length 32 of txRID, but got 7");
    });

    it("propagates errors when using a callback", done => {
      const callback: (err: Error | null | undefined, statusObj: any | null) => void = (
        err,
        statusObj,
      ) => {
        expect(err?.message).to.equal("expected length 32 of txRID, but got 7");
        expect(statusObj).to.be.null;
        done();
      };

      client.then(_client => {
        _client.getTransactionStatus(Buffer.from("invalid"), callback);
      });
    });

    it("retries default number of times", async () => {
      const _faultyClient = await faultyClient;
      try {
        const { txRID } = getTxWithGTXAndTxRID(_faultyClient);
        await _faultyClient.getTransactionStatus(txRID);
      } catch (err) {
        expect(handleRequestSpy.callCount).to.equal(9);
      }
    });
    it("uses default attemptInterval of 500", async () => {
      const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

      const _client = await createClient({
        nodeUrlPool: invalidEndpointPool,
        blockchainRid: process.env.BLOCKCHAIN_RID,
        failOverConfig: {
          attemptsPerEndpoint: 1,
        },
        directoryChainRid: mockStringDirectoryChainRid,
      });
      try {
        const { txRID } = getTxWithGTXAndTxRID(_client);
        _client.getTransactionStatus(txRID);
        expect(handleRequestSpy.callCount).to.equal(1);
        await sleep(500);
        expect(handleRequestSpy.callCount).to.equal(1);
        await sleep(500);
        expect(handleRequestSpy.callCount).to.equal(2);
        await sleep(500);
        expect(handleRequestSpy.callCount).to.equal(3);
        await sleep(500);
        expect(handleRequestSpy.callCount).to.equal(3);
      } catch (error) {
        expect(error).to.not.be.null;
      }
    }).timeout(4000);
  });

  describe("getTransactionRID", () => {
    it("takes a transaction object", async () => {
      const txRID = (await stableConfigClient).getTransactionRid({
        operations: [
          {
            name: "setInteger",
            args: [1],
          },
        ],
        signers: [],
      });
      expect(txRID.toString("hex")).to.equal(
        "9f8966574214d987ddcc918c09eb7f1b6a1a75855d8e03bbffb07ef2cb949534",
      );
    });

    it("takes an operation", async () => {
      const txRID = (await stableConfigClient).getTransactionRid({
        name: "setInteger",
        args: [1],
      });
      expect(txRID.toString("hex")).to.equal(
        "9f8966574214d987ddcc918c09eb7f1b6a1a75855d8e03bbffb07ef2cb949534",
      );
    });

    it("takes a signed transaction buffer", async () => {
      const signedTx = Buffer.from(signedNonUniqueTxHex, "hex");
      const txRID = (await stableConfigClient).getTransactionRid(signedTx);
      expect(txRID.toString("hex")).to.equal(
        "33754eb08c2a7c6be69821608378ef315399f3113de6ea229023ccad2b7ea7a7",
      );
    });
  });

  describe("getTransactionsInfo", () => {
    it("returns empty array when limit is invalid", async () => {
      const _client = await client;
      const transactions = await _client.getTransactionsInfo(-1);
      expect(transactions).to.be.instanceOf(Array).and.lengthOf(0);
    });
    it("returns empty array when before-time is invalid", async () => {
      const _client = await client;
      const transactions = await _client.getTransactionsInfo(undefined, new Date(-1));
      expect(transactions).to.be.instanceOf(Array).and.lengthOf(0);
    });
  });

  describe("getTransactionInfo", () => {
    it("should throw 404 error when transaction rid is not found", async () => {
      const _client = await client;

      const invalidTxRid = "8FB7809B264587F165771FDE89471F8C81838785FB9DAC65E3DFFD1B41B7EFFC";
      const transactionInfoPromise = _client.getTransactionInfo(toBuffer(invalidTxRid));

      await expect(transactionInfoPromise).to.be.rejectedWith(
        `{"error":"Can't find transaction with RID: ${invalidTxRid}"}`,
      );
    });
  });

  describe("getBlockInfo", () => {
    it("should return an error when provided with a block identifier of the wrong type.", async () => {
      const _client = await client;

      const rejectedPromise = _client.getBlockInfo(["test"] as any);
      await expect(rejectedPromise).to.be.rejectedWith(
        `Invalid "blockIdentifier" type. Expected string or number, but received object.`,
      );
    });

    it("should return an error when provided with a block identifier string of a wrong format.", async () => {
      const _client = await client;

      const rejectedPromise = _client.getBlockInfo("123213asdsad");
      await expect(rejectedPromise).to.be.rejectedWith(
        "Parameter 'blockIdentifier' does not have the correct format (64-character hexadecimal string).",
      );
    });

    it("should return null when given a non-existing block RID.", async () => {
      const _client = await client;

      const block = await _client.getBlockInfo(
        "B640E5935B54E9CB2029C56EF33F89D0A3C1041E2C1B38389F2930DFC1179657",
      );

      expect(block).to.be.null;
    });

    it("should return null when given a non-existing block height.", async () => {
      const _client = await client;

      const block = await _client.getBlockInfo(999999999999);

      expect(block).to.be.null;
    });

    it("should fetch a block when provided with a block height", async () => {
      const _client = await client;

      const block = await _client.getBlockInfo(1);
      expect(block).to.have.keys([
        "rid",
        "prevBlockRid",
        "header",
        "height",
        "transactions",
        "witness",
        "witnesses",
        "timestamp",
      ]);
    });

    it("should fetch a block when provided with a block RID.", async () => {
      const _client = await client;

      const blockFromHeight = await _client.getBlockInfo(1);

      const block = await _client.getBlockInfo(blockFromHeight.rid.toString("hex"));
      expect(block).to.have.keys([
        "rid",
        "prevBlockRid",
        "header",
        "height",
        "transactions",
        "witness",
        "witnesses",
        "timestamp",
      ]);
    });
  });
  describe("getLatestBlock", () => {
    it("get latest block", async () => {
      const blockInfo = await (await client).getLatestBlock();
      expect(blockInfo).to.have.keys([
        "rid",
        "prevBlockRid",
        "header",
        "height",
        "transactions",
        "witness",
        "witnesses",
        "timestamp",
      ]);
    });
  });

  describe("getBlocksInfo", () => {
    it("fetches all blocks for a chain", async () => {
      const _client = await client;

      const blocks = await _client.getBlocksInfo();
      expect(blocks).to.not.be.empty;
    });
    it("fetches all blocks before a block height", async () => {
      const _client = await client;

      const blocks = await _client.getBlocksInfo(undefined, undefined, 2);
      expect(blocks).to.have.length(2);
    });
  });

  describe("getClientNodeURLPool", () => {
    it("gets list of url from the client config", async () => {
      const nodeUrlPool = (await client).getClientNodeUrlPool();
      expect(nodeUrlPool).to.deep.equal([LOCAL_POOL]);
    });
  });

  describe("getConfirmationProof", () => {
    it("returns error", async () => {
      const _client = await client;
      const promise = _client.getConfirmationProof(mockThirtyTwoBytesBuffer);

      await expect(promise).to.be.rejectedWith(
        `{"error":"Can't find transaction with RID: 0000000000000000000000000000000000000000000000000000000000000000"}`,
      );
    });
  });

  describe("createClient", () => {
    it("initializes with default status polling configurations", async () => {
      const client = await createClient({
        nodeUrlPool: LOCAL_POOL,
        blockchainRid: "BFB34ABEA1045BB97ED18A5DBB2B8DCE84CB181585D55ABB321A33133DBA0595",
      });

      const defaultStatusPollingConfig = {
        interval: 500,
        count: 20,
      };

      expect(client.config.dappStatusPolling).to.deep.equal(defaultStatusPollingConfig);
      expect(client.config.clusterAnchoringStatusPolling).to.deep.equal(defaultStatusPollingConfig);
      expect(client.config.systemAnchoringStatusPolling).to.deep.equal(defaultStatusPollingConfig);
    });
    it("initializes with 3 different status polling configurations", async () => {
      const dappStatusPollingConfig = {
        interval: 100,
        count: 5,
      };
      const clusterAnchoringStatusPollingConfig = {
        interval: 200,
        count: 10,
      };
      const systemAnchoringStatusPollingConfig = {
        interval: 500,
        count: 20,
      };
      const client = await createClient({
        nodeUrlPool: LOCAL_POOL,
        blockchainRid: "BFB34ABEA1045BB97ED18A5DBB2B8DCE84CB181585D55ABB321A33133DBA0595",
        dappStatusPolling: dappStatusPollingConfig,
        clusterAnchoringStatusPolling: clusterAnchoringStatusPollingConfig,
        systemAnchoringStatusPolling: systemAnchoringStatusPollingConfig,
      });

      expect(client.config.dappStatusPolling).to.deep.equal(dappStatusPollingConfig);
      expect(client.config.clusterAnchoringStatusPolling).to.deep.equal(
        clusterAnchoringStatusPollingConfig,
      );
      expect(client.config.systemAnchoringStatusPolling).to.deep.equal(
        systemAnchoringStatusPollingConfig,
      );
    });
  });
});
