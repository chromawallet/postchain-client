import { expect } from "chai";
import { Buffer } from "buffer";
import * as pc from "../../index";
import { makeKeyPair } from "../../src/encryption/encryption";
import { randomInt } from "crypto";
import { UnexpectedStatusError } from "../../src/restclient/errors";
import * as dotenv from "dotenv";
import { LOCAL_POOL } from "../../src/constants";
dotenv.config({ path: `${__dirname}/.env` });

//to run test, start the rell dapp in folder called resources
//if you have done changes to your backend, make sure to run `chr start --wipe`
// and update the blockchainRID below.
const blockchainRid = process.env.BLOCKCHAIN_RID!;
const rest = pc.restClient.createRestClient([LOCAL_POOL], blockchainRid, 5);
const gtx = pc.gtxClient.createClient(rest, blockchainRid, [
  "setBoolean",
  "setInteger",
  "setBigInteger",
  "setMultivalue",
  "setEntity",
]);
const bigIntValue = "18446744073709551616";

describe("Query integration test", () => {
  describe("Basic types", () => {
    it("query boolean", async () => {
      const result = await gtx.query("test_boolean", {
        arg1: true,
      });
      expect(result).to.equal(1);
    });
    it("query number", async () => {
      const result = await gtx.query("test_number", {
        arg1: 1,
      });
      expect(result).to.equal(1);
    });
    it("query negative number", async () => {
      const result = await gtx.query("test_number", {
        arg1: -1,
      });
      expect(result).to.equal(-1);
    });
    it("query decimal", async () => {
      const result = await gtx.query("test_decimal", {
        arg1: "1.1",
      });
      expect(result).to.equal("1.1");
    });
    it("query string", async () => {
      const result = await gtx.query("test_string", {
        arg1: "test",
      });
      expect(result).to.equal("test");
    });
    it("query byteArray", async () => {
      const result = await gtx.query("test_byte_array", {
        arg1: Buffer.from("test"),
      });
      expect(Buffer.from("test").equals(result)).to.be.true;
    });
    it("query json", async () => {
      const result = await gtx.query("test_json", {
        arg1: JSON.stringify({ name: "Alice" }),
      });
      expect(result).to.deep.equal('{"name":"Alice"}');
    });
    it("query null", async () => {
      const result = await gtx.query("test_null");
      expect(result).to.equal(null);
    });
    it("query big integer 64 bits", async () => {
      const bigInt = BigInt(bigIntValue);
      const result = await gtx.query("test_big_integer", {
        arg1: bigInt,
      });
      expect(result.toString()).to.equal(bigInt.toString());
    });
  });
  //TODO:limitation in rell that will be enabled in next rell release (current version is 0.11.0)
  // it("test arg named type", async () => {
  //   const result = await gtx.query("test_type_as_arg_name", undefined, {
  //     type: "test",
  //   });
  //   expect(result).to.equal("test");
  // });

  describe("Collections", () => {
    it("query array", async () => {
      const result = await gtx.query("test_array", {
        arg1: ["test", "test2"],
      });
      expect(result).to.deep.equal(["test", "test2"]);
    });
    it("query empty array", async () => {
      const result = await gtx.query("test_array", {
        arg1: [],
      });
      expect(result).to.deep.equal([]);
    });
    it("query string key map", async () => {
      const result = await gtx.query("test_string_key_map", {
        arg1: { test: "test2", test3: "test4" },
      });
      expect(result).to.deep.equal({ test: "test2", test3: "test4" });
    });
    it("query empty string key map", async () => {
      const result = await gtx.query("test_string_key_map", {
        arg1: {},
      });
      expect(result).to.deep.equal({});
    });
    it("Fail query non string key map", async () => {
      const expectedErrorMessage =
        "Query 'test_non_string_key_map' failed: Decoding type 'map<integer,text>': expected ARRAY, actual DICT (parameter: arg1)";
      const result = await gtx
        .query("test_non_string_key_map", {
          arg1: { test: 1, test3: "test4" },
        })
        .catch(e => {
          return e;
        });

      expect(result).to.deep.equal(new UnexpectedStatusError(400, expectedErrorMessage));
    });
    //For the type when a map has a key of not string type, it needs to be formatted as a matrix. 2xN
    it("query non string key map", async () => {
      const result = await gtx.query("test_non_string_key_map", {
        arg1: [
          [1, "test4"],
          [2, "test5"],
        ],
      });
      expect(result).to.deep.equal([
        [1, "test4"],
        [2, "test5"],
      ]);
    });
    it("query set", async () => {
      const input: Set<string> = new Set(["foo", "bar", "bar"]);
      const result = await gtx.query("test_set", {
        arg1: Array.from(input),
      });
      expect(result).to.deep.equal(["foo", "bar"]);
    });
    it("query empty set", async () => {
      const result = await gtx.query("test_set", {
        arg1: [],
      });
      expect(result).to.deep.equal([]);
    });
  });
  describe("tuple test", () => {
    it("query unnamed tuple", async () => {
      const result = await gtx.query("test_unnamed_tuple", {
        arg1: [1, 2],
      });
      expect(result).to.deep.equal([1, 2]);
    });
    it("query incomplete unnamed tuple", async () => {
      await gtx
        .query("test_unnamed_tuple", {
          arg1: [], // expects array of size 2
        })
        .catch(e => {
          expect(e).to.deep.equal(new UnexpectedStatusError(400));
        });
    });
    it("query named tuple", async () => {
      const result = await gtx.query("test_named_tuple", {
        arg1: { x: 1, y: 2 },
      });
      expect(result).to.deep.equal({ x: 1, y: 2 });
    });
    it("query incomplete named tuple", async () => {
      const result = await gtx
        .query("test_named_tuple", {
          arg1: { x: 1 },
        })
        .catch(e => {
          return e;
        });
      expect(result).to.deep.equal(new UnexpectedStatusError(400));
    });
    it("query single tuple", async () => {
      const result = await gtx.query("test_single_tuple", {
        arg1: { x: 1 },
      });
      expect(result).to.deep.equal({ x: 1 });
    });
  });

  describe("enum test", () => {
    it("query enum", async () => {
      enum SampleEnum {
        A,
        B,
        C,
      }
      const result = await gtx.query("test_enum", {
        x: SampleEnum.B,
      });
      expect(result).to.deep.equal(1);
    });
    it("query enum out of range", async () => {
      const result = await gtx
        .query("test_enum", {
          x: 3,
        })
        .catch(e => {
          return e;
        });
      expect(result).to.deep.equal(new UnexpectedStatusError(400));
    });
  });

  describe("struct test", () => {
    type IntValueTester = {
      int: number;
    };
    const input: IntValueTester = { int: 13 };

    it("query struct", async () => {
      const result = await gtx.query("test_struct", {
        x: input,
      });
      expect(result).to.deep.equal(input);
    });
    it("query struct", async () => {
      const result = await gtx.query("test_struct", {
        x: [input.int],
      });
      expect(result).to.deep.equal(input);
    });
  });
});

describe("Transaction gtx integration tests", function () {
  const signerPrivKeyA = Buffer.alloc(32, "a");
  const keypair = makeKeyPair(signerPrivKeyA);
  describe("send operation", function () {
    it("send without signers and signature", done => {
      const req = gtx.newTransaction([]);
      req.addOperation("setMultivalue", 1, "test", "test2");
      req.addOperation("nop", randomInt(1000000));
      req.send((error, responseObject: any) => {
        expect(responseObject).to.eql({});
        done();
      });
    });

    it("send with signers and signatures", done => {
      const req = gtx.newTransaction([keypair.pubKey]);
      req.addOperation("setMultivalue", 1, "test", "test2");
      req.addOperation("nop", randomInt(1000000));
      req.sign(keypair.privKey, keypair.pubKey);
      req.send((error, responseObject: any) => {
        expect(responseObject).to.eql({});
        done();
      });
    });
    it("fails to send when missing signature", done => {
      const req = gtx.newTransaction([keypair.pubKey]);
      req.addOperation("setMultivalue", 1, "test", "test2");
      req.addOperation("nop", randomInt(1000000));
      req.send(error => {
        const expectedError = `Expected 1 signatures, found 0`;
        expect(error?.message).to.deep.equal(new UnexpectedStatusError(400, expectedError).message);
        done();
      });
    });
  });
  describe("operations with input types", function () {
    it("Sets bool value", async () => {
      const req = gtx.newTransaction([]);
      req.setBoolean(false);
      req.addOperation("nop", randomInt(1000000));
      const response = await req.postAndWaitConfirmation();
      expect(response).to.be.null;
    });
    it("Sets int value", async () => {
      const req = gtx.newTransaction([]);
      req.setInteger(12);
      req.addOperation("nop", randomInt(1000000));
      const response = await req.postAndWaitConfirmation();
      expect(response).to.be.null;
    });
    it("Sets bigInt value", async () => {
      const req = gtx.newTransaction([]);
      req.setBigInteger(BigInt(bigIntValue));
      req.addOperation("nop", randomInt(1000000));
      const response = await req.postAndWaitConfirmation();
      expect(response).to.be.null;
    });
    it("Sets multi value", async () => {
      const req = gtx.newTransaction([keypair.pubKey]);
      req.addOperation("setMultivalue", 1, "test", "test2");
      req.addOperation("nop", randomInt(1000000));
      req.sign(keypair.privKey, keypair.pubKey);
      const response = await req.postAndWaitConfirmation();
      expect(response).to.be.null;
    });
    describe("struct", function () {
      type MultiValueTester = {
        int: number;
        string1: string;
        string2: string;
      };
      const input: MultiValueTester = {
        int: 13,
        string1: "foo",
        string2: "bar",
      };
      it("Sets int value from struct as array", async () => {
        const req = gtx.newTransaction([]);
        req.addOperation("setEntityViaStruct", [input.int, input.string1, input.string2]);
        req.addOperation("nop", randomInt(1000000));
        const response = await req.postAndWaitConfirmation();
        expect(response).to.be.null;
      });
      it("Fails to set multiValueTester value from struct when wrong order in array input", done => {
        const req = gtx.newTransaction([keypair.pubKey]);
        req.addOperation("setEntityViaStruct", [input.string1, input.int, input.string2]);
        req.addOperation("nop", randomInt(1000000));
        req.sign(keypair.privKey, keypair.pubKey);
        req.send(error => {
          expect(error?.message).to.include(new UnexpectedStatusError(400).message);
          done();
        });
      });
    });
  });

  it("fails to find operation", done => {
    const req = gtx.newTransaction([]);
    req.addOperation("notExistingOperation");
    req.send(error => {
      expect(error).to.deep.equal(new UnexpectedStatusError(400));
      done();
    });
  });
});
