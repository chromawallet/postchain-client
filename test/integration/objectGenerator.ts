import { randomInt } from "crypto";
import { toBuffer } from "../../src/formatter";
import { RawGtv } from "../../src/gtv/types";
import { RawGtx } from "../../src/gtx/types";

const brid = toBuffer(process.env.BLOCKCHAIN_RID!);

export function generateGtx(opname: string, args: RawGtv): RawGtx {
  return [
    [
      brid,
      [
        ["nop", [randomInt(1000000)]],
        [opname, [args]],
      ],
      [],
    ],
    [],
  ];
}
