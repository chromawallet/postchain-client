import { expect } from "chai";
import { Buffer } from "buffer";
import * as pc from "../../index";
import { randomInt } from "crypto";
import * as dotenv from "dotenv";
import { encode } from "../../src/gtx";
import { rawGtxToGtx } from "../../src/formatter";
import { LostMessageError, UnexpectedStatusError } from "../../src/restclient/errors";
import { getDigestToSign } from "../../src/gtx/gtx";
import { generateGtx } from "./objectGenerator";
import { LOCAL_POOL } from "../../src/constants";
dotenv.config({ path: `${__dirname}/.env` });
//to run test, start the rell dapp in folder called resources
//if you have done changes to your backend, make sure to run `chr start --wipe`
// and update the blockchainRID below.
const blockchainRid = process.env.BLOCKCHAIN_RID!;
const rest = pc.restClient.createRestClient([LOCAL_POOL], blockchainRid, 5);
const gtx = pc.gtxClient.createClient(rest, blockchainRid, [
  "setBoolean",
  "setInteger",
  "setMultivalue",
]);

describe("RestClient get transaction", function () {
  this.timeout(5000);
  let txRID: Buffer;

  it("get transaction", done => {
    const req = gtx.newTransaction([]);
    req.setBoolean(false);
    req.addOperation("nop", randomInt(1000000));
    req.postAndWaitConfirmation().then(() => {
      txRID = req.getTxRID();
      rest.getTransaction(txRID, (error, serializedMessage) => {
        expect(serializedMessage).not.to.be.null;
        expect(error).to.be.null;
        done();
      });
    });
  });

  it("get transaction invalid tx rid result in error", done => {
    const invalidTxRID = Buffer.alloc(32, 1);
    rest.getTransaction(invalidTxRID, error => {
      expect(error?.message).to.equal(
        `Unexpected status code from server. Code: 404. Message: {"error":"Can't find transaction with RID: 0101010101010101010101010101010101010101010101010101010101010101"}.`,
      );
      done();
    });
  });
});

describe("RestClient post transaction", function () {
  this.timeout(5000);

  it("posts transaction", done => {
    const gtx = generateGtx("setBoolean", 1);
    const encodedGtx = encode(gtx);
    rest.postTransaction(encodedGtx, (error, responseObject) => {
      expect(responseObject).to.eql({});
      expect(error).to.be.null;
      done();
    });
  });
  it("fails to post invalid transaction", done => {
    const gtx = generateGtx("not_existing_operation", 0);
    const encodedTX = encode(gtx);
    rest.postTransaction(encodedTX, (error, responseObject) => {
      expect(responseObject).to.deep.equal("Unknown operation: not_existing_operation");
      expect(error).to.be.an.instanceof(UnexpectedStatusError);
      expect(error).to.deep.equal(new UnexpectedStatusError(400));
      expect(error?.message).to.deep.equal(
        "Unexpected status code from server. Code: 400. Message: Unknown operation: not_existing_operation.",
      );
      done();
    });
  });
});
describe("RestClient status", function () {
  this.timeout(5000);
  let txRID: Buffer;
  it("gets status confirmed", done => {
    const req = gtx.newTransaction([]);
    req.setBoolean(false);
    req.addOperation("nop", randomInt(1000000));
    req.postAndWaitConfirmation().then(() => {
      txRID = req.getTxRID();
      rest.status(txRID, (error, responseBody) => {
        expect(responseBody?.status).to.eql("confirmed");
        done();
      });
    });
  });
  it("gets status unknown", done => {
    const invalidTxRID = Buffer.alloc(32, 1);
    rest.status(invalidTxRID, (error, responseBody) => {
      expect(responseBody?.status).to.eql("unknown");
      done();
    });
  });
  it("fails to gets status if invalid BRID", done => {
    const invalidBlockchainRid = "5EDE785A13F2793B8204CDD961DD4159742DB36B0DE4AECC0383457AEF641A00";
    const restInvalid = pc.restClient.createRestClient([LOCAL_POOL], invalidBlockchainRid, 5);
    const newTxRID = Buffer.alloc(32, 1);
    restInvalid.status(newTxRID, (error, responseBody) => {
      expect(responseBody).to.be.null;
      done();
    });
  });
});

describe("RestClient make query", function () {
  it("query data", async () => {
    const result = await rest.query("test_array", {
      arg1: ["test", "test2"],
    });
    expect(result).to.deep.equal(["test", "test2"]);
  });
  it("fail to query data", async () => {
    const expectedErrorMessage = "Unknown query: not_existing_test_array";
    const result = await rest
      .query("not_existing_test_array", {
        arg1: ["test", "test2"],
      })
      .catch(e => {
        return e;
      });

    expect(result).to.deep.equal(new UnexpectedStatusError(400, expectedErrorMessage));
  });
});

describe("RestClient wait confirmation", function () {
  it("status confirmed", async () => {
    const req = gtx.newTransaction([]);
    req.setBoolean(false);
    req.addOperation("nop", randomInt(1000000));
    await req.postAndWaitConfirmation();
    const txRID = req.getTxRID();
    const confirmation = await rest.waitConfirmation(txRID);
    expect(confirmation).to.be.null;
  });
  it("status lost message", async () => {
    const req = gtx.newTransaction([]);
    req.setBoolean(false);
    const txRID = req.getTxRID();
    const result = await rest.waitConfirmation(txRID).catch(e => {
      return e;
    });
    expect(result).to.deep.equal(new LostMessageError());
  });
});
describe("RestClient postAndWaitConfirmation", function () {
  it("transaction gets confirmed", async () => {
    const rawGtx = generateGtx("setBoolean", 0);
    const encodedGtx = encode(rawGtx);
    const gtx = rawGtxToGtx(rawGtx);
    const txRID = getDigestToSign(gtx);
    const result = await rest.postAndWaitConfirmation(encodedGtx, txRID);
    expect(result).to.be.null;
  });
  it("transaction error", async () => {
    const gtx = generateGtx("non_existing_operation", 1);
    const encodedTX = encode(gtx);
    const rawGtx = rawGtxToGtx(gtx);
    const txRID = getDigestToSign(rawGtx);
    const result = await rest.postAndWaitConfirmation(encodedTX, txRID).catch(e => {
      return e;
    });
    expect(result).to.deep.equal(new UnexpectedStatusError(400));
  });
});
