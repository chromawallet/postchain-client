import { expect } from "chai";
import * as secp256k1 from "secp256k1";
import { encodeTransaction } from "../../src/utils/encodeTransaction";
import { decodeTransactionToGtx } from "../../src/utils/decodeTransactionToGtx";
import { toBuffer } from "../../src/formatter";

describe("encode and decode transaction", () => {
  const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
  const signerPubKeyA: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyA));

  it("encodes and decodes transaction", async () => {
    const encoded = encodeTransaction({
      blockchainRid: toBuffer("BFB34ABEA1045BB97ED18A5DBB2B8DCE84CB181585D55ABB321A33133DBA0595"),
      operations: [
        {
          opName: "setInteger",
          args: [1],
        },
      ],
      signers: [signerPubKeyA],
    });

    const decoded = decodeTransactionToGtx(encoded);
    expect(JSON.stringify(decoded)).to.deep.equal(
      JSON.stringify({
        blockchainRid: {
          type: "Buffer",
          data: [
            191, 179, 74, 190, 161, 4, 91, 185, 126, 209, 138, 93, 187, 43, 141, 206, 132, 203, 24,
            21, 133, 213, 90, 187, 50, 26, 51, 19, 61, 186, 5, 149,
          ],
        },
        operations: [{ opName: "setInteger", args: [1] }],
        signers: [
          {
            type: "Buffer",
            data: [
              2, 229, 160, 24, 179, 162, 225, 85, 49, 97, 9, 217, 205, 197, 234, 183, 57, 117, 156,
              14, 7, 224, 192, 11, 249, 252, 203, 130, 55, 254, 77, 127, 2,
            ],
          },
        ],
        signatures: [],
      }),
    );
  });
});
