import { getClientConfigFromSettings } from "../../src/blockchainClient/utils";
import { describe } from "mocha";
import { expect } from "chai";
import * as dotenv from "dotenv";
import { mockStringBlockchainRid, mockStringDirectoryChainRid } from "../unit/common/mocks";
import { DEVNET_POOL, LOCAL_POOL, MAINNET_POOL } from "../../src/constants";
dotenv.config({ path: `${__dirname}/.env` });

describe("getClientConfigFromSettings", () => {
  it("should use directoryChainRid from settings when provided", async () => {
    const settings = {
      nodeUrlPool: LOCAL_POOL,
      blockchainRid: mockStringBlockchainRid,
      directoryChainRid: mockStringDirectoryChainRid,
    };

    const clientConfig = await getClientConfigFromSettings(settings);

    expect(clientConfig.directoryChainRid).to.equal(settings.directoryChainRid);
  });

  it("should set directoryChainRid using getBlockchainRidFromIid when settings.directoryChainRid is undefined on devnet", async () => {
    const mockDevNetDirectoryChainBrid =
      "58FE4D15AA5BDA450CC8E55F7ED63004AB1D2535A123F860D1643FD4108809E3";
    const settings = {
      nodeUrlPool: DEVNET_POOL,
      blockchainRid: mockStringBlockchainRid,
    };

    const clientConfig = await getClientConfigFromSettings(settings);

    expect(clientConfig.directoryChainRid).to.equal(mockDevNetDirectoryChainBrid);
  });

  it("should set directoryChainRid using getBlockchainRidFromIid when settings.directoryChainRid is undefined on mainnet", async () => {
    const mainnetDirectoryChainBrid =
      "7E5BE539EF62E48DDA7035867E67734A70833A69D2F162C457282C319AA58AE4";
    const settings = {
      nodeUrlPool: MAINNET_POOL,
      blockchainRid: mockStringBlockchainRid,
    };

    const clientConfig = await getClientConfigFromSettings(settings);

    expect(clientConfig.directoryChainRid).to.equal(mainnetDirectoryChainBrid);
  });
});
