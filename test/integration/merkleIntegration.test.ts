import { expect } from "chai";
import { Buffer } from "buffer";
import * as pc from "../../index";
import * as dotenv from "dotenv";
import { toString } from "../../src/formatter";
import { randomInt } from "crypto";
import { makeKeyPair } from "../../src/encryption/encryption";
import { LOCAL_POOL } from "../../src/constants";
dotenv.config({ path: `${__dirname}/.env` });
//to run test, start the rell dapp in folder called resources
//if you have done changes to your backend, make sure to run `chr start --wipe`
// and update the blockchainRID below.
const blockchainRid = process.env.BLOCKCHAIN_RID!;
const rest = pc.restClient.createRestClient([LOCAL_POOL], blockchainRid, 5);
const gtx = pc.gtxClient.createClient(rest, blockchainRid, [
  "setBoolean",
  "setInteger",
  "setMultivalue",
]);
describe("postchain-client txRid(merkleHash) calculates the same txRid as postchain", function () {
  const signerPrivKeyA = Buffer.alloc(32, "a");
  const keypair = makeKeyPair(signerPrivKeyA);
  it("tx rid differs if order of argument change", () => {
    const req = gtx.newTransaction([]);
    req.setMultivalue(1, "test1", "test2");
    const txRID = req.getTxRID();
    const req2 = gtx.newTransaction([]);
    req2.setMultivalue(1, "test2", "test1");
    const txRID2 = req2.getTxRID();
    expect(toString(txRID)).to.not.equal(toString(txRID2));
  });
  it("calculates same merkle as postchain of transaction with one operation with one argument", async () => {
    const req = gtx.newTransaction([]);
    req.setInteger(randomInt(1000000));
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("calculates same merkle as postchain of transaction with one argument", async () => {
    const req = gtx.newTransaction([]);
    req.setInteger(12);
    req.addOperation("nop", randomInt(1000000));
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("gets status unknown if client has calculated the wrong txRID", done => {
    const invalidTxRID = Buffer.alloc(32, 1);
    rest.status(invalidTxRID, (error, responseBody) => {
      expect(responseBody?.status).to.eql("unknown");
      done();
    });
  });
  it("calculates same merkle as postchain of transaction with multiple arguments", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.addOperation("setMultivalue", 1, "test", "test2");
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("calculates same merkle as postchain of transaction with multiple operations", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.setMultivalue(1, "test", "test2");
    req.setBoolean(true);
    req.setInteger(2);
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("calculates same merkle as postchain of transaction with array argument", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.addOperation("setArray", ["test", "test2"]);
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation();
    expect(response).to.be.null;
  });
  it("calculates same merkle as postchain of transaction with dict argument", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.addOperation("setMap", { test: "test2", test3: "test4" });
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("calculates same merkle as postchain of transaction with nested arguments", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.addOperation("nestedArguments", [
      [1, "foo", "bar"],
      ["test", "test2"],
    ]);
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
  it("calculates same merkle as postchain of transaction with super nested argument", async () => {
    const req = gtx.newTransaction([keypair.pubKey]);
    req.addOperation("superNestedArguments", [
      [
        [
          [1, "foo", "bar"],
          ["test", "test2"],
        ],
      ],
      [1],
      [{ test: "test2", test3: "test4" }],
    ]);
    req.addOperation("nop", randomInt(1000000));
    req.sign(keypair.privKey, keypair.pubKey);
    const response = await req.postAndWaitConfirmation().catch(e => {
      return e;
    });
    expect(response).to.not.deep.equal(new Error());
  });
});
