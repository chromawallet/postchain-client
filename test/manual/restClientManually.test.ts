import express, { Express } from "express";
import bodyParser from "body-parser";
import { assert } from "chai";
import { Server } from "http";
import { describe } from "mocha";
import { restClient } from "../../index";
import { RestClient } from "../../src/restclient/interfaces";

describe("Rest client tests", function () {
  let app: Express;
  let client: RestClient;
  const blockchainRid = "78967baa4768cbcef11c508326ffb13a956689fcb6dc3ba17f4b895cbb1577a3";
  let server: Server;
  let port = 9000;

  beforeEach(function (done) {
    port++;
    app = express();
    app.use(bodyParser.raw());
    app.use(bodyParser.urlencoded({ extended: true }));
    server = app.listen(port, () => {
      client = restClient.createRestClient(
        [`http://localhost:${port}`, `http://localhost:${port}`],
        blockchainRid,
        5,
        100,
        { attemptsPerEndpoint: 1, attemptInterval: 100 },
      );
      done();
    });
  });

  afterEach(() => {
    server.close();
  });

  describe("Many request test", () => {
    it("Test 1000 simultaneous requests", done => {
      // This test is making sure that connection pooling works well.
      app.post(`/query/${blockchainRid}`, (req, res) => {
        assert.deepEqual(req.body, { type: "aQueryType" });
        setTimeout(() => res.status(200).send({ status: "OK" }), 10);
      });

      const callCount = 1000;
      let responseCount = 0;
      for (let i = 0; i < callCount; i++) {
        client
          .query({ type: "aQueryType" })
          .then(res => {
            assert.deepEqual(res, { status: "OK" });
            responseCount++;
            if (responseCount === callCount) {
              done();
            }
          })
          .catch(e => {
            //No errors should be thrown
            assert.ok(e == null);
          });
      }
    });
  });
});
