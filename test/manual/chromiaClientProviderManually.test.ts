import * as pc from "../../index";
import { chromiaClientProvider } from "../../src/chromia/chromiaClientProvider";
import { ChromiaClient } from "../../src/chromia/interfaces";
import { LOCAL_POOL } from "../../src/constants";
import { mockStringBlockchainRid } from "../unit/common/mocks";

describe("chromia client provider", function () {
  let chromiaClient: ChromiaClient;

  it("create blockchain connection to list of nodes where the dapp is located", async () => {
    const brid = "F9037C2F2C8E20A1D9EFCD8F9FB7C963CD852C9062A495D8BAD7965D278D8FC4";
    const baseUrl = LOCAL_POOL;
    const rest = pc.restClient.createRestClient([baseUrl], brid);
    chromiaClient = chromiaClientProvider(mockStringBlockchainRid, rest);

    //have to add the example dapp to the node
    const bridDapp = "FBEE1A0DED02CF6E222C709735326ECA716F2956C1D910FDE865BC4ADF3ACAEF";

    const blockchainClient = await chromiaClient.blockchainConnection(bridDapp);
    const urls = blockchainClient.getEndpointPool();
    console.log(urls);
  });
});
