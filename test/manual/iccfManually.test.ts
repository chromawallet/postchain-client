import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import * as secp256k1 from "secp256k1";
import { randomInt } from "crypto";

import { createIccfProofTx } from "../../src/ICCF/IccfProofTxMaterialBuilder";
import { signDigest } from "../../src/encryption/encryption";
import { IClient } from "../../src/blockchainClient/interface";
import { getDigestToSign } from "../../src/gtx/gtx";
import { GTX, GtxBody, RawGtxBody } from "../../src/gtx/types";
import { createClient } from "../../src/blockchainClient/blockchainClient";
import { toBuffer } from "../../src/formatter";
import { SignatureProvider, Transaction } from "../../src/blockchainClient/types";
import * as gtxTool from "../../src/gtx/gtx";
import { sleep } from "../../src/restclient/restclientutil";
import { DEVNET_POOL } from "../../src/constants";

const expect = chai.expect;
chai.use(chaiAsPromised);

const signerPrivKeyA: Buffer = Buffer.alloc(32, "a");
const signerPubKeyA: Buffer = Buffer.from(secp256k1.publicKeyCreate(signerPrivKeyA));
const sigProvA: SignatureProvider = {
  pubKey: signerPubKeyA,
  sign: async (rawGtxBody: RawGtxBody) => {
    const digest = gtxTool.getDigestToSignFromRawGtxBody(rawGtxBody);
    return signDigest(digest, signerPrivKeyA);
  },
};

const getTxWithGTXAndTxRID = (client: IClient) => {
  const operation = {
    name: "create_foo",
    args: ["carl"],
  };

  const gtxBody: GtxBody = {
    blockchainRid: toBuffer(client.config.blockchainRid),
    operations: [
      {
        opName: operation.name,
        args: operation.args,
      },
      {
        opName: "nop",
        args: [randomInt(10000)],
      },
    ],
    signers: [signerPubKeyA],
  };
  const gtx: GTX = {
    ...gtxBody,
    signatures: [],
  };
  const tx: Transaction = {
    operations: [
      operation,
      {
        name: "nop",
        args: [randomInt(10000)],
      },
    ],
    signers: [signerPubKeyA],
  };
  const txRID = getDigestToSign(gtx);

  return { operation, gtx, txRID, tx };
};

describe("ICCF", () => {
  let client: Promise<IClient>;
  const sourceBlockchainRid = "E65B249DCB9A34FFD43B58D49E8234652BC3DC10EC455AB2729B7AEFE6E8B878";

  before(done => {
    client = createClient({
      nodeUrlPool: DEVNET_POOL,
      blockchainRid: sourceBlockchainRid,
    }).finally(() => {
      done();
    });
  });

  describe("Proof", async () => {
    it("should return correct iccfTx for intra-network operation", async () => {
      const clientConfiguredToD1 = await createClient({
        directoryNodeUrlPool: DEVNET_POOL,
        blockchainRid: "162777934BD83E40EBE189B387C382F79CB83BCDE0FC32E9E8CB53B2934FF247",
      });

      const _client = await client;

      const { tx } = getTxWithGTXAndTxRID(_client);

      const signedTx = await _client.signTransaction(tx, sigProvA);
      const deserialized = gtxTool.deserialize(signedTx);

      const hashToProve = gtxTool.getDigest(deserialized);
      console.log("hash to be proven: ", hashToProve);

      const receipt = await _client.sendTransaction(signedTx);
      await sleep(4000);
      const iccfTx = await createIccfProofTx(
        clientConfiguredToD1,
        receipt.transactionRid,
        Buffer.from("sample-tx-hash"),
        [signerPubKeyA],
        sourceBlockchainRid,
        sourceBlockchainRid,
        [signerPubKeyA],
        true,
      );
      console.log("res iccf tx: ", JSON.stringify(iccfTx.iccfTx));

      expect(iccfTx.iccfTx).to.equal({
        operations: [
          {
            name: "iccf_proof",
            args: [
              sourceBlockchainRid,
              hashToProve,
              { hash: hashToProve },
              "anchoring-tx-data",
              0,
              { hash: "anchoring-tx-rid" },
            ],
          },
        ],
        signers: [signerPubKeyA],
      });
    }).timeout(8000);

    it.skip("should return correct iccfTx for intra-cluster operation");
    it.skip(
      "should throw error if block with the desired tx is not present in cluster anchoring chain",
    );
    it.skip("should throw error if source blockchain can not be found");
    it.skip("should throw error if target blockchain can not be found");
  });
});
