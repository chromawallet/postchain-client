Version 1.21.0

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.21.0]

### Changed

- Various input types of `Buffer` or `string` to `BufferId` (`Buffer | string`) for function parameters that expected transaction RIDs or blockchain RIDs. The following functions are affected:
  - `getTransaction` parameter `transactionRid`
  - `getTransactionStatus` parameter `transactionRid`
  - `getTransactionInfo` parameter `transactionRid`
  - `getTransactionConfirmationLevel` parameter `transactionRid`
  - `getConfirmationProof` parameter `txRID`
  - `getClusterAnchoringTransactionConfirmation` parameter `transactionRid`
  - `getSystemAnchoringTransactionConfirmation` parameter `anchoredTxRid`
  - `getGTXFromBufferOrTransactionOrOperation` parameter `blockchainRid`
  - `ConfirmationProofException` constructor parameter `transactionRid`
  - `createIccfProofTx` parameters `txToProveRid`, `sourceBlockchainRid` and `targetBlockchainRid`
  - `getBlockAnchoringTransaction` parameter `txRid`
  - `isBlockAnchored` parameter `txRid`
  - `getAnchoringClient` parameter `dappBlockchainRid`
  - `fetchAndVerifyTransaction` parameter `txToProveRID`
  - `composeProofTransactionObject` parameter `sourceBlockchainRid`
- The `client` function `getClusterAnchoringTransactionConfirmation` now takes an additional input argument of `anchoringClient`
- The `client` function `getSystemAnchoringTransactionConfirmation` now takes two additional input arguments of `anchoringClient` and `systemAnchoringChainRid`
- Changed the response type of the function `getSystemAnchoringChain` to now return `Buffer` instead of `Buffer | null`. The function will instead
  throw a `SystemChainException` instead if `null` is returned from the query, informing the user an invalid client was provided.
- The `client` function `getClusterAnchoringTransactionConfirmation` now takes an additional input argument of `anchoringClient`
- The `client` function `getSystemAnchoringTransactionConfirmation` now takes two additional input arguments of `anchoringClient` and `systemAnchoringChainRid`
- Changed the response type of the function `getSystemAnchoringChain` to now return `Buffer` instead of `Buffer | null`. The function will instead
  throw a `SystemChainException` instead if `null` is returned from the query, informing the user an invalid client was provided.
- Updated the types `ClientConfig` and `NetworkSettings` to contain separate status polling configurations (interval and count),
  allowing for different polling setups for dapp, cluster and system requests
- Updated the polling functions `getBlockAnchoringTransaction` and `awaitGetAnchoringTransactionForBlockRid` to receive the correct status polling properties
- The function `checkDigestSignature` to expect a `Buffer` signature instead of `Buffer` or `undefined`.

### Added

- The function `ensureString` that receives a `BufferId` and returns a hex string in upper case
- Updated the interface `TransactionReceipt` to contain the optional parameters `clusterAnchoringClientBrid` and `systemAnchoringClientBrid` that are populated
  once a transaction is anchored in Cluster Anchoring Chain and System Anchoring Chain
- The utility function `getAnchoringClientAndSystemChainRid` that returns the type `AnchoringClientAndSystemBrid` providing it with a dapp `client`.
  This utility function should be used to provide the correct inputs for the client functions `clusterAnchoringClientBrid` and `systemAnchoringClientBrid`
- The type `AnchoringClientAndSystemBrid` that returns an `anchoringClient` and the `systemAnchoringChainBridString`
- The types `TransactionsCount` and `TransactionConfirmationProof`
- Several zod validation functions, to be used for the api responses under the type `ResponseObject` and property `rspBody`
- The function `getSystemAnchoringTransaction` that returns a system anchored transaction or null if the cluster anchoring transaction is not anchored in the system anchoring chain
- The client function `getAnchoringStatusForBlockRid` that returns `BlockAnchoringState`
- The type `BlockAnchoringState` to be used as return type for `getAnchoringStatusForBlockRid`
- Created the utility function `setStatusPolling` that can be used to initialize default status polling properties if not specified
- The type `StatusPolling` to be used for the dapp, cluster and system polling config in the types `ClientConfig` and `NetworkSettings`
- Promi event enums `TransactionEvent` with values `ClusterAnchorConfirmed`, `SystemAnchorConfirmed`, `DappConfirmed`, `DappReceived`, `Signed`
- Promi events are emitted in the order of the transaction lifecycle/confirmation steps

### Fixed

- The `client` function `getClusterAnchoringTransactionConfirmation` now takes an additional input argument of `anchoringClient`
- The `client` function `getSystemAnchoringTransactionConfirmation` now takes two additional input arguments of `anchoringClient` and `systemAnchoringChainRid`
- Changed the response type of the function `getSystemAnchoringChain` to now return `Buffer` instead of `Buffer | null`. The function will instead
  throw a `SystemChainException` instead if `null` is returned from the query, informing the user an invalid client was provided.
- Changed the generic `ResponseObject` to a strict type. The property `rspBody` is of type `unknown` and it is validated for each different case
  using zod validation functions for the expected type
- An issue in the function `checkGTXSignatures` that will now return `false` if the `gtx.signatures` array is empty or not an array

### Removed

- The types `GetBlocksResponse` and `GetBlockResponse`
- The promi event "sent" is no longer emitted

## [1.20.2]

### Fixed

- Resolved an issue where retrieving an anchoring transaction and its ICCF proof could result in mismatch if the queries were made against unsynchronized nodes

## [1.20.1]

### Fixed

- An issue with the escaping of characters in the generated package.json content of the esm build
- Added @types/bn.js as runtime dependencies to fix the missing decleration one gets after installing postchain-client and trying to transpile from typescript to javascript

## [1.20.0]

### Added

- The function nullable `getTransactionConfirmationLevel` in `createClient` that expects a `transactionRid` and returns a `TransactionReceipt`
  that indicates the last successful confirmation of a transaction, as well as an anchoring transaction if the transaction is added in a block (Cluster Anchoring Confirmation).
- The function `getClusterAnchoringTransactionConfirmation` in `createClient`, that given a `transactionRid` will return an anchoring transaction
- Added `rollup-plugin-visualizer` to the Rollup configuration for bundle analysis.
- Added tests coverage scripts in package.json
- Created the client function `getSystemAnchoringTransactionConfirmation` that returns a system anchored transaction based on the provided anchored transaction rid
- Added the util function `getSystemAnchoringChain` that queries the directory client and returns the system anchoring chain rid
- Extended the logic of the function `sendTransaction`, `signAndSendUniqueTransaction` and `getTransactionConfirmationLevel` to also include system anchoring cluster confirmation
- Added the type `BufferId = string | Buffer` that also supports `ensureBuffer`

### Fixed

- A bug in the function `getAnchoringTransactionForBlockRid` that was not handling nullable response, given the query
  `get_anchoring_transaction_for_block_rid` can return null
- Moved validation tests from the `src/blockchainClient/validation` to `test/unit/validation`
- Fixed a bug in the function `getAnchoringClient` that did not set the created client network settings to the one provided as input argument to the function

### Changed

- Extended the `createClient` functions `sendTransaction` and `signAndSendUniqueTransaction` to accept a new input parameter `confirmationLevel`
  that will resolve the promise based on the configurable confirmation selection and will emit a “sent” promiEvent with the confirmation status in each confirmation step
- Extended the parameters of the types `ClientConfig` and `NetworkSettings` to contain `directoryChainRid`
- Extended the function `getBlockAnchoringTransaction` to retrieve an anchoring transaction with polling
- Updated the callback type of `getTransactionStatus` to `RestClientCallback<TransactionReceipt> | RestClientCallback<StatusObject>`
- Updated the callback type of `getTransactionConfirmationLevel` to `RestClientCallback<StatusObject | TransactionReceipt>`
- Updated the callback type of `getClusterAnchoringTransactionConfirmation` to `RestClientCallback<TransactionReceipt>`
- Updated the rest client `postTransaction` function, to support binary formatted transactions

## [1.19.1]

### Fixed

- bumped secp256k1 dependency to 4.0.4, to fix vulnerability

## [1.19.0]

### Added

- added new request strategy `Query Majority`. (See README for more information)
- new `getBlocks` method that has additional query params to search for blocks

### Fixed

- bug in decodeTransactionToGtx that occurs when data has Bigint values

### Deprecated

- `getBlocksInfo` method in `IClient` interface. Use `getBlocks` instead.

## [1.18.0]

### Changed

- release pipeline
- replace node.js server with MSW in iccf unit tests
- replace node.js server with MSW in restClient unit tests
- replace node.js server with MSW in blockChainClient utils' unit tests

### Added

- the possibliity to set `useStickyNode` in the config of `blockchainClient` and `restClient`.
- release guideline
- dumpGtx and dumpGtv util functions for debugging purposes. This functions transforms a GTX or GTV object into a human-readable string.

## [1.17.0]

### Added

- `getAppStructure` method to `IClient` interface that returns app structure in JSON format

### Changed

- Husky will no longer install in CI
- Added deprecation annotation as a function signature overload for `setLogLevel`
- pipelines for version release
- rework ResponseObject and add generic types TData, TErorr to it
- add RetryRequestProps type to RetryRequest and singleEndpoint methods

### Fixed

- typos across all app
- Updates wrong types for nodeUrl and nodeUrlPool in MissingNodeUrlError

## [1.16.1]

### Added

- `convertToRellOperation` is now part of pcl public API

## [1.16.0]

### Changed

- Separated non-backend dependent functions from interface `IClient`. Functions `getTransactionRid`, `encodeTransaction`, and `decodeTransactionToGtx` have been extracted and are now separate utility functions.
- Renamed `getTransactionRid` to `calculateTransactionRid` for clarity. Its input type has been changed to `RawGtxBody`. Conversion functions have been introduced to facilitate this transition.
- Modified the input type of `encodeTransaction` to `GTX`.

- Use PascalCase for types and class names:
  - `getTransactionRidException` -> `GetTransactionRidException`.
  - `transactionInfoInBlockResponse`-> `TransactionInfoInBlockResponse`.
  - `transactionInfoInBlock` -> `TransactionInfoInBlock`.
- change `npm install` to `npm ci` command

- split unit tests for `sendTransaction` to separate file. Replace Express mock server with MSW.
- rework validation for client and server errors. Now it covers all 4xx and status codes.
- Replace node server with MSW for unit tests in blockchainClient.test.ts.

### Added

- function for fetching information about the latest block for a chain. The funcition is added to the `IClient` interface and is called `getLatestBlock`.
- verify if changelog changed in pipeline to prevent merging branch without updating it
- create a custom error which include the http status code. Exported as `CustomError`.
- automate integration tests to get BRID from running test node.
- strictNullChecks in tsconfig.json to provide better type checking

### Fixed

    - bug in retryRequest function which in some cases treated https errors as a successfull request.

## [1.15.2]

### Fixed

- promiEvent ts error in npm package

## [1.15.1]

### Changed

- Removed library dependency: `web3-core-promievent`.
- The type `Web3PromiEvent` is now exported publicly.

## [1.15.0]

### Changed

- The types `RawGtxBody` and `RawGtxOp`are now exported publicly.
- function `getDigestToSignFromRawGtxBody`is now exported publicly.
- Changes `SignatureProvider` type callback method `sign()` to provide a paramater of type `rawGtxBody` instead of a `digest`. This change is implemented to make it possible to review the content of a transaction before signing.

## [1.14.0]

### Added

- better input validation for blockChainClient

### Fixed

- logLevel=ERROR will now only logs errors
- getConfirmationProof will now properly retrieve the transaction index

## [1.13.0]

### Added

- Both `sendTransaction` and `signAndSendUniqueTransaction` now takes a new flag parameter `doStatusPolling` which can be set to either `true` or `false`. Setting the flag to `false` will prevent any further automatic status polling requests from being sent for the transaction. The method will in this case return with a final transaction status of `waiting`.
- logLevel can be enum with values: [DEBUG, INFO, WARNING, ERROR]
- logger can be configured through node env i.e. LOG_LEVEL=DEBUG
- add `disable` method to logger
- Adds a `createStubClient` function returning an `IClient`. All methods returning a `Promise` return a rejected `Promise`, all methods which return `Web3PromiEvent` return a rejected `Web3PromiEvent` and other methods just throw an `Error`.

### Fixed

- bug in return type of `getAnchoringTransactionForBlockRid`.
- enhanced the README by providing a more detailed guide on creating a client, including an expanded description of its configuration settings.

## [1.12.0]

### Added

- Node URLs in the pool will be marked as 'unreachable' if a request returns a 50\* status. This feature, part of FailoverConfig, suspends a node's usage for a default duration of 30,000 milliseconds (5 minutes). Clients can be configure during initialization for tailored duration.

## [1.11.0]

### Added

- Adds client method `getBlockInfo` to enable the user to fetch the data for a single block by providing either `height` or `txRid`.

## [1.10.0]

### Changed

- `pollingInterval` has changed name to `statusPollInterval` in `NetworkSettings`. The reason is to easier understand what the polling interval is used for.
- `sendTransaction` and `signAndSendUniqueTransaction` continue polling the status of the transaction if the last returned status was `unknown`. Previously only status `waiting` triggered new fetching. To avoid polling infinitely, a maximum number has been added. It is configurable when creating a client, see section Added.
- remove index signature in exported type `GTX`.

```
export type GTX = {
  [x: string]: unknown;  // <---- removed
  blockchainRid: Buffer;
  operations: RellOperation[];
  signers: Buffer[];
  signatures?: Buffer[];
};
```

### Added

- possibility to configure the maximum number of times `sendTransaction` and`signAndSendUniqueTransaction` should poll for the status before quitting. It is included in `NetworkSettings` and is called `statusPollCount`. Default value is 20.

### Fixed

- `@types/node` as `devDependencies` instead of `dependencies`.

## [1.9.0]

### Added

- add the package `events` as peer-dependency. Solves users issue of missing `events` when using the lib in a non Node environment.
- function in IClient for fetching blocks info for a chain. It is called `getBlocksInfo`.

### Fixed

- correct small mistakes in README.
- fix bug in function `getDigestToSignFromBody`. It should take an input as type `RawGtxBody` instead of `GtxBody`. The function is renamed to `getDigestToSignFromRawGtxBody`.
- correct mistake in README in the instructions of a function called `askUserBToSign`.

## [1.8.1]

### Fixed

- export enum `FailoverStrategy`.

## [1.8.0]

### Changed

- Type called `KeyPair` changed from having `pubKey`as optional to being mandatory.
- `signTransaction` and `signAndSendUniqueTransaction` no longer accept `signMethod` as type `PrivKey`, instead it have to be a `KeyPair` or `SignatureProvider`.
- Changed plugin `commonJS()` of ESM build-step in `Rollup.config.mjs` to transform mixed modules to ESM modules.
- Moved up plugin `resolve()` of ESM build-step in `Rollup.config.mjs` to top of plugin list.

### Fixed

- randomly pick node from `directoryNodeUrlPool` when querying for the brid of directory chain.
- `txRejectedError` to have public properties.

## [1.7.1] - 2023-09-28

### Fixed

- When using `QueryObject`, you must specify return type.
- Use type `DictPair` instead of `QueryArguments` alias.

## [1.7.0] - 2023-09-27

### Fixed

- Change order of generic types of `query` to `query<TReturn, TArgs>`.

### Added

- Generic type of QueryObject, `QueryObject<TReturn, TArgs>`.

## [1.6.0] - 2023-09-22

### Added

- `BlockchainUrlUndefinedException` exported.
- in IClient, `getTransactionsInfo` which fetch info about transactions on the chain.
- in IClient, `getTransactionInfo` which fetch info about a transaction.
- in IClient, `getTransactionCount` which fetch number of successful transactions on the chain.

### Fixed

- bug in `TxRejectedError`, the error will print the rejection reason if it exist. From the error it is possible to get `shortReason`, `rellLine`, and `operation` (used like `error.operation`).
- updating to camelCase for the abbreviations. blockchainRID -> blockchainRid.

## [1.5.10] - 2023-09-15

### Added

- Ability to choose a failover strategy for a client. The following three are available: `AbortOnError`, `TryNextOnError`, `SingleEndpoint`. A description can be found in the README.

### Fixed

- encode txProof and anchroingProof that are included as arguments in iccf_proof operation
- remove check for only blockchainRid when initialize client, new flow either blockchainRid or blockchainIid need to exist.

## [1.5.9] - 2023-08-30

### Fixed

- Set content-type to octet-stream when making post requests

## [1.5.8] - 2023-08-25

### Added

- Abillity to check if transaction is anchored, `isBlockAnchored`, `getBlockAnchoringTransaction`,
- create client configured to anchoring chain, `getAnchoringClient`
- export ICCF types and errors
- improved error handling in get requests. When status code is not 200, it returns error
  containing information from servers error message.

## [1.5.7] - 2023-08-15

### Added

- Export custom errors
- ICCF experimental feature, exported function called `createIccfProofTx`

## [1.5.6] - 2023-07-07

### Fixed

- Add type `ReadonlyArray<RawGtv>` to union of `RawGtv`
- uncought promise in send transaction

## [1.5.5] - 2023-07-03

### Fixed

- Sort string maps before encoding
- Because of postchain update, add HTTP header to accept response as binary instead of JSON

## [1.5.4] - 2023-06-28

### Fixed

- Explicit import of Buffer
- Export type IClient
- Fix bug when using client initialized with blockchainID

## [1.5.3] - 2023-06-22

### Fixed

- Buffer as peer dependency

## [1.5.2] - 2023-06-20

### Fixed

- Export commonly used types

## [1.5.1] - 2023-06-20

### Fixed

- Move lodash from devDependency to dependency

## [1.5.0] - 2023-06-14

### Added

- support for negative numbers

## [1.4.0] - 2023-06-14

### Added

- simplifyed API, exported as `createClient` from `index.js`, simplifies usage by reducing boilerplate code for users. Its design focuses on streamlining the API and minimizing setup complexities, resulting in a more user-friendly experience.

## [1.3.2] - 2023-04-28

### Fixed

- bug when logger prints a bigInt

## [1.3.1] - 2023-04-26

### Fixed

- bug when sending big integer in transaction

## [1.3.0] - 2023-04-19

### Added

- bigInt support

## [1.2.0] - 2023-03-27

### Added

- integration tests for merkle root calculation

### Changed

- `newSignatureProvider()` now has keyPair as optional argument. If no keyPair is provided it will instead be generated.

## [1.1.0] - 2023-02-17

### Added

- signatureProvider interface and newSignatureProvider()

### Removed

- `RestClient.getConfirmationProof` function

### Changed

- `ITransaction.sign` now can be used with a single argument, which can implement the signatureProvider interface or be the privKey. This function is now async
- `GTX.sign` now has two overloads, both of which are async. The GTX is now passed as first parameter.
- `RestClient.query`and `GtxClient.query` now has two overloads, the new signature takes the query name and query arguments as separate inputs. The old signature is marked as depricated an will be removed in the future.

### Fixed

- `gtx` is now correctly typed in both `GTX.sign` and `GTX.addSignature`

## [1.0.3] - 2023-02-03

### Added

- Compatible with ESM, CommonJS and UMD
- Boolean support

### Changed

- A query uses postchain endpoint called `query_gtv` which means that data is sent and recieved in GTV-encoded format.
- Release notes now in changelog file.
- Node 18 required when using this library in a Node environment.

## [1.0.2] - 2022-12-23

### Added

- DecodeValueGtx ensures type of RawGtx before return. (commit missed in last release)

### Changed

- Rename export from chromiaClientProvider to chromiaClient.

## [1.0.1] - 2022-12-21

### Changed

- Update types of input parameters and return types of the following functions:

  - encodeValue (serialization.ts)
  - decodeValue (serialization.ts)
  - gtvHash (src/gtv/index.ts)

- Set default logging to be less verbose

### Fixed

- Bug in function blockchainConnection (chromiaClientProvider.ts)

## [1.0.0] - 2022-12-09

### Added

- New logger accessible in index.ts.
- Enable the client to connect to a blockchain through multiple nodes running the blockchain.
- Load balancing by randomly distributing transactions and queries between nodes.
- Retry policy added for request to the blockchain.
- Enables you to discover the nodes running your blockchain by querying D1 with your dapp´s blockchain RID. Read more in the Chromia client providers [Readme](./src/chromia/README.md).

### Changed

- Previously a rest client was initialized with one string containing the address of the node running your blockchain. Now an instance of the rest client is initiated with a list of strings representing the addresses of the nodes where your dapp is running.
- Previously a rest client query was called with two parameters; queryName and queryObject. Now this call only takes one parameter called queryObject, which is defined as:

```javascript
{
    type: string;
    [Arg
:
    string
]:
    RawGtv;
}
```

where type is what previously was called query name.

## [0.\*.*]

Early version of the postchain-client written in javascript.
