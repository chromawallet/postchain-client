import { Buffer } from "buffer";
import { GTX, RawGtx, RawGtxBody } from "./gtx/types";
import { QueryObject, QueryObjectGTV } from "./restclient/types";
import { DictPair } from "./gtv/types";
import { BufferId } from "./blockchainClient/types";

export function pgBytes(buffer: Buffer): string {
  if (!Buffer.isBuffer(buffer)) {
    throw new PgBytesInputException(buffer);
  }
  return `\\x${buffer.toString("hex")}`;
}

/**
 * Converts hex string to Buffer
 * @param key: string
 * @returns {Buffer}
 */
export function toBuffer(key: string): Buffer {
  return Buffer.from(key, "hex");
}

/**
 * Converts Buffer to hex string
 * @param buffer: Buffer
 * @returns {string}
 */
export function toString(buffer: Buffer): string {
  return buffer.toString("hex").toUpperCase();
}

export function toQueryObjectGTV(
  nameOrObject: string | QueryObject,
  queryArguments?: DictPair,
): QueryObjectGTV {
  let name;
  if (typeof nameOrObject === "string") {
    name = nameOrObject;
    return [name, { ...queryArguments }];
  } else {
    const objectCopy = { ...nameOrObject };
    const { type, ...restProps } = objectCopy;

    return [type, restProps];
  }
}

export class PgBytesInputException extends Error {
  constructor(buffer: Buffer) {
    super(`util.pgBytes expects a buffer, but got ${typeof buffer}`);
  }
}

export function ensureBuffer(value: BufferId): Buffer {
  if (value instanceof Buffer) {
    return value;
  } else {
    return toBuffer(value);
  }
}

export function ensureString(value: BufferId): string {
  if (typeof value === "string") {
    return value.toUpperCase();
  } else {
    return toString(value);
  }
}

export function checkGtvType(value: any): boolean {
  try {
    if (value == null) {
      return true;
    }
    if (Buffer.isBuffer(value)) {
      return true;
    }
    if (typeof value === "string") {
      return true;
    }
    if (typeof value === "number") {
      if (!Number.isInteger(value)) {
        throw Error("User error: Only integers are supported");
      }
      return true;
    }
    if (typeof value === "bigint") {
      return true;
    }
    if (value.constructor === Array) {
      value.map(item => checkGtvType(item));
      return true;
    }
    if (typeof value === "object") {
      Object.keys(value).map(function (key) {
        checkGtvType(value[key]);
      });
      return true;
    }
  } catch (error) {
    throw new Error(`Failed to check type: ${error}`);
  }
  return false;
}

export function rawGtxToGtx(rawGtx: RawGtx): GTX {
  const rawGtxBody = rawGtx[0];
  const signatures = rawGtx[1];

  const gtxBody = {
    blockchainRid: rawGtxBody[0],
    operations: rawGtxBody[1].map((operation: any[]) => ({
      opName: operation[0],
      args: operation[1],
    })),
    signers: rawGtxBody[2],
  };

  return {
    blockchainRid: gtxBody.blockchainRid,
    operations: gtxBody.operations,
    signers: gtxBody.signers,
    signatures,
  };
}

export function rawGtxToRawGtxBody(rawGtx: RawGtx): RawGtxBody {
  return rawGtx[0];
}

export function checkGtxType(value: any): boolean {
  try {
    rawGtxToGtx(value);
    return true;
  } catch (error) {
    return false;
  }
}

export function removeDuplicateSigners(signers: Buffer[]) {
  const signersAsString: string[] = [];
  signers.forEach(item => {
    const itemAsString = item.toString("hex");
    if (!signersAsString.includes(itemAsString)) {
      signersAsString.push(itemAsString);
    }
  });
  const result: Buffer[] = [];
  signersAsString.forEach(item => {
    result.push(Buffer.from(item, "hex"));
  });
  return result;
}

export function matchRellErrorString(rellError: string) {
  const parsed = rellError.match(/\[([^\]]+)\]\sOperation\s'([^']+)'\sfailed:\s(.+)$/);

  if (!parsed) return {};

  const [rellLine, operation, shortReason] = parsed.slice(1);

  return { shortReason, rellLine, operation };
}
