const { Buffer } = require('buffer');
var serialization = require('../gtx/serialization')
var HASH_PREFIX_LEAF = require('./binarytree').HASH_PREFIX_LEAF
var encryption = require('../../src/encryption/encryption')

function CryptoSystem() {}
CryptoSystem.prototype.digest = function(buffer) {
  return encryption.hash256(buffer)
}

/**
 * 
 * @param {Buffer} buffer 
 * @param {CryptoSystem} cryptoSystem 
 */
function hashingFun(buffer, cryptoSystem) {
  if (cryptoSystem === null) {
    throw new Error("In this case we need the CryptoSystem to calculate the hash")
  } else {
    return cryptoSystem.digest(buffer)
  }
}

/**
 * 
 * @param {CryptoSystem} cryptoSystem 
 */
function MerkleHashCalculator(cryptoSystem) {
  this.cryptoSystem = cryptoSystem
}

/**
 * @param {number} prefix
 * @param {Buffer} hashLeft
 * @param {Buffer} hashRight
 */
MerkleHashCalculator.prototype.calculateNodeHash = function(prefix, hashLeft, hashRight) {
  return this.calculateNodeHashInternal(prefix, hashLeft, hashRight, hashingFun)
}

/**
 * @param {*} value
 */
MerkleHashCalculator.prototype.calculateLeafHash = function(value) {
  return this.calculateHashOfValueInternal(value, serialization.encodeValue, hashingFun)
}

/**
 * @param {number} prefix
 * @param {Buffer} hashLeft
 * @param {Buffer} hashRight
 */
MerkleHashCalculator.prototype.calculateNodeHashInternal = function(prefix, hashLeft, hashRight, hashFunc) {
  var buf = Buffer.alloc(1)
  buf.writeInt8(prefix)
  var bufferSum = Buffer.concat([buf, hashLeft, hashRight])
  return hashFunc(bufferSum, this.cryptoSystem)
}

MerkleHashCalculator.prototype.calculateHashOfValueInternal = function(valuetoHash, serializeFun, hashFunc) {
  var buf = Buffer.alloc(1)
  buf.writeInt8(HASH_PREFIX_LEAF)
  var bufferSum = Buffer.concat([buf, serializeFun(valuetoHash)])
  return hashFunc(bufferSum, this.cryptoSystem)
}

MerkleHashCalculator.prototype.isContainerProofValueLeaf = function(value) {
  if (value == null) {
    return false
  }
  if (value.constructor === Array || typeof value === 'object') {
    return true
  } else {
    return false
  }
}

module.exports = {MerkleHashCalculator, CryptoSystem};