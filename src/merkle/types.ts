export type Step = {
    side: number;
    hash: Buffer;
}

export type StringStep = {
    side: number;
    hash: string;
}

export type Path = Step[];
export type StringPath = StringStep[];




