"use strict"

/**
 * 
 * @param {Buffer} merkleHash 
 */
function MerkleHashSummary(merkleHash) {
  this.merkleHash = merkleHash
}

/**
 * @param {MerkleHashSummary} other
 */
MerkleHashSummary.prototype.equals = function(other) {
  if (this === other) return true
  if (typeof this != typeof other) return false

  if (this.merkleHash.compare(other.merkleHash) === 0) {
    return true
  }
  return false
}


module.exports = {MerkleHashSummary}