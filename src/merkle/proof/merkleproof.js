"use strict"

var MerkleHashCalculator = require('../merklehashcalculator').MerkleHashCalculator
var MerkleHashSummaryFactory = require('./merklehashsummaryfactory').MerkleHashSummaryFactory
var BinaryTreeFactory = require('../binarytreefactory').BinaryTreeFactory
var MerkleProofTreeFactory = require('./merkleprooftreefactory').MerkleProofTreeFactory
var treeFactory = new BinaryTreeFactory()
var proofFactory = new MerkleProofTreeFactory()
var PathSet = require('../path').PathSet
var MerkleProofTree = require('./merkleprooftree').MerkleProofTree

/**
 * Calculates the merkle root hash of the structure.
 * 
 * @param {any} value
 * @param {MerkleHashCalculator} calculator describes the method we use for hashing and serialization
 * @return the merkle root hash (32 bytes) of the data structure.
 */
function merkleHash(value, calculator) {
  return merkleHashSummary(value, calculator).merkleHash
}

/**
 * 
 * @param {MerkleProofTree} tree 
 * @param {MerkleHashCalculator} calculator 
 */
function merkleTreeHash(tree, calculator) {
  return merkleProofHashSummary(tree, calculator).merkleHash
}

/**
 * Calculates the merkle root hash of the structure
 *
 * @param {any} value
 * @param {MerkleHashCalculator} calculator describes the method we use for hashing and serialization
 * @return the merkle root hash summary
 */
function merkleHashSummary(value, calculator) {
  var summaryFactory = new MerkleHashSummaryFactory(treeFactory, proofFactory)
  return summaryFactory.calculateMerkleRoot(value, calculator)
}

/**
 * 
 * @param {MerkleProofTree} tree 
 * @param {MerkleHashCalculator} calculator 
 */
function merkleProofHashSummary(tree, calculator) {
  var summaryFactory = new MerkleHashSummaryFactory(treeFactory, proofFactory)
  return summaryFactory.calculateMerkleTreeRoot(tree, calculator)  
}

/**
 * 
 * @param {any} value
 * @param {PathSet} pathSet 
 * @param {MerkleHashCalculator} calculator 
 */
function generateProof(value, pathSet, calculator) {
  var binaryTree = treeFactory.buildWithPath(value, pathSet)
  return proofFactory.buildFromBinaryTree(binaryTree, calculator)
}

module.exports = {merkleHash, merkleTreeHash, merkleHashSummary, generateProof}