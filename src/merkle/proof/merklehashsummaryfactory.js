var BinaryTreeFactory = require('../binarytreefactory').BinaryTreeFactory
var MerkleProofTreeFactory = require('./merkleprooftreefactory').MerkleProofTreeFactory
var MerkleHashCalculator = require('../merklehashcalculator').MerkleHashCalculator
var MerkleProofTree =  require('./merkleprooftree').MerkleProofTree
var MerkleProofElement = require('./merkleprooftree').MerkleProofElement
var ProofHashedLeaf = require('./merkleprooftree').ProofHashedLeaf
var ProofValueLeaf = require('./merkleprooftree').ProofValueLeaf
var ProofNode = require('./merkleprooftree').ProofNode
var MerkleHashSummary = require('./merklehashcarrier').MerkleHashSummary

/**
 * 
 * @param {BinaryTreeFactory} treeFactory 
 * @param {MerkleProofTreeFactory} proofFactory 
 */
function MerkleHashSummaryFactory(treeFactory, proofFactory) {
  this.treeFactory = treeFactory
  this.proofFactory = proofFactory
}

/**
 * @param {any} value
 * @param {MerkleHashCalculator} calculator
 */
MerkleHashSummaryFactory.prototype.calculateMerkleRoot = function(value, calculator) {
  var binaryTree = this.treeFactory.build(value)
  var proofTree = this.proofFactory.buildFromBinaryTree(binaryTree, calculator)

  return this.calculateMerkleRootOfTree(proofTree, calculator)
}

/**
 * @param {MerkleProofTree} value
 * @param {MerkleHashCalculator} calculator
 */
MerkleHashSummaryFactory.prototype.calculateMerkleTreeRoot = function(tree, calculator) {
  return this.calculateMerkleRootOfTree(tree, calculator)
}

/**
 * @param {MerkleProofTree} proofTree
 * @param {MerkleHashCalculator} calculator
 */
MerkleHashSummaryFactory.prototype.calculateMerkleRootOfTree = function(proofTree, calculator) {
  var calculatedSummary = this.calculateMerkleRootInternal(proofTree.root, calculator)
  return new MerkleHashSummary(calculatedSummary)
}

/**
 * @param {MerkleProofElement} currentElement
 * @param {MerkleHashCalculator} calculator
 */
MerkleHashSummaryFactory.prototype.calculateMerkleRootInternal = function(currentElement, calculator) {
  if (currentElement instanceof ProofHashedLeaf) {
    return currentElement.merkleHash
  } else if (currentElement instanceof ProofValueLeaf) {
    var value = currentElement.content
    if (calculator.isContainerProofValueLeaf(value)) {
      // We have a container value to prove, so need to convert the value to a binary tree, and THEN hash it
      var merkleProofTree = this.buildProofTree(value, calculator)
      return this.calculateMerkleRootInternal(merkleProofTree.root, calculator)
    } else {
      // This is a primitive value, just hash it
      return calculator.calculateLeafHash(value)
    }
  } else if (currentElement instanceof ProofNode) {
    var left = this.calculateMerkleRootInternal(currentElement.left, calculator)
    var right = this.calculateMerkleRootInternal(currentElement.right, calculator)
    return calculator.calculateNodeHash(currentElement.prefix, left, right)
  } else {
    throw new Error("Should have handled this type? " + typeof currentElement)
  }
}

/**
 * @param {any} value
 * @param {MerkleHashCalculator} calculator
 */
MerkleHashSummaryFactory.prototype.buildProofTree = function(value, calculator) {
  var root = this.treeFactory.build(value)
  return this.proofFactory.buildFromBinaryTree(root, calculator)
}

module.exports = {MerkleHashSummaryFactory}