
var SearchablePathElement = require('../path').SearchablePathElement
var HASH_PREFIX_NODE = require('../binarytree').HASH_PREFIX_NODE
var HASH_PREFIX_NODE_ARRAY = require('../binarytree').HASH_PREFIX_NODE_ARRAY
var HASH_PREFIX_NODE_DICT = require('../binarytree').HASH_PREFIX_NODE_DICT
/**
 * 
 */
function MerkleProofElement() {}

/**
 * 
 * @param {Buffer} prefix 
 * @param {MerkleProofElement} left 
 * @param {MerkleProofElement} right 
 */
function ProofNode(prefix, left, right) {
  this.prefix = prefix
  this.left = left
  this.right = right
}

ProofNode.prototype = Object.create(MerkleProofElement.prototype)
ProofNode.prototype.constructor = ProofNode

/**
 * 
 * @param {MerkleProofElement} left 
 * @param {MerkleProofElement} right 
 */
function ProofNodeSimple(left, right) {
  ProofNode.call(this, HASH_PREFIX_NODE, left, right)
}

ProofNodeSimple.prototype = Object.create(ProofNode.prototype)
ProofNodeSimple.prototype.constructor = ProofNodeSimple

/**
 * 
 * @param {*} content 
 * @param {SearchablePathElement} pathElem 
 */
function ProofValueLeaf(content, pathElem) {
  this.content = content
  this.pathElem = pathElem
}
ProofValueLeaf.prototype = Object.create(MerkleProofElement.prototype)
ProofValueLeaf.prototype.constructor = ProofValueLeaf

/**
 * 
 * @param {Buffer} merkleHash 
 */
function ProofHashedLeaf(merkleHash) {
  this.merkleHash = merkleHash
}
ProofHashedLeaf.prototype = Object.create(MerkleProofElement.prototype)
ProofHashedLeaf.prototype.constructor = ProofHashedLeaf

/**
 * @param {ProofHashedLeaf} other
 */
ProofHashedLeaf.prototype.equals = function(other) {
  if (other instanceof ProofHashedLeaf) {
    return this.merkleHash.equals(other.merkleHash)
  } else {
    return false
  }
}

/**
 * 
 * @param {MerkleProofElement} left 
 * @param {MerkleProofElement} right 
 * @param {SearchablePathElement} pathElem 
 */
function ProofNodeArrayHead(left, right, pathElem=null) {
  ProofNode.call(this, HASH_PREFIX_NODE_ARRAY, left, right)
  this.pathElem = pathElem
}
ProofNodeArrayHead.prototype = Object.create(ProofNode.prototype)
ProofNodeArrayHead.prototype.constructor = ProofNodeArrayHead

/**
 * 
 * @param {MerkleProofElement} left 
 * @param {MerkleProofElement} right 
 * @param {SearchablePathElement} pathElem 
 */
function ProofNodeDictHead(left, right, pathElem=null) {
  ProofNode.call(this, HASH_PREFIX_NODE_DICT, left, right)
  this.pathElem = pathElem
}
ProofNodeDictHead.prototype = Object.create(ProofNode.prototype)
ProofNodeDictHead.prototype.constructor = ProofNodeDictHead

/**
 * 
 * @param {MerkleProofElement} root 
 */
function MerkleProofTree(root) {
  this.root = root
}

MerkleProofTree.prototype.maxLevel = function() {
  return this.maxLevelInternal(this.root)
}

/**
 * @param {MerkleProofElement} node
 */
MerkleProofTree.prototype.maxLevelInternal = function(node) {
  if (node instanceof ProofValueLeaf) {
    return 1
  } else if (node instanceof ProofHashedLeaf) {
    return 1
  } else if (node instanceof ProofNode) {
    return Math.max(this.maxLevelInternal(node.left), this.maxLevelInternal(node.right)) + 1
  } else {
    throw new Error("Should be able to handle node type: " + typeof node)
  }
}

module.exports = {ProofNode, ProofNodeSimple, ProofHashedLeaf, ProofValueLeaf, 
  ProofNodeArrayHead, ProofNodeDictHead, MerkleProofElement, MerkleProofTree}