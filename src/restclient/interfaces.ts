import { StatusObject } from "../blockchainClient/types";
import { DictPair } from "../gtv/types";
import { QueryObject, RestClientCallback, RestClientConfig } from "./types";

export interface RestClient {
  config: RestClientConfig;
  /**
   * Retrieves the client message with the specified sha256 hash
   * @param txRID the id of the transaction
   * @param callback parameters (error, serializedMessage) if first
   * parameter is not null, an error occurred.
   * If first parameter is null, then the second parameter is a buffer
   * with the serialized client message. If no such client message exists,
   * the callback will be called with (null, null).
   */
  getTransaction: (txRID: Buffer, callback: RestClientCallback<Buffer>) => void;
  /**
   * Sends a transaction to postchain for inclusion in a block.
   * Use status() to monitor progress once this transaction is
   * posted.
   *
   * @param serializedTransaction The transaction (a buffer) to send
   * @param callback taking parameter (error, responseObject) if error is null
   * then responseObject is also null. If error is not null, then responseObject
   * is an object with the string property 'error'
   */
  postTransaction: (serializedTransaction: Buffer, callback: RestClientCallback<any>) => void;
  /**
   * Queries the status of a certain transaction.
   * @param txRID the id of the transaction
   * @param callback taking parameters (error, responseBody). If error is null
   * then responseBody is an object on the form
   * { status: '<confirmed|waiting|rejected|unknown>' }
   * If error is not null, then responseBody
   * is an object with the string property 'error'
   */
  status: (txRID: Buffer, callback: RestClientCallback<StatusObject>) => void;
  /**
   * Interfaces the query endpoint of the Rell backend. Returns either a resolved or rejected promise. The input is the name of the query followed by the arguments of the
   * query as an optional input argument.
   * @param name the name of the query in Rell
   * @param queryArguments optional argument following this pattern: { arg1: argValue1, arg2: argvalue2 }
   */
  query(name: string, queryArguments?: DictPair): Promise<any>;
  /**
   * Interfaces the query endpoint of the Rell backend. Returns either a resolved or rejected promise.
   * @param queryObject an object containing a "type" and follows this pattern: { type: "nameOfQuery", arg1: argValue1, arg2: argvalue2 }
   * @deprecated
   */
  query(object: QueryObject): Promise<any>;
  /**
   * Polls for status while waiting for response; confirmed, rejected or unknown. Returns either a resolved or rejected promise.
   * @param txRID the id of the transaction
   */
  waitConfirmation: (txRID: Buffer) => Promise<null | Error>;
  /**
   * Posts a transaction and polls for status while waiting for status response; confirmed, rejected or unknown. Returns either a resolved or rejected promise.
   * @param serializedTransaction The transaction (a buffer) to send
   * @param txRID the id of the transaction
   * @param validate true if the transaction needs to be validated
   */
  postAndWaitConfirmation: (
    serializedTransaction: Buffer,
    txRID: Buffer,
    validate?: boolean,
  ) => Promise<null>;
  /**
   * Returns a string array with the endpoints hosting the dApp the client
   * is connected to
   */
  getEndpointPool: () => string[];
}
