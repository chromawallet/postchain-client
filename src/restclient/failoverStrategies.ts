import { logger } from "../..";
import handleRequest from "./httpUtil";
import { bftMajority, sleep, racePromisesWithTimeout, groupResponses } from "./restclientutil";
import { HandleRequestInputProps, ResponseObject, RetryRequestProps } from "./types";

const isSuccessfullRequest = (statusCode: number) => statusCode >= 200 && statusCode < 300;
const hasClientError = (statusCode: number) => statusCode >= 400 && statusCode < 500;
const hasServerError = (statusCode: number) => statusCode >= 500 && statusCode < 600;

export async function abortOnError({
  method,
  path,
  config,
  postObject,
}: HandleRequestInputProps): Promise<ResponseObject> {
  return await retryRequest({
    method,
    path,
    config,
    postObject,
    validateStatusCode: statuscode => !hasServerError(statuscode),
  });
}
export async function tryNextOnError({
  method,
  path,
  config,
  postObject,
}: HandleRequestInputProps): Promise<ResponseObject> {
  return await retryRequest({
    method,
    path,
    config,
    postObject,
    validateStatusCode: statusCode => !hasClientError(statusCode) && !hasServerError(statusCode),
  });
}

export const errorMessages = {
  NO_CONSENSUS: "Nodes were not able to reach a consensus",
};

export async function queryMajority({
  method,
  path,
  config,
  postObject,
}: HandleRequestInputProps & {
  forceSingleEndpoint?: boolean;
}): Promise<ResponseObject> {
  // Set up constants
  const bftMajorityThreshold = bftMajority(config.endpointPool.length);
  const failureThreshold = config.endpointPool.length - bftMajorityThreshold + 1;
  const { nodeManager } = config;
  const timeout = 15000;

  const availableNodes = nodeManager.getAvailableNodes();

  // If the available nodes are less than bfthMajorityThreshold, set all nodes as reachable again.
  if (availableNodes.length < bftMajorityThreshold) {
    nodeManager.makeAllNodesAvailable();
  }

  // Start sending away requests
  type SuccessOutcome = { type: "SUCCESS"; result: ResponseObject };
  type FailureOutcome = { type: "FAILURE"; result: ResponseObject };
  type ErrorOutcome = { type: "ERROR"; result: Error };
  const outcomes: Array<SuccessOutcome | FailureOutcome | ErrorOutcome> = [];

  const promises = nodeManager.getAvailableNodes().map(node => {
    return handleRequest(method, path, node.url, postObject)
      .then(response => {
        const { statusCode } = response;
        if (statusCode && isSuccessfullRequest(statusCode)) {
          outcomes.push({ type: "SUCCESS", result: response });
        } else {
          if (statusCode && hasServerError(statusCode)) {
            nodeManager.makeNodeUnavailable(node.url);
          }
          outcomes.push({ type: "FAILURE", result: response });
        }
        return response;
      })
      .catch((error: Error) => {
        outcomes.push({ type: "ERROR", result: error });
        return error;
      });
  });

  let remainingPromises = promises;

  // Wait for the necessary majority of outcomes to come back or for the timeout to be reached, whichever comes first.
  for (let i = 0; i < bftMajorityThreshold; i++) {
    remainingPromises = await racePromisesWithTimeout(remainingPromises, timeout);
  }

  // Evaluate the outcomes
  const successfullOutcomes = outcomes.filter(outcome => outcome.type === "SUCCESS");
  const failureOutcomes = outcomes.filter(outcome => outcome.type === "FAILURE");
  const errorOutcomes = outcomes.filter(outcome => outcome.type === "ERROR");

  // group all of the same responses together into groups and count each groups size
  const groupedSuccessfullResponses = groupResponses(successfullOutcomes);

  // validate the responses
  // eslint-disable-next-line no-constant-condition
  while (true) {
    // Successfull majority response was found
    if (groupedSuccessfullResponses.maxNumberOfEqualResponses() >= bftMajorityThreshold) {
      if (groupedSuccessfullResponses.numberOfDistinctResponses() > 1) {
        logger.warning(`Got disagreeing responses, but could still reach BFT majority`);
      }
      return groupedSuccessfullResponses.majorityResponse() as ResponseObject;
    }

    // Too many failures or errors to be able to reach a consensus
    // If the number of failures and errors is greater than the failure threshold, return first failure, if any, otherwise throw the first error.
    if ([...failureOutcomes, ...errorOutcomes].length >= failureThreshold) {
      if (failureOutcomes.length > 0) {
        return failureOutcomes[0].result as ResponseObject;
      } else {
        throw errorOutcomes[0].result;
      }
    }

    // If all nodes have responded without a majority, throw an error.
    if (outcomes.length >= config.endpointPool.length) {
      throw new Error(errorMessages.NO_CONSENSUS);
    }

    // Wait for more one more response to come back. ( we need to set a timeout here)
    remainingPromises = await racePromisesWithTimeout(remainingPromises, timeout);
  }
}

export async function singleEndpoint({
  method,
  path,
  config,
  postObject,
}: Omit<RetryRequestProps, "unreachableDuration">): Promise<ResponseObject> {
  let statusCode: number | null, rspBody: unknown, error: Error | null;
  const { nodeManager } = config;
  const endpoint = nodeManager.getNode();

  if (!endpoint) {
    throw new Error("Cannot get endpoint. Node not found!");
  }

  for (let attempt = 0; attempt < config.attemptsPerEndpoint; attempt++) {
    ({ error, statusCode, rspBody } = await handleRequest(method, path, endpoint.url, postObject));

    const isError = statusCode ? hasServerError(statusCode) || hasClientError(statusCode) : false;

    if (!isError && !error) {
      return { error, statusCode, rspBody };
    }

    logger.info(
      `${method} request failed on ${config.endpointPool[0]}. Attempt: ${attempt + 1} / ${
        config.attemptsPerEndpoint
      }`,
    );
    await sleep(config.attemptInterval);
  }

  // TS issue. Variable 'error' is used before being assigned.
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  return { error, statusCode, rspBody };
}

type RetryRequest = RetryRequestProps & {
  validateStatusCode: (statusCode: number) => boolean;
};

export async function retryRequest({
  method,
  path,
  config,
  postObject,
  validateStatusCode,
}: RetryRequest): Promise<ResponseObject> {
  let statusCode: number | null;
  let rspBody: any;
  let error: Error | null;

  const { nodeManager } = config;

  for (const endpoint of nodeManager.getAvailableNodes()) {
    for (let attempt = 0; attempt < config.attemptsPerEndpoint; attempt++) {
      ({ error, statusCode, rspBody } = await handleRequest(
        method,
        path,
        endpoint.url,
        postObject,
      ));

      const isStatusCodeValid = statusCode ? validateStatusCode(statusCode) : false;
      const isServerError = statusCode ? hasServerError(statusCode) : false;

      if (isStatusCodeValid && !error) {
        // Find a way to have this handled more elegantly in the node manager.
        if (nodeManager.stickedNode !== endpoint) {
          nodeManager.setStickyNode(endpoint);
        }
        return { error, statusCode, rspBody };
      }
      if (isServerError) {
        nodeManager.makeNodeUnavailable(endpoint.url);
      }
      logger.info(
        `${method} request failed on ${endpoint.url}. Attempt: ${attempt + 1} / ${
          config.attemptsPerEndpoint
        }`,
      );
      await sleep(config.attemptInterval);
    }
  }

  // TS issue. Variable 'error' is used before being assigned.
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  return { error, statusCode, rspBody };
}
