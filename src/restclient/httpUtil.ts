import { Buffer } from "buffer";
import * as logger from "../logger";
import { decodeValue } from "../gtx/serialization";
import { PostRequestObjects, ResponseObject } from "./types";
import { Method } from "./enums";
import { RawGtv } from "../gtv/types";

async function handleRequest(
  method: Method,
  path: string,
  endpoint: string,
  postObject?: PostRequestObjects | Buffer,
): Promise<ResponseObject> {
  if (method == Method.GET) {
    return await get(path, endpoint);
  } else {
    return await post(path, endpoint, postObject);
  }
}
export default handleRequest;

/**
 * Sends request to get data from a given API endpoint.
 * @param path API endpoint of Rell backend
 * @param endpoint
 */
async function get(path: string, endpoint: string): Promise<ResponseObject> {
  logger.debug(`GET URL ${new URL(path, endpoint).href}`);

  try {
    const response = await fetch(new URL(path, endpoint).href);

    const contentType = response.headers.get("Content-Type");
    let rspBody;

    if (contentType?.includes("application/json")) {
      rspBody = await response.json();
    } else if (contentType?.includes("text/plain")) {
      rspBody = await response.text();
    }

    return {
      error: null,
      statusCode: response.status,
      rspBody,
    };
  } catch (error) {
    logger.error(error.message);
    return { error, statusCode: null, rspBody: null };
  }
}

/**
 * Sends request to post data to a given API endpoint.
 * @param path API endpoint of Rell backend
 * @param endpoint
 * @param requestBody request body
 */
async function post(
  path: string,
  endpoint: string,
  requestBody?: PostRequestObjects | Buffer,
): Promise<ResponseObject> {
  logger.debug(`POST URL ${new URL(path, endpoint).href}`);
  logger.debug(`POST body ${JSON.stringify(requestBody)}`);

  if (Buffer.isBuffer(requestBody)) {
    try {
      const requestOptions: RequestInit = {
        method: "post",
        body: requestBody,
        headers: {
          Accept: "application/octet-stream",
          "Content-Type": "application/octet-stream",
        },
      };

      const response = await fetch(new URL(path, endpoint).href, requestOptions);

      return {
        error: null,
        statusCode: response.status,
        rspBody: await constructBufferPostResponseBody(response),
      };
    } catch (error) {
      return { error, statusCode: null, rspBody: null };
    }
  } else {
    try {
      const response = await fetch(new URL(path, endpoint).href, {
        method: "post",
        body: JSON.stringify(requestBody),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      });

      return {
        error: null,
        statusCode: response.status,
        rspBody: await constructBufferPostResponseBody(response),
      };
    } catch (error) {
      return { error, statusCode: null, rspBody: null };
    }
  }
}

async function constructBufferPostResponseBody(response: Response): Promise<RawGtv> {
  const contentType = response.headers.get("content-type");

  if (contentType === "application/octet-stream") {
    const responseBuffer = await response.arrayBuffer();
    const buffer = Buffer.from(responseBuffer);

    return decodeValue(buffer);
  }

  if (contentType === "application/json") {
    return await response.json();
  }

  const responseText = await response.text();

  return responseText ? responseText : response.statusText;
}
