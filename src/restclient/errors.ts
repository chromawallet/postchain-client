import { CustomError } from "../customError";
import { matchRellErrorString } from "../formatter";

export class TxRejectedError extends Error {
  public fullReason: string;
  public shortReason?: string;
  public rellLine?: string;
  public operation?: string;

  constructor(rejectReason: string) {
    super(`Transaction was rejected, ${rejectReason}`);
    this.name = "TxRejectedError";
    this.fullReason = rejectReason;

    const result = matchRellErrorString(rejectReason);

    this.shortReason = result.shortReason;
    this.rellLine = result.rellLine;
    this.operation = result.operation;
  }
}

export class UnexpectedStatusError extends CustomError {
  constructor(status: number | null, error?: any) {
    let errorMessage = `Unexpected status code from server. Code: ${status}.`;
    if (status === null) {
      super(errorMessage, 500); // default status code
    } else {
      if (error) {
        errorMessage += ` Message: ${error}.`;
      }
      super(errorMessage, status);
    }
  }
}

export class LostMessageError extends Error {
  constructor() {
    super(`Server lost our message`);
  }
}

export class UnexpectedResponseError extends Error {
  constructor() {
    super(`got unexpected response from server`);
  }
}

export class InvalidTxRidException extends CustomError {
  constructor(txRID: Buffer) {
    super(`expected length 32 of txRID, but got ${txRID && txRID.length}`, 400);
  }
}

export class SerializedTransactionFormatException extends CustomError {
  constructor() {
    super(`messageHash is not a Buffer`, 400);
  }
}

export class GetBridFromChainException extends Error {
  constructor(chainId: number, reason: string) {
    super(`Error resolving BRID for chainId ${chainId}, reason: ${reason}`);
  }
}
export class EmptyListOfUrlsException extends CustomError {
  constructor() {
    super(`Failed to initialize rest client with empty list of urls`, 400);
  }
}
export class InvalidBlockchainRIDException extends CustomError {
  constructor() {
    super(`Failed to initialize rest client with blockchain RID that is not 32 bytes`, 400);
  }
}
