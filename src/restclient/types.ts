import { FailoverStrategy } from "../blockchainClient/enums";
import { ClientConfig, Endpoint, StatusObject } from "../blockchainClient/types";
import { RawGtv } from "../gtv/types";
import { SignerPair } from "../gtx/types";
import { StringPath } from "../merkle/types";
import { Method } from "./enums";
import { NodeManager } from "./nodeManager";

export type RestClientConfig = {
  nodeManager: NodeManager;
  endpointPool: Endpoint[];
  pool: {
    maxSockets: number;
  };
  pollingInterval: number;
  failoverStrategy: FailoverStrategy;
  attemptsPerEndpoint: number;
  attemptInterval: number;
  unreachableDuration: number;
};

export type RequestOptions = {
  method: string;
  url: string;
  json: boolean;
  body?: object;
  pool: { maxSockets: number };
};

export type QueryObject = {
  type: string;
  [arg: string]: RawGtv;
};

export type QueryObjectGTV = [name: string, args: Arg];

export type Arg = {
  [arg: string]: RawGtv;
};

export type ServerReturnProof = {
  hash: string;
  blockHeader: string;
  signatures: SignerPair[];
  merklePath: StringPath;
};

//tx has to be hex string
export type TransactionObject = {
  tx: string;
};

export type PostRequestObjects = TransactionObject | QueryObject;

export type GetResponseObjects = StatusObject | TransactionObject | ServerReturnProof;

export type FailOverConfig = {
  strategy?: FailoverStrategy;
  attemptsPerEndpoint?: number;
  attemptInterval?: number;
  unreachableDuration?: number;
};

export type ResponseObject = {
  statusCode: number | null;
  error: Error | null;
  rspBody: unknown;
};

export type RetryRequestSettings = {
  endpointPool: Endpoint[];
  nodeManager: NodeManager;
  attemptsPerEndpoint: number;
  unreachableDuration: number;
  attemptInterval: number;
};

export interface BaseRequestProps {
  method: Method;
  path: string;
  postObject?: PostRequestObjects | Buffer;
}

export interface HandleRequestInputProps extends BaseRequestProps {
  config: RestClientConfig | Omit<ClientConfig, "blockchainRid" | "directoryChainRid">;
}

export interface RetryRequestProps extends BaseRequestProps {
  config: RetryRequestSettings;
}

export type RestClientCallback<TData, TError = Error> = (
  error: TError | null | undefined,
  responseBody: TData | null | undefined,
  statusCode?: number | null | undefined,
) => void;
