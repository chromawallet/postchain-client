import { Buffer } from "buffer";
import { TransactionObject, FailOverConfig, RestClientCallback } from "./types";
import * as logger from "../logger";
import {
  EmptyListOfUrlsException,
  LostMessageError,
  SerializedTransactionFormatException,
  TxRejectedError,
  UnexpectedResponseError,
  UnexpectedStatusError,
  InvalidTxRidException,
} from "./errors";
import { RestClient } from "./interfaces";
import { toBuffer, toQueryObjectGTV } from "../formatter";
import { convertToPrintable, requestWithFailoverStrategy } from "./restclientutil";
import { encodeValue } from "../gtx/serialization";
import { DictPair, RawGtv } from "../gtv/types";
import { Method } from "./enums";
import { FailoverStrategy, ResponseStatus } from "../blockchainClient/enums";
import { createEndpointObjects, getUrlsFromEndpoints } from "../blockchainClient/utils";
import { isTxRidValid } from "../blockchainClient/validation/txRid";
import { createNodeManager } from "./nodeManager";
import { isRawGtv, isTransaction } from "../blockchainClient/validation/requests";

export function createRestClient(
  endpointPool: readonly string[],
  blockchainRid: string,
  maxSockets = 10,
  pollingInterval = 500,
  failOverConfig?: FailOverConfig,
  unreachableDuration = 5000,
  useStickyNode = false,
): RestClient {
  validateInput(endpointPool, failOverConfig ?? {});

  return {
    config: {
      nodeManager: createNodeManager({
        nodeUrls: endpointPool as string[],
        useStickyNode,
      }),
      endpointPool: createEndpointObjects(endpointPool),
      pool: { maxSockets },
      pollingInterval,
      failoverStrategy: failOverConfig?.strategy || FailoverStrategy.AbortOnError,
      attemptsPerEndpoint: failOverConfig?.attemptsPerEndpoint || 3,
      attemptInterval: failOverConfig?.attemptInterval || 500,
      unreachableDuration: unreachableDuration,
    },
    getTransaction: async function (txRID, callback) {
      if (!isTxRidValid(txRID)) {
        callback(new InvalidTxRidException(txRID), null);
      } else {
        const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
          Method.GET,
          `tx/${blockchainRid}/${txRID.toString("hex")}`,
          this.config,
        );

        const transaction = isTransaction(rspBody);

        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && transaction ? toBuffer(transaction.tx) : isRawGtv(rspBody),
          callback,
        );
      }
    },
    postTransaction: async function (serializedTransaction, callback) {
      if (!Buffer.isBuffer(serializedTransaction)) {
        throw new SerializedTransactionFormatException();
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.POST,
        `tx/${blockchainRid}`,
        this.config,
        serializedTransaction,
      );

      handlePostResponse(error, statusCode, isRawGtv(rspBody), callback);
    },
    status: async function (txRID, callback) {
      if (!isTxRidValid(txRID)) {
        callback(new InvalidTxRidException(txRID), null);
      } else {
        const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
          Method.GET,
          `tx/${blockchainRid}/${txRID.toString("hex")}/status`,
          this.config,
        );
        handleGetResponse(error, statusCode, isRawGtv(rspBody), callback);
      }
    },
    query: async function (nameOrQueryObject, queryArguments?: DictPair): Promise<RawGtv> {
      // eslint-disable-next-line no-async-promise-executor
      return new Promise(async (resolve, reject) => {
        const callback: RestClientCallback<RawGtv> = (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result as RawGtv);
          }
        };
        const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
          Method.POST,
          `query_gtv/${blockchainRid}`,
          this.config,
          encodeValue(toQueryObjectGTV(nameOrQueryObject, queryArguments)),
        );
        handlePostResponse(error, statusCode, isRawGtv(rspBody), callback);
      });
    },
    waitConfirmation(txRID: Buffer): Promise<null | Error> {
      return new Promise((resolve, reject) => {
        this.status(txRID, (error, result: any) => {
          if (error) {
            reject(error);
          } else {
            const status = result.status;
            switch (status) {
              case ResponseStatus.Confirmed:
                resolve(null);
                break;
              case ResponseStatus.Rejected:
                reject(new TxRejectedError(result.rejectReason));
                break;
              case ResponseStatus.Unknown:
                reject(new LostMessageError());
                break;
              case ResponseStatus.Waiting:
                setTimeout(
                  () => this.waitConfirmation(txRID).then(resolve, reject),
                  this.config.pollingInterval,
                );
                break;
              default:
                logger.error(status);
                reject(new UnexpectedResponseError());
            }
          }
        });
      });
    },
    postAndWaitConfirmation(
      serializedTransaction: Buffer,
      txRID: Buffer,
      validate: boolean | undefined,
    ): Promise<null> {
      if (validate === true) {
        return Promise.reject("Automatic validation is not yet implemented");
      }
      return new Promise((resolve, reject) => {
        this.postTransaction(serializedTransaction, error => {
          if (error) reject(error);
          else {
            setTimeout(
              () =>
                this.waitConfirmation(txRID)
                  .then(() => resolve(null))
                  .catch(reject),
              1011,
            );
          }
        });
      });
    },
    getEndpointPool() {
      return getUrlsFromEndpoints(this.config.endpointPool);
    },
  };
}

function validateInput(endpointPool: readonly string[], failOverConfig: FailOverConfig): void {
  if (!endpointPool.length) {
    throw new EmptyListOfUrlsException();
  }
  if (failOverConfig?.attemptsPerEndpoint && failOverConfig.attemptsPerEndpoint < 1) {
    logger.debug("Attempts can not be 0 or below, setting it to 1");
    failOverConfig.attemptsPerEndpoint = 1;
  }
}
/**
 * @param error response error
 * @param statusCode response status code
 * @param responseObject the responsebody from the server
 * @param callback the callback function to propagate the error and response back to the caller
 */
export function handleGetResponse<T>(
  error: Error | null | undefined,
  statusCode: number | null,
  responseObject: RawGtv | null | undefined,
  callback: RestClientCallback<T>,
): void {
  try {
    const responseObjectPrintable = convertToPrintable(responseObject);

    logger.debug(
      `error: ${error}, status code: ${statusCode}, response body: ${responseObjectPrintable}`,
    );

    if (error) {
      return callback(error, null);
    }

    if (statusCode !== 200) {
      return callback(new UnexpectedStatusError(statusCode ?? 400, responseObjectPrintable), null);
    }

    return callback(null, responseObject as T);
  } catch (error) {
    callback(error, null);
    logger.error(`restclient.handleGetResponse(): Failed to call the callback function. ${error}`);
  }
}
/**
 * @param error response error
 * @param statusCode response status code
 * @param responseObject the responsebody from the server
 * @param callback the callback function to propagate the error and response back to the caller
 */
export function handlePostResponse(
  error: Error | null | undefined,
  statusCode: number | null,
  responseObject: RawGtv | null | undefined,
  callback: RestClientCallback<RawGtv>,
) {
  const responseObjectPrintable = convertToPrintable(responseObject);

  logger.debug(
    `error: ${error}, status code: ${statusCode}, response body: ${responseObjectPrintable}`,
  );

  try {
    if (error) {
      logger.error(`In restclient post(). ${error}`);
      callback(error, null);
    } else if (statusCode != 200) {
      let errorMessage = `Unexpected status code from server. Code: ${statusCode}.`;
      if (responseObjectPrintable) {
        errorMessage += ` Message: ${responseObjectPrintable}.`;
      }
      logger.error(errorMessage);
      callback(
        new UnexpectedStatusError(statusCode ?? 400, responseObjectPrintable),
        responseObject,
      );
    } else {
      logger.info(`Calling responseCallback with responseObject: ${responseObjectPrintable}`);
      callback(null, responseObject);
    }
  } catch (error) {
    logger.error(`restclient.handlePostResponse(): Failed to call callback function ${error}`);
  }
}
