import { GetBridFromChainException } from "./errors";
import { ResponseObject, FailOverConfig, HandleRequestInputProps } from "./types";
import { Endpoint } from "../blockchainClient/types";
import { abortOnError, singleEndpoint, tryNextOnError, queryMajority } from "./failoverStrategies";
import { Method } from "./enums";
import { defaultFailoverConfig, setStatusPolling } from "../blockchainClient/utils";
import { FailoverStrategy } from "../blockchainClient/enums";
import { NodeManager } from "./nodeManager";
import { isString } from "../blockchainClient/validation/requests";

interface BlockchainRidFromIidPops {
  endpointPool: Endpoint[];
  chainId: number;
  nodeManager: NodeManager;
  failOverConfig?: FailOverConfig;
}

export async function getBlockchainRidFromIid({
  endpointPool,
  chainId,
  failOverConfig = {},
  nodeManager,
}: BlockchainRidFromIidPops): Promise<string> {
  const mergedFailOverConfig = {
    ...defaultFailoverConfig,
    ...failOverConfig,
  };
  const config = {
    endpointPool,
    nodeManager,
    dappStatusPolling: setStatusPolling(),
    clusterAnchoringStatusPolling: setStatusPolling(),
    systemAnchoringStatusPolling: setStatusPolling(),
    failoverStrategy: mergedFailOverConfig.strategy,
    attemptsPerEndpoint: mergedFailOverConfig.attemptsPerEndpoint,
    attemptInterval: mergedFailOverConfig.attemptInterval,
    unreachableDuration: mergedFailOverConfig.unreachableDuration,
  };

  const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
    Method.GET,
    `/brid/iid_${chainId}`,
    config,
  );

  const blockchainRid = isString(rspBody);

  if (!blockchainRid) {
    throw error;
  }

  if (error) {
    throw new GetBridFromChainException(chainId, error.message);
  } else if (statusCode !== 200) {
    throw new GetBridFromChainException(chainId, blockchainRid);
  }

  return blockchainRid;
}

export async function createTimeoutPromise(timeoutMs: number): Promise<never> {
  return new Promise((_, reject) => {
    setTimeout(() => {
      reject(new Error("Timeout exceeded"));
    }, timeoutMs);
  });
}

export const checkStateOfPromises = async <T>(
  promises: Array<Promise<T>>,
): Promise<Array<{ status: string; value: Promise<T> | any }>> => {
  const promiseList = [];
  const pendingState = { status: "pending" };

  for (const p of promises) {
    promiseList.push(
      await Promise.race([p, pendingState]).then(
        value =>
          value === pendingState ? { ...pendingState, value: p } : { status: "fulfilled", value },
        reason => ({ status: "rejected", value: reason }),
      ),
    );
  }

  return promiseList;
};

export async function racePromisesWithTimeout<T>(
  promises: Promise<T>[],
  timeout: number,
): Promise<Promise<T>[]> {
  let remainingPromises: Promise<T>[] = [];

  try {
    const resolvedPromise = await Promise.race([createTimeoutPromise(timeout), ...promises]);

    const promisesWithState = await checkStateOfPromises(promises);

    const indexOfpromiseToRemove = promisesWithState.findIndex(
      p => JSON.stringify(p.value) === JSON.stringify(resolvedPromise),
    );

    // Remove the promise that resolved from the list of promises
    promisesWithState.splice(indexOfpromiseToRemove, 1);

    // Remove the state before returning the promises.
    remainingPromises = promisesWithState.map(p => p.value);

    return remainingPromises;
  } catch (error) {
    if (error instanceof Error && error.message === "Timeout exceeded") {
      throw error;
    }
    return remainingPromises;
  }
}

interface HasResult<R> {
  result: R;
}

const stableStringify = (obj: any) => JSON.stringify(obj, Object.keys(obj).sort());

export const groupResponses = <T extends HasResult<R>, R>(responses: T[]) => {
  const responseMap: Record<string, { response: T; count: number }> = responses.reduce(
    (acc, response) => {
      const key = stableStringify(response.result);
      acc[key] = acc[key]
        ? { response: acc[key].response, count: acc[key].count + 1 }
        : { response, count: 1 };
      return acc;
    },
    {} as Record<string, { response: T; count: number }>,
  );

  const distinctResponses = Object.values(responseMap).sort((a, b) => b.count - a.count);

  // Returns the count of the most common response
  const maxNumberOfEqualResponses = () => {
    return distinctResponses.length > 0 ? distinctResponses[0].count : 0;
  };

  // Returns the number of distinct responses
  const numberOfDistinctResponses = () => {
    return distinctResponses.length;
  };

  // Returns the most common response
  const majorityResponse = (): R => {
    return distinctResponses[0].response.result as R;
  };

  return {
    maxNumberOfEqualResponses,
    numberOfDistinctResponses,
    majorityResponse,
  };
};

export async function requestWithFailoverStrategy(
  method: Method,
  path: string,
  config: HandleRequestInputProps["config"],
  postObject?: Buffer,
  forceSingleEndpoint = false,
): Promise<ResponseObject> {
  switch (config.failoverStrategy) {
    case FailoverStrategy.AbortOnError:
      return await abortOnError({ method, path, config, postObject });
    case FailoverStrategy.TryNextOnError:
      return await tryNextOnError({ method, path, config, postObject });
    case FailoverStrategy.SingleEndpoint:
      return await singleEndpoint({ method, path, config, postObject });
    case FailoverStrategy.QueryMajority:
      if (forceSingleEndpoint) {
        return await singleEndpoint({ method, path, config, postObject });
      }
      return await queryMajority({ method, path, config, postObject });
  }
}

export const sleep = (ms: number) => new Promise(r => setTimeout(r, ms));

export function convertToPrintable(responseObject: any) {
  if (typeof responseObject === "bigint") {
    return `${responseObject}n`;
  } else if (typeof responseObject === "object") {
    return JSON.stringify(responseObject, (key, value) =>
      typeof value === "bigint" ? `${value}n` : value,
    );
  } else {
    return responseObject;
  }
}

export const bftMajority = (n: number): number => n - (n - 1) / 3;
