type NetworkNode = {
  url: string;
  whenAvailable: number;
  isAvailable: boolean;
};

interface NodeManagerProps {
  nodeUrls: string[];
  useStickyNode?: boolean;
  unavailableDuration?: number;
}

const shuffleArray = <T>(array: T[]) => {
  const shuffledArray = [...array];
  for (let i = shuffledArray.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffledArray[i], shuffledArray[j]] = [shuffledArray[j], shuffledArray[i]];
  }
  return shuffledArray;
};

export const createNodeManager = ({
  nodeUrls,
  useStickyNode = false,
  unavailableDuration = 5000,
}: NodeManagerProps) => {
  const nodes: NetworkNode[] = nodeUrls.map(url => ({
    url,
    whenAvailable: 0,
    isAvailable: true,
  }));

  return {
    nodes,
    stickedNode: null as NetworkNode | null,
    getAvailableNodes(): NetworkNode[] {
      const shuffledAvailableNodes: NetworkNode[] = shuffleArray(
        this.nodes.filter((node: NetworkNode) => node.isAvailable),
      );

      // set sticky node as the first node, and then shuffle the rest
      if (useStickyNode && this.stickedNode && this.stickedNode.isAvailable) {
        const nodesWithoutSticky = shuffledAvailableNodes.filter(node => node !== this.stickedNode);
        return [this.stickedNode, ...nodesWithoutSticky];
      }

      return shuffledAvailableNodes;
    },
    setStickyNode(node: NetworkNode) {
      this.stickedNode = node;
    },
    getNode() {
      const nodes = this.getAvailableNodes();
      if (nodes.length === 0) {
        return null;
      }

      const index = Math.floor(Math.random() * nodes.length);
      const node = nodes[index];

      if (!useStickyNode) {
        return node;
      }

      if (!this.stickedNode) {
        this.setStickyNode(node);
        return node;
      }

      return this.stickedNode;
    },
    makeAllNodesAvailable() {
      this.nodes = this.nodes.map((node: NetworkNode) => {
        return {
          ...node,
          isAvailable: true,
          whenAvailable: 0,
        };
      });
    },
    makeNodeUnavailable(nodeUrl: string) {
      if (this.stickedNode && this.stickedNode.url === nodeUrl) {
        this.stickedNode = null;
      }

      const updatedNodes = this.nodes.map((node: NetworkNode) => {
        if (node.url === nodeUrl) {
          return {
            ...node,
            isAvailable: false,
            whenAvailable: Date.now() + unavailableDuration,
          };
        }
        return node;
      });

      this.nodes = updatedNodes;
    },
  };
};

export type NodeManager = ReturnType<typeof createNodeManager>;
