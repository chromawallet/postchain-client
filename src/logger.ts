enum MsgType {
  Debug = "DEBUG",
  Info = "INFO",
  Error = "ERROR",
  Warning = "WARNING",
}
enum LogColor {
  Red = "\x1b[91m",
  Green = "\x1b[92m",
  Blue = "\x1b[36m",
  Yellow = "\x1b[93m",
  StopColor = "\x1b[0m",
}

export enum LogLevel {
  Debug = "DEBUG",
  Info = "INFO",
  Error = "ERROR",
  Warning = "WARNING",
  Disabled = "DISABLED",
}

const logLevelMap = {
  [LogLevel.Warning]: 1,
  [LogLevel.Info]: 2,
  [LogLevel.Debug]: 3,
  [LogLevel.Error]: 0,
  [LogLevel.Disabled]: -1,
};

const DEFAULT_LOG_LEVEL = logLevelMap[LogLevel.Warning];

let logLevel =
  typeof process === "undefined"
    ? DEFAULT_LOG_LEVEL
    : logLevelMap[process.env.LOG_LEVEL as LogLevel] ?? DEFAULT_LOG_LEVEL;

function setLogLevel(level: LogLevel): void;
/** @deprecated */
function setLogLevel(level: number): void;
function setLogLevel(level: number | LogLevel) {
  if (typeof level === "number") {
    console.warn("Passing a number is deprecated, please use LogLevel enum instead");
    logLevel = level;
    return;
  }

  logLevel = logLevelMap[level];
}

function getLogLevel() {
  return logLevel;
}

function debug(message: string, moduleName?: string) {
  if (logLevel >= 3) {
    emitLogMessage({
      message,
      moduleName,
      msgType: MsgType.Debug,
      color: LogColor.Blue,
    });
  }
}
function info(message: string, moduleName?: string) {
  if (logLevel >= 2) {
    emitLogMessage({
      message,
      moduleName,
      msgType: MsgType.Info,
      color: LogColor.Green,
    });
  }
}
function error(message: string, moduleName?: string) {
  if (logLevel != -1) {
    emitLogMessage({
      message,
      moduleName,
      msgType: MsgType.Error,
      color: LogColor.Red,
    });
  }
}
function warning(message: string, moduleName?: string) {
  if (logLevel >= 1) {
    emitLogMessage({
      message,
      moduleName,
      msgType: MsgType.Warning,
      color: LogColor.Yellow,
    });
  }
}

function emitLogMessage({
  msgType,
  message,
  color,
  moduleName,
}: {
  msgType: MsgType;
  message: string;
  color: LogColor;
  moduleName?: string;
}) {
  const time = getTimestamp();
  const moduleNameMessage = moduleName ? `[${moduleName}]` : "";

  console.log(`[${time}] ${color}${msgType}:${LogColor.StopColor} ${moduleNameMessage} ${message}`);
}

function getTimestamp() {
  const pad = (n: number, s = 2) => `${new Array(s).fill(0)}${n}`.slice(-s);
  const date = new Date();
  return `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}:${pad(
    date.getMilliseconds(),
    3,
  )}`;
}

export function logger(moduleName: string) {
  return {
    debug: (message: string) => debug(message, moduleName),
    info,
    error,
    warning,
    setLogLevel,
    disable: () => setLogLevel(logLevelMap[LogLevel.Disabled]),
  };
}

export { debug, info, error, warning, setLogLevel, getLogLevel };
