import { GTX } from "../gtx/types";
import * as gtxTool from "../gtx/gtx";

/**
 *
 * Encodes the gtx using ASN.1.
 * @returns the gtx encoded
 */
export const encodeTransaction = (gtx: GTX): Buffer => {
  return gtxTool.serialize(gtx);
};
