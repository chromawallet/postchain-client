import { GetTransactionRidException } from "../blockchainClient/errors";
import * as gtxTool from "../gtx/gtx";
import { RawGtxBody } from "../gtx/types";

/**
 * Calculates and returns the transaction RID, i.e., the merkle root hash of the transaction.
 * @param transaction The transaction in format of RawGtxBody
 *
 */
export const calculateTransactionRid = (transaction: RawGtxBody): Buffer => {
  try {
    return gtxTool.getDigestToSignFromRawGtxBody(transaction);
  } catch (e) {
    throw new GetTransactionRidException(e);
  }
};
