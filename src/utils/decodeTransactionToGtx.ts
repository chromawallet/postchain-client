import { GTX } from "../gtx/types";
import * as gtxTool from "../gtx/gtx";
import { dumpGtv, logger } from "../..";

/**
 * Decodes the transaction using ASN.1.
 * @returns the transaction decoded in GTX format
 */
export const decodeTransactionToGtx = (encodedTransaction: Buffer): GTX => {
  const gtx = gtxTool.deserialize(encodedTransaction);
  logger.debug(`Output from deserializing a raw transaction: ${dumpGtv(gtx)}`);
  return gtx;
};
