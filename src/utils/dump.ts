import { formatter } from "../..";
import { RawGtv } from "../../";
import { GTX } from "../gtx/types";

export function dumpGtx(gtxTx: GTX): string {
  return `blockchainRid:\n${formatter.toString(gtxTx.blockchainRid)}\n
operations:
${gtxTx.operations.map(op => ` - ${op.opName}(${op.args.map(dumpGtv).join(", ")})\n`).join("")}
signers:
${gtxTx.signers.map(s => ` - ${formatter.toString(s)}\n`).join("")}
signatures:
${gtxTx.signatures?.map(s => ` - ${formatter.toString(s)}\n`).join("")}`;
}

export function dumpGtv(rawGtv: RawGtv): string {
  return JSON.stringify(rawGtv, (_key, value) => {
    if (typeof value === "bigint") {
      return `${value.toString()}L`;
    }

    if (Buffer.isBuffer(value)) {
      return formatter.toString(value);
    }

    if (
      typeof value === "object" &&
      value !== null &&
      value.type === "Buffer" &&
      Array.isArray(value.data)
    ) {
      return `0x${value.data}`;
    }

    return value;
  });
}
