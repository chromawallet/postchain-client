import { MerkleHashCalculator, CryptoSystem } from "../merkle/merklehashcalculator";
const theMerkleHashCalculator = new MerkleHashCalculator(new CryptoSystem());
import { merkleHashSummary } from "../merkle/proof/merkleproof";
import * as serialization from "../gtx/serialization";

const gtvHash = (obj: any) => {
  return merkleHashSummary(obj, theMerkleHashCalculator).merkleHash;
};
const encode = serialization.encodeValue;
const decode = serialization.decodeValue;

export { encode, decode, gtvHash };
