export type RawGtv =
  | null
  | boolean
  | Buffer
  | string
  | number
  | DictPair
  | Array<RawGtv>
  | ReadonlyArray<RawGtv>
  | bigint;

export type DictPair = {
  [key: string]: RawGtv;
};
