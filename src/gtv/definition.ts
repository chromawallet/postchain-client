/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-nocheck

// eslint-disable-next-line @typescript-eslint/no-var-requires
const asn = require("@chromia/asn1/lib/asn1");

export const ASNDictPair = asn.define("DictPair", function () {
  this.seq().obj(this.key("name").utf8str(), this.key("value").use(rawGTV));
});

export const rawGTV = asn.define("GtvValue", function () {
  this.choice({
    null: this.explicit(0).null_(),
    byteArray: this.explicit(1).octstr(),
    string: this.explicit(2).utf8str(),
    integer: this.explicit(3).int(),
    dict: this.explicit(4).seqof(ASNDictPair),
    array: this.explicit(5).seqof(rawGTV),
    bigInteger: this.explicit(6).int(),
  });
});

export const ASNBuffer = asn.define("Buffer", function () {
  this.octstr();
});
