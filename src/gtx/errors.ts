import { CustomError } from "../customError";

export class UnexpectedArgumentTypeError extends CustomError {
  constructor(typedArg: any) {
    super(`Cannot parse typedArg ${JSON.stringify(typedArg)}. Unknown type ${typedArg.type}`, 400);
  }
}

export class MissingGtxException extends CustomError {
  constructor() {
    super(
      `Missing instance of gtx protocol (used for communicating with postchain) to add operation to`,
      400,
    );
  }
}

export class AlreadySignedTransactionException extends CustomError {
  constructor(operation: string) {
    super(`Cannot add ${operation} calls to an already signed gtx`, 400);
  }
}

export class NumberOfSignersAndSignaturesException extends CustomError {
  constructor() {
    super(`Not matching number of signers and signatures`, 400);
  }
}

export class MissingSignerException extends CustomError {
  constructor() {
    super(`No such signer, remember to add signer to transaction before adding a signature`, 400);
  }
}
