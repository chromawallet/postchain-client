import { Buffer } from "buffer";
import { rawGTV } from "../gtv/definition";
import { ASNDictPair, ASNValue, RawGtx } from "./types";
import { BN } from "bn.js";
import { DictPair, RawGtv } from "../gtv/types";
import { UnexpectedArgumentTypeError } from "./errors";
import { checkGtxType } from "../formatter";

export function encodeValue(rawGtv: RawGtv): Buffer {
  return rawGTV.encode(createTypedArg(rawGtv));
}

export function encodeValueGtx(rawGtx: RawGtx): Buffer {
  return encodeValue(rawGtx as RawGtv);
}

export function decodeValue(bytes: Buffer): RawGtv {
  try {
    const obj = rawGTV.decode(bytes);
    return parseValue(obj);
  } catch (error) {
    throw new Error(`Invalid GTV data. ${error}`);
  }
}

export function decodeValueGtx(bytes: Buffer): RawGtx {
  const decodedValue = decodeValue(bytes);
  if (!checkGtxType(decodedValue)) {
    throw new Error(
      `Unexpected type of value: ${decodedValue}, expected decoded value to be of type RawGtx`,
    );
  }
  return decodedValue as RawGtx;
}

export function parseValue(typedArg: ASNValue): RawGtv {
  if (typedArg.type === "null") {
    return null;
  } else if (typedArg.type === "byteArray") {
    return typedArg.value as Buffer;
  } else if (typedArg.type === "string") {
    return typedArg.value as string;
  } else if (typedArg.type === "integer") {
    return Number(typedArg.value);
  } else if (typedArg.type === "array") {
    const arrayValue: ASNValue[] = typedArg.value as ASNValue[];
    return arrayValue.map((item: ASNValue) => parseValue(item));
  } else if (typedArg.type === "bigInteger") {
    return BigInt(typedArg.value?.toString() ?? "");
  } else if (typedArg.type === "dict") {
    const arrayValue: ASNDictPair[] = typedArg.value as ASNDictPair[];
    const result: { [name: string]: RawGtv } = {};
    arrayValue.forEach((pair: ASNDictPair) => {
      result[pair.name] = parseValue(pair.value);
    });
    return result;
  } else {
    throw new UnexpectedArgumentTypeError(typedArg);
  }
}

export function createTypedArg(value: RawGtv): ASNValue {
  try {
    if (value == null) {
      return { type: "null", value: null };
    }
    if (Buffer.isBuffer(value)) {
      return { type: "byteArray", value: value };
    }
    if (typeof value === "boolean") {
      return { type: "integer", value: value ? 1 : 0 };
    }
    if (typeof value === "string") {
      return { type: "string", value: value };
    }
    if (typeof value === "number") {
      if (!Number.isInteger(value)) {
        throw Error("User error: Only integers are supported");
      }
      return { type: "integer", value: new BN(value) };
    }
    if (typeof value === "bigint") {
      return { type: "bigInteger", value: new BN(value.toString()) };
    }
    if (value.constructor === Array) {
      return {
        type: "array",
        value: (value as Array<RawGtv>).map(item => createTypedArg(item)),
      };
    }

    if (typeof value === "object") {
      let valueAsDictPair = value as DictPair;

      if (isDictPairWithStringKey(valueAsDictPair)) {
        valueAsDictPair = sortDictPairByKey(valueAsDictPair);
      }

      return {
        type: "dict",
        value: Object.keys(valueAsDictPair).map(function (key) {
          return { name: key, value: createTypedArg(valueAsDictPair[key]) };
        }),
      };
    }
  } catch (error) {
    const message = value ? value.toString() : "RawGtv";

    throw new Error(`Failed to encode ${message}: ${error}`);
  }
  throw new Error(`value ${value} have unsupported type: ${typeof value}`);
}
function isDictPairWithStringKey(obj: DictPair): obj is DictPair {
  for (const key in obj) {
    if (typeof key !== "string") {
      return false;
    }
  }
  return true;
}
function sortDictPairByKey(dict: DictPair): DictPair {
  const sortedArray = Object.entries(dict).sort(([keyA], [keyB]) => {
    if (keyA < keyB) {
      return -1;
    }
    if (keyA > keyB) {
      return 1;
    }
    return 0;
  });

  const sortedDict: DictPair = {};

  for (const [key, value] of sortedArray) {
    sortedDict[key] = value;
  }

  return sortedDict;
}
