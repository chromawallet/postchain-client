import { Buffer } from "buffer";
import * as gtxTool from "./gtx";
import { DictPair, RawGtv } from "../gtv/types";
import { GTX } from "./types";
import { RestClient } from "../restclient/interfaces";
import { QueryObject } from "../restclient/types";
import { GtxClient, Itransaction, SignatureProvider } from "./interfaces";
import * as logger from "../logger";
import { removeDuplicateSigners } from "../formatter";

export function createClient(
  restApiClient: RestClient,
  blockchainRid: string,
  functionNames: string[],
): GtxClient {
  functionNames.push("message");

  function transaction(gtx: GTX): Itransaction {
    return {
      gtx,
      sign: async function (privOrSigProv: SignatureProvider | Buffer, pubKey?: Buffer) {
        logger.debug(
          `signing transaction with ${
            privOrSigProv instanceof Buffer
              ? `privkey: ${privOrSigProv.toString("hex")}`
              : `signature provider [pubKey: ${privOrSigProv.pubKey}]`
          }`,
        );
        if (privOrSigProv instanceof Buffer) {
          await gtxTool.sign(gtx, privOrSigProv, pubKey);
        } else {
          await gtxTool.sign(gtx, privOrSigProv);
        }
      },

      getTxRID: function (): Buffer {
        return this.getDigestToSign();
      },

      getDigestToSign: function (): Buffer {
        return gtxTool.getDigestToSign(this.gtx);
      },

      addSignature: function (pubKey: Buffer, signature: Buffer) {
        gtxTool.addSignature(pubKey, signature, gtx);
      },

      // raw call
      addOperation: function (name: string, ...args: RawGtv[]) {
        gtxTool.addTransactionToGtx(name, args, gtx);
      },

      postAndWaitConfirmation(): Promise<null> {
        return restApiClient.postAndWaitConfirmation(gtxTool.serialize(gtx), this.getTxRID());
      },

      send: function (callback: (error: Error | null | undefined, responseObject: any) => void) {
        const gtxBytes = gtxTool.serialize(gtx);
        restApiClient.postTransaction(gtxBytes, callback);
        // Todo it seems like an error to set a non nullable parent function's input to null
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        gtx = null;
        this.gtxBytes = gtxBytes;
      },

      encode: function (): Buffer {
        return gtxTool.serialize(gtx);
      },
    };
  }

  function addFunctions(req: Itransaction) {
    functionNames.forEach(functionName => {
      req[functionName] = function (...args: RawGtv[]) {
        gtxTool.addTransactionToGtx(functionName, args, this.gtx);
      };
    });
  }

  const client = {
    newTransaction: function (signers: Buffer[]): Itransaction {
      signers = removeDuplicateSigners(signers);
      const newGtx = gtxTool.emptyGtx(Buffer.from(blockchainRid, "hex"));
      signers.forEach(signer => gtxTool.addSignerToGtx(signer, newGtx));

      const req = transaction(newGtx);
      addFunctions(req);
      return req;
    },

    transactionFromRawTransaction: function (rawTransaction: Buffer): Itransaction {
      const gtx = gtxTool.deserialize(rawTransaction);
      logger.debug(`Output from deserializing a raw transaction: ${JSON.stringify(gtx)}`);
      const req = transaction(gtx);
      addFunctions(req);
      return req;
    },
    query: function (nameOrObject: string | QueryObject, queryArguments?: DictPair) {
      if (typeof nameOrObject === "string") {
        return restApiClient.query(nameOrObject, queryArguments);
      } else {
        return restApiClient.query(nameOrObject);
      }
    },
  };

  return client;
}
