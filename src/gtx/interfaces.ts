import { BufferId } from "../blockchainClient/types";
import { DictPair, RawGtv } from "../gtv/types";
import { QueryObject } from "../restclient/types";
import { GTX, RawGtxBody } from "./types";

export interface Itransaction {
  [x: string]: any;
  gtx: GTX;
  /**
   * Signs the transaction using a signature provider. The signature provider must contain a public key and a `sign` function that returns the signature of a digest.
   * @param signatureProvider a signature provider with a `sign` callback and a `pubKey`
   */
  sign(signatureProvider: SignatureProvider): Promise<void>;
  /**
   * Signs the transaction with a given private key.
   * @param privKey the private key to sign the transaction with
   * @param pubKey the public key corresponding to the private key (will be generated if not provided)
   */
  sign(privKey: Buffer, pubKey?: Buffer): Promise<void>;
  /**
   * Calculates and returns the transaction RID, i.e., the merkle root hash of the transaction.
   * @returns transaction RID
   */
  getTxRID: () => Buffer;
  /**
   * Returns a digest of the transaction that can be signed and then added to the transaction using `addSignature`.
   * @returns a digest of the transaction
   */
  getDigestToSign: () => Buffer;
  /**
   * Adds a signature that has been signed externally to the transaction.
   * @param pubKey the public key corresponding to the private key that signed the digest
   * @param signature a signature of the digest of the transaction that is returned by `getDigestToSign`
   */
  addSignature: (pubKey: Buffer, signature: Buffer) => void;
  /**
   * Adds an operation to the transaction.
   * @param name the name of the operation
   * @param args the arguments of the operation
   */
  addOperation: (name: string, ...args: RawGtv[]) => void;
  /**
   * Posts a transaction and polls for status while waiting for status response; confirmed, rejected or unknown. Returns either a resolved or rejected promise.
   */
  postAndWaitConfirmation(): Promise<null>;
  /**
   * Sends the transaction request.
   * @param callback a callback that will be run when the transaction is either rejected or approved
   */
  send: (callback: (error: Error | null | undefined, responseObject: any) => void) => void;
  /**
   * Serializes the transaction using ASN.1.
   * @returns the transaction serialized
   */
  encode: () => Buffer;
}

export interface GtxClient {
  newTransaction: (signers: Buffer[]) => Itransaction;
  transactionFromRawTransaction: (rawTransaction: Buffer) => Itransaction;
  /**
   * Interfaces the query endpoint of the Rell backend. Returns either a resolved or rejected promise. The input is the name of the query followed by the arguments of the
   * query as an optional input argument.
   * @param name the name of the query in Rell
   * @param queryArguments optional argument following this pattern: { arg1: argValue1, arg2: argvalue2 }
   */
  query(name: string, queryArguments?: DictPair): Promise<any>;
  /**
   * Interfaces the query endpoint of the Rell backend. Returns either a resolved or rejected promise.
   * @param queryObject an object containing a "type" and follows this pattern: { type: "nameOfQuery", arg1: argValue1, arg2: argvalue2 }
   * @deprecated
   */
  query(object: QueryObject): Promise<any>;
}

/**
 * A signature provider for `sign`.
 */
export interface SignatureProvider {
  /**
   * A callback used to return the signature of the gtx given as input. It needs to return the output of secp256k1.ecdsaSign(...params).signature.
   * @param rawGtxBody the rawGtxBody derived from a transaction
   */
  sign(rawGtxBody: RawGtxBody): Promise<Buffer>;
  /**
   * The public key corresponding to the private key that is used to sign the digest.
   */
  readonly pubKey: Buffer;
}

export interface IKeyPair {
  privKey: BufferId;
  pubKey?: BufferId;
}
