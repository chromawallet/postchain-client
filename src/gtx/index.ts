import * as serialization from "./serialization";

const encode = serialization.encodeValueGtx;
const decode = serialization.decodeValueGtx;

export { encode, decode };
