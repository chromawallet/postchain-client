import { GTX, GtxBody, RawGtx, RawGtxBody, RellOperation } from "./types";
import { RawGtv } from "../gtv/types";
import * as gtv from "../gtv";
import {
  checkDigestSignature,
  createPublicKey,
  makeKeyPair,
  signDigest,
} from "../encryption/encryption";
import {
  AlreadySignedTransactionException,
  MissingGtxException,
  MissingSignerException,
} from "./errors";
import { encode as gtxEncode, decode as gtxDecode } from "./index";
import { ensureBuffer } from "../formatter";
import { IKeyPair, SignatureProvider } from "./interfaces";

export function emptyGtx(blockchainRid: Buffer): GTX {
  return { blockchainRid, operations: [], signers: [] };
}

/**
 * Adds a function call to a GTX. Creates a new GTX if none specified.
 * This function will throw Error if gtx is already signed
 * @param opName the name of the function to call
 * @param args the array of arguments of the function call. If no args, this must be an empty array
 * @param gtx the function call will be added to this gtx
 * @returns the gtx
 * @throws if gtx is null or if gtx is already signed
 */
export function addTransactionToGtx(opName: string, args: RawGtv[], gtx: GTX): GTX {
  if (gtx == null) {
    throw new MissingGtxException();
  }
  if (gtx.signatures) {
    throw new AlreadySignedTransactionException("function");
  }
  gtx.operations.push({ opName: opName, args: args });
  return gtx;
}

export function addSignerToGtx(signer: Buffer, gtx: GTX): void {
  if (gtx.signatures) {
    throw new AlreadySignedTransactionException("signer");
  }
  gtx.signers.push(signer);
}
/**
 * Serializes the gtx to get tx hash
 * @param gtx the gtx to serialize
 */
export function getDigest(gtx: GTX): Buffer {
  return gtv.gtvHash(gtxToRawGtx(gtx));
}

/**
 * Serializes the gtx for signing
 * @param gtx the gtx to serialize
 */
export function getDigestToSign(gtx: GTX): Buffer {
  return gtv.gtvHash(gtxToRawGtxBody(gtx));
}

export function getDigestToSignFromRawGtxBody(gtxBody: RawGtxBody): Buffer {
  return gtv.gtvHash(gtxBody);
}

export function gtxToRawGtxBody(gtx: GTX): RawGtxBody {
  return [
    gtx.blockchainRid,
    gtx.operations.map((op: RellOperation) => [op.opName, op.args]),
    gtx.signers,
  ];
}

export function gtxToRawGtx(gtx: GTX): RawGtx {
  return [
    [
      gtx.blockchainRid,
      gtx.operations.map((op: RellOperation) => [op.opName, op.args]),
      gtx.signers,
    ],
    gtx.signatures ?? [],
  ];
}
/**
 * asks the signatureProvider to sign the gtx
 */
export async function sign(gtx: GTX, signatureProvider: SignatureProvider): Promise<GTX>;
export async function sign(gtx: GTX, privKey: Buffer, pubKey?: Buffer): Promise<GTX>;
export async function sign(
  gtx: GTX,
  privOrSigProv: Buffer | SignatureProvider,
  pubKey?: Buffer,
): Promise<GTX> {
  if (privOrSigProv instanceof Buffer) {
    const digestToSign = getDigestToSign(gtx);
    const signature = signDigest(digestToSign, privOrSigProv);
    return addSignature(pubKey || makeKeyPair(privOrSigProv).pubKey, signature, gtx);
  } else {
    // Removes signatures and extract the rawgtxBody
    const rawGtxBody = gtxToRawGtxBody(gtx);
    const signature = await privOrSigProv.sign(rawGtxBody);
    return addSignature(privOrSigProv.pubKey, signature, gtx);
  }
}

export function signRawTransaction(_keyPair: Buffer, _rawTransaction: Buffer) {
  throw Error("TODO");
  //TODO
  //const gtx = module.exports.deserialize(rawTransaction);
  //module.exports.sign(keyPair.privKey, keyPair.pubKey, gtx);
  // return module.exports.serialize(gtx)
}

/**
 * Adds a signature to the gtx
 */
export function addSignature(pubKeyBuffer: Buffer, signatureBuffer: Buffer, gtx: GTX): GTX {
  if (!gtx.signatures) {
    gtx.signatures = Array(gtx.signers.length);
  }

  const signerIndex = gtx.signers.findIndex((signer: Buffer) => pubKeyBuffer.equals(signer));
  if (signerIndex === -1) {
    throw new MissingSignerException();
  }
  gtx.signatures[signerIndex] = signatureBuffer;
  return gtx;
}

export function serialize(gtx: GTX): Buffer {
  if (!gtx.signatures) {
    // TODO
    // The gtx is not signed, but we must include
    // the signatures attribute, so let's add that.
    gtx.signatures = [];
  }
  return gtxEncode([gtxToRawGtxBody(gtx), gtx.signatures]);
}

export function deserialize(gtxBytes: Buffer): GTX {
  const deserializedTx = gtxDecode(gtxBytes);
  const body = deserializedTx[0];
  const gtvTxBody = {
    blockchainRid: body[0],
    operations: body[1].map((operation: any[]) => ({
      opName: operation[0],
      args: operation[1],
    })),
    signers: body[2],
  };
  const signatures = deserializedTx[1];

  return {
    blockchainRid: gtvTxBody.blockchainRid,
    operations: gtvTxBody.operations,
    signers: gtvTxBody.signers,
    signatures,
  };
}

export function checkGTXSignatures(txHash: Buffer, gtx: GTX): boolean {
  if (!Array.isArray(gtx.signatures) || gtx.signatures.length === 0) {
    return false;
  }

  for (const i in gtx.signers) {
    const signValid = checkDigestSignature(txHash, gtx.signers[i], gtx.signatures?.[i]);
    if (!signValid) return signValid;
  }
  return true;
}

export function checkExistingGTXSignatures(txHash: Buffer, gtx: GTX): boolean {
  for (const i in gtx.signers) {
    if (gtx.signatures?.[i]) {
      const signValid = checkDigestSignature(txHash, gtx.signers[i], gtx.signatures[i]);
      if (!signValid) return signValid;
    }
  }
  return true;
}

export function newSignatureProvider(keyPair?: IKeyPair): SignatureProvider {
  let pub: Buffer, priv: Buffer;
  if (keyPair) {
    priv = ensureBuffer(keyPair.privKey);
    pub = keyPair.pubKey ? ensureBuffer(keyPair.pubKey) : createPublicKey(priv);
  } else {
    ({ privKey: priv, pubKey: pub } = makeKeyPair());
  }
  return {
    pubKey: pub,
    sign: async gtx => {
      const signature = getDigestToSignFromRawGtxBody(gtx);
      return signDigest(signature, priv);
    },
  };
}

type GtvArray = RawGtv[];
export function rawGtvToGtx(gtv: RawGtv): GTX {
  if (Array.isArray(gtv) && gtv.length !== 2) {
    throw new Error("Gtv must be an array of size 2");
  }

  const gtvArray = gtv as GtvArray;

  if (!Array.isArray(gtvArray[0])) {
    throw new Error("First element must be an array");
  }
  if (!Array.isArray(gtvArray[1])) {
    throw new Error("Second element must be an array");
  }
  gtvArray[1].forEach(element => {
    if (!Buffer.isBuffer(element)) {
      throw new Error("Element must be a buffer");
    }
  });

  const gtxBody: GtxBody = rawGtvToGtxBody(gtvArray[0]);

  return { ...gtxBody, signatures: gtvArray[1] as Buffer[] };
}

function rawGtvToGtxBody(gtv: RawGtv): GtxBody {
  if (Array.isArray(gtv) && gtv.length !== 3) {
    throw new Error("Gtv must be an array of size 3");
  }
  const array = gtv as GtvArray;

  if (!Buffer.isBuffer(array[0])) {
    throw new Error("First element must be a byte array");
  }
  if (!Array.isArray(array[1])) {
    throw new Error("Second element must be an array");
  }
  if (!Array.isArray(array[2])) {
    throw new Error("Third element must be an array");
  }
  array[2].forEach(element => {
    if (!Buffer.isBuffer(element)) {
      throw new Error("Element must be a buffer");
    }
  });
  return {
    blockchainRid: array[0],
    operations: array[1].map(element => rawGtvToRellOp(element)),
    signers: array[2] as Buffer[],
  };
}

function rawGtvToRellOp(gtv: RawGtv): RellOperation {
  if (Array.isArray(gtv) && gtv.length !== 2) {
    throw new Error("Gtv must be an array of size 2");
  }

  const array = gtv as GtvArray;

  if (typeof array[0] !== "string") {
    throw new Error("First element must be a string");
  }
  if (!Array.isArray(array[1])) {
    throw new Error("Second element must be an array");
  }
  return { opName: array[0] as string, args: array[1] };
}
