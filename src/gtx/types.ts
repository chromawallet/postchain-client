import BN from "bn.js";
import { RawGtv } from "../gtv/types";

export type RawGtxOp = [opName: string, args: RawGtv[]];
export type RawGtxBody = [blockchainRid: Buffer, operations: RawGtxOp[], signers: Buffer[]];
export type GtxBody = {
  blockchainRid: Buffer;
  operations: RellOperation[];
  signers: Buffer[];
};
export type RawGtx = [transaction: RawGtxBody, signatures: Buffer[]];

export type GTX = {
  blockchainRid: Buffer;
  operations: RellOperation[];
  signers: Buffer[];
  signatures?: Buffer[];
};

export type RellOperation = {
  opName: string;
  args: RawGtv[];
};

export type SignerPair = {
  pubKey: string;
  signature: string;
};

export type BufferSignerPair = {
  pubKey: Buffer;
  signature: Buffer;
};

export type ASNValue = {
  type: string;
  value: BN | string | number | Buffer | ASNValue[] | ASNDictPair[] | null;
};

export type ASNDictPair = {
  name: string;
  value: ASNValue;
};
