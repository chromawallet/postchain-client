# Chromia client

The Chromia client allows a user to connect a frontend to a dapp running on the Chromia network. The
Chromia client makes it possible to set up a connection to a D1 node which is a service chain that
every blockchain is connected to. Through the D1 connection it finds all addresses where your dapp
is currently running and establishes a connection encountering the list of addresses.

## Example:

```javascript
// To be able to connect to the Chromia network a blockchainRID of a Chromia node running
// the D1 chain (service chain) have to be provided. The brid can be found HERE (point
// somewere). Same goes with the url for this node.
let chromiaD1blockchainRid =
  "7d565d92fd15bd1cdac2dc276cbcbc5581349d05a9e94ba919e1155ef4daf8f9";
let chromiaD1Url = "some url";

// Create an instance of the rest client and configure it for a specific Chromia
// base url and blockchinRID.
let restD1 = pcl.restClient.createRestClient(
  [chromiaUrl],
  chromiaD1blockchainRid,
  5,
  1000
);

// Create an instance of an chromia client provider. It will
// use the rest client instance and it will allow calls to the D1 chain.
let chromiaClient = pcl.chromiaClient.chromiaClientProvider(
  chromiaD1blockchainRid,
  restD1
);

// Each blockchain has a blockchainRID, that identifies the blockchain
// we want to work with. This blockchainRID must match the blockchain RID
// encoded into the first block of the blockchain.
let dappBlockchainRid =
  "FBEE1A0DED02CF6E222C709735326ECA716F2956C1D910FDE865BC4ADF3ACAED";

// Create a connection with a blochchain of the dapp blockchain RID. It will return a rest
// client with the addresses of the nodes that runs your blockchain.
let rest = await chromiaClient.blockchainConnection(dappBlockchainRid);

// Now when the rest client to the nodes running the blockchain is created you can initiate
// a gtx client as done in the postchain-client readme.
let gtx = gtxClient.createClient(rest, dappBlockchainRid, ["fun1", "fun2"]);
```
