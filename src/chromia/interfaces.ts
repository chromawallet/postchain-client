import { RestClient } from "../restclient/interfaces";

export interface ChromiaClient {
  blockchainConnection: (blockchainRid: string) => Promise<RestClient>;
}
