import * as pc from "../../index";
import type { RestClient } from "../restclient/interfaces";
import type { QueryObject } from "../restclient/types";
import type { ChromiaClient } from "./interfaces";
import { BlockchainUrlUndefinedException } from "../blockchainClient/errors";

/**
 * @deprecated Use the function createClient instead.
 * Provides postchain clients that can be used to communicate with dapps within the chromia network
 * @param chain0BRID brid of chain0
 * @param rest rest client configured to node running chain0
 */

export function chromiaClientProvider(chain0BRID: string, rest: RestClient): ChromiaClient {
  const chain0Client = pc.gtxClient.createClient(rest, chain0BRID, []);

  return {
    blockchainConnection: async function (dappBRID: string): Promise<RestClient> {
      const queryObject: QueryObject = {
        type: "cm_get_blockchain_api_urls",
        blockchain_rid: dappBRID,
      };

      const baseUrls: string[] = await chain0Client.query(queryObject);
      if (!baseUrls.length) {
        throw new BlockchainUrlUndefinedException(dappBRID);
      }
      return pc.restClient.createRestClient(baseUrls, dappBRID);
    },
  };
}
