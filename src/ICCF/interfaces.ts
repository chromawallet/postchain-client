export interface ICCFOperationArgsIntraCluster {
  sourceBlockchainRid: Buffer;
  transactionHash: Buffer;
  transactionProof: Buffer;
}

export interface ICCFOperationArgsIntraNetwork extends ICCFOperationArgsIntraCluster {
  transactionData: Buffer;
  transactionIndex: number;
  anchoringProof: Buffer;
}
