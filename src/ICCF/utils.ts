import { IClient } from "../blockchainClient/interface";
import {
  PubKey,
  ConfirmationProof,
  Transaction,
  StatusPolling,
  BufferId,
} from "../blockchainClient/types";
import { AnchoringTransactionSchema } from "../blockchainClient/validation/anchoringTransaction";
import { checkDigestSignature } from "../encryption/encryption";
import { ensureBuffer } from "../formatter";
import { gtvHash } from "../gtv";
import { getDigestToSign, rawGtvToGtx } from "../gtx/gtx";
import { decodeValue, encodeValue } from "../gtx/serialization";
import { GTX } from "../gtx/types";
import { TxRejectedError } from "../restclient/errors";
import { sleep } from "../restclient/restclientutil";
import {
  DifferentNumberOfSignersException,
  MissingTransactionProof,
  ProofRidException,
  SignatureException,
  SystemChainException,
} from "./error";
import { ICCFOperationArgsIntraCluster, ICCFOperationArgsIntraNetwork } from "./interfaces";
import {
  RequestObjectAnchoringTX,
  AnchoringTransaction,
  ResponseObjectClusterInfo,
  ResponseAnchoringTransaction,
} from "./types";

export async function getClusterOfBlockchain(
  client: IClient,
  blockchainRid: Buffer,
): Promise<string> {
  try {
    const clusterName: string = await client.query({
      name: "cm_get_blockchain_cluster",
      args: { brid: blockchainRid },
    });
    return clusterName;
  } catch (error) {
    throw new SystemChainException(error.message);
  }
}
export async function getClusterInfo(
  client: IClient,
  name: string,
): Promise<ResponseObjectClusterInfo> {
  try {
    const clusterInfo: ResponseObjectClusterInfo = await client.query({
      name: "cm_get_cluster_info",
      args: { name },
    });
    return clusterInfo;
  } catch (error) {
    throw new SystemChainException(error.message);
  }
}

export async function awaitGetAnchoringTransactionForBlockRid(
  client: IClient,
  blockchainRid: Buffer,
  blockRid: Buffer,
  systemAnchoringStatusPolling: StatusPolling,
): Promise<AnchoringTransaction | null> {
  let anchoringTransactionResponse: AnchoringTransaction | null = null;
  for (let i = 0; i < systemAnchoringStatusPolling.count; i++) {
    anchoringTransactionResponse = await getAnchoringTransactionForBlockRid(
      client,
      blockchainRid,
      blockRid,
    );

    if (anchoringTransactionResponse) {
      const anchoringTransactionValidaiton = AnchoringTransactionSchema.safeParse(
        anchoringTransactionResponse,
      );
      if (anchoringTransactionValidaiton.success) {
        return anchoringTransactionValidaiton.data;
      } else if (!anchoringTransactionValidaiton.success) {
        throw new TxRejectedError("Invalid anchoring transaction format");
      }
    }

    await sleep(systemAnchoringStatusPolling.interval);
  }

  return anchoringTransactionResponse;
}

export async function getAnchoringTransactionForBlockRid(
  client: IClient,
  blockchainRid: Buffer,
  blockRid: Buffer,
): Promise<AnchoringTransaction | null> {
  try {
    const anchoringTxForBlockRid = await client.query<
      ResponseAnchoringTransaction,
      RequestObjectAnchoringTX
    >({
      name: "get_anchoring_transaction_for_block_rid",
      args: { blockchain_rid: blockchainRid, block_rid: blockRid },
    });

    if (!anchoringTxForBlockRid) return null;

    return convertToAnchoringTransaction(anchoringTxForBlockRid);
  } catch (error) {
    throw new SystemChainException(error.message);
  }
}

export function convertToAnchoringTransaction(
  responseTx: ResponseAnchoringTransaction,
): AnchoringTransaction {
  const { tx_rid, tx_data, tx_op_index } = responseTx;

  return {
    txRid: tx_rid,
    txData: tx_data,
    txOpIndex: tx_op_index,
  };
}

export function calculateBlockRID(decodedTxProof: ConfirmationProof): Buffer {
  const sourceBlockHeader = decodedTxProof.blockHeader;

  if (!sourceBlockHeader) {
    throw new Error("Failed to get blockHeader from confirmation proof");
  }
  const decodeSourceBlockRid = decodeValue(sourceBlockHeader);
  return gtvHash(decodeSourceBlockRid);
}

// fetch tx from txRID and verifies with secp256k1.ecdsaVerify that txRID and signer creates signatures that are on the blockchain transaction
export async function fetchAndVerifyTransaction(
  sourceClient: IClient,
  txToProveRID: BufferId,
  proofHash: Buffer,
  txToProveSigners: PubKey[],
): Promise<{ verifiedTx: GTX; verifiedTxHash: Buffer }> {
  const rawTx = await sourceClient.getTransaction(txToProveRID);

  const txGtv = decodeValue(rawTx);

  const fetchedTxHash = gtvHash(txGtv);

  if (Buffer.compare(fetchedTxHash, proofHash)) {
    // We received another hash for tx RID than what was included in proof
    // Possibly rouge or faulty node(s). Anyway, we need to give up.
    throw new MissingTransactionProof(proofHash, fetchedTxHash);
  }
  const fetchedTx: GTX = rawGtvToGtx(txGtv);
  if (txToProveSigners.length != fetchedTx.signatures?.length) {
    throw new DifferentNumberOfSignersException(
      fetchedTx.signatures?.length ?? 0,
      txToProveSigners.length,
    );
  }
  const txRID = getDigestToSign(fetchedTx);
  if (Buffer.compare(txRID, ensureBuffer(txToProveRID))) {
    throw new ProofRidException();
  }
  for (const signer of txToProveSigners) {
    let hasSignature = false;
    for (const signature of fetchedTx.signatures) {
      // verify that txRID (hash of gtxBody) signed by signers equals the signatures from network
      if (checkDigestSignature(txRID, signer, signature)) {
        hasSignature = true;
        break;
      }
    }
    if (!hasSignature) throw new SignatureException(signer);
  }
  return { verifiedTx: fetchedTx, verifiedTxHash: fetchedTxHash };
}

export function composeProofTransactionObject(
  sourceBlockchainRid: BufferId,
  verifiedTxHash: Buffer,
  txProof: ConfirmationProof,
  iccfTxSigners: Buffer[],
  anchoringTx?: AnchoringTransaction,
  anchoringProof?: ConfirmationProof,
  isNetwork = true,
): Transaction {
  let operationArgs: ICCFOperationArgsIntraCluster | ICCFOperationArgsIntraNetwork;
  operationArgs = {
    sourceBlockchainRid: ensureBuffer(sourceBlockchainRid),
    transactionHash: verifiedTxHash,
    transactionProof: encodeValue(txProof),
  };

  if (isNetwork) {
    operationArgs = {
      ...operationArgs,
      transactionData: anchoringTx?.txData,
      transactionIndex: anchoringTx?.txOpIndex,
      anchoringProof: anchoringProof && encodeValue(anchoringProof),
    };
  }

  return {
    operations: [
      {
        name: "iccf_proof",
        args: Object.values(operationArgs),
      },
    ],
    signers: iccfTxSigners,
  };
}
