import { BufferId } from "../blockchainClient/types";
import { ensureString, toString } from "../formatter";
export class MissingTransactionProof extends Error {
  constructor(proofHash: Buffer, fetchedTxHash: Buffer) {
    super(
      `Unable to verify source transaction proof,transaction hash in proof ${toString(
        proofHash,
      )} does not match hash from fetched transaction ${toString(fetchedTxHash)}`,
    );
  }
}

export class DifferentNumberOfSignersException extends Error {
  constructor(length: number, comparableLength: number) {
    super(
      `Transaction signatures amount ${length} do not match expected amount of signers ${comparableLength}`,
    );
  }
}
export class SignatureException extends Error {
  constructor(signer: Buffer) {
    super(`Expected signer ${toString(signer)} has not signed source transaction`);
  }
}

export class ProofRidException extends Error {
  constructor() {
    super(
      "Unable to verify source transaction proof, got a different transaction from query than we asked for",
    );
  }
}

export class SystemChainException extends Error {
  constructor(errorMessage: string) {
    super(`Query to system chain failed with error: ${errorMessage}`);
  }
}

export class ConfirmationProofException extends Error {
  constructor(transactionRid: BufferId) {
    super(
      `Unable to fetch confirmation proof for transaction RID: ${ensureString(transactionRid)}.`,
    );
  }
}

export class BlockAnchoringException extends Error {
  constructor() {
    super(`Block is not present in cluster anchoring chain`);
  }
}
