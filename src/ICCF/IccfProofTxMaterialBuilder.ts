import { createClient } from "../blockchainClient/blockchainClient";
import { IClient } from "../blockchainClient/interface";
import { PubKey, ConfirmationProof, NetworkSettings, BufferId } from "../blockchainClient/types";
import { IccfProof, AnchoringTransaction, ResponseObjectClusterInfo } from "./types";
import {
  awaitGetAnchoringTransactionForBlockRid,
  calculateBlockRID,
  composeProofTransactionObject,
  fetchAndVerifyTransaction,
  getClusterInfo,
  getClusterOfBlockchain,
} from "./utils";
import { toString, toBuffer, ensureString, ensureBuffer } from "../formatter";
import { BlockAnchoringException, ConfirmationProofException } from "./error";
import { getUrlsFromEndpoints } from "../blockchainClient/utils";
import { FailoverStrategy } from "../blockchainClient/enums";

/**
 * Creates an ICCF (Inter-Chain Communication Framework) proof transaction.
 * This function generates a proof that a specific transaction has occurred on the source blockchain
 * and constructs an ICCF proof transaction that can be posted to the target blockchain.
 *
 * @param {IClient} client - The client configured to communicate with the management chain.
 * @param {BufferId} txToProveRid - The RID of the transaction to be proven.
 * @param {Buffer} txToProveHash - The hash of the transaction to be proven.
 * @param {PubKey[]} txToProveSigners - An array of public keys representing signers of the transaction to be proven.
 * @param {BufferId} sourceBlockchainRid - The RID of the source blockchain.
 * @param {BufferId} targetBlockchainRid - The RID of the target blockchain.
 * @param {PubKey[]} iccfTxSigners - An array of public keys representing signers of the ICCF proof transaction (optional, default: []).
 * @param {boolean} forceIntraNetworkIccfOperation - Whether to force the ICCF operation to be performed within the same network (optional, default: false).
 * @returns {Promise<IccfProof>} A promise that resolves to an ICCF proof object containing the ICCF proof transaction.
 */
export async function createIccfProofTx(
  client: IClient,
  txToProveRid: BufferId,
  txToProveHash: Buffer,
  txToProveSigners: PubKey[],
  sourceBlockchainRid: BufferId,
  targetBlockchainRid: BufferId,
  iccfTxSigners: PubKey[] = [],
  forceIntraNetworkIccfOperation = false,
): Promise<IccfProof> {
  const clientConfiguredToSource = await createClient({
    directoryNodeUrlPool: getUrlsFromEndpoints(client.config.endpointPool),
    blockchainRid: ensureString(sourceBlockchainRid),
  });

  const txProof: ConfirmationProof = await clientConfiguredToSource.getConfirmationProof(
    txToProveRid,
  );
  if (!txProof || !txProof.hash) {
    throw new ConfirmationProofException(txToProveRid);
  }
  const proofHash = txProof.hash;

  const { verifiedTx, verifiedTxHash } = !txToProveHash.equals(proofHash)
    ? await fetchAndVerifyTransaction(
        clientConfiguredToSource,
        txToProveRid,
        proofHash,
        txToProveSigners,
      )
    : { verifiedTx: null, verifiedTxHash: txToProveHash };

  const sourceCluster = await getClusterOfBlockchain(client, ensureBuffer(sourceBlockchainRid));
  const targetCluster = await getClusterOfBlockchain(client, ensureBuffer(targetBlockchainRid));

  if (!forceIntraNetworkIccfOperation && sourceCluster === targetCluster) {
    // intra-cluster
    const intraClusterProofTx = composeProofTransactionObject(
      sourceBlockchainRid,
      verifiedTxHash,
      txProof,
      iccfTxSigners,
      undefined,
      undefined,
      false,
    );
    return { iccfTx: intraClusterProofTx };
  } else {
    // intra-network
    const systemClientUsingStickyNode = {
      ...client,
      config: {
        ...client.config,
        failoverStrategy: FailoverStrategy.SingleEndpoint,
      },
    };
    const anchoringClient = await getAnchoringClient(
      systemClientUsingStickyNode,
      sourceBlockchainRid,
      sourceCluster,
    );

    const clusterAnchoredTx = await getBlockAnchoringTransaction(
      clientConfiguredToSource,
      anchoringClient,
      undefined,
      txProof,
    );

    if (!clusterAnchoredTx) {
      throw new BlockAnchoringException();
    }

    const anchoringProof = await anchoringClient.getConfirmationProof(clusterAnchoredTx.txRid);

    const intraNetworkProofTx = composeProofTransactionObject(
      sourceBlockchainRid,
      verifiedTxHash,
      txProof,
      iccfTxSigners,
      clusterAnchoredTx,
      anchoringProof,
    );
    return { iccfTx: intraNetworkProofTx, verifiedTx };
  }
}

/**
 * Checks whether a given transaction is included in the cluster anchoring chain and returns the
 * block anchoring transaction. If `txProof` is not provided, it fetches the confirmation proof
 * using the `sourceClient`.
 *
 * @param sourceClient - A client configured to the blockchain where the transaction was made.
 * @param anchoringClient - The client responsible for querying the anchoring blockchain.
 * @param txRid - The transaction RID to check for anchoring.
 * @param txProof - (Optional) The transaction proof for the specified `txRid`.
 * @returns A Promise that resolves to the anchored transaction response object.
 */
export async function getBlockAnchoringTransaction(
  sourceClient: IClient,
  anchoringClient: IClient,
  txRid?: BufferId,
  txProof?: ConfirmationProof,
): Promise<AnchoringTransaction | null> {
  if (!txRid && !txProof) {
    throw Error("Missing a txRid or TxProof");
  }
  const confirmationProof = txProof ?? (txRid && (await sourceClient.getConfirmationProof(txRid)));

  if (!confirmationProof) {
    throw Error("Confirmation proof not found");
  }

  const blockRid = calculateBlockRID(confirmationProof);
  const blockchainRid = sourceClient.config.blockchainRid;
  const anchoringTxResponse = await awaitGetAnchoringTransactionForBlockRid(
    anchoringClient,
    toBuffer(blockchainRid),
    blockRid,
    sourceClient.config.clusterAnchoringStatusPolling,
  );

  return anchoringTxResponse;
}

/**
 * Checks whether a given transaction is included in the anchoring blockchain.
 *
 * @param sourceClient - A client configured to the blockchain where the transaction was made.
 * @param anchoringClient - The client responsible for querying the anchoring blockchain.
 * @param txRid - The transaction RID to check for anchoring.
 * @param txProof - (Optional) The transaction proof for the specified `txRid`.
 * @returns A Promise that resolves to `true` if the transaction is anchored, `false` otherwise.
 */
export async function isBlockAnchored(
  sourceClient: IClient,
  anchoringClient: IClient,
  txRid: BufferId,
  txProof?: ConfirmationProof,
): Promise<boolean> {
  return !!(await getBlockAnchoringTransaction(sourceClient, anchoringClient, txRid, txProof));
}

/**
 * Gets a client configured for the cluster anchoring blockchain of a cluster. Takes a specific
 * cluster name or blockchain RID to determine the cluster.
 * @param client - The client configured to communicate with the management chain.
 * @param dappBlockchainRid - (Optional) The RID of a blockchain which anchoring wants to be checked.
 * @param cluster - (Optional) The cluster of interest.
 * @returns A Promise that resolves to the client configured to a cluster anchoring chain.
 */
export async function getAnchoringClient(
  client: IClient,
  dappBlockchainRid?: BufferId,
  cluster?: string,
): Promise<IClient> {
  if (!dappBlockchainRid && !cluster) {
    throw Error("Missing a dapp blockchainRid or cluster name");
  }
  const sourceCluster =
    cluster ??
    (dappBlockchainRid && (await getClusterOfBlockchain(client, ensureBuffer(dappBlockchainRid))));

  if (!sourceCluster) {
    throw Error("No cluster could be found");
  }
  const sourceClusterInfo: ResponseObjectClusterInfo = await getClusterInfo(client, sourceCluster);

  if (!sourceClusterInfo || !sourceClusterInfo.anchoring_chain) {
    throw Error("Cluster info could not be found");
  }

  const networkSettings: NetworkSettings = {
    ...client.config,
    directoryNodeUrlPool: getUrlsFromEndpoints(client.config.endpointPool),
    blockchainRid: toString(sourceClusterInfo.anchoring_chain),
  };

  const clientConfiguredToAnchoringchain = await createClient(networkSettings);

  return clientConfiguredToAnchoringchain;
}
