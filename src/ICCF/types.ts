import { Transaction } from "../blockchainClient/types";
import { RawGtv } from "../gtv/types";
import { GTX } from "../gtx/types";

export type ResponseObjectClusterInfo = {
  name: string;
  anchoring_chain: Buffer;
  peers: [PeerInfo];
};

export type PeerInfo = {
  pubkey: Buffer;
  api_url: string;
};

export type ResponseAnchoringTransaction = {
  tx_rid: Buffer;
  tx_data: Buffer;
  tx_op_index: number;
};

export type AnchoringTransaction = {
  txRid: Buffer;
  txData: Buffer;
  txOpIndex: number;
};

export type RequestObjectAnchoringTX = {
  blockchain_rid: Buffer;
  block_rid: Buffer;
};

export type IccfProof = {
  iccfTx: Transaction;
  verifiedTx?: GTX | null;
};

export type BlockHeader = {
  gtvBlockchainRid: Buffer;
  gtvPreviousBlockRid: Buffer;
  gtvMerkleRootHash: Buffer;
  gtvTimestamp: number;
  gtvHeight: number;
  gtvDependencies: RawGtv;
  gtvExtra: any;
};
