import { DictPair, RawGtv } from "../gtv/types";
import { FailOverConfig } from "../restclient/types";
import { AnchoringStatus, FailoverStrategy, ResponseStatus } from "./enums";
import { RawGtxBody } from "../gtx/types";
import { NodeManager } from "../restclient/nodeManager";
import { AnchoringTransaction } from "../ICCF/types";
import { IClient } from "./interface";

export type KeyPair = {
  privKey: PrivKey;
  pubKey: PubKey;
};

export type Endpoint = {
  readonly url: string;
  whenAvailable: number;
};

export type StatusPolling = {
  interval: number;
  count: number;
};

export type ClientConfig = {
  endpointPool: Endpoint[];
  nodeManager: NodeManager;
  blockchainRid: string;
  failoverStrategy: FailoverStrategy;
  attemptsPerEndpoint: number;
  attemptInterval: number;
  unreachableDuration: number;
  directoryChainRid: string;
  dappStatusPolling: StatusPolling;
  clusterAnchoringStatusPolling: StatusPolling;
  systemAnchoringStatusPolling: StatusPolling;
};

export type NetworkSettings = {
  nodeUrlPool?: string | string[];
  directoryNodeUrlPool?: string | string[];
  blockchainRid?: string;
  blockchainIid?: number;
  failOverConfig?: FailOverConfig;
  useStickyNode?: boolean;
  directoryChainRid?: string;
  dappStatusPolling?: StatusPolling;
  clusterAnchoringStatusPolling?: StatusPolling;
  systemAnchoringStatusPolling?: StatusPolling;
};

export interface TransactionReceipt {
  status: ResponseStatus | AnchoringStatus;
  statusCode: number | null;
  transactionRid: Buffer;
  clusterAnchoredTx?: AnchoringTransaction;
  clusterAnchoringClientBrid?: string;
  systemAnchoredTx?: AnchoringTransaction;
  systemAnchoringClientBrid?: string;
}

export type SignedTransaction = Buffer;

export interface SignatureProvider {
  sign(rawGtxBody: RawGtxBody): Promise<Buffer>;
  readonly pubKey: Buffer;
}

export type RestClientQueryObject = {
  type: string;
  [arg: string]: RawGtv;
};

export type QueryObject<TReturn extends RawGtv, TArgs extends DictPair | undefined = DictPair> = {
  name: string;
  args?: TArgs;
};

export type QueryCallback<TReturn> = (error: Error | null, response: TReturn) => void;

export type PrivKey = Buffer;

export type PubKey = Buffer;

export type Operation = {
  name: string;
  args?: RawGtv[];
};

export interface Transaction {
  operations: Operation[];
  signers: Buffer[];
}

export type StatusObject = {
  status: ResponseStatus | AnchoringStatus;
  rejectReason?: string;
};

export type SignerPair = {
  pubKey: string;
  signature: string;
};

export type BufferSignerPair = {
  pubKey: Buffer;
  signature: Buffer;
};
export type Step = {
  side: number;
  hash: Buffer;
};

export type Path = Step[];

export type ConfirmationProof = {
  hash: Buffer;
  blockHeader: Buffer;
  witness: Buffer;
  merkleProofTree: RawGtv;
  txIndex: number;
};

export type TransactionInfo = {
  blockRid: Buffer;
  blockHeight: number;
  blockHeader: Buffer;
  witness: Buffer;
  timestamp: number;
  txRid: Buffer;
  txHash: Buffer;
  txData: Buffer;
};

export type TransactionInfoResponse = {
  blockRID: string;
  blockHeight: number;
  blockHeader: string;
  witness: string;
  timestamp: number;
  txRID: string;
  txHash: string;
  txData: string;
};

export type BlockInfoResponse = {
  rid: string;
  prevBlockRID: string;
  header: string;
  height: number;
  transactions: TransactionInfoInBlockResponse[];
  witness: string;
  witnesses: string[];
  timestamp: number;
};
export type TransactionInfoInBlockResponse = {
  rid: string;
  hash: string;
  data?: string;
};

export type BlockInfo = {
  rid: Buffer;
  prevBlockRid: Buffer;
  header: Buffer;
  height: number;
  transactions: TransactionInfoInBlock[];
  witness: Buffer;
  witnesses: Buffer[];
  timestamp: number;
};

export type TransactionInfoInBlock = {
  rid: Buffer;
  hash: Buffer;
  data?: Buffer;
};

export type ClusterAnchoringConfirmation = {
  status: AnchoringStatus;
  clusterAnchoredTx: AnchoringTransaction;
};

export interface AppStructure extends DictPair {
  modules: BlockchainModules;
}

interface BlockchainModules extends DictPair {
  [key: string]: RellModule;
}

interface RellFunction extends DictPair {
  mount: string;
  parameters: RellParameter[];
  returnType: RawGtv;
}

interface RellParameter extends DictPair {
  name: string;
  type: RawGtv;
}

interface RellAttribute extends DictPair {
  type: RawGtv;
  mutable: number;
}

interface RellObject extends DictPair {
  mount: string;
  attributes: Record<string, RellAttribute>;
}

interface RellModule extends DictPair {
  name: string;
  queries: {
    [key: string]: RellFunction;
  };
  operations: {
    [key: string]: RellFunction;
  };
  objects: {
    [key: string]: RellObject;
  };
}

export type BufferId = string | Buffer;

export type BlockAnchoringState = {
  status: AnchoringStatus;
  clusterAnchoredTx?: AnchoringTransaction;
  systemAnchoredTx?: AnchoringTransaction;
};

export type AnchoringClientAndSystemBrid = {
  anchoringClient: IClient;
  systemAnchoringChainBridString: string;
};

export type TransactionsCount = {
  transactionsCount: number;
};

export type TransactionConfirmationProof = {
  proof: string;
};
