import {
  AppStructure,
  BlockInfo,
  KeyPair,
  TransactionInfo,
  TransactionReceipt,
  StatusObject,
  BufferId,
  BlockAnchoringState,
} from "./types";
import { DictPair, RawGtv } from "../gtv/types";
import { Web3PromiEvent } from "../promiEvent/promiEvents";
import { RestClientCallback } from "../restclient/types";
import {
  ClientConfig,
  Operation,
  QueryCallback,
  QueryObject,
  SignatureProvider,
  SignedTransaction,
  Transaction,
} from "./types";
import { GTX } from "../gtx/types";
import { ConfirmationProof } from "../blockchainClient/types";
import { AnchoringStatus, ChainConfirmationLevel } from "./enums";
import { AnchoringTransaction } from "../ICCF/types";
import { Web3EventMap } from "../promiEvent/promiEventEmitter";

export interface Queryable {
  /**
   * Performs an asynchronous query to the Rell backend. Returns either a resolved or rejected promise. The input is the name of the query followed by the arguments of the
   * query as an optional input argument.
   *
   * @template TReturn - The type of the query return value.
   * @template TArgs - The type of the query arguments. Defaults to `DictPair`.
   * @param nameOrQueryObject - the name of the query in Rell or a QueryObject containing name and args
   * @param args - The query argument following this pattern: { arg1: argValue1, arg2: argvalue2 }
   * @param callback - A callback function to handle the query response. Parameters (error, signedTx).
   * @returns - A promise that resolves with the query result.
   */
  query<TReturn extends RawGtv, TArgs extends DictPair | undefined = DictPair>(
    nameOrQueryObject: string | QueryObject<TReturn, TArgs>,
    args?: TArgs,
    callback?: QueryCallback<TReturn>,
  ): Promise<TReturn>;
}

export interface IClient extends Queryable {
  config: ClientConfig;

  /**
   * Signs a transaction using the provided signing method. The signature provider must contain a public key and a `sign` function that returns the signature of a digest.
   *
   * @param transaction - The transaction or signed transaction to be signed.
   * @param signMethod - The signing method, either a KeyPair or a SignatureProvider. A signature provider with a `sign` callback and a `pubKey`.
   * @param callback - A callback function to handle the signing result. Parameters (error, signedTx)
   * * @returns - A promise that resolves with the signed transaction.
   */
  signTransaction(
    transaction: Transaction | SignedTransaction,
    signMethod: KeyPair | SignatureProvider,
    callback?: RestClientCallback<Buffer>,
  ): Promise<SignedTransaction>;

  /**
   * Sends a transaction to the blockchain.
   *
   * @param transaction - The signed transaction, transaction, or operation to send.
   * @param doStatusPolling  - Sets whether or not requests should be sent to confirm the transaction status.
   * @param callback - A callback function to handle the transaction receipt. Parameter (error, transactionReceipt).
   * @param confirmationLevel  - Enables configuration of the confirmation level the requests should sent for the transaction status.
   * @returns - A Web3PromiEvent that resolves with the transaction receipt.
   */
  sendTransaction(
    transaction: Transaction | SignedTransaction | Operation,
    doStatusPolling?: boolean,
    callback?: RestClientCallback<TransactionReceipt>,
    confirmationLevel?: ChainConfirmationLevel,
  ): Web3PromiEvent<TransactionReceipt, Web3EventMap>;

  /**
   * Signs and sends a transaction for inclusion in a block. It adds a nop
   * if missing.
   * @param transaction - The operation or transaction to sign and send.
   * @param signMethod - The signing method, either a KeyPair or a SignatureProvider. A signature provider with a `sign` callback and a `pubKey`.
   * @param doStatusPolling  - Sets whether or not requests should be sent to confirm the transaction status.
   * @param callback - A callback function to handle the transaction receipt.
   * @param confirmationLevel  - Enables configuration of the confirmation level the requests should sent for the transaction status.
   * @returns - A Web3PromiEvent that resolves with the transaction receipt.
   *
   */
  signAndSendUniqueTransaction(
    transaction: Operation | Transaction,
    signMethod: KeyPair | SignatureProvider,
    doStatusPolling?: boolean,
    callback?: RestClientCallback<TransactionReceipt>,
    confirmationLevel?: ChainConfirmationLevel,
  ): Web3PromiEvent<TransactionReceipt, Web3EventMap>;

  /**
   * Retrieves a transaction given its transaction RID.
   *
   * @param transactionRid - The transaction RID.
   * @param callback - A callback function to handle the serialized message. Parameters (error, serializedMessage) if first
   * parameter is not null, an error occurred.
   * If first parameter is null, then the second parameter is a buffer
   * with the serialized client message. If no such client message exists,
   * the callback will be called with (null, null).
   * @returns- A promise that resolves with the serialized message of the transaction.
   */
  getTransaction(transactionRid: BufferId, callback?: RestClientCallback<Buffer>): Promise<Buffer>;

  /**
   * Retrieves the status of a transaction given its transaction RID.
   *
   * @param transactionRid - The transaction RID.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object on the form
   * { status: '<confirmed|waiting|rejected|unknown>' }
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the status object of the transaction.
   *
   */
  getTransactionStatus: (
    transactionRid: BufferId,
    callback?: RestClientCallback<TransactionReceipt> | RestClientCallback<StatusObject>,
  ) => Promise<StatusObject>;

  /**
   * Adds an operation containing a nonce which makes the transaction unique.
   *
   * @param transaction - The transaction to sign and send.
   */
  addNop(transaction: Transaction): Transaction;

  /**
   * @deprecated Use calculateTransactionRid util function instead.
   *
   * Calculates and returns the transaction RID, i.e., the merkle root hash of the transaction.
   * @param transaction The transaction to sign and send
   */
  getTransactionRid(transaction: Transaction | SignedTransaction | Operation): Buffer;

  /**
   * Fetch info about transactions from a chain.
   *
   * @param limit - Limit number of transactions returned. Minimum value is 0 and maximum 600. Defaults to 25.
   * @param beforeTime - Return only transactions before this time.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type TransactionInfo[].
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with an array of transaction information.
   *
   */
  getTransactionsInfo(
    limit?: number,
    beforeTime?: Date,
    callback?: RestClientCallback<TransactionInfo[]>,
  ): Promise<TransactionInfo[]>;

  /**
   * Fetch info about a transaction from a chain.
   *
   * @param transactionRID - The transaction RID.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type TransactionInfo.
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the transaction information.
   *
   */
  getTransactionInfo(
    transactionRid: BufferId,
    callback?: RestClientCallback<TransactionInfo>,
  ): Promise<TransactionInfo>;

  /**
   * Fetches the last known confirmation status for a transaction RID.
   *
   * @param transactionRid - The transaction RID to retrieve the last confirmation level for
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is of type TransactionReceipt
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the transaction confirmation level and an anchoring transaction if the transaction is anchored
   */

  getTransactionConfirmationLevel(
    transactionRid: BufferId,
    callback?: RestClientCallback<StatusObject | TransactionReceipt>,
  ): Promise<TransactionReceipt>;

  /**
   * Fetches the last known anchoring status for a block RID.
   *
   * @param blockRid - The block RID to retrieve the anchoring status for
   * @param anchoringClient - The anchoring client that will be used to retrieve the anchoring transaction confirmation
   * @param systemAnchoringChainRid - The system chain rid retrieved from the function `getSystemAnchoringChain`
   * @returns - A promise that resolves with the anchoring status for the block,
   * including the cluster anchored transaction and system anchored transaction based on the anchoring state
   */
  getAnchoringStatusForBlockRid(
    blockRid: BufferId,
    anchoringClient: IClient,
    systemAnchoringChainRid: string,
  ): Promise<BlockAnchoringState>;

  /**
   * Fetch number of successful transactions for a chain.
   *
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is a number.
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the number of successful transactions.
   *
   */
  getTransactionCount(callback?: RestClientCallback<number>): Promise<number>;

  /**
   * Fetch info about a block for a chain.
   *
   * @param blockIdentifier - The block height or RID of the block for which data should be retrieved.
   * @param tsx - Boolean to decide whether or not to include full transaction data. Defaults to false.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type BlockInfo.
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the block info.
   *
   */
  getBlockInfo(
    blockIdentifier: number | string,
    txs?: boolean,
    callback?: RestClientCallback<BlockInfo>,
  ): Promise<BlockInfo>;

  /**
   * Fetch info about the latest block for a chain.
   *
   * @param tsx - Boolean to decide whether or not to include full transaction data. Defaults to false.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type BlockInfo.
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with the block info.
   *
   */
  getLatestBlock(txs?: boolean, callback?: RestClientCallback<BlockInfo>): Promise<BlockInfo>;

  /**
   * Fetch info about blocks for a chain. If both beforeTime and beforeHeight is provided, only beforeTime will be used.
   *
   * @param limit - Limit number of transactions returned. Minimum value is 0 and maximum 100. Defaults to 25.
   * @param beforeTime - Return only transactions before this time. Incompatible with `beforeHeight` and `afterHeight`.
   * @param afterTime - Return only transactions after this time. Incompatible with `beforeHeight` and `afterHeight`.
   * @param beforeHeight - Return only transactions before this height. Incompatible with `beforeTime` and `afterTime`.
   * @param afterHeight - Return only transactions after this height. Incompatible with `beforeTime` and `afterTime`.
   * @param tsx - Boolean to decide whether or not to include full transaction data. Defaults to false.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type BlockInfo[].
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with an array of block info.
   *
   */
  getBlocks(props: {
    limit?: number;
    beforeTime?: Date;
    afterTime?: Date;
    beforeHeight?: number;
    afterHeight?: number;
    excludeEmpty?: boolean;
    txs?: boolean;
    callback?: RestClientCallback<TransactionInfo[]>;
  }): Promise<BlockInfo[]>;

  /**
   * Fetch info about blocks for a chain. If both beforeTime and beforeHeight is provided, only beforeTime will be used.
   *
   * @param limit - Limit number of transactions returned. Minimum value is 0 and maximum 100. Defaults to 25.
   * @param beforeTime - Return only transactions before this time.
   * @param beforeHeight - Return only transactions before this height.
   * @param tsx - Boolean to decide whether or not to include full transaction data. Defaults to false.
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is an object of type BlockInfo[].
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with an array of block info.
   * @deprecated Use getBlocks instead.
   *
   */
  getBlocksInfo(
    limit?: number,
    beforeTime?: Date,
    beforeHeight?: number,
    txs?: boolean,
    callback?: RestClientCallback<TransactionInfo[]>,
  ): Promise<BlockInfo[]>;

  /**
   * @deprecated Use encodeTransaction util function instead.
   *
   * Encodes the transaction using ASN.1.
   * @returns the transaction encoded
   */
  encodeTransaction(transaction: Transaction): Buffer;

  /**
   * @deprecated Use decodeTransactionToGtx util function instead.
   *
   * Decodes the transaction using ASN.1.
   * @returns the transaction decoded in GTX format
   */
  decodeTransactionToGtx(transaction: Buffer): GTX;

  /**
   * Retrieves the client node URL pool.
   *
   * @returns An array of the client node URL pool.
   */
  getClientNodeUrlPool(): string[];

  getConfirmationProof(
    txRID: BufferId,
    callback?: RestClientCallback<ConfirmationProof>,
  ): Promise<ConfirmationProof>;

  getAppStructure(callback?: RestClientCallback<AppStructure>): Promise<AppStructure>;

  /**
   *
   * @param transactionRid - The transaction RID to retrieve a cluster anchoring transaction or anchoring status
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is of type TransactionReceipt
   * @param anchoringClient - The cluster anchoring client that will be used to retrieve the cluster anchoring transaction confirmation
   *
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with an anchoring transaction or anchoring status based on the outcome
   */
  getClusterAnchoringTransactionConfirmation(
    transactionRid: BufferId,
    anchoringClient: IClient,
    callback?: RestClientCallback<TransactionReceipt>,
  ): Promise<AnchoringTransaction | AnchoringStatus>;

  /**
   *
   * @param anchoredTxRid - The anchored transaction RID retrieved from the cluster anchoring transaction, used to retrieve a system cluster anchoring transaction
   * @param anchoringClient - The anchoring client that will be used to retrieve the anchoring transaction confirmation
   * @param systemAnchoringChainRid - The system chain rid retrieved from the function `getSystemAnchoringChain`
   * to be used for the construction of the system anchroing chain client
   * @param callback - A callback function to handle the response body. Taking parameters (error, responseBody). If error is null
   * then responseBody is of type TransactionReceipt.
   *
   * If error is not null, then responseBody
   * is an object with the string property 'error'.
   * @returns - A promise that resolves with an anchoring transaction or null
   */
  getSystemAnchoringTransactionConfirmation(
    anchoredTxRid: BufferId,
    anchoringClient: IClient,
    systemAnchoringChainRid: string,
    callback?: RestClientCallback<TransactionReceipt>,
  ): Promise<AnchoringTransaction | null>;
}
