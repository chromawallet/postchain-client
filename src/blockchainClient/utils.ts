import { Buffer } from "buffer";
import * as logger from "../logger";
import { Web3PromiEvent } from "../promiEvent/promiEvents";
import { GTX, RellOperation } from "../gtx/types";
import {
  SerializedTransactionFormatException,
  InvalidTxRidException,
  TxRejectedError,
} from "../restclient/errors";
import { handlePostResponse } from "../restclient/restclient";
import {
  BlockInfo,
  BlockInfoResponse,
  ClientConfig,
  Endpoint,
  KeyPair,
  NetworkSettings,
  Operation,
  QueryObject,
  SignedTransaction,
  Transaction,
  TransactionInfo,
  TransactionInfoResponse,
  TransactionInfoInBlock,
  TransactionInfoInBlockResponse,
  StatusObject,
  ClusterAnchoringConfirmation,
  TransactionReceipt,
  BufferId,
  AnchoringClientAndSystemBrid,
  StatusPolling,
} from "./types";
import * as gtxTool from "../gtx/gtx";
import {
  BlockchainUrlUndefinedException,
  DirectoryNodeUrlPoolException,
  InvalidTransactionFormatException,
  MissingBlockchainIdentifierError,
} from "./errors";
import { getBlockchainRidFromIid, sleep } from "../restclient/restclientutil";
import { ensureBuffer, toBuffer } from "../formatter";
import { FailOverConfig } from "../restclient/types";
import { createClient } from "./blockchainClient";
import {
  AnchoringStatus,
  ChainConfirmationLevel,
  FailoverStrategy,
  ResponseStatus,
  TransactionEvent,
} from "./enums";
import { NodeManager, createNodeManager } from "../restclient/nodeManager";
import { AnchoringTransactionSchema } from "./validation/anchoringTransaction";
import { Web3EventMap } from "../promiEvent/promiEventEmitter";
import { IClient } from "./interface";
import { AnchoringTransaction } from "../ICCF/types";
import { SystemChainException } from "../ICCF/error";
import { getAnchoringClient } from "../ICCF/IccfProofTxMaterialBuilder";
import { awaitGetAnchoringTransactionForBlockRid, calculateBlockRID } from "../ICCF/utils";

export async function getClientConfigFromSettings(
  settings: NetworkSettings,
): Promise<ClientConfig> {
  const nodeUrlPoolToUse: string[] = await getNodeUrlsFromSettings(settings);
  if (nodeUrlPoolToUse.length === 0) {
    const id = settings.blockchainRid ?? settings.blockchainIid ?? "Unknown";
    throw new BlockchainUrlUndefinedException(id);
  }
  const endpointPool = createEndpointObjects(nodeUrlPoolToUse);

  const nodeManager = createNodeManager({
    nodeUrls: nodeUrlPoolToUse,
    useStickyNode: settings.useStickyNode ?? false,
    unavailableDuration: settings.failOverConfig?.unreachableDuration,
  });

  const blockchainRidToUse = await (async () => {
    if (settings.blockchainRid) {
      return settings.blockchainRid;
    }

    if (settings.blockchainIid !== undefined) {
      return await getBlockchainRidFromIid({
        nodeManager,
        endpointPool,
        chainId: settings.blockchainIid,
      });
    }

    throw new MissingBlockchainIdentifierError();
  })();

  const directoryChainRid = await (async () => {
    if (settings.directoryChainRid) {
      return settings.directoryChainRid;
    }

    const directoryChainIid = 0;
    return await getBlockchainRidFromIid({
      nodeManager,
      endpointPool,
      chainId: directoryChainIid,
    });
  })();

  return {
    endpointPool,
    nodeManager: nodeManager,
    blockchainRid: blockchainRidToUse,
    dappStatusPolling: setStatusPolling(settings.dappStatusPolling),
    clusterAnchoringStatusPolling: setStatusPolling(settings.clusterAnchoringStatusPolling),
    systemAnchoringStatusPolling: setStatusPolling(settings.systemAnchoringStatusPolling),
    failoverStrategy: settings.failOverConfig?.strategy || defaultFailoverConfig.strategy,
    attemptsPerEndpoint:
      settings.failOverConfig?.attemptsPerEndpoint || defaultFailoverConfig.attemptsPerEndpoint,
    attemptInterval:
      settings.failOverConfig?.attemptInterval || defaultFailoverConfig.attemptInterval,
    unreachableDuration:
      settings.failOverConfig?.unreachableDuration || defaultFailoverConfig.unreachableDuration,
    directoryChainRid: settings.directoryChainRid || directoryChainRid,
  };
}

export function validTxRid(txRID: Buffer): boolean {
  if (txRID.length != 32) {
    const error = new InvalidTxRidException(txRID);
    logger.error(error.toString());
    return false;
  }
  return true;
}

interface NodeDiscoveryProps {
  nodeManager: NodeManager;
  directoryEndpointPool: Endpoint[];
  failOverConfig?: FailOverConfig;
  blockchainRid?: string;
  blockchainIid?: number;
}

export async function nodeDiscovery({
  nodeManager,
  directoryEndpointPool,
  failOverConfig,
  blockchainRid,
  blockchainIid,
}: NodeDiscoveryProps): Promise<string[]> {
  if (directoryEndpointPool.length === 0) {
    throw new DirectoryNodeUrlPoolException();
  }

  if (!blockchainRid && blockchainIid === undefined) {
    throw new MissingBlockchainIdentifierError();
  }

  const directoryIid = 0;
  const directoryBRID = await getBlockchainRidFromIid({
    nodeManager,
    endpointPool: directoryEndpointPool,
    chainId: directoryIid,
    failOverConfig,
  });

  const blockchainRidToUse = await (async () => {
    if (blockchainRid) {
      return blockchainRid;
    }

    if (blockchainIid !== undefined) {
      return await getBlockchainRidFromIid({
        nodeManager,
        endpointPool: directoryEndpointPool,
        chainId: blockchainIid,
        failOverConfig,
      });
    }

    throw new MissingBlockchainIdentifierError();
  })();

  const queryObject: QueryObject<string[], { blockchain_rid: Buffer }> = {
    name: "cm_get_blockchain_api_urls",
    args: { blockchain_rid: toBuffer(blockchainRidToUse) },
  };

  const D1Client = await createClient({
    nodeUrlPool: getUrlsFromEndpoints(directoryEndpointPool),
    blockchainRid: directoryBRID,
  });

  const baseUrls = await D1Client.query(queryObject);

  return baseUrls;
}

export function convertToRellOperation(operations: Operation[]): RellOperation[] {
  return operations.map(operation => {
    return {
      opName: operation.name,
      args: operation.args ?? [],
    };
  });
}

export function getSerializedGTX(gtx: GTX): Buffer {
  const gtxBytes = gtxTool.serialize(gtx);

  if (!Buffer.isBuffer(gtxBytes)) {
    throw new SerializedTransactionFormatException();
  }

  return gtxBytes;
}

export function getGTXFromBufferOrTransactionOrOperation(
  transaction: SignedTransaction | Transaction | Operation,
  blockchainRid: BufferId,
): GTX {
  if (Buffer.isBuffer(transaction)) {
    return gtxTool.deserialize(transaction);
  } else if ("operations" in transaction) {
    return {
      blockchainRid: ensureBuffer(blockchainRid),
      operations: convertToRellOperation((transaction as Transaction).operations),
      signers: transaction.signers,
      signatures: [],
    };
  } else if ("name" in transaction) {
    return {
      blockchainRid: ensureBuffer(blockchainRid),
      operations: convertToRellOperation([transaction]),
      signers: [],
      signatures: [],
    };
  } else {
    throw new InvalidTransactionFormatException();
  }
}

export const callbackPromiseBuilder = (
  reject: (reason?: any) => void,
  resolve: (value?: any) => void,
  callback?: (error: Error | null, result: any) => void,
) => {
  return (error: Error | null | undefined, result: any) => {
    if (error) {
      if (typeof callback === "function") {
        callback(error, null);
      }
      reject(error);
    } else {
      if (typeof callback === "function") {
        callback(null, result);
      }
      resolve(result);
    }
  };
};

export const handlePostResponsePromisified = (
  error: Error | null,
  statusCode: number | null,
  rspBody: any,
) => {
  return new Promise<void>((resolve, reject) => {
    handlePostResponse(error, statusCode, rspBody, _error => {
      if (_error) {
        reject(_error);
      } else {
        resolve();
      }
    });
  });
};

function ensureArray(input: string | string[]): string[] {
  if (typeof input === "string") {
    return [input];
  }
  return input;
}

export const formatTransactionInfoResponse = (
  transactionInfoResponse: TransactionInfoResponse,
): TransactionInfo => {
  return {
    blockRid: toBuffer(transactionInfoResponse.blockRID),
    blockHeight: transactionInfoResponse.blockHeight,
    blockHeader: toBuffer(transactionInfoResponse.blockHeader),
    witness: toBuffer(transactionInfoResponse.witness),
    timestamp: transactionInfoResponse.timestamp,
    txRid: toBuffer(transactionInfoResponse.txRID),
    txHash: toBuffer(transactionInfoResponse.txHash),
    txData: toBuffer(transactionInfoResponse.txData),
  };
};

export const formatBlockInfoResponse = (blockInfoResponse: BlockInfoResponse): BlockInfo => {
  return {
    rid: toBuffer(blockInfoResponse.rid),
    prevBlockRid: toBuffer(blockInfoResponse.prevBlockRID),
    header: toBuffer(blockInfoResponse.header),
    transactions: blockInfoResponse.transactions.map(formatTransaction),
    height: blockInfoResponse.height,
    witness: toBuffer(blockInfoResponse.witness),
    witnesses: blockInfoResponse.witnesses.map(witness => {
      return toBuffer(witness);
    }),
    timestamp: blockInfoResponse.timestamp,
  };
};
const formatTransaction = (transaction: TransactionInfoInBlockResponse): TransactionInfoInBlock => {
  const formattedTransaction: TransactionInfoInBlock = {
    rid: toBuffer(transaction.rid),
    hash: toBuffer(transaction.hash),
  };

  if (transaction.data !== undefined) {
    formattedTransaction.data = toBuffer(transaction.data);
  }

  return formattedTransaction;
};
export const isKeyPair = (keypair: unknown): keypair is KeyPair => {
  return (
    typeof keypair === "object" &&
    keypair !== null &&
    "privKey" in keypair &&
    "pubKey" in keypair &&
    keypair.privKey instanceof Buffer &&
    keypair.pubKey instanceof Buffer
  );
};

async function getNodeUrlsFromSettings(settings: NetworkSettings): Promise<string[]> {
  if (settings.directoryNodeUrlPool) {
    // If directoryNodeUrlPool is provided, use nodeDiscovery
    const nodeManager = createNodeManager({
      nodeUrls: ensureArray(settings.directoryNodeUrlPool),
      unavailableDuration: settings.failOverConfig?.unreachableDuration,
    });

    return await nodeDiscovery({
      nodeManager,
      directoryEndpointPool: createEndpointObjects(ensureArray(settings.directoryNodeUrlPool)),
      failOverConfig: settings.failOverConfig,
      blockchainRid: settings.blockchainRid,
      blockchainIid: settings.blockchainIid,
    });
  } else if (typeof settings.nodeUrlPool === "string") {
    // If nodeUrlPool is a string, convert it to an array
    return [settings.nodeUrlPool];
  } else if (Array.isArray(settings.nodeUrlPool)) {
    // If nodeUrlPool is already an array, use it as-is
    return settings.nodeUrlPool;
  } else {
    // Default to an empty array if no valid configuration is provided
    return [];
  }
}

export async function getSystemClient(
  directoryNodeUrlPool: string[],
  directoryChainRid: string,
): Promise<IClient> {
  return await createClient({
    directoryNodeUrlPool,
    blockchainRid: directoryChainRid,
  });
}

export async function getSystemAnchoringChain(directoryClient: IClient): Promise<Buffer> {
  try {
    const queryObject: QueryObject<Buffer | null, { blockchainRid: Buffer }> = {
      name: "cm_get_system_anchoring_chain",
    };

    const systemAnchoringChain = await directoryClient.query(queryObject);

    if (!systemAnchoringChain) {
      throw new SystemChainException("Directory chain client must be provided.");
    }

    return systemAnchoringChain;
  } catch (error) {
    throw new SystemChainException(error.message);
  }
}

export const defaultFailoverConfig = {
  strategy: FailoverStrategy.AbortOnError,
  attemptsPerEndpoint: 3,
  attemptInterval: 500,
  unreachableDuration: 30000,
} satisfies FailOverConfig;

export const createEndpointObjects = (endpointPoolUrls: readonly string[]): Endpoint[] => {
  const endpoints: Endpoint[] = endpointPoolUrls.map(endpointUrl => {
    return { url: endpointUrl, whenAvailable: 0 };
  });
  return endpoints;
};

export const getUrlsFromEndpoints = (endpointPool: Endpoint[]): string[] => {
  return endpointPool.map(endpoint => endpoint.url);
};

export async function awaitDappConfirmation(
  txRID: Buffer,
  dappStatusPolling: StatusPolling,
  getTransactionStatus: (txRID: Buffer) => Promise<StatusObject>,
): Promise<ResponseStatus> {
  let lastKnownResult: StatusObject;
  for (let i = 0; i < dappStatusPolling.count; i++) {
    lastKnownResult = await getTransactionStatus(txRID);

    if (lastKnownResult.status === ResponseStatus.Confirmed) {
      return ResponseStatus.Confirmed;
    } else if (lastKnownResult.status === ResponseStatus.Rejected) {
      throw new TxRejectedError(lastKnownResult.rejectReason ?? "");
    }
    await sleep(dappStatusPolling.interval);
  }

  // TS issue. This could be fixed by implementing new retry strategy
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-expect-error
  return lastKnownResult.status;
}

export async function awaitClusterAnchoringChainConfirmation(
  txRID: Buffer,
  clusterAnchoringStatusPolling: StatusPolling,
  getClusterAnchoringTransactionConfirmation: (
    txRID: Buffer,
  ) => Promise<AnchoringTransaction | AnchoringStatus>,
): Promise<ClusterAnchoringConfirmation> {
  let clusterAnchoringConfirmation: AnchoringTransaction | AnchoringStatus;
  for (let i = 0; i < clusterAnchoringStatusPolling.count; i++) {
    clusterAnchoringConfirmation = await getClusterAnchoringTransactionConfirmation(txRID);
    const anchoringTransactionValidaiton = AnchoringTransactionSchema.safeParse(
      clusterAnchoringConfirmation,
    );
    if (anchoringTransactionValidaiton.success) {
      return {
        status: AnchoringStatus.ClusterAnchored,
        clusterAnchoredTx: anchoringTransactionValidaiton.data,
      };
    } else if (!anchoringTransactionValidaiton.success) {
      throw new TxRejectedError(AnchoringStatus.FailedAnchoring ?? "");
    }
    await sleep(clusterAnchoringStatusPolling.interval);
  }

  // TS issue. This could be fixed by implementing new retry strategy
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-expect-error
  return clusterAnchoringConfirmation;
}

export async function handleTransactionConfirmations(
  transactionRid: Buffer,
  doStatusPolling: boolean,
  confirmationLevel: ChainConfirmationLevel,
  promiEvent: Web3PromiEvent<TransactionReceipt, Web3EventMap>,
  statusCode: number | null,
  statusPollingConfig: {
    dappStatusPolling: StatusPolling;
    clusterAnchoringStatusPolling: StatusPolling;
    systemAnchoringStatusPolling: StatusPolling;
  },
  anchoringClient: IClient,
  systemAnchoringChainBrid: string,
  getTransactionStatus: () => Promise<StatusObject>,
  getClusterAnchoringTransactionConfirmation: () => Promise<AnchoringTransaction | AnchoringStatus>,
  getSystemAnchoringTransactionConfirmation: (
    anchoredTxRid: Buffer,
    anchoringClient: IClient,
    systemAnchoringChainBrid: string,
  ) => Promise<AnchoringTransaction | null>,
): Promise<TransactionReceipt> {
  const transactionReceipt: TransactionReceipt = {
    status: ResponseStatus.Waiting,
    statusCode: statusCode,
    transactionRid: transactionRid,
  };

  promiEvent.emit(TransactionEvent.DappReceived, transactionReceipt);

  if (doStatusPolling === false || confirmationLevel === ChainConfirmationLevel.None) {
    return transactionReceipt;
  }

  const dappConfirmationStatus = await awaitDappConfirmation(
    transactionRid,
    statusPollingConfig.dappStatusPolling,
    getTransactionStatus,
  );

  transactionReceipt.status = dappConfirmationStatus;
  promiEvent.emit(TransactionEvent.DappConfirmed, transactionReceipt);

  if (confirmationLevel === ChainConfirmationLevel.Dapp) {
    return transactionReceipt;
  }

  const clusterAnchoringChainConfirmation = await awaitClusterAnchoringChainConfirmation(
    transactionRid,
    statusPollingConfig.clusterAnchoringStatusPolling,
    getClusterAnchoringTransactionConfirmation,
  );

  transactionReceipt.status = clusterAnchoringChainConfirmation.status;
  transactionReceipt.clusterAnchoredTx = clusterAnchoringChainConfirmation.clusterAnchoredTx;
  transactionReceipt.clusterAnchoringClientBrid = anchoringClient.config.blockchainRid;

  promiEvent.emit(TransactionEvent.ClusterAnchoringConfirmation, transactionReceipt);

  if (confirmationLevel === ChainConfirmationLevel.ClusterAnchoring) {
    return transactionReceipt;
  }

  const systemAnchoringConfirmation = await getSystemAnchoringTransactionConfirmation(
    clusterAnchoringChainConfirmation.clusterAnchoredTx.txRid,
    anchoringClient,
    systemAnchoringChainBrid,
  );

  if (!systemAnchoringConfirmation) {
    return transactionReceipt;
  }

  transactionReceipt.status = AnchoringStatus.SystemAnchored;
  transactionReceipt.systemAnchoredTx = systemAnchoringConfirmation;
  transactionReceipt.systemAnchoringClientBrid = systemAnchoringChainBrid;

  promiEvent.emit(TransactionEvent.SystemAnchoringConfirmation, transactionReceipt);

  if (confirmationLevel === ChainConfirmationLevel.SystemAnchoring) {
    return transactionReceipt;
  }

  return transactionReceipt;
}

export async function getAnchoringClientAndSystemChainRid(
  client: IClient,
): Promise<AnchoringClientAndSystemBrid> {
  const directoryClient = await getSystemClient(
    getUrlsFromEndpoints(client.config.endpointPool),
    client.config.directoryChainRid,
  );

  const anchoringClient = await getAnchoringClient(directoryClient, client.config.blockchainRid);

  const systemAnchoringChainRidBuffer = await getSystemAnchoringChain(directoryClient);
  const systemAnchoringChainBridString = systemAnchoringChainRidBuffer.toString("hex");

  return { anchoringClient, systemAnchoringChainBridString };
}

export async function getSystemAnchoringTransaction(
  dappClientEndpointPool: Endpoint[],
  anchoredTxRid: BufferId,
  anchoringClient: IClient,
  systemAnchoringChainRid: string,
  systemAnchoringStatusPolling: StatusPolling,
): Promise<AnchoringTransaction | null> {
  const systemAnchoringChainClient = await getSystemClient(
    getUrlsFromEndpoints(dappClientEndpointPool),
    systemAnchoringChainRid,
  );

  const clusterAnchoringProof = await anchoringClient.getConfirmationProof(anchoredTxRid);
  const clusterBlockRid = calculateBlockRID(clusterAnchoringProof);

  const systemAnchoringTransactionConfirmation = await awaitGetAnchoringTransactionForBlockRid(
    systemAnchoringChainClient,
    toBuffer(anchoringClient.config.blockchainRid),
    clusterBlockRid,
    systemAnchoringStatusPolling,
  );

  const systemAnchoringTransactionValidation = AnchoringTransactionSchema.safeParse(
    systemAnchoringTransactionConfirmation,
  );

  if (!systemAnchoringTransactionValidation.success) {
    return null;
  }

  return systemAnchoringTransactionValidation.data;
}

export function setStatusPolling(statusPolling?: StatusPolling): StatusPolling {
  return {
    interval: statusPolling?.interval ?? 500,
    count: statusPolling?.count ?? 20,
  };
}

export function linkPromiEvents(
  event1: Web3PromiEvent<TransactionReceipt, Web3EventMap>,
  event2: Web3PromiEvent<TransactionReceipt, Web3EventMap>,
) {
  const transactionEvents = Object.values(TransactionEvent).filter(
    (event): event is TransactionEvent => typeof event === "string",
  );
  const forwardEvent = (eventName: TransactionEvent) => (receipt: TransactionReceipt | Buffer) =>
    event2.emit(eventName, receipt);

  for (const eventName of transactionEvents) {
    event1.on(eventName, forwardEvent(eventName));
  }
}