import { randomBytes } from "crypto";
import cloneDeep from "lodash/cloneDeep";
import { AnchoringTransaction } from "../ICCF/types";
import { getBlockAnchoringTransaction } from "../ICCF/IccfProofTxMaterialBuilder";
import * as logger from "../logger";
import { toBuffer, toString, toQueryObjectGTV, ensureString, ensureBuffer } from "../formatter";
import { decodeValue, encodeValue } from "../gtx/serialization";
import { handleGetResponse, handlePostResponse } from "../restclient/restclient";
import { requestWithFailoverStrategy } from "../restclient/restclientutil";
import * as gtxTool from "../gtx/gtx";
import {
  NetworkSettings,
  QueryCallback,
  QueryObject,
  SignedTransaction,
  Transaction,
  TransactionReceipt,
  ConfirmationProof,
  TransactionInfo,
  BlockInfo,
  Endpoint,
  StatusObject,
  BlockAnchoringState,
} from "./types";
import {
  callbackPromiseBuilder,
  formatBlockInfoResponse,
  formatTransactionInfoResponse,
  getClientConfigFromSettings,
  getGTXFromBufferOrTransactionOrOperation,
  getSerializedGTX,
  handlePostResponsePromisified,
  handleTransactionConfirmations,
  isKeyPair,
  getAnchoringClientAndSystemChainRid,
  getSystemAnchoringTransaction,
  linkPromiEvents,
} from "./utils";
import { IClient } from "./interface";
import { DictPair, RawGtv } from "../gtv/types";
import { Web3PromiEvent } from "../promiEvent/promiEvents";
import { NumberOfSignersAndSignaturesException } from "../gtx/errors";
import { GetTransactionRidException } from "./errors";
import { GTX } from "../gtx/types";
import { Method } from "../restclient/enums";
import { ResponseStatus, ChainConfirmationLevel, AnchoringStatus, TransactionEvent } from "./enums";
import { isBlockIdentifierValid } from "./validation/blockIdentifier";
import { isNetworkSettingValid } from "./validation/networkSettings";
import { isSignMethodValid } from "./validation/signMethod";
import { isTxRidValid } from "./validation/txRid";
import { AnchoringTransactionSchema } from "./validation/anchoringTransaction";
import { Web3EventMap } from "../promiEvent/promiEventEmitter";
import {
  isBlockInfoResponse,
  isBlockInfoResponseArray,
  isRawGtv,
  isTransaction,
  isTransactionConfirmationProof,
  isTransactionInfoResponse,
  isTransactionInfoResponseArray,
  isTransactionsCount,
} from "./validation/requests";
import { awaitGetAnchoringTransactionForBlockRid } from "../ICCF/utils";

export async function createClient(settings: NetworkSettings): Promise<IClient> {
  isNetworkSettingValid(settings, { throwOnError: true });

  const config = await getClientConfigFromSettings(settings);

  return {
    config,

    async query<TReturn extends RawGtv, TArgs extends DictPair | undefined>(
      nameOrQueryObject: string | QueryObject<TReturn, TArgs>,
      args?: TArgs,
      callback?: QueryCallback<TReturn>,
    ): Promise<TReturn> {
      let _name, _args;
      if (typeof nameOrQueryObject === "string") {
        _name = nameOrQueryObject;
        _args = args;
      } else {
        _name = nameOrQueryObject?.name;
        _args = nameOrQueryObject?.args;
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.POST,
        `query_gtv/${config.blockchainRid}`,
        config,
        encodeValue(toQueryObjectGTV(_name, _args)),
      );

      return new Promise((resolve, reject) => {
        handlePostResponse(
          error,
          statusCode,
          isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async signTransaction(transaction, signMethod, callback): Promise<SignedTransaction> {
      logger.debug(
        `Signing transaction with ${
          !isKeyPair(signMethod) ? "signature provider containing " : ""
        }pubKey: ${toString(signMethod.pubKey)}`,
      );

      const gtx = getGTXFromBufferOrTransactionOrOperation(transaction, config.blockchainRid);

      try {
        const signedTx = await (isKeyPair(signMethod)
          ? gtxTool.sign(gtx, signMethod.privKey, signMethod.pubKey)
          : gtxTool.sign(gtx, signMethod));

        const gtxBytes = getSerializedGTX(signedTx);

        if (typeof callback === "function") {
          callback(null, gtxBytes);
        }

        return gtxBytes;
      } catch (error) {
        if (typeof callback === "function") {
          callback(error, null);
        }

        throw new Error(error);
      }
    },

    sendTransaction(
      transaction,
      doStatusPolling = true,
      callback,
      confirmationLevel = ChainConfirmationLevel.Dapp,
    ): Web3PromiEvent<TransactionReceipt, Web3EventMap> {
      const promiEvent: Web3PromiEvent<TransactionReceipt, Web3EventMap> = new Web3PromiEvent<
        TransactionReceipt,
        Web3EventMap
      >(async (resolve, reject) => {
        try {
          const gtx = getGTXFromBufferOrTransactionOrOperation(transaction, config.blockchainRid);
          if (gtx.signers.length !== gtx.signatures?.length) {
            reject(new NumberOfSignersAndSignaturesException());
          }
          const gtxBytes = getSerializedGTX(gtx);

          const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
            Method.POST,
            `tx/${config.blockchainRid}`,
            config,
            gtxBytes,
          );

          const transactionRid = gtxTool.getDigestToSign(gtx);

          try {
            await handlePostResponsePromisified(error, statusCode, rspBody);
            if (typeof callback === "function") {
              callback(null, {
                status: ResponseStatus.Waiting,
                statusCode,
                transactionRid: transactionRid,
              });
            }
          } catch (_error) {
            if (typeof callback === "function") {
              callback(_error, null);
            }
            return reject(_error);
          }

          const client = this as IClient;
          const { anchoringClient, systemAnchoringChainBridString } =
            await getAnchoringClientAndSystemChainRid(client);
          const statusPollingConfig = {
            dappStatusPolling: config.dappStatusPolling,
            clusterAnchoringStatusPolling: config.clusterAnchoringStatusPolling,
            systemAnchoringStatusPolling: config.systemAnchoringStatusPolling,
          };

          resolve(
            await handleTransactionConfirmations(
              transactionRid,
              doStatusPolling,
              confirmationLevel,
              promiEvent,
              statusCode,
              statusPollingConfig,
              anchoringClient,
              systemAnchoringChainBridString,
              () => client.getTransactionStatus(transactionRid, callback),
              () =>
                client.getClusterAnchoringTransactionConfirmation(
                  transactionRid,
                  anchoringClient,
                  callback,
                ),
              anchoredTxRid =>
                client.getSystemAnchoringTransactionConfirmation(
                  anchoredTxRid,
                  anchoringClient,
                  systemAnchoringChainBridString,
                  callback,
                ),
            ),
          );
        } catch (error) {
          callback?.(error, null);
          reject(error);
        }
      });

      return promiEvent;
    },

    signAndSendUniqueTransaction(
      transactionOrOperation,
      signMethod,
      doStatusPolling = true,
      callback,
      confirmationLevel = ChainConfirmationLevel.Dapp,
    ): Web3PromiEvent<TransactionReceipt, Web3EventMap> {
      isSignMethodValid(signMethod, { throwOnError: true });
      const promiEvent: Web3PromiEvent<TransactionReceipt, Web3EventMap> = new Web3PromiEvent<
        TransactionReceipt,
        Web3EventMap
      >(async (resolve, reject) => {
        const client = this as IClient;
        const transaction =
          "name" in transactionOrOperation
            ? {
                operations: [transactionOrOperation],
                signers: [signMethod.pubKey],
              }
            : transactionOrOperation;

        const hasNop: boolean = transaction.operations.some(operation => {
          return operation.name === "nop";
        });

        const transactionWithNop = hasNop ? transaction : client.addNop(transaction);
        try {
          const signedTx = await client.signTransaction(transactionWithNop, signMethod);
          promiEvent.emit(TransactionEvent.Signed, signedTx);
          const sendTransactionPromiEvent = client.sendTransaction(
            signedTx,
            doStatusPolling,
            callback,
            confirmationLevel,
          );
          linkPromiEvents(sendTransactionPromiEvent, promiEvent);
          resolve(await sendTransactionPromiEvent);
        } catch (error) {
          reject(error);
        }
      });
      return promiEvent;
    },

    async getTransaction(transactionRid, callback): Promise<Buffer> {
      try {
        isTxRidValid(transactionRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `tx/${config.blockchainRid}/${ensureString(transactionRid)}`,
        config,
      );

      const transaction = isTransaction(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && transaction ? toBuffer(transaction.tx) : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getTransactionStatus(
      transactionRid,
      callback,
    ): Promise<TransactionReceipt | StatusObject> {
      try {
        isTxRidValid(transactionRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `tx/${config.blockchainRid}/${transactionRid.toString("hex")}/status`,
        config,
      );

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    addNop(transaction): Transaction {
      const _transaction = cloneDeep(transaction);
      const noOperation = {
        name: "nop",
        args: [randomBytes(32)],
      };
      _transaction.operations = [..._transaction.operations, noOperation];
      return _transaction;
    },

    getTransactionRid(transaction): Buffer {
      try {
        const gtx = getGTXFromBufferOrTransactionOrOperation(transaction, config.blockchainRid);
        return gtxTool.getDigestToSign(gtx);
      } catch (e) {
        throw new GetTransactionRidException(e);
      }
    },

    async getTransactionsInfo(limit = 25, beforeTime, callback): Promise<TransactionInfo[]> {
      const beforeTimeQueryParam = beforeTime ? `&before-time=${beforeTime.getTime()}` : "";

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `transactions/${config.blockchainRid}?limit=${limit}${beforeTimeQueryParam}`,
        config,
      );

      const transactionsInfoResponseArray = isTransactionInfoResponseArray(rspBody);
      const body =
        statusCode === 200 && transactionsInfoResponseArray
          ? transactionsInfoResponseArray.map(formatTransactionInfoResponse)
          : rspBody;

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          isRawGtv(body),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getTransactionInfo(transactionRid, callback): Promise<TransactionInfo> {
      try {
        isTxRidValid(transactionRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }
      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `transactions/${config.blockchainRid}/${ensureString(transactionRid)}`,
        config,
      );

      const transactionInfoResponse = isTransactionInfoResponse(rspBody);

      const body =
        statusCode === 200 && transactionInfoResponse
          ? formatTransactionInfoResponse(transactionInfoResponse)
          : rspBody;

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          isRawGtv(body),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getAnchoringStatusForBlockRid(
      blockRid,
      anchoringClient,
      systemAnchoringChainRid,
    ): Promise<BlockAnchoringState> {
      const client = this as IClient;
      const result: BlockAnchoringState = { status: AnchoringStatus.NotAnchored };
      const clusterAnchoredTx = await awaitGetAnchoringTransactionForBlockRid(
        anchoringClient,
        ensureBuffer(client.config.blockchainRid),
        ensureBuffer(blockRid),
        config.clusterAnchoringStatusPolling,
      );

      if (!clusterAnchoredTx) {
        return result;
      }

      const anchoringTransactionValidation =
        AnchoringTransactionSchema.safeParse(clusterAnchoredTx);

      if (!anchoringTransactionValidation.success) {
        return result;
      }

      result.status = AnchoringStatus.ClusterAnchored;
      result.clusterAnchoredTx = anchoringTransactionValidation.data;

      const systemAnchoredTransaction = await getSystemAnchoringTransaction(
        config.endpointPool,
        anchoringTransactionValidation.data.txRid,
        anchoringClient,
        systemAnchoringChainRid,
        config.systemAnchoringStatusPolling,
      );

      if (!systemAnchoredTransaction) {
        return result;
      }

      result.status = AnchoringStatus.SystemAnchored;
      result.systemAnchoredTx = systemAnchoredTransaction;

      return result;
    },

    async getTransactionConfirmationLevel(transactionRid, callback): Promise<TransactionReceipt> {
      try {
        const client = this as IClient;
        const dappConfirmation: StatusObject = await client.getTransactionStatus(transactionRid);

        const response: TransactionReceipt = {
          status: dappConfirmation.status,
          statusCode: 200,
          transactionRid: ensureBuffer(transactionRid),
        };

        if (dappConfirmation.status === ResponseStatus.Waiting) {
          return response;
        }

        let clusterAnchoringResult: AnchoringTransaction | AnchoringStatus | null = null;

        if (dappConfirmation.status === ResponseStatus.Confirmed) {
          const { anchoringClient, systemAnchoringChainBridString } =
            await getAnchoringClientAndSystemChainRid(client);

          clusterAnchoringResult = await client.getClusterAnchoringTransactionConfirmation(
            transactionRid,
            anchoringClient,
          );

          if (clusterAnchoringResult === AnchoringStatus.NotAnchored || !clusterAnchoringResult) {
            return response;
          }

          if (clusterAnchoringResult === AnchoringStatus.FailedAnchoring) {
            response.status = AnchoringStatus.FailedAnchoring;
            response.statusCode = 400;

            return response;
          }

          const anchoringTransactionValidation =
            AnchoringTransactionSchema.safeParse(clusterAnchoringResult);

          if (!anchoringTransactionValidation.success) {
            return response;
          }

          if (anchoringTransactionValidation.success) {
            response.status = AnchoringStatus.ClusterAnchored;
            response.statusCode = 200;
            response.clusterAnchoredTx = anchoringTransactionValidation.data;
            response.clusterAnchoringClientBrid = anchoringClient.config.blockchainRid;

            const systemAnchoringResult = await client.getSystemAnchoringTransactionConfirmation(
              anchoringTransactionValidation.data.txRid,
              anchoringClient,
              systemAnchoringChainBridString,
            );

            if (!systemAnchoringResult) {
              return response;
            }

            response.status = AnchoringStatus.SystemAnchored;
            response.systemAnchoredTx = systemAnchoringResult;
            response.systemAnchoringClientBrid = systemAnchoringChainBridString;

            return response;
          }
        }

        return response;
      } catch (error) {
        callback?.(error, null);
        throw error;
      }
    },

    async getTransactionCount(callback): Promise<number> {
      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `transactions/${config.blockchainRid}/count`,
        config,
        undefined,
        true,
      );

      const transactionsCount = isTransactionsCount(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 ? transactionsCount?.transactionsCount : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getBlockInfo(blockIdentifier, txs = false, callback): Promise<BlockInfo> {
      isBlockIdentifierValid(blockIdentifier, { throwOnError: true });

      const queryString =
        typeof blockIdentifier === "string" ? blockIdentifier : `height/${blockIdentifier}`;

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `blocks/${config.blockchainRid}/${queryString}?txs=${txs}`,
        config,
      );

      const blockInfoResponse = isBlockInfoResponse(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && blockInfoResponse !== null && blockInfoResponse
            ? formatBlockInfoResponse(blockInfoResponse)
            : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getLatestBlock(txs = false, callback): Promise<BlockInfo> {
      const shouldIncludeFullTransaction = txs ? `&txs=${txs}` : "";
      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `blocks/${config.blockchainRid}?limit=1${shouldIncludeFullTransaction}`,
        config,
        undefined,
        true,
      );
      const indexOfLatestBlock = 0;

      const blockInfoResponseArray = isBlockInfoResponseArray(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && blockInfoResponseArray !== null && blockInfoResponseArray
            ? formatBlockInfoResponse(blockInfoResponseArray[indexOfLatestBlock])
            : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getBlocks({ limit, beforeTime, afterTime, beforeHeight, afterHeight, txs, callback }) {
      const searchValues = {
        limit,
        txs,
        "before-time": beforeTime,
        "after-time": afterTime,
        "before-height": beforeHeight,
        "after-height": afterHeight,
      };
      const searchParams = new URLSearchParams();

      for (const [key, value] of Object.entries(searchValues)) {
        if (value !== undefined) {
          searchParams.append(key, value.toString());
        }
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `blocks/${config.blockchainRid}?${searchParams}`,
        config,
      );

      const blockInfoResponseArray = isBlockInfoResponseArray(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && blockInfoResponseArray
            ? blockInfoResponseArray.map(formatBlockInfoResponse)
            : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getBlocksInfo(limit = 25, beforeTime, beforeHeight, txs, callback): Promise<BlockInfo[]> {
      let filteringQueryParam = "";
      if (beforeTime) {
        filteringQueryParam = `&before-time=${beforeTime.getTime()}`;
      } else if (beforeHeight) {
        filteringQueryParam = `&before-height=${beforeHeight}`;
      }
      const shouldIncludeFullTransaction = txs ? `&txs=${txs}` : "";

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `blocks/${config.blockchainRid}?limit=${limit}${filteringQueryParam}${shouldIncludeFullTransaction}`,
        config,
      );

      const blockInfoResponseArray = isBlockInfoResponseArray(rspBody);

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 && blockInfoResponseArray
            ? blockInfoResponseArray.map(formatBlockInfoResponse)
            : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    encodeTransaction(transaction): Buffer {
      const gtx = getGTXFromBufferOrTransactionOrOperation(transaction, config.blockchainRid);
      return gtxTool.serialize(gtx);
    },

    decodeTransactionToGtx(encodedTransaction): GTX {
      const gtx = gtxTool.deserialize(encodedTransaction);
      logger.debug(`Output from deserializing a raw transaction: ${JSON.stringify(gtx)}`);
      return gtx;
    },

    getClientNodeUrlPool(): string[] {
      return config.endpointPool.map((endpoint: Endpoint) => endpoint.url);
    },

    /**
     * Retrieves a confirmation proof for a transaction with the specified sha256
     * hash.
     * @param txRid A buffer of 32 bytes
     * @param callback parameters (error, responseObjectProof) if first
     * parameter is not null, an error occurred.
     * If first parameter is null, then the second parameter is an object
     * like the following:
     *
     * {hash: messageHashBuffer,
     *  blockHeader: blockHeaderBuffer,
     *  signatures: [{pubKey: pubKeyBuffer, signature: sigBuffer}, ...],
     *  merklePath: [{side: <0|1>, hash: <hash buffer level n-1>},
     *               ...
     *               {side: <0|1>, hash: <hash buffer level 1>}]}
     *
     * If no such transaction RID exists, the callback will be called with (null, null).
     *
     * The proof object can be validated using
     * postchain-common.util.validateMerklePath(proof.merklePath, proof.hash,
     * proof.blockHeader.slice(32, 64))
     *
     * The signatures must be validated agains some know trusted source for valid signers
     * at this specific block height.
     */

    async getConfirmationProof(txRid, callback): Promise<ConfirmationProof> {
      try {
        isTxRidValid(txRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }

      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `tx/${config.blockchainRid}/${ensureString(txRid)}/confirmationProof`,
        config,
      );

      const confirmationProof: ConfirmationProof = {
        merkleProofTree: "",
        txIndex: 0,
      } as ConfirmationProof;

      if (statusCode === 200) {
        try {
          const transactionConfirmationProof = isTransactionConfirmationProof(rspBody);
          if (!transactionConfirmationProof) {
            throw error;
          }
          const decodedProof: ConfirmationProof = decodeValue(
            toBuffer(transactionConfirmationProof.proof),
          ) as ConfirmationProof;

          confirmationProof.txIndex = decodedProof.txIndex;
          confirmationProof.hash = decodedProof.hash;
          confirmationProof.blockHeader = decodedProof.blockHeader;
          confirmationProof.witness = decodedProof.witness;
          confirmationProof.merkleProofTree = decodedProof.merkleProofTree;
        } catch (decodeError) {
          if (callback) {
            callback(decodeError, null);
          }
          throw decodeError;
        }
      }
      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          statusCode === 200 ? confirmationProof : isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },

    async getClusterAnchoringTransactionConfirmation(
      transactionRid,
      anchoringClient,
      callback,
    ): Promise<AnchoringTransaction | AnchoringStatus> {
      try {
        isTxRidValid(transactionRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }

      const client = this as IClient;

      try {
        let confirmationProof: ConfirmationProof;

        try {
          confirmationProof = await client.getConfirmationProof(transactionRid);
        } catch (error) {
          callback?.(error, null);
          return AnchoringStatus.NotAnchored;
        }

        const anchoringTransaction: AnchoringTransaction | null =
          await getBlockAnchoringTransaction(
            client,
            anchoringClient,
            transactionRid,
            confirmationProof,
          );

        if (!anchoringTransaction) return AnchoringStatus.NotAnchored;

        return anchoringTransaction;
      } catch (error) {
        callback?.(error, null);
        return AnchoringStatus.FailedAnchoring;
      }
    },

    async getSystemAnchoringTransactionConfirmation(
      anchoredTxRid,
      anchoringClient,
      systemAnchoringChainRid,
      callback,
    ): Promise<AnchoringTransaction | null> {
      try {
        isTxRidValid(anchoredTxRid, { throwOnError: true });
      } catch (error) {
        callback?.(error, null);
        throw error;
      }

      const systemAnchoringTransactionTransaction = await getSystemAnchoringTransaction(
        config.endpointPool,
        anchoredTxRid,
        anchoringClient,
        systemAnchoringChainRid,
        config.systemAnchoringStatusPolling,
      );

      return systemAnchoringTransactionTransaction;
    },
    /**
     * Get app structure. Returns list of app modules in JSON format
     * @param callback - leverages error-first pattern and called when promise resolves
     */
    async getAppStructure(callback) {
      const { error, statusCode, rspBody } = await requestWithFailoverStrategy(
        Method.GET,
        `/query/${config.blockchainRid}?type=rell.get_app_structure`,
        config,
      );

      return new Promise((resolve, reject) => {
        handleGetResponse(
          error,
          statusCode,
          isRawGtv(rspBody),
          callbackPromiseBuilder(reject, resolve, callback),
        );
      });
    },
  };
}


