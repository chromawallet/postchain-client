export enum FailoverStrategy {
  AbortOnError = "abortOnError",
  TryNextOnError = "tryNextOnError",
  SingleEndpoint = "singleEndpoint",
  QueryMajority = "queryMajority",
}

export enum ResponseStatus {
  Confirmed = "confirmed",
  Rejected = "rejected",
  Unknown = "unknown",
  Waiting = "waiting",
}

export enum TransactionEvent {
  ClusterAnchoringConfirmation = "clusterAnchoringConfirmed",  
  SystemAnchoringConfirmation = "systemAnchoringConfirmed",
  DappConfirmed = "dappConfirmed",
  DappReceived = "dappReceived",
  Signed = "signed",
}

export enum AnchoringStatus {
  NotAnchored = "notAnchored",
  ClusterAnchored = "clusterAnchored",
  SystemAnchored = "systemAnchored",
  FailedAnchoring = "failedAnchoring",
}

export enum ChainConfirmationLevel {
  None = "none",
  Dapp = "dapp",
  ClusterAnchoring = "clusterAnchoring",
  SystemAnchoring = "systemAnchoring",
  EvmNetwork = "evmNetwork",
}
