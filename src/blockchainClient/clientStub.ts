import { IClient } from "./interface";
import { FailoverStrategy } from "./enums";
import { Web3PromiEvent } from "../promiEvent/promiEvents";
import { createNodeManager } from "../restclient/nodeManager";
import { setStatusPolling } from "./utils";

export async function createStubClient(): Promise<IClient> {
  return {
    config: {
      nodeManager: createNodeManager({
        nodeUrls: [] as string[],
      }),
      endpointPool: [],
      blockchainRid: "0000000000000000000000000000000000000000000000000000000000000000",
      dappStatusPolling: setStatusPolling({ interval: 5000, count: 5 }),
      clusterAnchoringStatusPolling: setStatusPolling(),
      systemAnchoringStatusPolling: setStatusPolling(),
      failoverStrategy: FailoverStrategy.AbortOnError,
      attemptsPerEndpoint: 3,
      attemptInterval: 5000,
      unreachableDuration: 30000,
      directoryChainRid: "1111111111111111111111111111111111111111111111111111111111111111",
    },

    async query(): Promise<never> {
      return Promise.reject("query rejected");
    },

    async signTransaction(): Promise<never> {
      return Promise.reject("signTransaction rejected");
    },

    sendTransaction(): Web3PromiEvent<never, never> {
      return new Web3PromiEvent((_resolve, reject) => reject("sendTransaction rejected"));
    },

    signAndSendUniqueTransaction(): Web3PromiEvent<never, never> {
      return new Web3PromiEvent((_resolve, reject) =>
        reject("signAndSendUniqueTransaction rejected"),
      );
    },

    async getTransaction(): Promise<never> {
      return Promise.reject("getTransaction rejected");
    },

    async getTransactionStatus(): Promise<never> {
      return Promise.reject("getTransactionStatus rejected");
    },

    addNop(): never {
      throw new Error("addNop error");
    },

    getTransactionRid(): never {
      throw new Error("getTransactionRid error");
    },

    async getTransactionsInfo(): Promise<never> {
      return Promise.reject("getTransactionsInfo rejected");
    },

    async getTransactionInfo(): Promise<never> {
      return Promise.reject("getTransactionInfo rejected");
    },

    async getAnchoringStatusForBlockRid(): Promise<never> {
      return Promise.reject("getAnchoringStatusForBlockRid rejected");
    },

    async getTransactionConfirmationLevel(): Promise<never> {
      return Promise.reject("getTransactionConfirmationLevel rejected");
    },

    async getTransactionCount(): Promise<never> {
      return Promise.reject("getTransactionCount rejected");
    },

    async getBlocks(): Promise<never> {
      return Promise.reject("getBlockInfo rejected");
    },

    async getBlockInfo(): Promise<never> {
      return Promise.reject("getBlockInfo rejected");
    },

    async getLatestBlock(): Promise<never> {
      return Promise.reject("getLatestBlock rejected");
    },

    async getBlocksInfo(): Promise<never> {
      return Promise.reject("getBlocksInfo rejected");
    },

    encodeTransaction(): never {
      throw new Error("encodeTransaction error");
    },

    decodeTransactionToGtx(): never {
      throw new Error("decodeTransactionToGtx error");
    },

    getClientNodeUrlPool(): string[] {
      return [];
    },

    async getConfirmationProof(): Promise<never> {
      return Promise.reject("getConfirmationProof rejected");
    },

    async getClusterAnchoringTransactionConfirmation(): Promise<never> {
      return Promise.reject("getClusterAnchoringTransactionConfirmation rejected");
    },

    async getSystemAnchoringTransactionConfirmation(): Promise<never> {
      return Promise.reject("getSystemAnchoringTransactionConfirmation rejected");
    },

    getAppStructure() {
      return Promise.reject("getAppStructure rejected");
    },
  };
}
