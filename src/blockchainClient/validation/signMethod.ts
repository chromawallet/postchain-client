import { z } from "zod";
import { BufferSchema } from "./bufferSchema";
import { Validation } from "./validation.types";

const KeyPairSchema = z.object({
  privKey: BufferSchema,
  pubKey: BufferSchema,
});

const SignatureProviderSchema = z.object({
  pubKey: BufferSchema,
  sign: z.function().args(BufferSchema).returns(z.promise(BufferSchema)),
});

export const SignMethodSchema = z.union([KeyPairSchema, SignatureProviderSchema]);

export type SignMethod = z.infer<typeof SignMethodSchema>;

export const isSignMethodValid: Validation = (signMethod, options) => {
  const signMethodValidationCtx = SignMethodSchema.safeParse(signMethod);
  const { throwOnError = false } = options || {};
  const hasError = "error" in signMethodValidationCtx;

  if (!hasError) {
    return { success: true };
  }

  const message = "Invalid sign method";

  if (throwOnError) {
    throw new Error(message);
  }

  return {
    success: false,
    error: signMethodValidationCtx.error,
    message,
  };
};
