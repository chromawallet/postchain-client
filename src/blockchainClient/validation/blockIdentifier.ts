import { z } from "zod";
import { ValidationOptions } from "./validation.types";
const WRONG_STRING_LENGTH = "wrong string length";

export const BlockIdentifierSchema = z.union([
  z.string().refine(val => /^[0-9a-fA-F]{64}$/.test(val), WRONG_STRING_LENGTH),
  z.number(),
]);

export const isBlockIdentifierValid = (blockIdentifier: unknown, options?: ValidationOptions) => {
  const ctx = BlockIdentifierSchema.safeParse(blockIdentifier);
  const { success } = ctx;
  const hasError = "error" in ctx;
  const { throwOnError = false } = options || {};

  if (!hasError) {
    return { success };
  }

  const { error } = ctx;
  const isInvalidInputType = error.issues.some(({ code }) => code === "invalid_union");
  const isInvalidStringLength = error.issues.some(({ message }) => message === WRONG_STRING_LENGTH);

  const message = (() => {
    if (isInvalidInputType) {
      return `Invalid "blockIdentifier" type. Expected string or number, but received ${typeof blockIdentifier}.`;
    }

    if (isInvalidStringLength) {
      return "Parameter 'blockIdentifier' does not have the correct format (64-character hexadecimal string).";
    }

    return error.issues.map(issue => issue.message).join(", ");
  })();

  if (throwOnError) {
    throw new Error(message);
  }

  return {
    success,
    error,
    message: error.issues.map(issue => issue.message).join(", "),
  };
};
