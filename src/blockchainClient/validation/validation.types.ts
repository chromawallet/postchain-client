import { ZodError } from "zod";

export type ValidationOptions = {
  throwOnError?: boolean;
};

export type Validation = (
  value: unknown,
  options?: ValidationOptions,
) => {
  success: boolean;
  error?: ZodError;
  message?: string;
};
