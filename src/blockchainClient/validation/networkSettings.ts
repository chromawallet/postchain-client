import { z } from "zod";
import { FailoverStrategy } from "../enums";
import { Validation, ValidationOptions } from "./validation.types";
import { MissingBlockchainIdentifierError, MissingNodeUrlError } from "../errors";

const IdentifierSchema = z.union([
  z.object({
    blockchainRid: z.undefined(),
    blockchainIid: z.number(),
  }),
  z.object({
    blockchainRid: z.string(),
    blockchainIid: z.undefined(),
  }),
]);

const NodeUrlSchema = z.union([
  z.object({
    nodeUrlPool: z.union([z.string(), z.array(z.string())]),
    directoryNodeUrlPool: z.undefined(),
  }),
  z.object({
    nodeUrlPool: z.undefined(),
    directoryNodeUrlPool: z.union([z.string(), z.array(z.string())]),
  }),
]);

const RestNetworkSettingsSchema = z.object({
  statusPollingInterval: z.number().optional(),
  statusPollingCount: z.number().optional(),
  failOverConfig: z
    .object({
      strategy: z.nativeEnum(FailoverStrategy).optional(),
      attemptsPerEndpoint: z.number().optional(),
      attemptInterval: z.number().optional(),
      unreachableDuration: z.number().optional(),
    })
    .optional(),
});

export const validateBlockChainIdentifier = (
  networkSettings: unknown,
  options?: ValidationOptions,
) => {
  const { throwOnError = false } = options || {};

  const identifierValidationContext = IdentifierSchema.safeParse(networkSettings);

  if ("error" in identifierValidationContext) {
    const missingBlockchainIdentifierError = new MissingBlockchainIdentifierError();

    if (throwOnError) {
      throw missingBlockchainIdentifierError;
    }

    return {
      success: identifierValidationContext.success,
      error: identifierValidationContext.error,
      message: missingBlockchainIdentifierError.message,
    };
  }

  return { success: true };
};

const validateNodeUrl = (networkSettings: unknown, options?: ValidationOptions) => {
  const { throwOnError = false } = options || {};

  const nodeUrlValidationContext = NodeUrlSchema.safeParse(networkSettings);

  if ("error" in nodeUrlValidationContext) {
    const missingNodeUrlError = new MissingNodeUrlError();

    if (throwOnError) {
      throw missingNodeUrlError;
    }

    return {
      success: nodeUrlValidationContext.success,
      error: nodeUrlValidationContext.error,
      message: missingNodeUrlError.message,
    };
  }

  return { success: true };
};

const validateRestNetworkSettings = (networkSettings: unknown, options?: ValidationOptions) => {
  const { throwOnError = false } = options || {};

  const restNetworkSettingsValidationContext = RestNetworkSettingsSchema.safeParse(networkSettings);

  if ("error" in restNetworkSettingsValidationContext) {
    const {
      error: { issues },
    } = restNetworkSettingsValidationContext;
    const errorMessage = issues.map(({ message, path }) => `${path[0]}: ${message}`).join(", ");

    if (throwOnError) {
      throw new Error(errorMessage);
    }

    return {
      success: restNetworkSettingsValidationContext.success,
      error: restNetworkSettingsValidationContext.error,
      message: errorMessage,
    };
  }

  return { success: true };
};

export const isNetworkSettingValid: Validation = (networkSettings, options) => {
  const identifierValidationContext = validateBlockChainIdentifier(networkSettings, options);

  if ("error" in identifierValidationContext) {
    return identifierValidationContext;
  }

  const nodeUrlValidationContext = validateNodeUrl(networkSettings, options);

  if ("error" in nodeUrlValidationContext) {
    return nodeUrlValidationContext;
  }

  const restNetworkSettingsValidationContext = validateRestNetworkSettings(
    networkSettings,
    options,
  );

  if ("error" in restNetworkSettingsValidationContext) {
    return restNetworkSettingsValidationContext;
  }

  return { success: true };
};
