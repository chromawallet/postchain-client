import { DictPair, RawGtv } from "../../gtv/types";
import { TransactionObject } from "../../restclient/types";
import {
  BlockInfoResponse,
  ConfirmationProof,
  TransactionConfirmationProof,
  TransactionInfoInBlockResponse,
  TransactionInfoResponse,
  TransactionsCount,
} from "../types";
import { z } from "zod";

const RawGtvSchema = z.union([
  z.boolean(),
  z.string(),
  z.number(),
  z.bigint(),
  z.instanceof(Buffer),
  z.array(z.unknown()),
  z.record(z.unknown()),
]);

const DictPairSchema = z.record(RawGtvSchema);

const TransactionInfoInBlockResponseSchema = z.object({
  rid: z.string(),
  hash: z.string(),
  data: z.string().optional(),
});

const BlockInfoResponseSchema = z.object({
  rid: z.string(),
  prevBlockRID: z.string(),
  header: z.string(),
  height: z.number(),
  transactions: z.array(TransactionInfoInBlockResponseSchema),
  witness: z.string(),
  witnesses: z.array(z.string()),
  timestamp: z.number(),
});

const TransactionObjectSchema = z.object({
  tx: z.string(),
});

const TransactionsCountSchema = z.object({
  transactionsCount: z.number(),
});

const TransactionConfirmationProofSchema = z.object({
  proof: z.string(),
});

const ConfirmationProofSchema = z.object({
  hash: z.instanceof(Buffer),
  blockHeader: z.instanceof(Buffer),
  witness: z.instanceof(Buffer),
  merkleProofTree: RawGtvSchema,
  txIndex: z.number(),
});

const TransactionInfoResponseSchema = z.object({
  blockRID: z.string(),
  blockHeight: z.number(),
  blockHeader: z.string(),
  witness: z.string(),
  timestamp: z.number(),
  txRID: z.string(),
  txHash: z.string(),
  txData: z.string(),
});

export function isString(value: unknown): string | null {
  const result = z.string().safeParse(value);
  return result.success ? (value as string) : null;
}

export function isDictPair(value: unknown): DictPair | null {
  const result = DictPairSchema.safeParse(value);
  return result.success ? (value as DictPair) : null;
}

export function isRawGtv(value: unknown): RawGtv | null {
  const result = RawGtvSchema.safeParse(value);
  return result.success ? (value as RawGtv) : null;
}

export function isTransactionInfoInBlockResponse(
  value: unknown,
): TransactionInfoInBlockResponse | null {
  const result = TransactionInfoInBlockResponseSchema.safeParse(value);
  return result.success ? (value as TransactionInfoInBlockResponse) : null;
}

export function isTransactionInfoInBlockResponseArray(
  value: unknown,
): TransactionInfoInBlockResponse[] | null {
  const result = z.array(TransactionInfoInBlockResponseSchema).safeParse(value);
  return result.success ? (value as TransactionInfoInBlockResponse[]) : null;
}

export function isBlockInfoResponse(value: unknown): BlockInfoResponse | null {
  const result = BlockInfoResponseSchema.safeParse(value);
  return result.success ? (value as BlockInfoResponse) : null;
}

export function isBlockInfoResponseArray(value: unknown): BlockInfoResponse[] | null {
  const result = z.array(BlockInfoResponseSchema).safeParse(value);
  return result.success ? (value as BlockInfoResponse[]) : null;
}

export function isTransaction(value: unknown): TransactionObject | null {
  const result = TransactionObjectSchema.safeParse(value);
  return result.success ? (value as TransactionObject) : null;
}

export function isTransactionsCount(value: unknown): TransactionsCount | null {
  const result = TransactionsCountSchema.safeParse(value);
  return result.success ? (value as TransactionsCount) : null;
}

export function isTransactionConfirmationProof(
  value: unknown,
): TransactionConfirmationProof | null {
  const result = TransactionConfirmationProofSchema.safeParse(value);
  return result.success ? (value as TransactionConfirmationProof) : null;
}

export function isConfirmationProof(value: unknown): ConfirmationProof | null {
  const result = ConfirmationProofSchema.safeParse(value);
  return result.success ? (value as ConfirmationProof) : null;
}

export function isTransactionInfoResponse(value: unknown): TransactionInfoResponse | null {
  const result = TransactionInfoResponseSchema.safeParse(value);
  return result.success ? (value as TransactionInfoResponse) : null;
}

export function isTransactionInfoResponseArray(value: unknown): TransactionInfoResponse[] | null {
  const result = z.array(TransactionInfoResponseSchema).safeParse(value);
  return result.success ? (value as TransactionInfoResponse[]) : null;
}
