import { z } from "zod";
import { TxRidSchema } from "./txRid";

export const AnchoringTransactionSchema = z.object({
  txRid: z.instanceof(Buffer).superRefine((rid, refCtx) => {
    const txRidValidation = TxRidSchema.safeParse(rid);

    if (!txRidValidation.success)
      txRidValidation.error.issues.forEach(issue => refCtx.addIssue(issue));
  }),
  txData: z.instanceof(Buffer).refine(data => data.length > 0, {
    message: "txData must be a non-empty Buffer",
  }),
  txOpIndex: z.number().int().nonnegative({ message: "txOpIndex must be a non-negative integer" }),
});
