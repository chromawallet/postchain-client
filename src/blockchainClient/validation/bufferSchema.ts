import { z } from "zod";

export const BufferSchema = z.union([z.instanceof(Uint8Array), z.instanceof(Buffer)]);
