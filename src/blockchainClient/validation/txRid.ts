import { BufferSchema } from "./bufferSchema";
import { Validation } from "./validation.types";
import { InvalidTxRidException } from "../../restclient/errors";

export const TxRidSchema = BufferSchema.refine(x => x.length === 32, "Rid must be 32 bytes long");

export const isTxRidValid: Validation = (rid: unknown, options) => {
  const TxRidValidationContext = TxRidSchema.safeParse(rid);
  const { throwOnError = false } = options || {};
  const hasError = "error" in TxRidValidationContext;

  if (!hasError) {
    return { success: true };
  }

  const validationError = new InvalidTxRidException(rid as Buffer);

  if (throwOnError) {
    throw validationError;
  }

  return {
    success: false,
    error: TxRidValidationContext.error,
    message: validationError.message,
  };
};
