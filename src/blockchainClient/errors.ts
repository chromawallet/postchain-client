import { CustomError } from "../customError";

export class MissingPubKeyError extends CustomError {
  constructor() {
    super(`No public key was provided`, 400);
  }
}
export class MissingBlockchainIdentifierError extends CustomError {
  constructor() {
    super(
      `No blockchain identifier was provided. Include either a blockchainRid (string) or a blockchainIid (number).`,
      400,
    );
  }
}
export class MissingNodeUrlError extends CustomError {
  constructor() {
    super(
      `No node url or directory node url was provided. Include either a nodeUrl (string or string[]) or a directory node url (string or string[]).`,
      400,
    );
  }
}

export class BlockchainUrlUndefinedException extends CustomError {
  constructor(brid: string | number) {
    const idType = typeof brid === "string" ? "BRID" : "IID";
    super(`Cannot find nodes hosting the blockchain with ${idType} ${brid}`, 400);
  }
}

export class DirectoryNodeUrlPoolException extends CustomError {
  constructor() {
    super(`No directory node url was provided`, 400);
  }
}

export class InvalidTransactionFormatException extends CustomError {
  constructor() {
    super(`The transaction is not in the right format`, 400);
  }
}

export class GetTransactionRidException extends CustomError {
  constructor(error: Error) {
    super(`"Error occurred while getting transaction RID:", ${error}`, 400);
  }
}
