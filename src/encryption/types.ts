export type KeyPair = { pubKey: Buffer; privKey: Buffer };
