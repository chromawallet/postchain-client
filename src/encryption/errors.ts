export class PrivKeyFormatException extends Error {
  constructor(privKey: Buffer) {
    super(`Invalid key length. Expected 32, but got ${privKey.length}`);
  }
}

export class MissingPrivKeyArgumentException extends Error {
  constructor() {
    super(`Missing argument privKey`);
  }
}
