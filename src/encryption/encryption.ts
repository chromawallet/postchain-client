"use strict";

import * as crypto from "crypto";
import * as secp256k1 from "secp256k1";
import { Buffer } from "buffer";
import { ensureBuffer } from "../formatter";
import { MissingPrivKeyArgumentException, PrivKeyFormatException } from "./errors";
import { KeyPair } from "./types";
import { BufferId } from "../blockchainClient/types";

export function createPublicKey(privKey: Buffer): Buffer {
  validatePrivKeyFormat(privKey);

  return Buffer.from(secp256k1.publicKeyCreate(privKey, true).buffer);
}

export function randomBytes(size: number): Buffer {
  return crypto.randomBytes(size);
}

export function sha256(buffer: Buffer): Buffer {
  return crypto.createHash("sha256").update(buffer).digest();
}

export const hash256 = sha256;

export function hashConcat(items: Buffer[]): Buffer {
  return hash256(Buffer.concat(items));
}

/**
 * @param content the content that the signature signs. It will be digested before validating.
 * @param pubKey The pubKey to validate the signature with
 * @param signature the signature to validate
 *
 * @return true if signature ok, false otherwise
 */
export function checkSignature(content: Buffer, pubKey: Buffer, signature: Buffer): boolean {
  const digest = hash256(content);
  return checkDigestSignature(digest, pubKey, signature);
}

/**
 * @param digest the signed digest. It will not be digested before validating.
 * @param pubKey The pubKey to validate the signature with
 * @param signature the signature to validate
 *
 * @return true if signature ok, false otherwise
 */
export function checkDigestSignature(digest: Buffer, pubKey: Buffer, signature: Buffer): boolean {
  return secp256k1.ecdsaVerify(signature, digest, pubKey);
}

/**
 * @param content to sign. It will be digested before signing.
 * @param privKey The private key to sign the content with
 *
 * @return the signature
 */
export function sign(content: Buffer, privKey: Buffer): Buffer {
  validatePrivKeyFormat(privKey);
  const digestBuffer = sha256(content);
  return signDigest(digestBuffer, privKey);
}

/**
 * @param digestBuffer to sign. It will not be digested before signing.
 * @param privKey The private key to sign the digest with
 *
 * @return the signature
 */
export function signDigest(digestBuffer: Buffer, privKey: Buffer): Buffer {
  return Buffer.from(secp256k1.ecdsaSign(digestBuffer, privKey).signature);
}

/**
 * Creates a key pair (which usually represents one user)
 * @param privKey to create key pair based on
 * @returns {{pubKey: Buffer, privKey: Buffer}}
 */
export function makeKeyPair(privKey?: BufferId): KeyPair {
  let pubKey;
  if (privKey) {
    privKey = ensureBuffer(privKey);
    pubKey = createPublicKey(privKey);
  } else {
    do {
      privKey = randomBytes(32);
    } while (!secp256k1.privateKeyVerify(privKey));
    pubKey = Buffer.from(secp256k1.publicKeyCreate(privKey).buffer);
  }
  return { pubKey, privKey };
}

/**
 * Generates a 16bytes TUID (Text unique ID) (a 32characters long string)
 * @returns string
 */
export function makeTuid(): string {
  return randomBytes(16).toString("hex");
}

/**
 * Verify that keypair is correct. Providing the private key, this function returns its associated public key
 * @param privKey: Buffer
 * @returns {{pubKey: Buffer, privKey: Buffer}}
 */
export function verifyKeyPair(privKey: Buffer): KeyPair {
  validatePrivKeyFormat(privKey);
  const pubKey = Buffer.from(secp256k1.publicKeyCreate(privKey).buffer);
  return { pubKey, privKey };
}

function validatePrivKeyFormat(privKey: Buffer) {
  if (!privKey) {
    throw new MissingPrivKeyArgumentException();
  }
  if (!Buffer.isBuffer(privKey) || privKey.length !== 32) {
    throw new PrivKeyFormatException(privKey);
  }
}
