import { http, HttpResponse } from "msw";
import { ResponseStatus } from "../src/blockchainClient/enums";
import { blockDataResponse } from "../resources/testData";
import {
  mockStringBlockchainRid,
  mockStringDirectoryChainRid,
  mockStringFaultyBrid,
  exampleTxHashString,
  mockHexStringOfThirtyTwoBytesBuffer,
  mockConfirmationProof,
} from "../test/unit/common/mocks";

const rejectReason =
  "[bc-rid=8E:DFB4, chain-id=0] Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";

const invalidFormatRejectReason =
  "Operation 'news_feed_ch5.news_feed:make_post' failed: Expected at least two operations, make sure that you included auth";

export const domain = "http://localhost";
const transactionInfoResponse = {
  blockRID: mockHexStringOfThirtyTwoBytesBuffer,
  blockHeight: 0,
  blockHeader: mockHexStringOfThirtyTwoBytesBuffer,
  witness: mockHexStringOfThirtyTwoBytesBuffer,
  timestamp: 0,
  txRID: mockHexStringOfThirtyTwoBytesBuffer,
  txHash: mockHexStringOfThirtyTwoBytesBuffer,
  txData: mockHexStringOfThirtyTwoBytesBuffer,
};
const limit = 15;
export const handlers = [
  http.post(`${domain}/tx/${mockStringBlockchainRid}`, () => {
    return HttpResponse.json({});
  }),
  http.get(`${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/status`, () => {
    const responseBody = {
      status: ResponseStatus.Confirmed,
    };
    return HttpResponse.json(responseBody);
  }),
  http.get(`${domain}/blocks/${mockStringBlockchainRid}/height/*`, () => {
    return HttpResponse.json(blockDataResponse);
  }),
  http.get(`${domain}/blocks/${mockStringBlockchainRid}`, () => {
    return HttpResponse.json([blockDataResponse]);
  }),
  http.get(`${domain}/brid/iid_0`, () => {
    return HttpResponse.json(mockStringDirectoryChainRid);
  }),
  http.get(`${domain}/transactions/${mockStringBlockchainRid}/${exampleTxHashString}`, () => {
    return HttpResponse.json(transactionInfoResponse);
  }),
  http.get(`${domain}/transactions/${mockStringBlockchainRid}/count`, () => {
    return HttpResponse.json({ transactionsCount: 0 });
  }),
  http.get(
    `${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/confirmationProof`,
    () => {
      return HttpResponse.json(mockConfirmationProof);
    },
  ),
  http.get(`${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}`, () => {
    return HttpResponse.json({ tx: mockHexStringOfThirtyTwoBytesBuffer });
  }),
  http.get(
    `${domain}/transactions/${mockStringBlockchainRid}?limit=${limit}&before-time=${new Date().getTime()}`,
    () => {
      return HttpResponse.json([transactionInfoResponse]);
    },
  ),
];

const responseBody = {
  status: ResponseStatus.Rejected,
  rejectReason,
};

const responseBodyInvalidRejectError = {
  status: ResponseStatus.Rejected,
  rejectReason: invalidFormatRejectReason,
};

export const errorHandler = {
  txError: http.post(
    `${domain}/tx/${mockStringBlockchainRid}`,
    () => {
      return new HttpResponse('{"error": "Could not parse JSON"}', {
        status: 400,
      });
    },
    { once: true },
  ),
  txStatusInvalidRejectFormat: http.get(
    `${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/status`,
    () => {
      return HttpResponse.json(responseBodyInvalidRejectError);
    },
    { once: true },
  ),
  statusRejected: http.get(
    `${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/status`,
    () => {
      return HttpResponse.json(responseBody);
    },
    { once: true },
  ),
  statusWaiting: http.get(
    `${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/status`,
    () => {
      return HttpResponse.json({
        status: ResponseStatus.Waiting,
      });
    },
  ),
  statusUnknown: http.get(
    `${domain}/tx/${mockStringBlockchainRid}/${exampleTxHashString}/status`,
    () => {
      return HttpResponse.json({
        status: ResponseStatus.Unknown,
      });
    },
  ),
  getTransactionInfo: http.get(
    `${domain}/transactions/*`,
    () => {
      return new HttpResponse(
        `{"error":"Can't find blockchain with blockchainRID: ${mockStringFaultyBrid}"}`,
        {
          status: 404,
        },
      );
    },
    { once: true },
  ),
};
